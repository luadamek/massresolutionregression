import os
import pickle
import argparse
import glob
import pickle
from datasets import get_x_from_df, get_df, get_fold_selections
import ROOT
from models import get_numpy_transform, gen_model
import numpy as np

def load_model(h5_file):
    import keras
    #try to laod the model either as a set of weights in an h5 file, or as an h5 file containing the model. whatever works!
    print("retrieving the model {}".format(h5_file))
    try:
        print("Loading from the h5 file")
        model = keras.models.load_model(h5_file)
    except Exception as e:
        print(e)
        print("Falling back to loading the weights")
        metadata = load_metadata(h5_file.replace(".h5", ".pkl"))
        variables = metadata["variables"]
        model_flavour =  metadata["model_flavour"]
        inputs, main_out = gen_model(model_flavour, variables)
        model = keras.models.Model(inputs = inputs,  output=[main_out])
        model.load_weights(h5_file)
    return model

def load_metadata(metadata_file):
    #Get the model and it's associated configuration file
    print("retrieving the metadata")
    with open(metadata_file, "rb") as f:
        print("Opening metadata {}".format(metadata_file))
        #metadata = pickle.load(f, encoding='latin1')
        metadata = pickle.load(f)
    return metadata

def get_prediction_names(metadata, model, get_extra_description = True):
    predictions = []
    if get_extra_description: descriptor = get_descriptor(metadata, get_extra_description = get_extra_description)
    else: descriptor = ""
    for name in [l.name for l in model.layers]:
        if "prediction" in name:
            predictions.append(name + descriptor)
    return predictions

def get_score_array_from_df(metadata, model, df, get_extra_description = True):
    scalings = metadata["scalings"]
    target_str = metadata["target"]
    variables = metadata["variables"]
    if "scalings" in metadata: X,t,w = get_x_from_df(df, variables, target_str, scalings=scalings)
    else: X,t,w = get_x_from_df(df, variables, target_str)
    scores = model.predict(X)
    predictions = get_prediction_names(metadata, model, get_extra_description = get_extra_description)
    return scores, predictions

def get_descriptor(metadata, get_extra_description=True):
    model_file = metadata["model_file"]
    model_filename = os.path.split(model_file)[-1].replace(".h5", "")
    descriptor = ""
    if get_extra_description:
        if "_4mu" in model_filename: descriptor = "_4mu"
        if "_4e" in model_filename: descriptor = "_4e"
        if "_2e2mu" in model_filename: descriptor = "_2e2mu"
        if "_2mu2e" in model_filename: descriptor = "_2mu2e"
    return descriptor

def get_score_array(metadata, model, file, get_extra_description = True):
    if not type(file) == list:files=[file]
    else: files = file
    df = get_df(files, shuffle=False) #don't apply any selection
    return get_score_array_from_df(metadata, model, df, get_extra_description = get_extra_description)

#def create_files_with_scores(file_directory, out_path, regressor_list, process_originals=False):
#   multiple_files = len(regrssor_list) > 1
#   assert len(regressor_list) != 0

#   args.file_directory = args.file_directory.replace("~",os.getenv("HOME"))
#   out_path = "_".join([args.stamp.rstrip(".txt"), args.file_directory.split("/")[-1]])
#   out_path = os.path.join(args.out_path, out_path)

#   if not os.path.exists("trees"):
#       os.makedirs("trees")

#   out_path = os.path.join("trees", out_path)

#   if not os.path.exists(out_path):
#       os.makedirs(out_path)

#   #check for duplicates
#   files_to_score = []
#   if not args.original:
#       files_to_score = glob.glob(os.path.join(args.file_directory, "*.root"))
#       tmp_files_to_score=[]
#       for f in files_to_score:
#           if f not in tmp_files_to_score:
#               tmp_files_to_score.append(f)
#       files_to_score = tmp_files_to_score

#   if args.original:
#       print("Searching {}".format(os.path.join(args.file_directory, "mc16a", "Nominal", "*.root")))
#       files_to_score += glob.glob(os.path.join(args.file_directory, "mc16a", "Nominal", "*.root"))
#       files_to_score += glob.glob(os.path.join(args.file_directory, "mc16d", "Nominal", "*.root"))
#       files_to_score += glob.glob(os.path.join(args.file_directory, "mc16e", "Nominal", "*.root"))

#   files_to_score = sorted(files_to_score)

#   models = {}
#   model_metadatas = {}
#   model_stamps = []
#   if multiple_files:
#       with open(os.path.join(os.getenv("MassRegressionResolutionDir"), args.stamp)) as f:
#           lines = f.readlines()
#       for l in lines:
#           print("Appending model {}".format(l))
#           model_stamps.append(l.rstrip("\n"))
#   else:
#       model_stamps = [args.stamp]

#   for stamp in model_stamps:
#       #Get the model and it's associated configuration file
#       config_file = "regressors/"+".".join([stamp, "pkl"])
#       h5_file = "regressors/"+".".join([stamp, "h5"])
#       with open(config_file, "rb") as f:
#           print("Opening config {}".format(config_file))
#           metadata = pickle.load(f, encoding='latin1')
#           #metadata = pickle.load(f)
#       model_metadatas[stamp] = metadata

#       #try to laod the model either as a set of weights in an h5 file, or as an h5 file containing the model. whatever works!
#       try:
#           model = keras.models.load_model(h5_file)
#       except Exception as e:
#           variables = metadata["variables"]
#           model_flavour =  metadata["model_flavour"]
#           inputs, main_out = gen_model(model_flavour, variables)
#           model = keras.models.Model(inputs = inputs,  output=[main_out])
#           model.load_weights(h5_file)
#       models[stamp] = model

#   new_files = []
#   #create a directory containing all of the root files
#   print("Creating copies of the root files")
#   print("Running over these files {}".format(files_to_score))
#   print("They are originals: {}".format(args.original))
#   for f in files_to_score:
#       f = "/" + f
#       if not args.original:
#           f = f.split("/")[-1]
#           new_file = os.path.join(out_path, f)
#           new_files.append(new_file)
#           old_file = os.path.join(args.file_directory, f)

#           print("Copying {} to {}".format(old_file, new_file))

#           f_old = ROOT.TFile(old_file, "READ")
#           t = f_old.Get("tree_incl_all")

#           f_new = ROOT.TFile(new_file, "RECREATE")
#           new_t = t.CloneTree()
#           new_t.Write()
#           entries = new_t.GetEntries()
#           f_new.Close()

#           f_old.cd()
#           f_old.Close()

#           print("Putting the scores in the file {}".format(new_file))
#           df = get_df([new_file])

#           train_test_val_set = False
#           for m, stamp in enumerate(model_stamps):
#               metadata = model_metadatas[stamp]
#               model = models[stamp]

#               print(metadata)
#               scalings = metadata["scalings"]
#               target_str = metadata["target"]
#               variables = metadata["variables"]

#               if "scalings" in metadata: X,t,w = get_x_from_df(df, variables, target_str, scalings=scalings)
#               else: X,t,w = get_x_from_df(df, variables, target_str)

#               descriptor = ""
#               if "2e2mu" in stamp:
#                   descriptor = "2e2mu"
#               if "2mu2e" in stamp:
#                   descriptor = "2mu2e"
#               if "4mu" in stamp:
#                   descriptor = "4mu"
#               if "4e" in stamp:
#                   descriptor = "4e"
#               assert (descriptor in stamp)
#               #get the names of the predictions:
#               predictions = []
#               for name in [l.name for l in model.layers]:
#                   if "prediction" in name:
#                       if descriptor != "":
#                           predictions.append(name + "_" + descriptor)
#                       else:
#                           predictions.append(name)
#               print("Writing the following predicitons {} to file {}".format(predictions, f))

#               #calculate the predictions and write them to the output file
#               scores = model.predict(X)
#               assert len(scores) == entries

#               print(scores)
#               for i, name in enumerate(predictions):
#                   if type(scores) == list and len(scores) == len(predictions):
#                       transformed_score = get_numpy_transform(scores[i], varname = name)[:,0]
#                   else:
#                       transformed_score = get_numpy_transform(scores[:,i], varname = name)
#                   print("Here are the predictions for {}".format(name))
#                   print(transformed_score)
#                   to_write_scores = np.core.records.fromarrays( [(transformed_score).transpose()], names=name)
#                   root_numpy.array2root(to_write_scores, new_file, treename="tree_incl_all", mode="update")

#               selections = get_fold_selections(metadata["fold"])
#               if not train_test_val_set:
#                  training_events = df.query(selections["train"]).eval("event_count").values
#                  validation_events = df.query(selections["valid"]).eval("event_count").values
#                  testing_events = df.query(selections["test"]).eval("event_count").values
#                  event_count =  df.eval("event_count").values

#                  training_events = 1.0 * np.isin(event_count, training_events)
#                  validation_events = 1.0 * np.isin(event_count, validation_events)
#                  testing_events = 1.0 * np.isin(event_count, testing_events)

#                  assert len(training_events) == len(validation_events)
#                  assert len(testing_events) == len(validation_events)

#                  to_write_scores = np.core.records.fromarrays( [(training_events).transpose()], names="training_event" + descriptor)
#                  root_numpy.array2root(to_write_scores, new_file, treename="tree_incl_all", mode="update")
#                  to_write_scores = np.core.records.fromarrays( [(validation_events).transpose()], names="validation_event" + descriptor)
#                  root_numpy.array2root(to_write_scores, new_file, treename="tree_incl_all", mode="update")
#                  to_write_scores = np.core.records.fromarrays( [(testing_events).transpose()], names="testing_event" + descriptor)
#                  root_numpy.array2root(to_write_scores, new_file, treename="tree_incl_all", mode="update")
#                  train_test_val_set = True

#       else:
#           f_split = f.split("/")
#           lowest_dir = "/"
#           f_split[-4] = f_split[-4] + args.stamp.rstrip(".txt")
#           for split in f_split[:-1]:
#               lowest_dir = os.path.join(lowest_dir, split)
#               if not (os.path.exists(lowest_dir)):
#                   os.makedirs(lowest_dir)
#           f_new = "/" + "/".join(f_split)
#           print("Creating a new file at {}".format(f_new))
#           #copy the trees over to a new directory
#           os.system("cp {} {}".format(f, f_new))
#           root_macro = os.path.join(os.getenv("MassRegressionResolutionDir"), "macros", "tree.cxx")
#           f_new_tmp = f_new + "_tmp"
#           mass_point = 0.0
#           for key in mass_point_map:
#                if key in f_new: #is this a file with a specific mass point?
#                     mass_point = mass_point_map[key]

#           #make a local copy of the root_file
#           f_new_tmp = make_local_files.make_minitree(f_new, f_new_tmp, tree_name = "tree_incl_all"):
#           #get the predictions from that tree
#           print("Calculating the predictions")
#           df = get_df([f_new_tmp])

#           train_test_val_set = False
#           for m, stamp in enumerate(model_stamps):
#               metadata = model_metadatas[stamp]
#               model = models[stamp]

#               scalings = metadata["scalings"]
#               target_str = metadata["target"]
#               variables = metadata["variables"]

#               descriptor = ""
#               if "2e2mu" in stamp:
#                   descriptor = "2e2mu"
#               if "2mu2e" in stamp:
#                   descriptor = "2mu2e"
#               if "4mu" in stamp:
#                   descriptor = "4mu"
#               if "4e" in stamp:
#                   descriptor = "4e"
#               assert (descriptor in stamp)

#               if "scalings" in metadata: X,t,w = get_x_from_df(df, variables, target_str, scalings=scalings)
#               else: X,t,w = get_x_from_df(df, variables, target_str)

#               scores = model.predict(X)
#               #remove the tree
#               print("Finished calculating the predictions")
#               os.system("rm {}".format(f_new_tmp))

#               #get the names of the predictions:
#               predictions = []
#               for name in [l.name for l in model.layers]:
#                   if "prediction" in name:
#                       print(name)
#                       if descriptor != "":
#                           predictions.append(name + "_" + descriptor)
#                       else:
#                           predictions.append(name)

#               #calculate the predictions and write them to the output file
#               scores = model.predict(X)
#               #assert len(scores) == entries

#               print(scores)
#               print(predictions)
#               for i, name in enumerate(predictions):
#                   if type(scores) == list and len(scores) == len(predictions):
#                       transformed_score = get_numpy_transform(scores[i], varname = name)[:,0]
#                   else:
#                       transformed_score = get_numpy_transform(scores[:,i], varname = name)
#                   print("Here are the predictions for {}".format(name))
#                   print(transformed_score)
#                   to_write_scores = np.core.records.fromarrays( [(transformed_score).transpose()], names=name)
#                   root_numpy.array2root(to_write_scores, f_new, treename="tree_incl_all", mode="update")

#               selections = get_fold_selections(metadata["fold"])

#               if not train_test_val_set:
#                  training_events = df.query(selections["train"]).eval("event_count").values
#                  validation_events = df.query(selections["valid"]).eval("event_count").values
#                  testing_events = df.query(selections["test"]).eval("event_count").values
#                  event_count =  df.eval("event_count").values

#                  training_events = 1.0 * np.isin(event_count, training_events)
#                  validation_events = 1.0 * np.isin(event_count, validation_events)
#                  testing_events = 1.0 * np.isin(event_count, testing_events)

#                  assert len(training_events) == len(validation_events)
#                  assert len(testing_events) == len(validation_events)

#                  to_write_scores = np.core.records.fromarrays( [(training_events).transpose()], names="training_event" + descriptor)
#                  root_numpy.array2root(to_write_scores, f_new, treename="tree_incl_all", mode="update")
#                  to_write_scores = np.core.records.fromarrays( [(validation_events).transpose()], names="validation_event" + descriptor)
#                  root_numpy.array2root(to_write_scores, f_new, treename="tree_incl_all", mode="update")
#                  to_write_scores = np.core.records.fromarrays( [(testing_events).transpose()], names="testing_event" + descriptor)
#                  root_numpy.array2root(to_write_scores, f_new, treename="tree_incl_all", mode="update")
#                  train_test_val_set = True

#       print("Finished!")
#       print("\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Put scores in a tree')
    parser.add_argument('--stamp', '-js', dest="stamp", type=str, required=True, help='the stamp for the NN files to be popped')
    parser.add_argument('--file_directory', '-fd', dest="file_directory", default="bianca_prodv23_appended", type=str, required=False, help='a keyword present in the name of the deepest layer to not be removed from the model')
    parser.add_argument('--out_path', '-op', dest="out_path", required=False, default="/scratch/ladamek/")
    parser.add_argument('--original', '-org', dest='original', default=False, type=bool, required=False)
    args = parser.parse_args()
