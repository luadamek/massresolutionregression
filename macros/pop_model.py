import os
import pickle
import argparse
import glob
from models import model_popper, gen_model
import keras


def pop_model(pkl_file, h5_file, rm_truth = False):

    with open(pkl_file, "rb") as f:
        metadata = pickle.load(f)

    #get the model. Either load the weights, or load the model itself. Whatever works!.
    print("loading model {}".format(h5_file.split("/")[-1]))
    try:
        model = keras.models.load_model(h5_file)
    except Exception as e:
        variables = metadata["variables"]
        model_flavour =  metadata["model_flavour"]
        fold = metadata["fold"]
        inputs, main_out = gen_model(model_flavour, variables)
        model = keras.models.Model(inputs = inputs,  output=[main_out])
        model.load_weights(h5_file)
    print("Loaded model")

    #create the new model by removing all the final layers until the layer name has prediction in it
    popped_model = model_popper(model, "prediction")
    metadata["replacement_map"] = {}

    #remove unneccesary inputs
    new_model_inputs = []
    new_input_variables = []
    for i, input in enumerate(popped_model.inputs):
        if i == 0: #skip the m4l constrained input
            continue
        #skip inputs where the alpha_hi, n_hi, n_low, etc have been fixed
        if ("alpha" in input.name or "n_hi" in input.name or "n_low" in input.name or "mean" in input.name or "range_low" in input.name or "range_hi" in input.name) and ("input" in input.name) :
            continue
        new_input_variables.append(variables[i])
        new_model_inputs.append(input)

    #create a new model
    new_model_outputs = []
    for l in popped_model.layers:
        if ("prediction" in l.name or "prediciont" in l.name):
            new_model_outputs.append(l.output)

    #update the metadata to use the new variables
    metadata["variables"] = new_input_variables
    for v_list in new_input_variables:
        for vi, v in enumerate(v_list):
            if "_truth_born" or "_truth_matched_bare" in v and rm_truth:
                for replace in ["_truth_born", "_truth_matched_bare"]:
                    if replace not in v: continue
                    replaced_v = v.replace(replace, "")
                    print("replacing variable {} with {}".format(v, replaced_v))
                    metadata["replacement_map"][replaced_v]= v
                    metadata["scalings"][replaced_v]= metadata["scalings"][v] #take the scaling form the original variable
                    v_list[vi] = replaced_v

    new_stamp = pkl_file.rstrip(".pkl") + "_popped"
    if rm_truth:
        new_stamp += "_truth_to_reco"
    new_h5_file = new_stamp + ".h5"
    new_model = keras.models.Model(inputs=new_model_inputs, outputs=new_model_outputs)
    new_model.set_weights(model.get_weights()) #set the weights of the new model

    print("Here is the new model:")
    new_model.summary()
    keras.models.save_model(new_model, new_h5_file)

    print("Here are the inputs for the new model {}".format(metadata["variables"]))
    new_pkl_file = new_stamp + ".pkl"
    with open(new_pkl_file, 'wb') as f:
        pickle.dump(metadata, f, protocol = 2)

    return new_h5_file, new_pkl_file

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Submit plotting batch jobs for the EoverPAnalysis plotting')
    parser.add_argument('--stamp', '-js', dest="stamp", type=str, required=True, help='the stamp for the NN files to be popped')
    parser.add_argument('--stop_key', '-sk', dest="stop_key", default="prediction", type=str, required=False, help='a keyword present in the name of the deepest layer to not be removed from the model')
    parser.add_argument('--last_key_name', '-lkn', dest="last_key_name", default="lastkeyname", type=str, required=False, help="The name of the final layer of the NN")
    parser.add_argument('--remove_truth', '-rt', dest="rm_truth", default=False, type=bool, required=False, help="Should I substitute truth variables with reco variables")
    args = parser.parse_args()

    h5_files = glob.glob(os.path.join(os.getenv("MassRegressionResolutionDir"),"regressors/{}*.h5".format(args.stamp)))
    pkl_files = glob.glob(os.path.join(os.getenv("MassRegressionResolutionDir"),"regressors/{}*.pkl".format(args.stamp)))

    assert len(h5_files) > 0
    assert len(pkl_files) > 0

    file_pairs = []
    for fh5 in h5_files:
        if "popped" in fh5:
            continue
        for fpkl in pkl_files:
            if fpkl.rstrip(".pkl") == fh5.rstrip(".h5"):
                print("Matched pair of pkl and h5 files for {}".format( fpkl.rstrip(".pkl")))
                file_pairs.append({"pkl":fpkl, "h5":fh5})

    for pair in file_pairs:
        pkl_file = pair["pkl"]
        h5_file = pair["h5"]

    pop_model(pkl_file, h5_file, rm_truth = args.rm_truth)

    print("finished")
