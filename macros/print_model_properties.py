import os
import pickle
import argparse
import glob
import root_numpy
import pickle
from datasets import get_x_from_df, get_df
import ROOT
from models import get_numpy_transform, gen_model
import keras
import numpy as np

parser = argparse.ArgumentParser(description='Submit plotting batch jobs for the EoverPAnalysis plotting')
parser.add_argument('--stamp', '-js', dest="stamp", type=str, required=True, help='the stamp for the NN files to be popped')
args = parser.parse_args()

#Get the model and it's associated configuration file
config_file = ".".join([args.stamp, "pkl"])
h5_file = ".".join([args.stamp, "h5"])

with open(config_file, "rb") as f:
    print("Opening config {}".format(config_file))
    metadata = pickle.load(f)
variables = metadata['variables']
target_str = metadata['target']
selection = metadata['selection']
files = metadata['files']
fold = metadata['fold']

try:
    model = keras.models.load_model(h5_file)
except Exception as e:
    variables = metadata["variables"]
    model_flavour =  metadata["model_flavour"]
    inputs, main_out = gen_model(model_flavour, variables)
    model = keras.models.Model(inputs = inputs,  output=[main_out])
    model.load_weights(h5_file)

model.summary()
print("\n")
print("Here are the variables that were used")
if type(variables[0]) == list:
    for var_list in variables:
        for var in var_list:
             if "replacement_map" in metadata and var in metadata["replacement_map"]:
                 original_var = metadata["replacement_map"][var]
                 print("{} replaced with {}".format(original_var, var))
                 continue
             print(var)
else:
    for var in variables:
         if "replacement_map" in metadata and var in metadata["replacement_map"]:
             original_var = metadata["replacement_map"][var]
             print("{} replaced with {}".format(original_var, var))
             continue
         print(var)

print("\n")
print("This was the selection")
print(selection.replace(" and "," and \n").replace(" or "," or \n"))

print("\n")
print("Here are the files used for training")
for f in files:
    print(f)
