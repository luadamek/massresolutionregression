import pandas as pd
import os
import utils
import xgboost as xgb
import numpy as np

parameter_list = []
parameter_list.append({"variables": [["pt4l_unconstrained", "KD_discriminant", "y4l_unconstrained"], ["pt4l_unconstrained", "KD_discriminant", "y4l_unconstrained", "mZ1_unconstrained"]]})
parameter_list.append({"eta":[0.10, 0.05]})
parameter_list.append({"max_depth":[1, 2, 3, 4, 6, 8, 10,  12]})
parameter_list.append({"subsample":[1.0]})
parameter_list.append({"min_child_weight":[128, 144, 160, 186, 202, 218]})
parameter_list.append({"objective":["binary:logistic"]})
parameter_list.append({"gamma":[0.0, 0.2, 0.4]})
parameter_list.append({"lambda":[0.0, 0.25, 0.5]})
parameter_list.append({"alpha":[0.0]})
parameter_list.append({"eval_metric":["logloss"]})
parameter_list.append({"nthread":[1]})

def get_fold_selections(fold, variable_to_fold = "event"):
    '''
    return a dictionary of functions that select events for the training, validation and testing samples
    '''
    assert fold < 5
    assert fold > 0
    remainders = {'train':[(fold+3)%4, (fold+2)%4], 'valid':[(fold+1)%4], 'test':[fold%4]}
    remainder_function = {}
    for rem in remainders:
        remainder_function[rem] = lambda x,rem_list=remainders[rem]: np.logical_or.reduce([getattr(x,variable_to_fold) % 4 == r for r in rem_list])
    return remainder_function

def folded_retrieval(retriever, fold, wcards, variables, selection, label=-1):
    variables = list(set(variables + ["event"])) 
    files = retriever.get_root_files(wcards)
    dataframe = retriever.get_dataframe(files, variables,selection=selection)
    dataframe["label"] = np.ones(len(dataframe), dtype=int) * label
    selection = get_fold_selections(fold, variable_to_fold = "event")
    folded_frames = {}
    for fold in selection:
        folded_frames[fold] = dataframe.iloc[selection[fold]]
    return folded_frames

def merge_folded_frames(frames):
    if len(frames) == 0: return None
    keys = list(frames[0].keys())
    to_return = {}
    for k in keys:
       to_return[k] = []
       for f in frames: to_return[k].append(f[k])
       to_return[k] = pd.concat(to_return[k])
    return to_return
        
def get_training_matrix_from_frame(training_frame, variables):
    to_return = {}
    for f in training_frame:
        frame = training_frame[f]
        dmatrix = xgb.DMatrix(frame[variables], label=frame["label"].values, weight=frame.eval("weight").values)
        to_return[f] = dmatrix
    return to_return

def get_training_matrices(retriever, fold, signal_card, bkg_card, training_variable_set, full_selection):
    signal_frames= folded_retrieval(retriever, fold, signal_card, training_variable_set, full_selection, label=1)
    bkg_frames= folded_retrieval(retriever,fold, bkg_card, training_variable_set, full_selection, label=0)
    n_signal = float(len(signal_frames["train"]))
    n_bkg = float(len(bkg_frames["train"]))
    for f in signal_frames: signal_frames[f]["weight"] = n_signal * signal_frames[f]["weight"].values/np.sum(signal_frames[f]["weight"].values)
    for f in bkg_frames: bkg_frames[f]["weight"] = n_signal * bkg_frames[f]["weight"].values/np.sum(bkg_frames[f]["weight"].values)
    training_frame = merge_folded_frames([signal_frames, bkg_frames])
    return get_training_matrix_from_frame(training_frame, training_variable_set)

def flatten_dict(d):
    output = []
    assert len(d.items()) == 1
    for key, vals in d.items():
        for val in vals:
            output.append({key: val})
    return output

def merge_dicts(d_list_one, d_list_two):
    output = []
    for d_one in d_list_one:
        for d_two in d_list_two:
            output.append({**d_one, **d_two})
    return output

def generate_hyperparameter_combinations(parameters):
    output = []
    if len(parameters) < 1:
        return output
    output = flatten_dict(parameters[0])
    for parameter_dict in parameters[1:]:
        flat_dicts = flatten_dict(parameter_dict)
        output = merge_dicts(output, flat_dicts)
    return output

#use a FOM to find the best classifier
def calculate_significance():
    pass

def train(parameters, retriever, signal_card = ["_ggH125_"], bkg_card = ["364250", "364251", "364252"]):
    models = {}
    for cat in ["4mu", "2e2mu", "2mu2e", "4e"]:
        models[cat] = {}
        base_selection = utils.get_full_selection(cat, "Incl", "m4l_constrained")
        selection = "((m4l_constrained < 110.0) and (m4l_constrained < 140.0))"
        full_selection = "({}) and ({})".format(base_selection, selection)
        for fold in [1, 2, 3, 4]:
            training_variable_set = parameters["variables"]
            xgb_parameters = {key:value for (key, value) in parameters.items() if key != "variables"}
            matrices = get_training_matrices(retriever, fold, signal_card, bkg_card, training_variable_set, full_selection)
            train = matrices["train"]
            valid = matrices["valid"]
            eval_set = []
            eval_set.append((train, 'train'))
            eval_set.append((valid, 'valid'))
            model = xgb.train(xgb_parameters, train, 10000, early_stopping_rounds = 50, evals=eval_set)
            models[cat][fold] = model
    return models

import argparse
import pickle
if __name__ == "__main__":
    bkg_card = ["364250", "364251", "364252"]
    signal_card = ["_ggH125_"]
    retriever = utils.get_v24_retriever()
    #train one per fold, and per category
    parameters = generate_hyperparameter_combinations(parameter_list)
    outdir = utils.get_xgboost_model_dir()

    parser = argparse.ArgumentParser()
    parser.add_argument("--slurm", dest="slurm", action="store_true", help="run a slurm job")
    parser.add_argument("--ids", dest="ids", type=str, help="comma separated list of job ids to run")
    parser.add_argument("--job_submission", dest="job_submission", action="store_true", help="comma separated list of job ids to run")
    args = parser.parse_args()
    if not args.slurm and not args.job_submission: models = train(parameters[0], retriever)

    if args.slurm:
        ids = args.ids.split(",")
        for id in ids:
            models = train(parameters[int(id)], retriever)
            import pickle
            if not os.path.exists(os.path.join(outdir, "models")): os.makedirs(os.path.join(outdir, "models"))
            with open(os.path.join(outdir, "models", "XGB_{}.pkl".format(id)), "wb") as f: pickle.dump({"models":models, "params":parameters}, f)

    if args.job_submission:
        submission_dir = os.path.join(utils.get_xgboost_model_dir(), "submission")
        if not os.path.exists(submission_dir): os.makedirs(submission_dir)
        batches = []
        j = 0
        i = 10
        while i < len(parameters):
            batches.append((j, i))
            i += 10
            j += 10
        batches.append((j, len(parameters)))
        for b in batches:
            jobs = ",".join([str(el) for el in list(range(b[0], b[1]))])
            descr = "_".join([str(el) for el in list(range(b[0], b[1]))])
            error_file = os.path.join(submission_dir, "XGB_{}.err".format(descr))
            output_file = os.path.join(submission_dir, "XGB_{}.out".format(descr))
            commands = ["cd /home/ladamek/MassResolutionRegression/", "source ./setup.sh", "python xgboost_training/data.py --ids={} --slurm".format(jobs)]
            submission_file = os.path.join(submission_dir, "XGB_{}.sh".format(descr))
            slurm_file = os.path.join(submission_dir, "XGB_slurm_{}.sh".format(descr))
            with open(submission_file, "w") as f:
                for c in commands:
                    f.write(c + "\n")
            os.system("batchScript \"source {}\" -O {}".format(submission_file, slurm_file))
            print(submission_file)
            os.system("sbatch --mem=15000M --time=03:00:00 --error={} --output={} {}".format(error_file, output_file, slurm_file))
