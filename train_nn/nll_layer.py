import keras
import tensorflow as tf
#from tensorflow.python.framework import ops
import numpy as np
from scipy.special import erf
import glob
import math
import os
import root_numpy

class NllLayer(keras.layers.Layer):
    def __init__(self, **kwargs):
        super(NllLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        super(NllLayer, self).build(input_shape)  # Be sure to call this at the end

    def call(self, inputs):
        #to_return = my_nll(masses, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_hi)
        masses, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_hi = inputs[0], inputs[1], inputs[2], inputs[3], inputs[4], inputs[5], inputs[6], inputs[7], inputs[8]
        return tf_log_dcb(masses, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_hi)

    def compute_output_shape(self, input_shape):
        return (input_shape[0],1)

    def get_config(self):
        config = super(NllLayer, self).get_config()
        return config

def safe_where(condition, true_vals, false_vals):
  safe_true = tf.where(condition, true_vals, tf.ones_like(true_vals))
  safe_false = tf.where(tf.math.logical_not(condition), false_vals, tf.ones_like(false_vals))
  return tf.where(condition, safe_true, safe_false)

def tf_gaussian_cdf(x, sigma=1.0, mean=0.0):
    return 0.5 * ( 1.0 + tf.math.erf( (x - mean)/ (np.sqrt(2.0) * sigma) ) )

def tf_calc_norm_center(sigma, tmin, tmax):
    return sigma * np.sqrt(2.0 * np.pi) * (tf_gaussian_cdf(tmax) - tf_gaussian_cdf(tmin))

def tf_calc_norm_tail(sigma, tmin, tmax, alpha, n):
  a = tf.exp(-0.5*alpha*alpha);
  b = n/alpha - alpha
  return sigma * a/(1.0 - n)*( (b - tmin)/(tf.pow(alpha/n*(b - tmin), n)) - (b - tmax)/(tf.pow(alpha/n*(b - tmax), n)) )

def tf_log_dcb(x, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_hi):
    tmin = (range_low - mean)/sigma
    tmax = (range_hi - mean)/sigma
    t = (x-mean)/sigma

    b = n_low/alpha_low - alpha_low
    low_log_arg = (alpha_low/n_low)*(b - t)
    low_vals = -0.5*alpha_low*alpha_low - n_low * tf.math.log(tf.clip_by_value(low_log_arg, 1e-10, 1e+10))
    #low_vals = -0.5*alpha_low*alpha_low - n_low * tf.math.log(low_log_arg)

    b = n_hi/alpha_hi - alpha_hi
    hi_log_arg = (alpha_hi/n_hi)*(b + t)
    hi_vals = -0.5*alpha_hi*alpha_hi - n_hi * tf.math.log(tf.clip_by_value(hi_log_arg, 1e-10, 1e+10))
    #hi_vals = -0.5*alpha_hi*alpha_hi - n_hi * tf.math.log(hi_log_arg)

    low_cond = t < (-1.0 * alpha_low)
    hi_cond = t > (alpha_hi)

    central_vals = -0.5*t*t
    to_return = central_vals
    to_return = safe_where(hi_cond, hi_vals, to_return)
    to_return = safe_where(low_cond, low_vals, to_return)

    norm_center = tf_calc_norm_center(sigma, tf.maximum(tmin, -1.0*alpha_low), tf.minimum(tmax, alpha_hi))
    norm_center = safe_where(tmin > alpha_hi, tf.zeros_like(norm_center), norm_center)
    norm_center = safe_where(tmax < -1.0 * alpha_low, tf.zeros_like(norm_center), norm_center)

    norm_hi_tail = tf_calc_norm_tail(sigma , -1.0 * tmax, tf.minimum(-1.0 * tmin, -1.0 * alpha_hi), alpha_hi, n_hi)
    norm_hi_tail = safe_where(tmax < alpha_hi, tf.zeros_like(norm_hi_tail), norm_hi_tail)
    norm_low_tail = tf_calc_norm_tail(sigma, tmin, tf.minimum(tmax, -1.0 * alpha_low), alpha_low, n_low)
    norm_low_tail = safe_where(tmin > - 1.0 * alpha_low, tf.zeros_like(norm_low_tail), norm_low_tail)

    to_return = to_return - tf.math.log(tf.clip_by_value(norm_center + norm_hi_tail + norm_low_tail, 1e-10, 1e+10)) #normalize the distribution
    #to_return = to_return - tf.math.log(norm_center + norm_hi_tail + norm_low_tail) #normalize the distribution

    to_return = safe_where( x < range_low, tf.zeros_like(to_return), to_return)
    to_return = safe_where( x > range_hi, tf.zeros_like(to_return), to_return)

    #add penalty terms for too-small and to large alphas, sigmas, etc
    #to_return += safe_where(sigma < 0.1, 0.1 * (1.0 * ((sigma) * (sigma)) + 2000.0 * sigma - 300.0), 0.0)
    #to_return += safe_where(sigma > 6.0, 0.1 * (sigma - 6.0) * (sigma - 6.0), 0.0)

    return to_return

