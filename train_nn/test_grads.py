from nll_layer import numpy_log_dcb, grad_numpy_log_dcb
import numpy as np

xs = np.linspace(120.0, 130.0, 100)
alpha_his = np.linspace(0.5, 1.5, 100)
alpha_lows = np.linspace(0.5, 1.5, 100)
n_his = np.linspace(1.1, 9.0, 100)
n_lows = np.linspace(1.1, 9.0, 100)
means = np.linspace(124.5, 125.5, 100)
sigmas = np.linspace(1.5, 3.0, 100)
range_lows = np.linspace(110.0, 120.0, 100)
range_highs = np.linspace(130.0, 140.0, 100)

h = 0.0000001
args = [xs, alpha_his, n_his, alpha_lows, n_lows, means, sigmas, range_lows, range_highs]
names = ["x", "ahi", "nhi", "alow", "nlow", "mean", "sigma", "range_low", "range_high"]

for j in range(0, len(args)):
    these_args_up = [np.ones(arg.shape)*arg for arg in args]
    these_args_down = [np.ones(arg.shape)*arg for arg in args]
    print(args[j] + h)
    these_args_up[j] = args[j] + h
    print(args[j] - h)
    these_args_down[j] = args[j] - h
    print(np.any(these_args_up[j] != these_args_down[j]))
    assert np.any(these_args_up[j] != these_args_down[j])
    step = 2.0 * h
    numerical_grad = (numpy_log_dcb(*these_args_up) - numpy_log_dcb(*these_args_down)) / step
    analytic_grad = grad_numpy_log_dcb(*args)
    print("For arg {}".format(names[j]))
    for i in range(0, 100):
        print(numerical_grad[i] - analytic_grad[j][i])
        print("numeric {}".format(numerical_grad[i]))
        print("analytic {}".format(analytic_grad[j][i]))
    input("OK?")
    print("\n")
