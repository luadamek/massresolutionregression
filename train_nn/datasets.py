import pandas as pd
import uproot as ur
import numpy as np

def mean_scale(array, mean, std_dev):
    array = array - mean
    array = array/std_dev
    return array

def get_fold_selections(fold):
    '''
    return a dictionary of strings that selects events for the training, validation and testing samples
    '''
    assert fold < 5
    assert fold > 0
    remainders = {'train':[(fold+3)%4, (fold+2)%4], 'valid':[(fold+1)%4], 'test':[fold%4]}
    selections = {}
    for type in remainders:
        selection_string = ["({}%{}=={})".format("event_count", 4, rem) for rem in remainders[type]]
        selection_string = " or ".join(selection_string)
        selections[type] = selection_string
    return selections

def fold_df(fold, frame):
    '''
    given the fold number, a dataframe return the fold'th fold  of the dataframe
    '''
    frames_folded = {}
    selections = get_fold_selections(fold)
    for type in selections:
        frames_folded[type] = frame.query(selections[type])
    return frames_folded

def get_df(root_files, branches_to_read = [], selection = "", shuffle=True):
    df = None
    df_set = False
    root_files = sorted(root_files)
    for root_file in root_files:
       print("opening {}".format(root_file))
       tree = ur.open(root_file)["tree_incl_all"]
       branches = tree.allkeys()

       if branches_to_read: df_tmp = tree.pandas.df(branches_to_read)
       else: df_tmp = tree.pandas.df()
       for col in df_tmp.columns:
           df_tmp[col] = df_tmp[col].astype(float)
       if selection != "": df_tmp = df_tmp.query(selection)

       if not df_set:
           df = df_tmp
           df_set = True

       else:
           df = pd.concat([df, df_tmp])

    #shuffle the frame
    df = df.reset_index()
    assert len(df.index.values) == len(np.unique(df.index.values))
    np.random.seed(42)
    if shuffle: df = df.reindex(np.random.permutation(df.index))
    print(df)
    #convert all columns to floats
    return df

def scale(X_train, X_valid, X_test, mean_scales, std_scales):
    for i in range(0, len(X_train[0,:])):
        X_train[:,i]-=mean_scales[i]
        X_train[:,i]/=std_scales[i]
        X_valid[:,i]-=mean_scales[i]
        X_valid[:,i]/=std_scales[i]
        X_test[:,i]-=mean_scales[i]
        X_test[:,i]/=std_scales[i]

def get_x(root_files, variables, target_str =  "abs(truth_lepton_pt - lepton_pt)", fold = 1,selection="", scalings = {}):
    df =  get_folded_df(root_files, variables, fold = fold, selection=selection)
    return get_x_from_folded_df(df, variables, target_str=target_str, scalings = scalings)

def get_folded_df(root_files, variables, fold = 1,selection=""):
    df = get_df(root_files)
    print("selecting from a frame with {} events".format(len(df)))
    if selection != "":
        df = df.query(selection)
    print("the new frame has {} events".format(len(df)))
    df = fold_df(fold, df)
    return df

def get_x_from_df(df, input_vars, target_str = "abs(truth_lepton_pt - lepton_pt)", scalings={}, shuffle=False):
    #shuffle the frame
    df = df.reset_index()
    assert len(df.index.values) == len(np.unique(df.index.values))
    np.random.seed(42)
    if shuffle: df = df.reindex(np.random.permutation(df.index))

    #convert all columns to floats
    has_weight_column =  'weight' in df.columns
    if type(input_vars[0]) == list:
        X =[]
        for var_list in input_vars:
            to_append = np.zeros((len(df), len(var_list)))
            for i,var in enumerate(var_list):
                evaluated = df.eval(var)
                if type(evaluated) == pd.core.series.Series: to_append[:,i] = df.eval(var).values
                else:  to_append[:,i] = evaluated
                if var in scalings:
                    print("Scaling {} with mean: {} std dev: {}".format(var, scalings[var][0], scalings[var][1]))
                    to_append[:,i] = mean_scale(to_append[:,i], scalings[var][0], scalings[var][1])
            X.append(to_append)
    else:
        X = np.zeros((len(df), len(input_vars)))
        for i,var in enumerate(input_vars):
            evaluated = df.eval(var)
            if type(evaluated) == pd.core.series.Series: X[:,i] = df.eval(var).values
            else:  X[:,i] = evaluated
            if var in scalings:
                print("Scaling {} with mean: {} std dev: {}".format(var, scalings[var][0], scalings[var][1]))
                X[:,i] = mean_scale(X[:,i], scalings[var][0], scalings[var][1])
    target = df.eval(target_str).values
    if has_weight_column: weights = df["weight"].values
    else: weights = np.ones(len(target))

    return X, target, weights

def get_x_from_folded_df(df, input_vars, target_str =  "abs(truth_lepton_pt - lepton_pt)", scalings={}, shuffle=False):
    #get the targets, weights, and feature vectors

    X_train, target_train, weights_train = get_x_from_df(df['train'], input_vars, target_str =  target_str, scalings=scalings, shuffle=shuffle)
    X_valid, target_valid, weights_valid = get_x_from_df(df['valid'], input_vars, target_str =  target_str, scalings=scalings, shuffle=shuffle)
    X_test, target_test, weights_test = get_x_from_df(df['test'], input_vars, target_str =  target_str, scalings=scalings, shuffle=shuffle)

    return {"X_train":X_train, "X_valid": X_valid, "X_test": X_test, "weights_train": weights_train, "weights_valid": weights_valid, "weights_test": weights_test, "target_train": target_train, "target_valid": target_valid, "target_test": target_test}




