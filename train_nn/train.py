import uproot as ur
import make_plots
import argparse
import tensorflow as tf
import numpy as np
import os
from datasets import fold_df, get_df, get_x, scale, get_x_from_df, get_x_from_folded_df
import pickle
from datetime import datetime
import glob
import pandas as pd
import utils
import keras
from models import gen_model, model_library, tilted_loss, sum_preds_loss
from parameters import get_parameters, get_new_parameters
import workspace_manager

retriever = utils.get_v24_retriever()
def return_bindex(arr, bins):
    return np.searchsorted(bins, arr, side='left') - 1

def optimizer(lr=500.0, decay=0.0):
    opt = keras.optimizers.Adam(lr=lr, decay=decay)
    #opt = keras.optimizers.Nadam(lr=lr)
    #opt = keras.optimizers.SGD(lr=lr, decay=decay, momentum=0.1, nesterov=True)
    #opt = keras.optimizers.RMSProp(lr=lr)
    return opt

variable_bank = {}
variable_bank["truth_eta_pt"] =[ \
    "lepton_pt_truth_born_1",\
    "lepton_pt_truth_born_2",\
    "lepton_pt_truth_born_3",\
    "lepton_pt_truth_born_4",\
    "lepton_eta_truth_born_1",\
    "lepton_eta_truth_born_2",\
    "lepton_eta_truth_born_3",\
    "lepton_eta_truth_born_4"]
variable_bank["truth_eta_pt_m4lerr"] = [\
    "lepton_pt_truth_born_1",\
    "lepton_pt_truth_born_2",\
    "lepton_pt_truth_born_3",\
    "lepton_pt_truth_born_4",\
    "lepton_eta_truth_born_1",\
    "lepton_eta_truth_born_2",\
    "lepton_eta_truth_born_3",\
    "lepton_eta_truth_born_4",\
    "m4lerr_constrained"]
variable_bank["m4lerr"] = [\
    "m4lerr_constrained"]
variable_bank["truth_eta_pt_phi"] = [\
    "lepton_pt_truth_born_1",\
    "lepton_pt_truth_born_2",\
    "lepton_pt_truth_born_3",\
    "lepton_pt_truth_born_4",\
    "lepton_eta_truth_born_1",\
    "lepton_eta_truth_born_2",\
    "lepton_eta_truth_born_3",\
    "lepton_eta_truth_born_4",\
    "lepton_phi_truth_born_1",\
    "lepton_phi_truth_born_2",\
    "lepton_phi_truth_born_3",\
    "lepton_phi_truth_born_4",\
    ]
variable_bank["truth_eta_pt_phi_m4lerr"] = [\
    "lepton_pt_truth_born_1",\
    "lepton_pt_truth_born_2",\
    "lepton_pt_truth_born_3",\
    "lepton_pt_truth_born_4",\
    "lepton_eta_truth_born_1",\
    "lepton_eta_truth_born_2",\
    "lepton_eta_truth_born_3",\
    "lepton_eta_truth_born_4",\
    "lepton_phi_truth_born_1",\
    "lepton_phi_truth_born_2",\
    "lepton_phi_truth_born_3",\
    "lepton_phi_truth_born_4",\
    "m4lerr_constrained"]
variable_bank["reco_eta"] = [\
    "lepton_eta_1",\
    "lepton_eta_2",\
    "lepton_eta_3",\
    "lepton_eta_4",\
    ]
variable_bank["reco_eta_pt"] = [\
    "lepton_eta_1",\
    "lepton_eta_2",\
    "lepton_eta_3",\
    "lepton_eta_4",\
    "lepton_pt_1",\
    "lepton_pt_2",\
    "lepton_pt_3",\
    "lepton_pt_4",\
    ]
variable_bank["massnorm_pt_eta"] = [\
    "lepton_eta_1",\
    "lepton_eta_2",\
    "lepton_eta_3",\
    "lepton_eta_4",\
    "lepton_pt_1/m4l_unconstrained",\
    "lepton_pt_2/m4l_unconstrained",\
    "lepton_pt_3/m4l_unconstrained",\
    "lepton_pt_4/m4l_unconstrained",\
    ]
variable_bank["blank"] = [\
    "blank",\
    ]
variable_bank["Constr_Lead_"]= [\
        "m4lerr_constrained",\
        "lepton_pt_3",\
        "lepton_pt_4",\
        "lepton_eta_3",\
        "lepton_eta_4"]



variable_bank = {}
variable_bank["truth_bare_eta_pt"] =[ \
    "lepton_pt_truth_matched_bare_1",\
    "lepton_pt_truth_matched_bare_2",\
    "lepton_pt_truth_matched_bare_3",\
    "lepton_pt_truth_matched_bare_4",\
    "lepton_eta_truth_matched_bare_1",\
    "lepton_eta_truth_matched_bare_2",\
    "lepton_eta_truth_matched_bare_3",\
    "lepton_eta_truth_matched_bare_4"]
variable_bank["truth_bare_eta_pt_m4lerr"] = [\
    "lepton_pt_truth_matched_bare_1",\
    "lepton_pt_truth_matched_bare_2",\
    "lepton_pt_truth_matched_bare_3",\
    "lepton_pt_truth_matched_bare_4",\
    "lepton_eta_truth_matched_bare_1",\
    "lepton_eta_truth_matched_bare_2",\
    "lepton_eta_truth_matched_bare_3",\
    "lepton_eta_truth_matched_bare_4",\
    "m4lerr_constrained"]
variable_bank["truth_bare_eta_pt_phi"] = [\
    "lepton_pt_truth_matched_bare_1",\
    "lepton_pt_truth_matched_bare_2",\
    "lepton_pt_truth_matched_bare_3",\
    "lepton_pt_truth_matched_bare_4",\
    "lepton_eta_truth_matched_bare_1",\
    "lepton_eta_truth_matched_bare_2",\
    "lepton_eta_truth_matched_bare_3",\
    "lepton_eta_truth_matched_bare_4",\
    "lepton_phi_truth_matched_bare_1",\
    "lepton_phi_truth_matched_bare_2",\
    "lepton_phi_truth_matched_bare_3",\
    "lepton_phi_truth_matched_bare_4",\
    ]
variable_bank["truth_bare_eta_pt_phi_m4lerr"] = [\
    "lepton_pt_truth_matched_bare_1",\
    "lepton_pt_truth_matched_bare_2",\
    "lepton_pt_truth_matched_bare_3",\
    "lepton_pt_truth_matched_bare_4",\
    "lepton_eta_truth_matched_bare_1",\
    "lepton_eta_truth_matched_bare_2",\
    "lepton_eta_truth_matched_bare_3",\
    "lepton_eta_truth_matched_bare_4",\
    "lepton_phi_truth_matched_bare_1",\
    "lepton_phi_truth_matched_bare_2",\
    "lepton_phi_truth_matched_bare_3",\
    "lepton_phi_truth_matched_bare_4",\
    "m4lerr_constrained"]



variable_choices = ["reco_eta", "massnorm_pt_eta", "truth_eta_pt_phi_m4lerr", "truth_eta_pt_phi", "truth_eta_pt_m4lerr", "truth_eta_pt"]

def train(root_files = None, data = None, time = None, extra_str = None, stamp = None, scale_target = True, model_flavour = "shallow_relu_drop_middle", lr=5e-4, decay=3e-3, epochs=25000, variables = None, fold = 1, patience = 150, batch_size = 512, selection="", target_str = "abs(truth_lepton_pt - lepton_pt)", scalings={}, descriptor = "", loss = sum_preds_loss, extra_models = [], normalize_mass_points=False):
    if extra_models: retriever.register_extra_models(extra_models)

    if time == None:
        time = datetime.now()
    if stamp == None: 
        stamp = "{}_{}_{}_{}_{}_{}".format(time.year, time.month, time.day ,time.hour, time.minute, model_flavour) + "_" + descriptor

    inputs, main_out = gen_model(model_flavour, variables)
    opt = optimizer(lr=lr, decay=decay)
    model = keras.models.Model(inputs = inputs,  output=[main_out])
    model.compile(loss=loss, optimizer=opt)
    model.summary()
    all_variables = []
    for el in variables: all_variables += el 

    print("Deriving the scalings for the input variables")
    if len(scalings) != 0:
        df = retriever.get_dataframe(root_files, variables = ["mass_point"] + all_variables +  list(scalings.keys()), selection=selection)
        df = fold_df(1, df)["train"] #fold the df
        weights = df["weight"]
        for scale in scalings:
            to_scale = df.eval(scale)
            mean = np.sum(weights*to_scale)/np.sum(weights)
            sq_mean = np.sum(weights * to_scale**2)/np.sum(weights)
            mean_sq = mean ** 2
            print(scale)
            print(to_scale)
            print("mean squared : {}".format(mean_sq))
            print("squared mean: {}".format(sq_mean))
            print("Scaling variable {}".format(scale))
            assert len(to_scale) >= 0
            assert sq_mean >= mean_sq
            std_dev = np.sqrt(sq_mean - mean_sq)
            if std_dev == 0.0: std_dev = 1.0
            scale_mean_stddev = (mean, std_dev)
            scalings[scale] = scale_mean_stddev

    df = retriever.get_dataframe(root_files, variables = ["mass_point"] + all_variables, selection=selection)
    if normalize_mass_points: df = workspace_manager.normalize_mass_points(df)
    df = fold_df(1, df)
    x = get_x_from_folded_df(df, variables, target_str=target_str,  scalings = scalings, shuffle=True)
    X_train=x["X_train"]
    X_valid=x["X_valid"]
    X_test=x["X_test"]
    weights_train=x["weights_train"]
    weights_valid=x["weights_valid"]
    weights_test=x["weights_test"]
    target_train=x["target_train"]
    target_valid=x["target_valid"]
    target_test=x["target_test"]

    #scale the targets
    #get the bindices
    if scale_target:
       edges = np.linspace(-5.0,+5.0,31)
       vals, edges = np.histogram(target_train ,bins=edges, weights=weights_train/np.sum(weights_train))
       sel_train = (target_train > edges[0]) & (target_train < edges[-1])
       sel_valid = (target_valid > edges[0]) & (target_valid < edges[-1])
       sel_test = (target_test > edges[0]) & (target_test < edges[-1])
       bindex_train = return_bindex(target_train[sel_train], edges)
       bindex_valid = return_bindex(target_valid[sel_valid], edges)
       bindex_test = return_bindex(target_test[sel_test], edges)
       weights_train[sel_train] /= vals[bindex_train]
       weights_valid[sel_valid] /= vals[bindex_valid]
       weights_test[sel_test] /= vals[bindex_test]

    weights_train *= np.average(1.0 * (weights_train!=0.0)) * (float(len(weights_train))/np.sum(weights_train))
    weights_valid *= np.average(1.0 * (weights_valid!=0.0)) * (float(len(weights_valid))/np.sum(weights_valid))
    weights_test *= np.average(1.0 * (weights_test!=0.0)) * (float(len(weights_test))/np.sum(weights_test))

    print("targets {}".format(target_train))
    if not type(variables[0]) == list:
        X_train = [X_train]
        X_valid = [X_valid]

    #check for pathological variables
    for X in [X_train, X_valid, X_test]:
         for el in X:
              assert not np.any(el < -900.0)

    histories = []
    lr_o = lr
    lrs = []
    for i in [1, 3, 9, 27]:
        lrs.append(lr_o/float(i**(2)))
        opt = optimizer(lr=lr_o/float(i**(2)), decay=0.005)
        model.compile(loss=loss, optimizer=opt)
        model.summary()
        history = model.fit(X_train,target_train, shuffle=True, epochs = 10000000, batch_size=batch_size, validation_data = (X_valid, target_valid, weights_valid), callbacks = [keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=30, verbose=0, mode='auto')], sample_weight=weights_train)
        del history.model
        histories.append(history)

    assert utils.get_model_save_dir()
    out_dir = os.path.join(utils.get_model_save_dir(), stamp)
    if not os.path.exists(out_dir): os.makedirs(out_dir)
    model_file = os.path.join(out_dir, '{}.h5'.format(stamp))
    model.save_weights(model_file)

    #don't save the model, it's in the hdf5 file
    with open(os.path.join(out_dir, "{}.pkl".format(stamp)), "wb") as f:
        pickle.dump({"fold" : fold,\
        "model_file":model_file,\
        "selection":selection,\
        "variables":variables,\
        "history":histories,\
        "target":target_str,\
        'scale_target':scale_target,\
        "scalings":scalings,\
        "files" :root_files,\
        'lr':lrs,\
        'model_flavour':model_flavour,\
        'decay':decay,\
        'patience': patience,\
        'stamp':stamp,\
        'extra_models':extra_models,\
        'batch_size':batch_size,\
        'normalize_mass_points': normalize_mass_points},\
        f)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Event Type")
    parser.add_argument('--flavour', '-f', dest="flavour", type=str, required=True, help='The Event Type to be trained on')
    parser.add_argument('--lr', '-l', dest="lr", type=float, required=True, help='The Event Type to be trained on')
    parser.add_argument('--stamp', '-s', dest="stamp", type=str, required=True, help='The stamp for the filenames')
    parser.add_argument('--model_flavour', '-mf', dest="model_flavour", type=str, required=True, help='the name of the model type to train')
    parser.add_argument('--var_selection', '-vs', dest="var_selection", type=str, required=True, help='The types of variables to train over')
    parser.add_argument('--file_tokens', '-ft', dest="file_tokens", type=str, required=True, help='tokens to search for when looking for training files')
    parser.add_argument('--extra_models', '-em', dest="extra_models", type=str, required=False, help='extra models to include scores for when training the network.')
    parser.add_argument('--mass_variable', '-mv', dest="mass_variable", type=str, required=False, help="Mass Variable used to create fit and model", default="m4l_constrained")
    parser.add_argument('--normalize_mass_points', '-mnp', dest="normalize_mass_points", type=bool, required=False, default=False, help='extra models to include scores for when training the network.')
    args = parser.parse_args()
    if hasattr(args, "extra_models") and args.extra_models: retriever.register_extra_models(args.extra_models.split(","))

    time = datetime.now()
    if args.stamp == "": non_descr_stamp = "{}_{}_{}_{}_{}_{}".format(time.year, time.month, time.day ,time.hour, time.minute, args.model_flavour)

    sel_low = 105.0
    sel_hi = 160.0
    for flavour in args.flavour.split(","):
        event_type_string = args.flavour
        if args.stamp == "": stamp = non_descr_stamp + "_" + flavour
        else: stamp = args.stamp
        selection = utils.get_full_selection(flavour, "Incl", args.mass_variable)

        #make sure that everything is loaded
        poi, model, pdfs, observables, parameters, nuis_parameters, functions, categories = workspace_manager.merge_bkg_signal_models(retriever, mass_observable = args.mass_variable, channels = [flavour], systematics = [], categories=["Incl"], do_bkg=False, do_sig =True, model_types="Che", bkgs=[])

        #now get the parameters
        import signal_model as __signal_model__
        params = __signal_model__.get_params(args.mass_variable, flavour, "Incl", "Nominal")
        poi = workspace_manager.get_variable("mass_point")

        target_str = args.mass_variable
        mean_str = "0"
        alpha_low_str = "0"
        alpha_hi_str = "0"
        n_low_str = "0"
        n_hi_str = "0"
        extra_plus_str = " + (1.0*( (mass_point <= ({mp} + 0.1)) and (mass_point >= ({mp} - 0.1)) )*({val}) )"
        for i in list(range(122,129)) + [i + 0.5 for i in range(122, 129)]:
            poi.setVal(i)
            alpha_low_str = alpha_low_str + extra_plus_str.format(mp=i,val=params["alpha_low"].getVal())
            alpha_hi_str = alpha_low_str + extra_plus_str.format(mp=i,val=params["alpha_hi"].getVal())
            n_hi_str = n_hi_str + extra_plus_str.format(mp=i,val=params["n_hi"].getVal())
            n_low_str = n_low_str + extra_plus_str.format(mp=i,val=params["n_low"].getVal())
            mean_str = mean_str + extra_plus_str.format(mp=i,val=params["mean"].getVal())

        assert args.var_selection in variable_bank or args.model_flavour == "residual_correction_network"
        training_variables = variable_bank[args.var_selection]

        if args.stamp == "": stamp = stamp + "_" + args.var_selection

        variables = \
        [\
        [target_str],\
        [alpha_hi_str],\
        [alpha_low_str],\
        [n_hi_str],\
        [n_low_str],\
        [mean_str],\
        ["{}".format(sel_low)],\
        ["{}".format(sel_hi)],\
        training_variables,\
        ]

        #change the training variables to be only that which must be corrected
        to_scale = []
        if "residual_correction_network" in args.model_flavour:
            variables[-1] = ["sigma_prediction_{}".format(flavour)]
        else:
            to_scale+=training_variables + [t.replace("_truth_born", "") for t in training_variables if ("_truth_born" in t)]
            to_scale=list(set(to_scale))
        if  "residual_correction_network" in args.model_flavour:
            to_scale += variables[-1]

        needed_branches = list(set(variables[-1] + training_variables + to_scale))

        scalings={}
        for string in to_scale:
             scalings[string] = None

        root_files = []
        file_tokens = args.file_tokens.split(",")
        root_files = retriever.get_root_files(file_tokens)

        if stamp == "" and args.normalize_mass_points: stamp += "_MPNormed_"
        if args.stamp == "": stamp += "_" + "_".join(file_tokens)

        time = datetime.now()
        #do some checks on the dcb parameters
        print("retrieving root files {}".format(root_files))
        dfs = retriever.get_dataframe(root_files, variables =list(set( ["mass_point"] + needed_branches)), selection=selection)
        dfs = fold_df(1, dfs)["train"] #fold the df

        model_flavour = args.model_flavour

        loss = sum_preds_loss
        train_config = {}
        train_config["data"] = None
        train_config["root_files"] = root_files
        train_config["time"] = time
        train_config["scale_target"] = False
        train_config["model_flavour"] = model_flavour
        train_config["lr"] = args.lr
        train_config["decay"] = 0.0
        train_config["epochs"] = 100000000
        train_config["variables"] = variables
        train_config["fold"] = 1
        train_config["patience"] = 50
        train_config["batch_size"] = int(len(dfs)/30.0)
        train_config["selection"] = selection
        train_config["target_str"] = target_str
        train_config["scalings"] = scalings
        train_config["descriptor"] = event_type_string
        train_config["loss"] = sum_preds_loss
        train_config["stamp"] = stamp
        train_config["normalize_mass_points"] = args.normalize_mass_points
        if hasattr(args, "extra_models") and args.extra_models: train_config["extra_models"] = args.extra_models.split(",")
        else:  train_config["extra_models"] = []

        del dfs

        #off we go! train the models :)
        for key in train_config:
            print("{} set to {}".format(key, train_config[key]))
        train(**train_config)
        print("THIS JOB HAS FINISHED! HOORAY")
