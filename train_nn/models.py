
from __future__ import print_function
import keras
import tensorflow as tf
from tensorflow.python.framework import ops
import numpy as np
from nll_layer import NllLayer

def get_numpy_transform(predictions, varname = ""):
    '''
    Transform the predictions of a NLL NN to those that correspond to the maximum likelihood estimates.
    '''
    if "alpha_hi" in varname:
       return np.maximum( 0.1 * np.exp(-1.0 * np.abs(10.0 * predictions + 9.0)), predictions + 1.0) + 0.001
    if "alpha_low" in varname:
       return np.maximum( 0.1 * np.exp(-1.0 * np.abs(10.0 * predictions + 9.0)), predictions + 1.0) + 0.001
    if "n_hi" in varname:
       return np.maximum( 0.1 * np.exp(-1.0 * np.abs(10.0 * predictions + 9.0 )), predictions + 1.0) + 1.001
    if "n_low" in varname:
       return np.maximum( 0.1 * np.exp(-1.0 * np.abs(10.0 * predictions + 9.0)), predictions + 1.0) + 1.001
    if "sigma" in varname:
       return np.maximum( 0.1 * np.exp(-1.0 * np.abs(10.0 * predictions + 9.0)), predictions + 1.0) + 0.001
    if "mean" in varname:
       return predictions

def get_keras_transform(predictions, varname = ""):
    '''
    Custom activations for the NN so that the predictions remain in the bounds of the allowed DCB parameters.
    '''
    if "alpha_hi" in varname:
       return keras.backend.maximum(0.1 * keras.backend.exp(-1.0 * tf.abs(10.0 * predictions + 9.0)), predictions + 1.0) + 0.001
    if "alpha_low" in varname:
       return keras.backend.maximum(0.1 *  keras.backend.exp(-1.0 * tf.abs(10.0 * predictions + 9.0)), predictions + 1.0) + 0.001
    if "n_hi" in varname:
       return keras.backend.maximum(0.1 * keras.backend.exp(-1.0 * tf.abs(10.0 * predictions + 9.0)), predictions + 1.0) + 1.001
    if "n_low" in varname:
       return keras.backend.maximum(0.1 * keras.backend.exp(-1.0 * tf.abs(10.0 * predictions + 9.0)), predictions + 1.0) + 1.001
    if "sigma" in varname:
       return keras.backend.maximum(0.1 * keras.backend.exp(-1.0 * tf.abs(10.0 * predictions + 9.0)), predictions + 1.0) + 0.001
    if "mean" in varname:
       return predictions

def model_popper(model, stop_key = ""):
    '''
    remove layers from a model until the last layer contains the word stop_key in it's name.
    '''
    print("Here is the model before removing layers")
    model.summary()
    while (stop_key not in model.layers[-1].name and len(model.layers) > 0):
        print("Popping layer {}".format(model.layers[-1].name))
        model.layers.pop()
    print("Here it is after removing the layers")
    model.summary()
    return model

model_library = ["shallow_relu_drop_middle", "shallow_leakyrelu_drop_middle", "shallow_many_hidden_relu_drop_middle", "shallow_many_hidden_leakyrelu_drop_middle", "deep_many_hidden_relu_drop_middle", "deep_many_hidden_leakyrelu_drop_middle", "NLLModel"]
def gen_model(library_name, input_vars):
    if library_name == "DCB_Model":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_low = keras.Input(shape=(len(mean_var),), name = "range_low_input")
        range_high = keras.Input(shape=(len(mean_var),), name = "range_high_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(inputs)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction", bias_initializer = keras.initializers.Constant(0.5))(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "DCB_Model_Pyramid":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_low = keras.Input(shape=(len(mean_var),), name = "range_low_input")
        range_high = keras.Input(shape=(len(mean_var),), name = "range_high_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(50, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(inputs)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(30, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(10, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction", bias_initializer = keras.initializers.Constant(0.5))(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "DCB_residual_correction_network":
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]
         
        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_high = keras.Input(shape=(len(range_high_var),), name = "range_high_input")
        range_low = keras.Input(shape=(len(range_low_var),), name = "range_low_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform')(inputs)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        sigma = keras.layers.Dense(1, activation='linear', name="sigma_corrected_prediction", bias_initializer = keras.initializers.Constant(0.5))(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "DCB_Model_Deep_Pyramid":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_low = keras.Input(shape=(len(mean_var),), name = "range_low_input")
        range_high = keras.Input(shape=(len(mean_var),), name = "range_high_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(50, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(inputs)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(40, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(30, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(15, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(10, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction", bias_initializer = keras.initializers.Constant(0.5))(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "DCB_Model_Deepest_Pyramid":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_low = keras.Input(shape=(len(mean_var),), name = "range_low_input")
        range_high = keras.Input(shape=(len(mean_var),), name = "range_high_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(50, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(inputs)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(40, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(30, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(15, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(10, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction", bias_initializer = keras.initializers.Constant(0.8))(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "NllModel":
        mass_input = input_vars[0]
        inputs  = keras.Input(shape=(len(input_vars[1]),), name="inputs")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")
        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(15, activation='relu')(x)
        x = keras.layers.Dense(15, activation='relu')(x)
        alpha_hi = keras.layers.Dense(1, activation='linear', name="alpha_hi_prediction")(x)
        n_hi = keras.layers.Dense(1, activation='linear', name = "n_hi_prediction")(x)
        alpha_low = keras.layers.Dense(1, activation='linear', name = "alpha_low_prediction")(x)
        n_low = keras.layers.Dense(1, activation='linear', name = "n_low_prediction")(x)
        mean = keras.layers.Dense(1, activation='linear', name = "mean_prediction")(x)
        sigma = keras.layers.Dense(1, activation='linear', name = "sigma_prediction")(x)
        alpha_hi = keras.layers.Lambda(lambda a, varname="alpha_hi" : get_keras_transform(a, varname=varname))(alpha_hi)
        alpha_low = keras.layers.Lambda(lambda a, varname="alpha_low" : get_keras_transform(a, varname=varname))(alpha_low)
        n_hi = keras.layers.Lambda(lambda a, varname="n_hi" : get_keras_transform(a, varname=varname))(n_hi)
        n_low = keras.layers.Lambda(lambda a, varname="n_low" : get_keras_transform(a, varname=varname))(n_low)
        mean = keras.layers.Lambda(lambda a, varname="mean" : get_keras_transform(a, varname=varname))(mean)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)
        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,inputs], main_out

    if library_name == "NllModel_FloatMeanSigma":
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(15, activation='relu')(x)
        x = keras.layers.Dense(15, activation='relu')(x)

        mean = keras.layers.Dense(1, activation='linear', name="mean_predidiction")(x)
        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        mean = keras.layers.Lambda(lambda a, varname="mean" : get_keras_transform(a, varname=varname))(mean)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)
        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low,inputs], main_out

    if library_name == "NllModel_Deep_Leaky_FloatAllButMean":
        mass_input = input_vars[0]
        mean_var = input_vars[1]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)

        alpha_hi = keras.layers.Dense(20, activation='tanh')(sigma)
        alpha_hi = keras.layers.Dense(1, activation='linear', name="alpha_hi_prediction")(alpha_hi)
        alpha_low = keras.layers.Dense(20, activation='tanh')(sigma)
        alpha_low = keras.layers.Dense(1, activation='linear', name="alpha_low_prediction")(alpha_low)
        n_hi = keras.layers.Dense(20, activation='tanh')(sigma)
        n_hi = keras.layers.Dense(1, activation='linear', name="n_hi_prediction")(n_hi)
        n_low = keras.layers.Dense(20, activation='tanh')(sigma)
        n_low = keras.layers.Dense(1, activation='linear', name="n_low_prediction")(n_low)

        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)
        alpha_hi = keras.layers.Lambda(lambda a, varname="alpha_hi" : get_keras_transform(a, varname=varname))(alpha_hi)
        alpha_low = keras.layers.Lambda(lambda a, varname="alpha_low" : get_keras_transform(a, varname=varname))(alpha_low)
        n_hi = keras.layers.Lambda(lambda a, varname="n_hi" : get_keras_transform(a, varname=varname))(n_hi)
        n_low = keras.layers.Lambda(lambda a, varname="n_low" : get_keras_transform(a, varname=varname))(n_low)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,mean,inputs], main_out

    if library_name == "NllModel_Deep_Leaky_FloatAll":
        mass_input = input_vars[0]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation = 'linear')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)

        mean = keras.layers.Dense(20, activation='tanh')(sigma)
        mean = keras.layers.Dense(1, activation='linear', name="mean_prediction")(mean)
        alpha_hi = keras.layers.Dense(20, activation='tanh')(sigma)
        alpha_hi = keras.layers.Dense(1, activation='linear', name="alpha_hi_prediction")(alpha_hi)
        alpha_low = keras.layers.Dense(20, activation='tanh')(sigma)
        alpha_low = keras.layers.Dense(1, activation='linear', name="alpha_low_prediction")(alpha_low)
        n_hi = keras.layers.Dense(20, activation='tanh')(sigma)
        n_hi = keras.layers.Dense(1, activation='linear', name="n_hi_prediction")(n_hi)
        n_low = keras.layers.Dense(20, activation='tanh')(sigma)
        n_low = keras.layers.Dense(1, activation='linear', name="n_low_prediction")(n_low)

        mean = keras.layers.Lambda(lambda a, varname="mean" : get_keras_transform(a, varname=varname))(mean)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)
        alpha_hi = keras.layers.Lambda(lambda a, varname="alpha_hi" : get_keras_transform(a, varname=varname))(alpha_hi)
        alpha_low = keras.layers.Lambda(lambda a, varname="alpha_low" : get_keras_transform(a, varname=varname))(alpha_low)
        n_hi = keras.layers.Lambda(lambda a, varname="n_hi" : get_keras_transform(a, varname=varname))(n_hi)
        n_low = keras.layers.Lambda(lambda a, varname="n_low" : get_keras_transform(a, varname=varname))(n_low)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,inputs], main_out

    if library_name == "NllModel_Deep_Leaky_FloatAll_DeepAux":
        mass_input = input_vars[0]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)

        mean = keras.layers.Dense(20, activation='tanh')(sigma)
        mean = keras.layers.Dense(20, activation='tanh')(mean)
        mean = keras.layers.Dense(1, activation='linear', name="mean_prediction")(mean)
        alpha_hi = keras.layers.Dense(20, activation='tanh')(sigma)
        alpha_hi = keras.layers.Dense(20, activation='tanh')(alpha_hi)
        alpha_hi = keras.layers.Dense(1, activation='linear', name="alpha_hi_prediction")(alpha_hi)
        alpha_low = keras.layers.Dense(20, activation='tanh')(sigma)
        alpha_low = keras.layers.Dense(20, activation='tanh')(alpha_low)
        alpha_low = keras.layers.Dense(1, activation='linear', name="alpha_low_prediction")(alpha_low)
        n_hi = keras.layers.Dense(20, activation='tanh')(sigma)
        n_hi = keras.layers.Dense(20, activation='tanh')(n_hi)
        n_hi = keras.layers.Dense(1, activation='linear', name="n_hi_prediction")(n_hi)
        n_low = keras.layers.Dense(20, activation='tanh')(sigma)
        n_low = keras.layers.Dense(20, activation='tanh')(n_low)
        n_low = keras.layers.Dense(1, activation='linear', name="n_low_prediction")(n_low)

        mean = keras.layers.Lambda(lambda a, varname="mean" : get_keras_transform(a, varname=varname))(mean)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)
        alpha_hi = keras.layers.Lambda(lambda a, varname="alpha_hi" : get_keras_transform(a, varname=varname))(alpha_hi)
        alpha_low = keras.layers.Lambda(lambda a, varname="alpha_low" : get_keras_transform(a, varname=varname))(alpha_low)
        n_hi = keras.layers.Lambda(lambda a, varname="n_hi" : get_keras_transform(a, varname=varname))(n_hi)
        n_low = keras.layers.Lambda(lambda a, varname="n_low" : get_keras_transform(a, varname=varname))(n_low)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,inputs], main_out

    if library_name == "QuantileModel_Deep_Leaky":
        inputs  = keras.Input(shape=(len(input_vars),), name="inputs")
        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        return [inputs], sigma

    if library_name == "NllModel_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(15, activation='relu')(x)
        x = keras.layers.Dense(15, activation='relu')(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,inputs], main_out

    if library_name == "NllModel_Deeper_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(15, activation='relu')(x)
        x = keras.layers.Dense(15, activation='relu')(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,inputs], main_out

    if library_name == "NllModel_Deeper_Leaky_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        range_high = keras.Input(shape=(len(range_high_var),), name = "range_high_input")
        range_low = keras.Input(shape=(len(range_low_var),), name = "range_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "NllModel_Deeper_LeakyRelu_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(30)(inputs)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(15)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(10)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(1)(x, name="sigma_prediction")

        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(x)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,inputs], main_out

    if library_name == "NllModel_Deeper_Tanh_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_low = keras.Input(shape=(len(mean_var),), name = "range_low_input")
        range_high = keras.Input(shape=(len(mean_var),), name = "range_high_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear', bias_initializer = 'random_uniform', kernel_initializer = "normal")(inputs)
        x = keras.layers.Dense(20, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(20, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(20, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(20, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "NllModel_Deeper_NoLinear_Tanh_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_low = keras.Input(shape=(len(mean_var),), name = "range_low_input")
        range_high = keras.Input(shape=(len(mean_var),), name = "range_high_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(20, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(inputs)
        x = keras.layers.Dense(20, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(20, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(20, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "NllModel_Deeper_NoLinear_NotToDense_Tanh_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_low = keras.Input(shape=(len(mean_var),), name = "range_low_input")
        range_high = keras.Input(shape=(len(mean_var),), name = "range_high_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(32, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(inputs)
        x = keras.layers.Dense(64, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(32, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(16, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "NllModel_Deepest_NoLinear_Tanh_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_low = keras.Input(shape=(len(mean_var),), name = "range_low_input")
        range_high = keras.Input(shape=(len(mean_var),), name = "range_high_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(32, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(inputs)
        x = keras.layers.Dense(64, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(32, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(16, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(8, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "NllModel_Deeper_NoLinear_Dense_Tanh_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_low = keras.Input(shape=(len(mean_var),), name = "range_low_input")
        range_high = keras.Input(shape=(len(mean_var),), name = "range_high_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(64, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(inputs)
        x = keras.layers.Dense(128, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(64, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)
        x = keras.layers.Dense(16, activation='tanh', bias_initializer = 'random_uniform', kernel_initializer = "normal")(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    if library_name == "NllModel_Deeper_TanhLotsRelu_FloatSigma":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='relu')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='relu')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)
        x = keras.layers.Dense(20, activation='tanh')(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,inputs], main_out

    if library_name == "NllModel_Deeper_FloatSigma_l2Reg":

        #get the input variabes
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]

        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='tanh', kernel_regularizer=keras.regularizers.l2(0.001))(x)
        x = keras.layers.Dense(20, activation='tanh', kernel_regularizer=keras.regularizers.l2(0.001))(x)
        x = keras.layers.Dense(20, activation='tanh', kernel_regularizer=keras.regularizers.l2(0.001))(x)
        x = keras.layers.Dense(20, activation='tanh', kernel_regularizer=keras.regularizers.l2(0.001))(x)

        sigma = keras.layers.Dense(1, activation='linear', name="sigma_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,inputs], main_out

    if library_name == "shallow_relu_drop_middle":
        inputs  = keras.Input(shape=(len(input_vars),), name="inputs")
        x = keras.layers.Dense(15, activation='relu')(inputs)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(20, activation='relu')(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(15, activation='relu')(x)
        x = keras.layers.Dense(10, activation='linear')(x)
        main_out = keras.layers.Dense(1, activation=None,name="main_out")(x)
        return ([inputs], main_out)

    if library_name == "shallow_relu_drop_middle_fiveoutputs":
        inputs  = keras.Input(shape=(len(input_vars),), name="inputs")
        x = keras.layers.Dense(15, activation='relu')(inputs)
        x = keras.layers.Dense(20, activation='relu')(x)
        x = keras.layers.Dense(15, activation='relu')(x)
        x = keras.layers.Dense(10, activation='linear')(x)
        main_out = keras.layers.Dense(5, activation=None,name="main_out")(x)
        return ([inputs], main_out)

    if library_name == "shallow_linear_drop_middle":
        inputs  = keras.Input(shape=(len(input_vars),), name="inputs")
        x = keras.layers.Dense(15, activation='linear')(inputs)
        x = keras.layers.Dense(20, activation='linear')(x)
        x = keras.layers.Dense(15, activation='linear')(x)
        x = keras.layers.Dense(10, activation='linear')(x)
        main_out = keras.layers.Dense(1, activation=None,name="main_out")(x)
        return ([inputs], main_out)

    elif library_name == "shallow_leakyrelu_drop_middle":
        inputs  = keras.Input(shape=(len(input_vars),), name="inputs")
        x = keras.layers.Dense(15, activation=None)(inputs)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(20, activation=None)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(15, activation=None)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(10, activation='linear')(x)
        main_out = keras.layers.Dense(1, activation=None,name="main_out")(x)
        return ([inputs], main_out)

    if library_name == "shallow_many_hidden_relu_drop_middle":
        inputs  = keras.Input(shape=(len(input_vars),), name="inputs")
        x = keras.layers.Dense(30, activation='relu')(inputs)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(40, activation='relu')(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(30, activation='relu')(x)
        x = keras.layers.Dense(20, activation='linear')(x)
        main_out = keras.layers.Dense(1, activation=None,name="main_out")(x)
        return ([inputs], main_out)

    elif library_name == "shallow_many_hidden_leakyrelu_drop_middle":
        inputs  = keras.Input(shape=(len(input_vars),), name="inputs")
        x = keras.layers.Dense(30, activation=None)(inputs)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(40, activation=None)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(30, activation=None)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear')(x)
        main_out = keras.layers.Dense(1, activation=None,name="main_out")(x)
        return ([inputs], main_out)

    if library_name == "deep_many_hidden_relu_drop_middle":
        inputs  = keras.Input(shape=(len(input_vars),), name="inputs")
        x = keras.layers.Dense(30, activation='relu')(inputs)
        x = keras.layers.Dense(40, activation='relu')(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(40, activation='relu')(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(40, activation='relu')(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(30, activation='relu')(x)
        x = keras.layers.Dense(20, activation='linear')(x)
        main_out = keras.layers.Dense(1, activation=None,name="main_out")(x)
        return ([inputs], main_out)

    elif library_name == "deep_many_hidden_leakyrelu_drop_middle":
        inputs  = keras.Input(shape=(len(input_vars),), name="inputs")
        x = keras.layers.Dense(30, activation=None)(inputs)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(40, activation=None)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(40, activation=None)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(40, activation=None)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dropout(0.05)(x)
        x = keras.layers.Dense(30, activation=None)(x)
        x = keras.layers.LeakyReLU(alpha=0.1)(x)
        x = keras.layers.Dense(20, activation='linear')(x)
        main_out = keras.layers.Dense(1, activation=None,name="main_out")(x)
        return ([inputs], main_out)

    elif library_name == "residual_correction_network":
        mass_input = input_vars[0]
        alpha_hi_var = input_vars[1]
        alpha_low_var = input_vars[2]
        n_hi_var = input_vars[3]
        n_low_var = input_vars[4]
        mean_var = input_vars[5]
        range_low_var = input_vars[6]
        range_high_var = input_vars[7]
         
        inputs  = keras.Input(shape=(len(input_vars[-1]),), name="inputs")
        alpha_hi = keras.Input(shape=(len(alpha_hi_var),), name="alpha_hi_input")
        alpha_low  = keras.Input(shape=(len(alpha_low_var),), name="alpha_low_input")
        n_hi = keras.Input(shape=(len(n_hi_var),), name = "n_hi_input")
        n_low = keras.Input(shape=(len(n_low_var),), name = "n_low_input")
        mean = keras.Input(shape=(len(mean_var),), name = "mean_input")
        range_high = keras.Input(shape=(len(range_high_var),), name = "range_high_input")
        range_low = keras.Input(shape=(len(range_low_var),), name = "range_low_input")
        mass_input_tensor = keras.Input(shape=(len(mass_input),), name="mass_input")

        x = keras.layers.Dense(5, activation='linear', bias_initializer = 'random_uniform')(inputs)
        x = keras.layers.Dense(15, activation='tanh', bias_initializer = 'random_uniform')(x)
        x = keras.layers.Dense(15, activation='tanh', bias_initializer = 'random_uniform')(x)
        x = keras.layers.Dense(5, activation='tanh', bias_initializer = 'random_uniform')(x)
        sigma = keras.layers.Dense(1, activation='linear', name="sigma_corrected_prediction")(x)
        sigma = keras.layers.Lambda(lambda a, varname="sigma" : get_keras_transform(a, varname=varname))(sigma)

        NLL_input = [mass_input_tensor, alpha_hi, n_hi, alpha_low, n_low, mean, sigma, range_low, range_high]
        main_out = NllLayer()(NLL_input)
        return [mass_input_tensor,alpha_hi, alpha_low, n_hi, n_low, mean,range_low, range_high,inputs], main_out

    elif library_name == "cheat_network":
        for el in input_vars:
            assert type(el) == list
        input_e  = keras.Input(shape=(len(input_vars[0]),), name="inputs_1")
        input_px  = keras.Input(shape=(len(input_vars[1]),), name="inputs_2")
        input_py  = keras.Input(shape=(len(input_vars[2]),), name="inputs_3")
        input_pz = keras.Input(shape=(len(input_vars[3]),), name="inputs_4")
        x = keras.layers.Dense(4, activation='linear')(input_e)
        #x = keras.layers.Dense(1, activation='linear')(x)
        y = keras.layers.Dense(4, activation='linear')(input_px)
        #y = keras.layers.Dense(1, activation='linear')(y)
        z = keras.layers.Dense(4, activation='linear')(input_py)
        #z = keras.layers.Dense(1, activation='linear')(z)
        w = keras.layers.Dense(4, activation='linear')(input_pz)
        #w = keras.layers.Dense(1, activation='linear')(w)
        all_in = keras.layers.Concatenate()([x,y,z,w])
        all_squared = keras.layers.Lambda(lambda a : a**2)(all_in)
        add_up = keras.layers.Dense(4, activation='linear')(all_squared)
        add_up = keras.layers.Dense(1, activation='linear')(add_up)
        square_rooted = keras.layers.Lambda( lambda a : tf.sign(a) * ( tf.abs(a) ** 0.5))(add_up)
        main_out = keras.layers.Dense(1, activation='linear', name='bias_me')(square_rooted)
        return ([input_e, input_px, input_py, input_pz], main_out)

def tilted_loss(q,y,f):
    e = (y-f)
    return keras.backend.mean(keras.backend.maximum(q*e, (q-1)*e), axis=-1)

def sum_preds_loss(y,f):
    return keras.backend.sum(1000.0 * (-1.0 * f), axis=-1)

