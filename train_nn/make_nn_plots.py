import os
from datasets import fold_df, get_df, get_x, scale
import keras
import pickle
os.system("rm -rf /home/"+os.getenv("USER")+"/.cache/matplotlib/tex.cache/.matplotlib_lock-*")
import uproot as ur
import utils
import glob
import keras
import tensorflow as tf
from models import gen_model
from nll_layer import NllLayer
CB_color_cycle = [r'#377eb8', r'#ff7f00',r'#dede00', r'#4daf4a',\
                  r'#f781bf', r'#a65628', r'#984ea3',\
                  r'#999999']
from models import tilted_loss, sum_preds_loss
#from root_numpy import fill_profile
##'#dede00'
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
#plt.tight_layout()
#matplotlib.rcParams['axes.prop_cycle'] = matplotlib.cycler(color=CB_color_cycle) 
#matplotlib.rcParams['text.usetex'] = True
#matplotlib.rcParams['font.size'] = 13
#matplotlib.rcParams['figure.autolayout'] = True
#matplotlib.rcParams['mathtext.fontset'] = 'custom'
#matplotlib.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
#matplotlib.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
#matplotlib.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
import numpy as np
import pandas as pd
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
#plt.tight_layout()


#stamp = "2019_9_9_13_11"
#stamp = "2019_9_11_13_37"
#stamp  = "2019_9_11_20_24"#_cheat_network
#stamp = "2019_9_19_15_26_shallow_linear_drop_middle_test"
stamp = "2019_9_29_19_10_NllModel_test"
#stamp = "2019_9_29_18_47_NllModel_test"
#stamp = "2019_9_29_18_47_NllModel_test"
#all right lets do some plottng
model_search = "{}*.h5".format(stamp)
metadata_search = "{}*.pkl".format(stamp)

files = glob.glob(model_search)
pkl_files = glob.glob(metadata_search)
list_of_model_types = []
for f in files:
    mod_type = "_".join(f.lstrip(stamp + "_").rstrip(".h5").split("_")[:-1])
    if mod_type not in list_of_model_types:
        list_of_model_types.append(mod_type)

assert len(files) == len(pkl_files)

for mod_type in list_of_model_types:
    print("Studying the {} model type".format(mod_type))
    mod_pkl_files = [f for f in pkl_files if mod_type in f]
    mod_files = [f for f in files if mod_type in f]
    mod_pkl_files = sorted(mod_pkl_files)
    mod_files = sorted(mod_files)
    print(mod_pkl_files)
    print(mod_files)
    means = []
    mean_errs = []
    best = -1
    best_mean = 1000000000000000000000000000000000000.0
    for i,f in enumerate(mod_pkl_files):
        print("opening {}".format(f))
        with open(f, "rb") as f:
            stuff = pickle.load(f)
            val_loss = stuff["history"].history['val_loss']
            if (len(val_loss) > 1000):
                mean_loss = np.mean(val_loss[:-70])
                mean_err = np.std(val_loss[:-70])
            elif (len(val_loss) > 100):
                mean_loss = np.mean(val_loss[:-10])
                mean_err = np.std(val_loss[:-10])
            else:
                mean_loss = np.mean(val_loss)
                mean_err = np.std(val_loss)
            if mean_loss < best_mean:
                best = i
                best_mean = mean_loss
            means.append(mean_loss)
            mean_errs.append(mean_err)
            f.close()
    print("For files {} the mean loss was {} pm {}".format(f, means, mean_errs))

    model_file = mod_files[best]
    metadata_file = mod_pkl_files[best]
    print(model_file)
    print(metadata_file)

    with open(metadata_file, "rb") as f:
        metadata = pickle.load(f)

    stamp = metadata_file.rstrip(".pkl")
    variables = metadata["variables"]
    selection = metadata["selection"]
    history = metadata["history"]
    fold = metadata["fold"]
    target_str = metadata["target"]
    scaled = metadata["scaled"]
    scale_target = metadata["scale_target"]
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    root_files = metadata["files"]
    mean_scales = metadata["mean_scales"]
    std_scales = metadata["std_scales"]

    #get the model, and load the weights
    model_flavour =  metadata["model_flavour"]
    inputs, main_out = gen_model(model_flavour, variables)
    model = keras.models.Model(inputs = inputs,  output=[main_out])
    model.load_weights(model_file)

    x = get_x(root_files, variables, target_str=target_str, selection = selection, fold=fold)
    X_train=x["X_train"]
    X_valid=x["X_valid"]
    X_test=x["X_test"]
    weights_train=x["weights_train"]
    weights_valid=x["weights_valid"]
    weights_test=x["weights_test"]
    target_train=x["target_train"]
    target_valid=x["target_valid"]
    target_test=x["target_test"]

    #scale the targets
    #get the bindices
    if scale_target:
       edges = np.linspace(-5.0,+5.0,31)
       vals, edges = np.histogram(target_train ,bins=edges, weights=weights_train/np.sum(weights_train))
       sel_train = (target_train > edges[0]) & (target_train < edges[-1])
       sel_valid = (target_valid > edges[0]) & (target_valid < edges[-1])
       sel_test = (target_test > edges[0]) & (target_test < edges[-1])
       bindex_train = return_bindex(target_train[sel_train], edges)
       bindex_valid = return_bindex(target_valid[sel_valid], edges)
       bindex_test = return_bindex(target_test[sel_test], edges)
       weights_train[sel_train] /= vals[bindex_train]
       weights_valid[sel_valid] /= vals[bindex_valid]
       weights_test[sel_test] /= vals[bindex_test]

    if len(mean_scales) != 0:
        scale(X_train, X_valid, X_test, mean_scales, std_scales)

    prediction_train = model.predict(X_train)[:,0]
    prediction_valid = model.predict(X_valid)[:,0]
    prediction_test = model.predict(X_test)[:,0]

    train_resid = prediction_train - target_train
    valid_resid = prediction_valid - target_valid
    test_resid = prediction_test - target_test

    train_pull = train_resid/target_train
    valid_pull = valid_resid/target_valid
    test_pull = test_resid/target_test

    to_plots = []
    to_weights = []
    labels = []
    names = []

    to_plots.append( (train_resid, valid_resid, test_resid) )
    labels.append("{} - {}".format("Prediction","target"))
    names.append("resid")
    to_plots.append( (train_pull, valid_pull, test_pull) )
    labels.append("{}".format("(target - Prediction / target)"))
    names.append("pull")

    weights_train/=np.sum(weights_train)
    weights_valid/=np.sum(weights_valid)
    weights_test/=np.sum(weights_test)

    for to_plot,label,name in zip(to_plots, labels,names):
         bins = np.linspace(-5.0,5.0, 31)
         train, valid, test = to_plot
         plt.plot()
         plt.close()

         hist_train, edges = np.histogram(train,bins=bins, weights=weights_train)
         hist_valid, edges = np.histogram(valid,bins=bins, weights=weights_valid)
         hist_test, edges = np.histogram(test,bins=bins, weights=weights_test)
         train_errsq, edges = np.histogram(train,bins=bins, weights=weights_train**2)
         valid_errsq, edges = np.histogram(valid,bins=bins, weights=weights_valid**2)
         test_errsq, edges = np.histogram(test,bins=bins, weights=weights_test**2)
         train_err = train_errsq**0.5
         valid_err = valid_errsq**0.5
         test_err = test_errsq**0.5

         plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_train, train_err, fmt='o', label="train")
         plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_valid, valid_err, fmt='o', label="valid")
         plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_test, test_err, fmt='o', label="test")
         plt.xlim([bins[0], bins[-1]])
         plt.legend(loc='best')
         plt.close()

         plt.plot()
         plt.close()

         plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_train, train_err, fmt='o', label="train")
         plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_valid, valid_err, fmt='o', label="valid")
         plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_test, test_err, fmt='o', label="test")
         plt.xlabel(label, fontsize=20)
         plt.ylabel("Arb. Units", fontsize=20)
         plt.xlim([bins[0], bins[-1]])
         plt.legend(loc='best')
         plt.savefig("{}.png".format(name))
         plt.close()

    bins = np.linspace(-5.0, +5.0, 31)
    hist_prediction_train, edges = np.histogram(prediction_train,bins=bins, weights=weights_train)
    hist_prediction_valid, edges = np.histogram(prediction_valid,bins=bins, weights=weights_valid)
    hist_prediction_test, edges = np.histogram(prediction_test,bins=bins, weights=weights_test)
    hist_target_test, edges = np.histogram(target_test,bins=bins, weights=weights_test)
    train_errsq, edges = np.histogram(prediction_train,bins=bins, weights=weights_train**2)
    valid_errsq, edges = np.histogram(prediction_valid,bins=bins, weights=weights_valid**2)
    test_errsq, edges = np.histogram(prediction_test,bins=bins, weights=weights_test**2)
    target_errsq, edges = np.histogram(target_test,bins=bins, weights=weights_test**2)
    train_err = train_errsq**0.5
    valid_err = valid_errsq**0.5
    test_err = test_errsq**0.5
    target_err = target_errsq**0.5

    name = "TargetAndPred"
    plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_prediction_train, train_err, fmt='o', label="training set Predictionictions")
    plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_prediction_valid, valid_err, fmt='o', label="validation set Predictionictions")
    plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_prediction_test, test_err, fmt='o', label="testing set Predictionictions")
    plt.errorbar([(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges)-1)], hist_target_test, target_err, fmt='o', label="testing set targets")
    plt.xlabel("target", fontsize=26)
    plt.ylabel("Arb. Units", fontsize=26)
    plt.xlim([bins[0], bins[-1]])
    plt.legend(loc='best')
    plt.savefig("{}_{}.png".format(name, stamp))
    plt.close()

    print(np.any(prediction_test == np.nan))
    print(np.any(prediction_test == np.inf))
    print(np.any(target_test == np.nan))
    print(np.any(target_test == np.inf))

    hist, xbins, ybins, stuff = plt.hist2d(target_test,prediction_test, bins = [np.linspace(-5.0,5.0,31), np.linspace(-5.0,5.0,31)], weights=weights_test, cmap=plt.get_cmap('Reds'))
    plt.xlabel(r"target ({})".format(target_str.replace("_", " ")), fontsize=26)
    plt.ylabel(r"Prediction", fontsize=26)
    bar = plt.colorbar()
    bar.set_label("Arb Units",fontsize=26)
    plt.savefig("{}_Predictionvstarget_test.png".format(stamp))
    plt.close()

    x = list(range(1,101))
    plt.plot(x, loss[:100], label='train')
    plt.plot(x, val_loss[:100], label='valid')
    plt.xlabel("epoch",fontsize=26)
    plt.ylabel("loss",fontsize=26)
    if "Nll" in model_file:
        plt.ylabel("NLL", fontsize=26)

    #plt.yscale('log')
    plt.xscale('log')
    plt.ylim(2.2, 4.0)
    plt.legend(loc='best',fontsize=26)
    plt.savefig("{}_loss.png".format(stamp))
    plt.close()

    tp = ROOT.TProfile("Linear Regression Plot", "Linear Regression Plot", 100.0, -5.0, 5.0)
    to_fill = np.zeros((len(target_test), 2))
    to_fill[:,0] = target_test
    to_fill[:,1] = prediction_test
    c1 = ROOT.TCanvas("TestCanvas", "TestCanvas")
    tp.Draw()
    c1.Print("{}_ProfilePredictionvstarget_test.png".format(stamp))
    c1.Close()

    utils.print_model_params("{}.pkl".format(stamp), skip=["std_scales", "mean_scales", "history"], model_file = "{}.h5".format(stamp))

