import unittest
import utils
import os
import numpy as np
import data_retriever
from post_processing import get_model_metadata

def assert_equal_vectors(v1, v2):
     return not np.any( np.abs(v2  - v1) > 1e-7)
    

class TestDataRetriever(unittest.TestCase):

    def test_retrieval(self):
        retriever = utils.get_v24_retriever()
        root_file = "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.root"
        df = retriever.get_dataframe([root_file], variables=["m4l_constrained", "mass_point"], campaigns=["mc16a"], systematic="Nominal")
        self.assertTrue(len(df) != 0)
        self.assertTrue("m4l_unconstrained" not in df.columns)
        self.assertTrue("lepton_eta_1" not in df.columns)
        self.assertTrue(not np.any(df["mass_point"].values > 0))

    def test_retrieve_all(self):
        retriever = utils.get_v24_retriever()
        root_file = "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.root"
        df = retriever.get_dataframe([root_file], variables=[], campaigns=["mc16a"], systematic="Nominal")
        self.assertTrue(len(df) != 0)
        self.assertTrue("m4l_constrained" in df.columns)
        self.assertTrue("lepton_eta_1" in df.columns)
        self.assertTrue("lepton_pt_4" in df.columns)
        self.assertTrue(not np.any(df["mass_point"].values > 0))

        root_file = "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root"
        df = retriever.get_dataframe([root_file], variables=[], campaigns=["mc16a"], systematic="Nominal")
        self.assertTrue("lepton_eta_truth_born_1" in df.columns)

    def test_mass_point(self):
        retriever = utils.get_v24_retriever()
        root_file = "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root"
        df = retriever.get_dataframe([root_file], variables=["m4l_constrained", "mass_point"], campaigns=["mc16a"], systematic="Nominal")
        self.assertFalse(np.any(df["mass_point"].values != 125.0))

        root_file = "mc16_13TeV.345576.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH123_ZZ4l.root"
        df = retriever.get_dataframe([root_file], variables=["m4l_constrained", "mass_point"], campaigns=["mc16a"], systematic="Nominal")
        self.assertFalse(np.any(df["mass_point"].values != 123.0))

        root_file = "mc16_13TeV.345577.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH124_ZZ4l.root"
        df = retriever.get_dataframe([root_file], variables=["m4l_constrained", "mass_point"], campaigns=["mc16a"], systematic="Nominal")
        self.assertFalse(np.any(df["mass_point"].values != 124.0))

    def test_yield(self):
        retriever = utils.get_v24_retriever()
        root_file = "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root"
        selection = utils.get_full_selection("4mu", "Incl", "m4l_constrained")
        e_yield = retriever.get_yield([root_file])
        df = retriever.get_dataframe([root_file])
        self.assertTrue(e_yield == np.sum(df["weight"].values))

    def test_campaign_retrieval(self):
        retriever = utils.get_v24_retriever()
        root_file = "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root"
        df_all = retriever.get_dataframe([root_file], variables=["m4l_constrained", "mass_point"], campaigns=["mc16a", "mc16d"], systematic="Nominal")
        df_mc16a = retriever.get_dataframe([root_file], variables=["m4l_constrained", "mass_point"], campaigns=["mc16a"], systematic="Nominal")
        df_mc16d = retriever.get_dataframe([root_file], variables=["m4l_constrained", "mass_point"], campaigns=["mc16d"], systematic="Nominal")

        self.assertEqual(len(df_all),len(df_mc16a) + len(df_mc16d))
        self.assertTrue(assert_equal_vectors(df_all["m4l_constrained"].values[:len(df_mc16a)], df_mc16a["m4l_constrained"].values))
        self.assertTrue(assert_equal_vectors(df_all["m4l_constrained"].values[len(df_mc16a):], df_mc16d["m4l_constrained"].values))

    def test_vectorized_branches(self):
        retriever = utils.get_v24_retriever()
        root_file = "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root"
        df_vectors = retriever.get_dataframe([root_file], variables=["m4l_constrained", "lepton_eta"], campaigns=["mc16d"], systematic="Nominal")
        df_no_vectors = retriever.get_dataframe([root_file], variables=["m4l_constrained"], campaigns=["mc16d"], systematic="Nominal")

        self.assertTrue(assert_equal_vectors(df_vectors["m4l_constrained"].values, df_no_vectors["m4l_constrained"].values))
        for i in range(1, 5): self.assertTrue("lepton_eta_{}".format(i) in df_vectors.columns)

    def test_variables_from_selection(self):
        selection = "(a + b)/(a ** c + d) == (e + f) - g + 125.0 - 124.0 + m4l_constrained"
        variables = data_retriever.reduce_string_to_variables(selection)
        self.assertTrue("a" in variables)
        self.assertTrue("b" in variables)
        self.assertTrue("c" in variables)
        self.assertTrue("d" in variables)
        self.assertTrue("e" in variables)
        self.assertTrue("m4l_constrained" in variables)
        self.assertTrue("g" in variables)
        self.assertFalse("." in variables)
        for i in range(0, 10): self.assertFalse(str(i) in variables)
        self.assertFalse("+" in variables)
        self.assertFalse(" " in variables)
        self.assertFalse("**" in variables)
        self.assertFalse("*" in variables)
        self.assertFalse("=" in variables)
        self.assertFalse(")" in variables)
        self.assertFalse("(" in variables)
        self.assertFalse("" in variables)
        self.assertFalse("m" in variables)
        self.assertFalse("l_constrained" in variables)

    def test_selections(self):
        retriever = utils.get_v24_retriever()
        root_file = "mc16_13TeV.346645.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO_shw.root"
        df = retriever.get_dataframe([root_file], variables=["m4l_constrained"], campaigns=["mc16d"], systematic="Nominal", selection = "(m4l_constrained > 120.0) and (m4l_constrained < 130.0)")
        self.assertFalse(np.any(df["m4l_constrained"].values < 120.0))
        self.assertFalse(np.any(df["m4l_constrained"].values > 130.0))

    def test_selection_variable_branches_added(self):
        retriever = utils.get_v24_retriever()
        root_file = "mc16_13TeV.346228.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4lep_notau.root"
        df = retriever.get_dataframe([root_file], variables=["lepton_eta"], campaigns=["mc16d"], systematic="Nominal", selection = "(m4l_constrained > 120.0) and (m4l_constrained < 130.0)")
        self.assertTrue("m4l_constrained" in df.columns)

    def test_channel_number(self):
        chan = data_retriever.get_channel("mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root")
        self.assertEqual(chan, 345060)
        chan = data_retriever.get_channel("mc16_13TeV.346645.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO_shw.root")
        self.assertEqual(chan, 346645)

    def test_channel_number_in_frame(self):
        retriever = utils.get_v24_retriever()
        root_file_346228 = "mc16_13TeV.346228.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4lep_notau.root"
        root_file_345060 = "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root"
        root_files = [root_file_346228, root_file_345060]
        selection = "(m4l_constrained > 120.0) and (m4l_constrained < 130.0)"
        df = retriever.get_dataframe(root_files, variables=["lepton_eta"], campaigns=["mc16d"], systematic="Nominal", selection = selection)
        df_346228 = retriever.get_dataframe([root_file_346228], variables=["lepton_eta"], campaigns=["mc16d"], systematic="Nominal", selection = selection)
        df_345060 = retriever.get_dataframe([root_file_345060], variables=["lepton_eta"], campaigns=["mc16d"], systematic="Nominal", selection = selection)

        self.assertTrue("channel" in df_346228.columns)
        self.assertTrue("channel" in df_345060.columns)
        self.assertTrue("channel" in df.columns)
        print(df)
        print(df_345060)
        print(len(df))
        print(len(df_345060))
        self.assertFalse(np.any(df_345060["channel"].values != 345060))
        self.assertFalse(np.any(df_346228["channel"].values != 346228))
        for i in range(1, 5):
             self.assertTrue(assert_equal_vectors(df.query("channel == 345060")["lepton_eta_{}".format(i)].values, df_345060["lepton_eta_{}".format(i)].values))
             self.assertTrue(assert_equal_vectors(df.query("channel == 346228")["lepton_eta_{}".format(i)].values, df_346228["lepton_eta_{}".format(i)].values))

    def test_listing_of_files(self):
       retriever = utils.get_v24_retriever()
       wildcards = "_ggH125_"
       files = retriever.get_root_files(wildcards)
       self.assertTrue(len(files) == 1)
       self.assertEqual(files[0],"mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root")
       wildcards = "_ggH125_,_ggH124_,_ggH123_,_ggH127_"
       wildcards = wildcards.split(",")
       files = retriever.get_root_files(wildcards)
       self.assertTrue(len(files) == 4)
       
    def test_type2_systematics(self):
       retriever = utils.get_v24_retriever()
       wildcards = "_ggH125_"
       files = retriever.get_root_files( wildcards)
       t2_systematics = retriever.get_list_of_systematics_type2()
       self.assertTrue(len(t2_systematics) > 0.5)
       example_syst = t2_systematics[0]
       selection = ""
       df = retriever.get_dataframe(files, variables=["lepton_eta"], campaigns=["mc16e"], systematic=example_syst, selection = selection)
       self.assertTrue(len(df) > 0)

    def test_type1_systematics(self):
       retriever = utils.get_v24_retriever()
       wildcards = "_ggH125_"
       files = retriever.get_root_files(wildcards)
       t1_systematics = retriever.get_list_of_systematics_type1(files[0])
       self.assertTrue(len(t1_systematics) > 0.5)
       example_syst = t1_systematics[0]
       selection = ""
       df_syst = retriever.get_dataframe(files, variables=["lepton_eta"], campaigns=["mc16e"], systematic=example_syst, selection = selection)
       self.assertTrue(len(df_syst) > 0)

    def test_variable_reduction(self):
       retriever = utils.get_v24_retriever()
       root_file = "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root"
       root_files = [retriever._get_full_path(root_file, systematic="Nominal", campaign = "mc16a")]
      
       reduced_variables = ["lepton_eta_1", "lepton_phi_2", "m4l_constrained", "lepton_pt_4", "lepton_pt_truth_born_4", "lepton_pt_truth_born_3", "lepton_pt_1"]
       vectorized_variables = retriever.reduce_to_vectorized_variables(reduced_variables, root_files)
       self.assertTrue("lepton_eta" in vectorized_variables)
       self.assertTrue("lepton_phi" in vectorized_variables)
       self.assertTrue("m4l_constrained" in vectorized_variables)
       self.assertTrue("lepton_pt" in vectorized_variables)
       self.assertEqual(len(vectorized_variables), 5)

   # def test_score_additions(self):
   #    model_base = "2019_12_13_14_37_ggH125_NllModel_Deeper_Tanh_FloatSigma_{}_reco_eta"
   #    base_directory = utils.get_model_save_dir()
   #    models = [os.path.join(base_directory,model_base.format(f)) for f in ["4mu", "2e2mu", "4e", "2mu2e"]]
   #    model_retrievers = [lambda model = m, contains = ["_popped"], skip = ["_truth_to_reco"]: get_model_metadata(model, contains = contains, skip=skip) for m in models]
   #    retriever = utils.get_v24_retriever(model_retrievers)
   #    root_file = "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root"
   #    df= retriever.get_dataframe([root_file], variables=["m4l_constrained", "mass_point"], campaigns=["mc16e"], systematic="Nominal", selection = "m4l_constrained < 160.0")
   #    self.assertTrue("sigma_prediction_4mu" in df.columns)
   #    self.assertTrue("sigma_prediction_4e" in df.columns)
   #    self.assertTrue("sigma_prediction_2e2mu" in df.columns)
   #    self.assertTrue("sigma_prediction_2mu2e" in df.columns)

    def test_file_without_norm_syst(self):
       retriever = utils.get_v24_retriever()
       files = ["mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.root"]
       data = retriever.get_dataframe(files, variables=["lepton_eta", "m4l_constrained"], selection=utils.get_full_selection("4mu", "BDT1", "m4l_constrained"))
       self.assertTrue(len(data) == len(data.query(utils.get_full_selection("4mu", "BDT1", "m4l_constrained"))))
       

if __name__ == '__main__':
    unittest.main()
    #test = TestDataRetriever()
    #test.test_yield()
    #test.test_channel_number_in_frame()
    #test.test_file_without_norm_syst()
    #test.test_variable_reduction()
    #test.test_score_additions()
    #test.test_retrieve_all()
    #test.test_selections()
    #test.test_mass_point()
