import ROOT
import numpy as np
import nll_layer
import unittest
import tensorflow as tf

def feed_parameters_to_dcb(params):
     roodcb = ROOT.RooTwoSidedCBShape("roodcb", "roodcb", params["m4lmass"], params["mean"], params["sigma"], params["alpha_low"], params["n_low"], params["alpha_hi"], params["n_hi"])
     return roodcb

def feed_tf_constants_to_tf_log_dcb(params):
     return nll_layer.tf_log_dcb(params["m4lmass"], params["alpha_hi"], params["n_hi"], params["alpha_low"], params["n_low"], params["mean"], params["sigma"], params["range_low"], params["range_hi"])

def generate_scan_parameters(param_to_scan = "mean", scan_range = [110.0, 150.0], nsteps = 100, fit_range = (115.0, 140.0), mass_val = 123.0):
      scan_vals = np.linspace(scan_range[0], scan_range[1], nsteps)
      params = ["mean", "sigma", "alpha_hi", "alpha_low", "n_hi", "n_low"]
      default_vals = [125.0, 2.0, 1.3, 1.5, 3.0, 5.0]
      dcb_params = {}
      for param, default_val in zip(params, default_vals):
           if param == param_to_scan:
               dcb_params[param] = ROOT.RooRealVar(param, param, (scan_range[0] + scan_range[1])/2.0, scan_range[0] - 10.0, scan_range[1] + 10.0)
           else:
               dcb_params[param] = ROOT.RooRealVar(param, param, default_val, default_val - 10.0, default_val + 10.0)
      dcb_params["m4lmass"] = ROOT.RooRealVar("m4lmass", "m4lmass", mass_val, fit_range[0], fit_range[1])

      #create the tensorflow parameters
      tf_constants = {}
      for param in dcb_params:
           if param == param_to_scan:
               tf_constants[param] = tf.constant(scan_vals)
           else:
               tf_constants[param] = tf.constant(np.ones(len(scan_vals)) * dcb_params[param].getVal())
      tf_constants["range_low"] = tf.constant(np.ones(len(scan_vals)) * fit_range[0])
      tf_constants["range_hi"] = tf.constant(np.ones(len(scan_vals)) * fit_range[1])

      return scan_vals, dcb_params, tf_constants

class TestNLLLayer(unittest.TestCase):

    def test_tf_dcb_values(self):
        print("Testing tensorflow implementation of RooTwoSidedCBShape by scanning values")
        variables = ["mean", "sigma", "alpha_hi", "alpha_low", "n_low", "n_hi"]
        ranges = [ (110.0, 150.0), (0.1, 5.0), (0.1, 5.0), (0.1, 5.0), (1.5, 5.0), (1.5, 5.0)]
        for scan_var, scan_range in zip(variables, ranges):
             print("Scanning {}".format(scan_var))
             scan_vals, dcb_params, tf_constants = generate_scan_parameters(scan_var, scan_range = scan_range)
             dcb = feed_parameters_to_dcb(dcb_params)
             dcb_vals = []
             for val in scan_vals:
                  dcb_params[scan_var].setVal(val)
                  log_dcb = np.log(dcb.getVal(ROOT.RooArgSet(dcb_params["m4lmass"])))
                  dcb_vals.append(log_dcb)
             tf_dcb = feed_tf_constants_to_tf_log_dcb(tf_constants)
             with tf.Session() as sess:
                 tf_dcb_vals = tf_dcb.eval()
             for dcb_val, tf_dcb_val in zip(dcb_vals, tf_dcb_vals):
                 self.assertAlmostEqual(dcb_val, tf_dcb_val)

    def test_tf_dcb_gradients(self):
        print("Testing tensorflow implementation of RooTwoSidedCBShape by making sure that gradients are defined")
        variables = ["mean", "sigma", "alpha_hi", "alpha_low", "n_low", "n_hi"]
        ranges = [ (110.0, 150.0), (0.1, 5.0), (0.1, 5.0), (0.1, 5.0), (1.5, 5.0), (1.5, 5.0)]
        for scan_var, scan_range in zip(variables, ranges):
             print("Scanning {}".format(scan_var))
             scan_vals, dcb_params, tf_constants = generate_scan_parameters(scan_var, scan_range = scan_range)
             tf_dcb = feed_tf_constants_to_tf_log_dcb(tf_constants)
             with tf.Session() as sess:
                grad_alpha_hi = tf.gradients(tf_dcb,tf_constants["alpha_hi"])
                grad_alpha_low = tf.gradients(tf_dcb,tf_constants["alpha_low"])
                grad_n_hi = tf.gradients(tf_dcb,tf_constants["n_hi"])
                grad_n_low = tf.gradients(tf_dcb,tf_constants["n_low"])
                grad_mean = tf.gradients(tf_dcb,tf_constants["mean"])
                grad_sigma = tf.gradients(tf_dcb,tf_constants["sigma"])
                for el in grad_n_hi: self.assertFalse(np.any(np.isnan(el.eval())))
                for el in grad_n_low: self.assertFalse(np.any(np.isnan(el.eval())))
                for el in grad_alpha_hi: self.assertFalse(np.any(np.isnan(el.eval())))
                for el in grad_alpha_low: self.assertFalse(np.any(np.isnan(el.eval())))
                for el in grad_mean: self.assertFalse(np.any(np.isnan(el.eval())))
                for el in grad_sigma: self.assertFalse(np.any(np.isnan(el.eval())))
              
                for el in grad_n_hi: self.assertFalse(np.any(np.isinf(el.eval())))
                for el in grad_n_low: self.assertFalse(np.any(np.isinf(el.eval())))
                for el in grad_alpha_hi: self.assertFalse(np.any(np.isinf(el.eval())))
                for el in grad_alpha_low: self.assertFalse(np.any(np.isinf(el.eval())))
                for el in grad_mean: self.assertFalse(np.any(np.isinf(el.eval())))
                for el in grad_sigma: self.assertFalse(np.any(np.isinf(el.eval())))
      

if __name__ == '__main__':
    unittest.main()
