import ROOT as r
import utils
import root_numpy as rnp
import numpy as np
import workspace_manager
from data_retriever import reduce_string_to_variables
import os

if __name__ == "__main__":
	r.RooMsgService.instance().setGlobalKillBelow(r.RooFit.ERROR)
	r.RooAbsReal.defaultIntegratorConfig().method1D().setLabel("RooAdaptiveGaussKronrodIntegrator1D")  ## Better numerical integrator

def get_dsid_from_dataset(dataset):
    items = dataset.split(".")
    dsid = int(items[1])
    return dsid

def get_name_from_datasets(datasets):
    dsids = []
    for dataset in datasets:
        dsids.append(get_dsid(dataset))
    return "_".join(dsids)

def get_bkg_model(retriever, mass_var, datasets, decay_channel_name, bdt_cat_name, syst, rhos=[2.2], selection = "", name="", extra_vars = []):
    data = workspace_manager.get_dataset(retriever, datasets, [mass_var], selection = selection, syst=syst)
    pdfs = {}
    pdfs[False] = {}
    for rho in rhos:
        rho_name = str(rho).replace(".", "_")
        descr = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, mass_var, syst=syst)
        full_name = "_".join([name, descr, rho_name])
        print("Retrieveing Data")
        print("Making PDF")
        pdf = r.RooKeysPdf(full_name, full_name, workspace_manager.get_variable(mass_var), data, r.RooKeysPdf.MirrorBoth, rho)
        print("Finished Making PDF")
        pdfs[False][rho] = pdf
    return pdfs, data


def get_full_chebychev_name(mass_observable, channel, category, syst, order, bkg_name, extended):
    descr = workspace_manager.join_cat_varname(channel, category, mass_observable, syst=syst)
    if extended: descr += "_Ext"
    else: descr += "_Shape"
    return "_".join(["RooChe", str(order), bkg_name,  descr])

def get_full_bernstein_name(mass_observable, channel, category, syst, order, bkg_name, extended):
    descr = workspace_manager.join_cat_varname(channel, category, mass_observable, syst=syst)
    if extended: descr += "_Ext"
    else: descr += "_Shape"
    return "_".join(["RooBern", str(order), bkg_name,  descr])

def get_chebychev_model_params(full_name, order,extended):
    params = {}
    params["CheParams"] = []
    for i in range(0, order):
        varname = full_name + "_param_{}".format(i)
        if not workspace_manager.check_for_variable(varname): params["CheParams"].append(workspace_manager.get_variable(varname, 0.0, -100.0, +100.0))
        else: params["CheParams"].append(workspace_manager.get_variable(varname))
    if extended:
        extended_varname = "{}_Norm".format(full_name)
        if not workspace_manager.check_for_variable(extended_varname): params["Norm"] = [workspace_manager.get_variable(extended_varname, 50.0, 0.0, 10000.0)]
        else: params["Norm"] = [workspace_manager.get_variable(extended_varname)]
    return params

def get_bernstein_model_params(full_name, order, extended):
    params = {}
    params["BernParams"] = []
    for i in range(0, order):
        varname = full_name + "_param_{}".format(i)
        if not workspace_manager.check_for_variable(varname): params["BernParams"].append(workspace_manager.get_variable(varname, 50.0, 0.0, +300.0))
        else: params["BernParams"].append(workspace_manager.get_variable(varname))
    if extended:
        extended_varname = "{}_Norm".format(full_name)
        if not workspace_manager.check_for_variable(extended_varname): params["Norm"] = [workspace_manager.get_variable(extended_varname, 50.0, 0.0, 10000.0)]
        else: params["Norm"] = [workspace_manager.get_variable(extended_varname)]
    return params

def get_chebychev_model_from_params(full_name, params, mass_var=""):
    vars = params["CheParams"]
    roo_mass_var = workspace_manager.get_variable(mass_var)
    arglist = workspace_manager.list_to_arglist(vars)
    pdf = r.RooChebychev(full_name, full_name, roo_mass_var, arglist)
    pdf_ext = None
    if "Norm" in params:
        norm = params["Norm"][0]
        workspace_manager.force_roo_variable(pdf) #keep in alive in memory. F**K ROOT.
        pdf_ext = r.RooExtendPdf(pdf.GetName() + "_Extended", pdf.GetName()+ "_Extended", pdf, norm)
    return pdf, pdf_ext

def get_bernstein_model_from_params(full_name, params, mass_var=""):
    vars = params["BernParams"]
    roo_mass_var = workspace_manager.get_variable(mass_var)
    arglist = workspace_manager.list_to_arglist(vars)
    pdf = r.RooBernstein(full_name, full_name, roo_mass_var, arglist)
    pdf_ext = None
    if "Norm" in params:
        norm = params["Norm"][0]
        workspace_manager.force_roo_variable(pdf) #keep in alive in memory. F**K ROOT.
        pdf_ext = r.RooExtendPdf(pdf.GetName() + "_Extended", pdf.GetName()+ "_Extended", pdf, norm)
    return pdf, pdf_ext

def get_chebychev_model(retriever, mass_var, datasets, decay_channel_name, bdt_cat_name, syst, orders=[5], selection="", name="", extended_options=[], extra_vars = []):
    pdfs = {}
    data = workspace_manager.get_dataset(retriever, datasets, [mass_var], selection = selection, syst=syst)
    for extended in extended_options:
        pdfs[extended] = {}
        for order in orders:
            full_name = get_full_chebychev_name(mass_var, decay_channel_name, bdt_cat_name, syst, order, name, extended)
            params = get_chebychev_model_params(full_name, order, extended)
            pdf, pdf_ext = get_chebychev_model_from_params(full_name, params, mass_var=mass_var)
            if extended: pdf_ext.fitTo(data, r.RooFit.SumW2Error(True)) #fit the model to the data
            else: pdf.fitTo(data, r.RooFit.SumW2Error(True)) #fit the model to the data
            if extended: pdfs[extended][order] = pdf_ext
            else: pdfs[extended][order] = pdf
    return pdfs, data

def get_bernstein_model(retriever, mass_var, datasets, decay_channel_name, bdt_cat_name, syst, orders=[5], selection="", name="", extended_options=[], extra_vars = []):
    pdfs = {}
    data = workspace_manager.get_dataset(retriever, datasets, [mass_var], selection = selection, syst=syst)
    for extended in extended_options:
        pdfs[extended] = {}
        for order in orders:
            full_name = get_full_bernstein_name(mass_var, decay_channel_name, bdt_cat_name, syst, order, name, extended)
            params = get_bernstein_model_params(full_name, order, extended)
            pdf, pdf_ext = get_bernstein_model_from_params(full_name, params, mass_var=mass_var)
            if extended: pdf_ext.fitTo(data, r.RooFit.SumW2Error(True)) #fit the model to the data
            else: pdf.fitTo(data, r.RooFit.SumW2Error(True)) #fit the model to the data
            if extended: pdfs[extended][order] = pdf_ext
            else: pdfs[extended][order] = pdf
    return pdfs, data

def run():
    import os
    import argparse
    parser = argparse.ArgumentParser(description='parse them args.')
    parser.add_argument('--syst', '-s', dest="syst", type=str, required=True, help='')
    parser.add_argument('--channel', '-c', dest="channel", type=str, required=True, help='')
    parser.add_argument('--mass_var', '-mv', dest="mass_var", type=str, required=True, help='')
    parser.add_argument('--category', '-cat', dest="category", type=str, required=True, help='')
    parser.add_argument('--model_type', '-mt', dest="model_type", type=str, required=True, help='')
    parser.add_argument('--categorization', '-cator', dest="categorization", type=str, required=False, default="NOMINAL", help='')
    args = parser.parse_args()
    if args.categorization != "NOMINAL": utils.BINNING = args.categorization

    bkg_sources, bkg_source_names = utils.get_all_bkgs()
    assert len(bkg_sources) == len(bkg_source_names)

    if args.model_type == "Keys": model_pars = np.linspace(0.6, 1.6, 6) #scan a set of rhos to try for each model
    elif args.model_type == "Che": model_pars = list(range(1, 8))
    elif args.model_type == "Bern": model_pars = list(range(1, 10))
    #elif args.model_type == "Che": model_pars = list(range(2, 3))
    else: raise ValueError("This was not an option for the model parameters")

    retriever = utils.get_v24_retriever()
    extra_vars = []
    for event_type in args.channel.split(","):
        for category in args.category.split(","):
            extra_vars = list(set(extra_vars + reduce_string_to_variables(utils.get_full_selection(event_type, category, args.mass_var))))

    #Close the output file
    outfilename = "{}_{}_{}.root".format(args.mass_var, args.model_type, args.syst)
    #outfilename = "{}_{}.root".format(args.model_type, args.syst)
    folder = utils.get_bkg_model_directory()
    folder = os.path.join(folder, args.syst)
    if not os.path.exists(folder): os.makedirs(folder)
    outfile = os.path.join(folder, outfilename)
    routfile = r.TFile(outfile, "RECREATE")
    routfile.Close()

    utils.FITTING_RANGE = "WIDE"

    for event_type in args.channel.split(","):
        for category in args.category.split(","):
            for bkg_set, name in zip(bkg_sources, bkg_source_names):
                workspace_manager.clear_cache()
                print("\n"*15)
                print("Processing {} {}".format(bkg_set, name))
                files = retriever.get_root_files(wildcards = bkg_set)
                if not retriever.has_syst(args.syst, files):
                    print("Skipping {} because {} doesn't apply as a systematic".format(name, args.syst))
                    continue
                for f in files:
                    if retriever.has_syst(args.syst, f): print("{} Found in {}".format(args.syst, f))

                e_yield, yield_stat = retriever.get_yield(files, selection = utils.get_full_selection(event_type, category, args.mass_var), systematic=args.syst)
                yield_name = "CountYield_{}_{}_{}".format(name, category, event_type)
                roo_yield = r.RooRealVar(yield_name, yield_name, e_yield)
                roo_yield.setError(yield_stat)

                if args.model_type == "Keys": extendeds = [False]
                elif args.model_type == "Che" or args.model_type == "Bern": extendeds = [False, True]

                selection = utils.get_full_selection(event_type, category, args.mass_var)
                #yes, the systematic applies. continue making the models
                if args.model_type == "Keys": bkg_models, data = get_bkg_model(retriever, args.mass_var, files, event_type, category, args.syst,rhos=model_pars, name = name, extra_vars = extra_vars, selection=selection)
                elif args.model_type == "Che": bkg_models, data = get_chebychev_model(retriever, args.mass_var, files, event_type, category, args.syst, orders=model_pars, name=name, extended_options=extendeds, extra_vars = extra_vars, selection=selection)
                elif args.model_type == "Bern": bkg_models, data = get_bernstein_model(retriever, args.mass_var, files, event_type, category, args.syst, orders=model_pars, name=name, extended_options=extendeds, extra_vars = extra_vars, selection=selection)

                #data_ws_name = "data_{}_{}_{}_{}_{}_{}".format(args.model_type, args.syst, event_type, args.mass_var, category, name)
                #ws = r.RooWorkspace(data_ws_name, data_ws_name)
                #getattr(ws, "import")(data)
                #ws.Write()

                for extended in bkg_models:
                    for model_par in model_pars:
                        bkg_model = bkg_models[extended][model_par]
                        if extended: extend_str = "Extended"
                        else: extend_str = "Shape"
                        if utils.BINNING == "NOMINAL": outwsname = "bkg_model_{}_{}_{}_{}_{}_{}_{}_{}".format(extend_str, args.model_type, args.syst, event_type, args.mass_var, category, name, str(model_par).replace(".", "_"))
                        else: outwsname = "bkg_model_{}_{}_{}_{}_{}_{}_{}_{}_{}".format(extend_str, args.model_type, args.syst, event_type, args.mass_var, category, name, str(model_par).replace(".", "_"), utils.BINNING)
                      
                        routfile = r.TFile(outfile, "UPDATE")
                        w = r.RooWorkspace(outwsname, outwsname)
                        getattr(w, "import")(bkg_model)
                        getattr(w, "import")(roo_yield)
                        w.Write()
                        routfile.Close()
    print("FINISHED!")

if __name__ == "__main__":
    run()
    print("FINISHED!")
