import ROOT as r
import workspace_manager
import root_numpy as rnp
import numpy as np
import utils

def load_signal_model(name=None, mass=None, mean=None, sigma=None, alpha_low=None, n_low=None, alpha_hi=None, n_hi=None):
    print("name: {}".format(name))
    print("mean: {}".format(mean))
    print("sigma: {}".format(sigma))
    print("alpha_low: {}".format(alpha_low))
    print("alpha_hi: {}".format(alpha_hi))
    print("n_hi: {}".format(n_hi))
    print("n_low: {}".format(n_low))
    return r.RooTwoSidedCBShape(name, name, mass, mean, sigma, alpha_low, n_low, alpha_hi, n_hi)

def get_signal_model(mass_var, decay_channel_name, bdt_cat_name, syst):
    varbounds = {}
    varbounds["mass"] = workspace_manager.get_global_fit_range(mass_var)
    varbounds["mean"] = (125.0 , 100.0, 150.0)
    varbounds["sigma"] = (1.0, 0.01, 3.0)
    varbounds["alpha_low"] = (1.5, 0.01, 5.0)
    varbounds["alpha_hi"] = (1.5, 0.01, 5.0)
    varbounds["n_low"] = (3.0, 1.01, 15.0)
    varbounds["n_hi"] = (3.0, 1.01, 15.0)

    params = {}
    for varname in varbounds:
        full_varname = join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst)
        if not workspace_manager.check_for_variable(params[varname]): 
           workspace_manager.add_variable(r.RooRealVar(full_varname, full_varname, varbounds[varname][0], varbounds[varname][1], varbounds[varname][2]))
        parames[varname] = workspace_manager.get_variable(full_varname)

    return load_signal_model(**params)

def get_raw_params(mass_var, decay_channel_name, bdt_cat_name, syst):
    raw_params = {}
    mass_point = workspace_manager.get_variable("mass_point")
    varname = "mean_slope"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): mean_slope_var = workspace_manager.get_variable(varname, 1.0, -10.0, +10.0)
    else: mean_slope_var = workspace_manager.get_variable(varname)
    raw_params[varname] = workspace_manager.get_variable(varname)

    varname = "mean_intercept"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): raw_params[varname] = workspace_manager.get_variable(varname, 125.0, 100.0, +150.0)
    else: raw_params[varname] = workspace_manager.get_variable(varname)

    varname = "sigma_slope"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): raw_params[varname] = workspace_manager.get_variable(varname, 0.0, -10.0, +10.0)
    else: raw_params[varname] = workspace_manager.get_variable(varname)

    varname = "sigma_intercept"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): raw_params[varname] = workspace_manager.get_variable(varname, 1.5, 0.0, +10.0)
    else: raw_params[varname] = workspace_manager.get_variable(varname)

    varname = "alpha_hi_slope"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): raw_params[varname] = workspace_manager.get_variable(varname, 0.0, -10.0, +10.0)
    else: raw_params[varname] = workspace_manager.get_variable(varname)

    varname = "alpha_hi_intercept"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): raw_params[varname] = workspace_manager.get_variable(varname, 1.5, 0.0, +10.0)
    else: raw_params[varname] = workspace_manager.get_variable(varname)

    varname = "alpha_low_slope"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): raw_params[varname] = workspace_manager.get_variable(varname, 0.0, -10.0, +10.0)
    else: raw_params[varname] = workspace_manager.get_variable(varname)

    varname = "alpha_low_intercept"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): raw_params[varname] = workspace_manager.get_variable(varname, 1.5, 0.0, +10.0)
    else: raw_params[varname] = workspace_manager.get_variable(varname)

    varname = "n_hi"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): raw_params[varname] =  workspace_manager.get_variable(varname,default = 4.0, range_low = 1.0, range_high = 20.0)
    else: raw_params[varname] =  workspace_manager.get_variable(varname)

    varname = "n_low"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    if not workspace_manager.check_for_variable(varname): raw_params[varname] = workspace_manager.get_variable(varname,default = 4.0, range_low = 1.0, range_high = 20.0)
    else: raw_params[varname] = workspace_manager.get_variable(varname)

    raw_params["mass"] = workspace_manager.get_variable(mass_var)
    raw_params["name"] = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, "signal_model", syst=syst)

    return raw_params

def get_params(mass_var, decay_channel_name, bdt_cat_name, syst):
    params = {}

    raw_params = get_raw_params(mass_var, decay_channel_name, bdt_cat_name, syst)
    mass_point = workspace_manager.get_variable("mass_point")

    mean_varname = "mean"
    mean_varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, mean_varname, syst=syst)
    if not workspace_manager.check_for_variable(mean_varname):
        print("mean not found... loading new mean")

        varname = "mean_slope"
        varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
        mean_slope_var = raw_params[varname]

        varname = "mean_intercept"
        varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
        mean_intercept_var = raw_params[varname]

        mean = r.RooFormulaVar(mean_varname, mean_varname, "{slope} * ({mass_point} - 125.0) + {intercept}".format(slope=mean_slope_var.GetName(), mass_point = mass_point.GetName(), intercept=mean_intercept_var.GetName()), r.RooArgList(mean_slope_var, mass_point, mean_intercept_var))
        workspace_manager.add_roo_variable(mean)
    params["mean"] = workspace_manager.get_variable(mean_varname)

    sigma_varname = "sigma"
    sigma_varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, sigma_varname, syst=syst)
    if not workspace_manager.check_for_variable(sigma_varname):
        print("sigma not found... loading new mean")

        varname = "sigma_slope"
        varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
        sigma_slope_var = raw_params[varname]

        varname = "sigma_intercept"
        varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
        sigma_intercept_var = raw_params[varname]

        sigma = r.RooFormulaVar(sigma_varname, sigma_varname, "{slope} * ({mass_point} - 125.0) + {intercept}".format(slope=sigma_slope_var.GetName(), mass_point = mass_point.GetName(), intercept=sigma_intercept_var.GetName()), r.RooArgList(sigma_slope_var, sigma_intercept_var, mass_point))
        workspace_manager.add_roo_variable(sigma)
    params["sigma"] = workspace_manager.get_variable(sigma_varname)

    alpha_hi_varname = "alpha_hi"
    alpha_hi_varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, alpha_hi_varname, syst=syst)
    if not workspace_manager.check_for_variable(alpha_hi_varname):
        print("alpha_hi not found... loading new mean")

        varname = "alpha_hi_slope"
        varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
        alpha_hi_slope_var = raw_params[varname]

        varname = "alpha_hi_intercept"
        varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
        alpha_hi_intercept_var = raw_params[varname]

        alpha_hi = r.RooFormulaVar(alpha_hi_varname, alpha_hi_varname, "{slope} * ({mass_point} - 125.0) + {intercept}".format(slope=alpha_hi_slope_var.GetName(), mass_point = mass_point.GetName(), intercept=alpha_hi_intercept_var.GetName()), r.RooArgList(alpha_hi_slope_var, alpha_hi_intercept_var, mass_point))
        workspace_manager.add_roo_variable(alpha_hi)
    params["alpha_hi"] = workspace_manager.get_variable(alpha_hi_varname)

    alpha_low_varname = "alpha_low"
    alpha_low_varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, alpha_low_varname, syst=syst)
    if not workspace_manager.check_for_variable(alpha_low_varname):
        print("alpha_low not found... loading new mean")

        varname = "alpha_low_slope"
        varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
        alpha_low_slope_var = raw_params[varname]

        varname = "alpha_low_intercept"
        varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
        alpha_low_intercept_var = raw_params[varname]

        alpha_low = r.RooFormulaVar(alpha_low_varname, alpha_low_varname, "{slope} * ({mass_point} - 125.0) + {intercept}".format(slope=alpha_low_slope_var.GetName(), mass_point = mass_point.GetName(), intercept=alpha_low_intercept_var.GetName()), r.RooArgList(mass_point, alpha_low_slope_var, alpha_low_intercept_var))
        workspace_manager.add_roo_variable(alpha_low)
    params["alpha_low"] = workspace_manager.get_variable(alpha_low_varname)

    varname = "n_hi"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    params["n_hi"] = raw_params[varname]

    varname = "n_low"
    varname = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, varname, syst=syst)
    params["n_low"] = raw_params[varname]

    params["mass"] = workspace_manager.get_variable(mass_var)
    params["name"] = workspace_manager.join_cat_varname(decay_channel_name, bdt_cat_name, "signal_model", syst=syst)

    return params

def get_linear_signal_model(mass_var, decay_channel_name, bdt_cat_name, syst):
    params = get_params(mass_var, decay_channel_name, bdt_cat_name, syst)
    return load_signal_model(**params)

def run():
    import os
    import argparse
    parser = argparse.ArgumentParser(description='parse them args.')
    parser.add_argument('--syst', '-s', dest="syst", type=str, required=True, help='')
    parser.add_argument('--channel', '-c', dest="channel", type=str, required=True, help='')
    parser.add_argument('--mass_var', '-mv', dest="mass_var", type=str, required=True, help='')
    parser.add_argument('--category', '-cat', dest="category", type=str, required=True, help='')
    args = parser.parse_args()

    retriever = utils.get_v24_retriever()
    files = retriever.get_root_files(["_ggH12"], skip=["_ggH123p5_","_ggH125_"])
    for event_type in args.channel.split(","):
        for category in args.category.split(","):
            selection = utils.get_full_selection(event_type, category, args.mass_var)
            data = workspace_manager.get_dataset(retriever, files, [args.mass_var, "mass_point"],syst=args.syst, selection = selection, normalize_mass_points = True)
            signal_model = get_linear_signal_model(args.mass_var, event_type, category, args.syst)
            signal_model.fitTo(data, r.RooFit.ConditionalObservables(r.RooArgSet(workspace_manager.get_variable("mass_point"))), r.RooFit.PrintLevel(3), r.RooFit.SumW2Error(True))
            extended_var_name = workspace_manager.join_cat_varname(event_type, category, "Nuis_Norm_Signal", syst=args.syst)
            norm = r.RooRealVar(extended_var_name, extended_var_name, 50.0, 0.0, 10000.0)
            signal_model_ext = r.RooExtendPdf(signal_model.GetName() + "_Extended", signal_model.GetName()+ "_Extended", signal_model, norm)
            outfile = "signal_model_{}_{}_{}_{}.root".format(args.syst, event_type, args.mass_var, category)
            folder = utils.get_signal_model_directory()
            import os
            if not os.path.exists(folder): os.makedirs(folder)
            routfile = r.TFile(os.path.join(folder, outfile), "RECREATE")
            ws_name = os.path.split(outfile)[-1].replace(".root","")
            w = r.RooWorkspace(ws_name, ws_name)
            getattr(w, "import")(signal_model_ext)
            routfile.cd()
            w.Write()
            routfile.Close()
    print("FINISHED")

if __name__ == "__main__":
    run()

