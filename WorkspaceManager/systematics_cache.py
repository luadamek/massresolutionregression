CORRELATED_SYST_CACHE = {}
CORRELATED_SYST_CACHE["constraining_pdf"] = {}
CORRELATED_SYST_CACHE["np"] = {}
UNCORRELATED = []

CATEGORIES = ["BDT1", "BDT2", "BDT3", "BDT4", "Incl"]
CHANNELS = ["4mu", "4e", "2e2mu", "2mu2e"]

UNCORRELATED_SYST_CACHE = {}
for cat in CATEGORIES:
    UNCORRELATED_SYST_CACHE[cat] = {}
    for chan in CHANNELS:
        UNCORRELATED_SYST_CACHE[cat][chan]={}
        UNCORRELATED_SYST_CACHE[cat][chan]["constraining_pdf"] = {}
        UNCORRELATED_SYST_CACHE[cat][chan]["np"] = {}

CORRELATIONS = []
SYSTEMATICS = []
SYSTEMATIC_PAIRS = {}
PAIRED_SYSTEMATICS = {}
import workspace_manager
import ROOT as r

def check_if_theory(syst):
    return "HOEW_syst" in syst or "QCD_scale" in syst or "weight_var_H4l" in syst or "weight_var_th" in syst or "HOEW_QCD" in syst # or "WTOT" in syst

def get_syst_variable(syst, correlated=False, channel=None, category=None):
    if not correlated and not channel: raise ValueError("The systematic {} is not correlated accross channels, but you did not specify a channel".format(syst))
    if not correlated and not category: raise ValueError("The systematic {} is not correlated accross categories, but you did not specify a category")
    if correlated: #one np and gaussian constraint accross all categories
        gaussian_name = "gaus_constraint_{}".format(syst)
        if not workspace_manager.check_for_variable("Nuis_{}".format(syst)): np = workspace_manager.get_variable("Nuis_{}".format(syst), 0.0, -5.0, +5.0)
        else: np = workspace_manager.get_variable("Nuis_{}".format(syst))
        gaussian = r.RooGaussian(gaussian_name, gaussian_name, np, r.RooFit.RooConst(0.0), r.RooFit.RooConst(1.0))
    else: #one np and gaussian constraint for each category
        if not workspace_manager.check_for_variable("Nuis_{}_{}_{}".format(syst, channel, category)): np = workspace_manager.get_variable("Nuis_{}_{}_{}".format(syst, channel, category), 0.0, -5.0, 5.0)
        else: np = workspace_manager.get_variable("Nuis_{}_{}_{}".format(syst, channel, category))
        gaussian_name = "gaus_constraint_{}_{}_{}".format(syst, channel, category)
        gaussian = r.RooGaussian(gaussian_name, gaussian_name, np, r.RooFit.RooConst(0.0), r.RooFit.RooConst(1.0))
    return np, gaussian

def get_all_systematic_pairs(systematics, skip_theory=True):
    systematics = systematics[:]
    systematics = sorted(systematics)
    if "Nominal" in systematics: systematics.remove("Nominal")
    
    syst_names = []
    leftovers = []
    for sys in systematics:
       if check_if_theory(sys) and skip_theory: leftovers.append(sys)
       elif sys[-5:] == "__1up" and sys.replace("__1up", "__1down") in systematics: syst_names.append(sys.replace("__1up",""))
       elif sys[-7:] == "__1down" and sys.replace("__1down", "__1up") in systematics: syst_names.append(sys.replace("__1down",""))
       else: leftovers.append(sys)
    raw_systs = syst_names
    assert len(syst_names) == 2 * len(list(set(syst_names)))
    syst_names = sorted(list(set(syst_names)))
    theory_leftovers = []
    for l in leftovers:
        if "weight_var_th" in l or "weight_var_H4l" in l: theory_leftovers.append(l)
        systematics.remove(l)
    assert 2 * len(syst_names) == len(systematics)

    pairs = {}
    all_systematics = []
    for syst_name in syst_names:
         pairs[syst_name] = {}
         for sys in systematics:
             if syst_name in sys and sys == syst_name + "__1up" in sys:
                 pairs[syst_name]["up"] = sys
                 all_systematics.append(sys)
             elif syst_name in sys and sys == syst_name + "__1down" in sys:
                 pairs[syst_name]["down"] = sys
                 all_systematics.append(sys)

    return pairs, all_systematics

def define_all_nps(systematics, correlations):
    global CORRELATIONS 
    global SYSTEMATICS 
    global SYSTEMATIC_PAIRS
    global PAIRED_SYSTEMATICS
    global UNCORRELATED
    CORRELATIONS = correlations
    SYSTEMATICS = systematics
    SYSTEMATIC_PAIRS, _ = get_all_systematic_pairs(SYSTEMATICS, False)
    PAIRED_SYSTEMATICS = list(SYSTEMATIC_PAIRS.keys())
    UNCORRELATED = [s for s in PAIRED_SYSTEMATICS if s not in CORRELATIONS]

    for syst in CORRELATIONS:
        np, gaus = get_syst_variable(syst, True)
        CORRELATED_SYST_CACHE["constraining_pdf"][syst] = gaus
        CORRELATED_SYST_CACHE["np"][syst] = np
    for syst in UNCORRELATED:
        for chan in CHANNELS:
            for cat in CATEGORIES:
                 np, gaus = get_syst_variable(syst, False, chan, cat)
                 UNCORRELATED_SYST_CACHE[cat][chan]["constraining_pdf"][syst] = gaus
                 UNCORRELATED_SYST_CACHE[cat][chan]["np"][syst] = np


def get_constraint_list(systematics, categoires, channels):
    constraint_list = []
    for s in systematics:
        if s in CORRELATIONS:
            constraint_list.append(CORRELATED_SYST_CACHE["constraining_pdf"][s])
        elif s in UNCORRELATED:
            for cat in categoires:
                for ch in channels:
                    constraint_list.append(UNCORRELATED_SYST_CACHE[cat][chan]["constraining_pdf"][s])
        else: raise ValueError("Systematic {} is not present in the cache".format(s))
    return constraint_list

def get_np_list(systematics, categories, channels):
    np_list = []
    for s in systematics:
        if s in CORRELATIONS:
             np_list.append(CORRELATED_SYST_CACHE["np"][s])
        elif s in UNCORRELATED:
            for cat in categories:
                for ch in channels:
                    np_list.append(UNCORRELATED_SYST_CACHE[cat][chan]["np"][s])
    return np_list

def get_nuisance_parameter(syst, category, channel):
    if syst in CORRELATIONS:
        return CORRELATED_SYST_CACHE["np"][syst]
    elif syst in UNCORRELATED:
        return UNCORRELATED_SYST_CACHE[category][channel]["np"][syst]
    else: raise ValueError("Systematic {} is not present in the cache".format(syst))
