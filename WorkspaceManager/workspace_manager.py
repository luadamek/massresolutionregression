import utils
from guppy import hpy
h = hpy()
import root_numpy as rnp
import numpy as np
import ROOT as r
import os
import signal_model as __signal_model__
import bkg_model as __bkg_model__
import systematics_cache

#create an invariant mass variable
keep_ws_alive = []
variable_bank = {}

def clear_cache():
   global keep_ws_alive
   keep_ws_alive = []
   global variable_bank
   variable_bank = {}

def get_global_fit_ranges():
    ranges = {}
    if utils.FITTING_RANGE == "WIDE":
        ranges["m4l_constrained"] = (100.0, 165.0)
        ranges["m4l_unconstrained"] = (100.0, 165.0)

    elif utils.FITTING_RANGE == "NOMINAL":
        ranges["m4l_constrained"] = (105.0, 160.0)
        ranges["m4l_unconstrained"] = (105.0, 160.0)

    for el in ["4mu", "4e", "2e2mu", "2mu2e"]:
        ranges["sigma_prediction_{}".format(el)] = (0.0, 10.0)
        ranges["sigma_corrected_prediction_{}".format(el)] = (0.0, 10.0)
    ranges["weight"] = (-10e10, 10e10)
    ranges["mass_point"] = (120.0, 130.0)
    return ranges

def get_all_observables():
    return ["m4l_constrained", "m4l_unconstrained", "sigma_prediction", "sigma_corrected_prediction"] + ["sigma_prediction_{}".format(el) for el in ["4mu", "4e", "2e2mu", "2mu2e"]] + ["sigma_corrected_prediction_{}".format(el) for el in ["4mu", "4e", "2e2mu", "2mu2e"]]

def get_global_fit_range(varname):
    ranges = get_global_fit_ranges()
    assert varname in ranges
    return ranges[varname]

def get_variable_type(varname):
    if "event_type" in varname:
        return "category"
    if "bdt_cat" in varname:
        return "category"
    if "cat_index" in varname:
        return "category"
    return "observable"

def check_for_variable(varname):
    found = varname in variable_bank
    if found: print("Found variable {}".format(varname))
    return found 

def category_bank(varname):
    if varname == "event_type":
        return {"4mu":0, "4e":1, "2mu2e":2, "2e2mu":3}
    if varname == "bdt_cat":
        return {"BDT1":0, "BDT2":1,"BDT3":2,"BDT4":3}
    if varname == "cat_index":
        dict = {}
        for chan in ["4mu", "4e", "2e2mu", "2mu2e"]:
            for category in ["BDT1", "BDT2", "BDT3", "BDT4"]:
                dict["{}_{}".format(category, chan)] = utils.get_global_category_value(category, chan)
        return dict

def create_category_variable(varname):
    categories = category_bank(varname)
    cat_var = r.RooCategory(varname, varname)
    for cat in categories:
        cat_var.defineType(cat, categories[cat])
    return cat_var

def add_variable(varname, default=None, range_low=None, range_high=None):
    print("Adding {}".format(varname))
    assert varname not in variable_bank
    if (default!=None or range_low!=None or range_high!=None) and varname in get_global_fit_ranges():
        raise ValueError("Variable {} has a default range. Do not try to supply one".format(varname))

    if varname not in get_global_fit_ranges() and get_variable_type(varname) != "category" and (range_low==None or range_high==None):
        raise ValueError("Variable {} needs a range".format(varname))

    if varname in get_global_fit_ranges():
        variable = r.RooRealVar(varname, varname, get_global_fit_range(varname)[0], get_global_fit_range(varname)[1])
    elif get_variable_type(varname) == "category": 
        variable = create_category_variable(varname)
    elif default==None: variable = r.RooRealVar(varname, varname, range_low, range_high)
    else: variable = r.RooRealVar(varname, varname, default, range_low, range_high)
    variable_bank[variable.GetName()] = variable

def add_roo_variable(var):
    assert var.GetName() not in variable_bank
    variable_bank[var.GetName()] = var

def force_roo_variable(var):
    variable_bank[var.GetName()] = var

def get_variable(varname, default=None, range_low=None, range_high=None):
    if check_for_variable(varname) and (default!=None or range_low!=None or range_high!=None):
        raise ValueError("The variable {} already exists. You can't specify it's range again...".format(varname))
    if not check_for_variable(varname):
        add_variable(varname, default=default, range_low=range_low, range_high=range_high)
    print("Retrieving variable {}".format(varname))
    return variable_bank[varname]

def join_cat_varname(decay_channel, catname, varname, syst = "Nominal"):
    to_return = "__".join([decay_channel, catname, varname])
    if syst != "Nominal": to_return = "__".join([to_return, syst])
    return to_return

def normalize_mass_points(df):
    print("NORMALZING MASSPOINTS")
    assert "mass_point" in df.columns
    unique_points = np.unique(df.eval("mass_point").values)
    mass_points = df.eval("mass_point").values
    weights = df.eval("weight").values
    for mp in unique_points:
        mp_weights = weights[mass_points==mp]
        mp_weights/=np.sum(mp_weights)
        weights[mass_points==mp] = mp_weights
    print("There were {} mass points".format(len(unique_points)))
    df["weight"] = weights
    return df

def get_dataset_from_df(df, variables, selection="", normalize_mass_points=False):
    if "mass_point" in variables: assert not np.any(df.eval("mass_point").values < 0.0)
    if "weight" not in variables: variables.append("weight")
    for var in variables:
        assert var in df.columns
    if selection: df = df.query(selection)
    if normalize_mass_points: df = normalize_mass_points(df)
    for_tree = [df.eval(var).values for var in variables]
    roo_vars = []
    for var in variables:
        roo_vars.append(get_variable(var))
    for_tree = np.core.records.fromarrays(for_tree, names=",".join(variables))
    rvs = r.RooArgSet(*roo_vars)
    t = rnp.array2tree(for_tree)
    del for_tree
    data = r.RooDataSet("Data_"+"_".join(variables), "Data_"+"_".join(variables), rvs, r.RooFit.Import(t), r.RooFit.WeightVar("weight"))
    data.Print()
    t.Reset()
    del t
    return data

def get_dataset(retriever, files, variables, selection="", syst = "Nominal", campaigns = ["mc16a", "mc16d", "mc16e"], normalize_mass_points=False):
    data = retriever.get_dataframe(files, variables=variables, systematic=syst, campaigns=campaigns, selection=selection)
    data = get_dataset_from_df(data, variables, selection, normalize_mass_points=normalize_mass_points)
    #create a set of observables for each variable:
    return data

def roo_to_list(set_or_list):
    list_to_return = []
    iter = set_or_list.createIterator()
    var = iter.Next()
    while var :
        if var: list_to_return.append(var)
        var = iter.Next()
    return list_to_return

def load_model(full_path_model_file, ws_name, model_name):
    print("Loading from {}".format(full_path_model_file))
    print("Loading ws {}".format(ws_name))
    tf = r.TFile(full_path_model_file, "READ")
    ws = tf.Get(ws_name)
    #ws.Print()
    model = ws.pdf(model_name)
    #model.Print()
    ws = redefine_env_with_model(model)

    all_observables = []
    all_parameters = []
    all_nuis_parameters = []

    poi = "mass_point"
    all_functions = roo_to_list(ws.allFunctions())
    all_vars = roo_to_list(ws.allVars())
    all_pdfs = roo_to_list(ws.allPdfs())

    if "weight" not in all_observables: all_observables.append("weight")
    print("Making sure all variables are loaded")
    for el in all_vars:
        print(el.GetName())
        if not check_for_variable(el.GetName()): add_roo_variable(el)
    print("DONE")

    for af in all_functions:
       force_roo_variable(af)

    for av in all_vars:
        if av.GetName() in get_all_observables(): 
            all_observables.append(av.GetName())
        elif "mass_point" in av.GetName(): pass
        elif av.GetName().startswith("Nuis_"): 
            all_nuis_parameters.append(av)
        else:
            all_parameters.append(av)
        force_roo_variable(av)

    if type(poi) == str: poi = get_variable(poi)
    tf.Close()
    return poi, model, all_pdfs, all_observables, all_parameters, all_nuis_parameters, all_functions

def redefine_env_with_model(model, data=[]):
    ws = r.RooWorkspace("test", "test")
    keep_ws_alive.append(ws)
    model_argset = r.RooArgSet(model)
    data_argset = list_to_argset(data)
    to_import = r.RooArgSet(model_argset, data_argset)
    getattr(ws, "import")(to_import)
    all_functions = roo_to_list(ws.allFunctions())
    all_cats = roo_to_list(ws.allCats())
    all_vars = roo_to_list(ws.allVars())
    all_pdfs = roo_to_list(ws.allPdfs())
    all_pdfs = [ws.pdf(el.GetName()) for el in all_pdfs]
    all_cats = [ws.cat(el.GetName()) for el in all_cats]
    all_functions = [ws.function(el.GetName()) for el in all_functions]
    all_vars = [ws.var(el.GetName()) for el in all_vars]
    for el in all_pdfs + all_functions + all_vars + all_cats:
        force_roo_variable(el)
    return ws

def list_to_argset(list_to_set):
    to_return = r.RooArgSet()
    for el in list_to_set:
        to_return = r.RooArgSet(to_return, r.RooArgSet(el))
    return to_return

def list_to_arglist(list_to_roolist):
    to_return = r.RooArgList()
    for el in list_to_roolist:
        to_return.add(el)
    return to_return

def get_systematic(name, par_up, par_down, par_nom, nuis):
    up_slope = "({} - {})".format(par_up.GetName(), par_nom.GetName())
    down_slope = "({} - {})".format(par_nom.GetName(), par_down.GetName())
    down_condition = "({} < 0.0)".format(nuis.GetName())
    up_condition = "({} >= 0.0)".format(nuis.GetName())
    formula_string = "( ( {us} * {uc} ) + ( {ds} * {dc} ) ) * {nuis}".format(us=up_slope, ds=down_slope, uc=up_condition, dc=down_condition, nuis=nuis.GetName())
    linear_formula = r.RooFormulaVar(name, name, formula_string, r.RooArgList(par_up, par_down, par_nom, nuis))
    return linear_formula

def get_signal_model(mass_observable = "m4l_constrained", channel = None, syst = "Nominal", category="Incl",extend=False, do_per=False, pervar=""):
    model_file = "signal_model_{}_{}_{}_{}.root".format(syst, channel, mass_observable, category)
    folder = utils.get_signal_model_directory()
    full_path_model_file = os.path.join(folder, model_file)
    ws_name = "signal_model_{}_{}_{}_{}".format(syst, channel, mass_observable, category)
    if syst == "Nominal": model_name = "{}__{}__signal_model".format(channel, category)
    else: model_name = "{}__{}__signal_model__{}".format(channel, category, syst)
    if not os.path.exists(full_path_model_file): 
        raise ValueError("This file doesn't exist {}".format(full_path_model_file))
    poi, model, pdfs, all_observables, all_parameters, all_nuis_parameters, all_functions = load_model(full_path_model_file,ws_name, model_name) #make sure that all observables are in memory
    if extend: 
        pdfs.append(model)
        extension_name = model.GetName() +  "_Norm_Ext"
        extension_var = get_variable(extension_name, 10.0, 0.0, 300.0)
        all_nuis_parameters.append(extension_var)
        model = r.RooExtendPdf(model.GetName() + "_Ext", model.GetName() + "_Ext", model, extension_var)
    for par in all_parameters: par.setConstant(True) #fix the signal model
    if do_per:
        params = __signal_model__.get_params(mass_observable, channel, category, syst)
        assert pervar
        params["sigma"] = get_variable(pervar)
        params["name"] = params["name"] + "_PER"
        all_params_new = []
        all_functions_new = []
        for p in all_parameters:
            if "_sigma_" in p.GetName(): continue
            all_params_new.append(p)
        for p in all_functions:
            if "_sigma_" in p.GetName(): continue
            all_functions_new.append(p)
        all_functions = all_functions_new
        all_parameters = all_params_new
        if pervar not in all_observables: all_observables.append(pervar)

        signal_model_pdf = __signal_model__.load_signal_model(**params)
        model = signal_model_pdf
        all_observables.append(pervar)
        if extend:
            pdfs.append(signal_model_pdf)
            extension_name = model.GetName() +  "_Norm_Ext"
            extension_var = get_variable(extension_name, 10.0, 0.0, 300.0)
            all_nuis_parameters.append(extension_var)
            model = r.RooExtendPdf(model.GetName() + "_Ext", model.GetName() + "_Ext", model, extension_var)

    return poi,model, pdfs, all_observables, all_parameters, all_nuis_parameters, all_functions

def check_systematic_effect(pdf_nom, pdf_up, pdf_down):
    #return the effect of the systematic
    nomhist = pdf_nom.generateBinned(r.RooFit.ExpectedData())
    uphist = pdf_up.generateBinned(r.RooFit.ExpectedData())
    downhist = pdf_down.generateBinned(r.RooFit.ExpectedData())
    upeffect = uphist.Divide(nomhist)
    downeffect = downhist.Divide(nomhist)

def get_bkg_model_all_systematics(retriever, mass_observable = "m4l_constrained", channel = None, systematics=[], category="Incl", orders={"tXX_VVV":4,"ZZ":7}, bkgs=["tXX_VVV", "ZZ"], model_types={}, float_ZZ=True):
    all_pdfs = []
    all_observables = []
    all_parameters = []
    all_nuis_parameters = []
    all_functions = []

    param_functions = {}
    name_functions = {}
    for b in bkgs:
        if model_types[b] == "Che": param_functions[b], name_functions[b] =  __bkg_model__.get_chebychev_model_params, __bkg_model__.get_full_chebychev_name
        if model_types[b] == "Bern": param_functions[b], name_functions[b] =  __bkg_model__.get_bernstein_model_params, __bkg_model__.get_full_bernstein_name

    #load all of the models first:
    assert "Nominal" not in systematics
    for syst in systematics + ["Nominal"]:
        for bkg_name in bkgs:
            load_one_bkg_model(retriever, mass_observable=mass_observable, channel = channel, syst = syst, category=category, bkg_name=bkg_name, extended=True, model_type=model_types[bkg_name], model_par=orders[bkg_name])

    parameters = {}
    names = {}
    for bkg in bkgs:
        parameters[bkg] = {}
        names[bkg] = {}
        for syst in systematics:
             files = utils.get_files_given_name(bkg, retriever)
             if not retriever.has_syst(syst, files): continue
             names[bkg][syst] = name_functions[bkg](mass_observable, channel, category, syst, orders[bkg], bkg, True)
             parameters[bkg][syst] = param_functions[bkg](names[bkg][syst], orders[bkg], extended=True)

    nominal_parameters_bkgs = {}
    nominal_names_bkgs = {}
    for bkg in bkgs:
        nominal_names_bkgs[bkg] = name_functions[bkg](mass_observable, channel, category, "Nominal", orders[bkg], bkg, True)
        nominal_parameters_bkgs[bkg] = param_functions[bkg](nominal_names_bkgs[bkg], orders[bkg], extended=True)

    #now get the systematically varied parameters
    systematic_pairs,_ = systematics_cache.get_all_systematic_pairs(systematics)

    varied_parameters = {}
    for bkg in bkgs:
        varied_parameters[bkg] = {}
        nominal_parameters = nominal_parameters_bkgs[bkg]
        systematically_varied_parameters = parameters[bkg]

        pars_varied = {}
        varied_parameters[bkg]=pars_varied
        for par in nominal_parameters:
           pars_nom = nominal_parameters[par]
           pars_varied[par] = []
           if par == "Norm" and bkg == "ZZ" and float_ZZ:
               pars_varied[par] += nominal_parameters[par]
               continue
           for i, par_nom in enumerate(pars_nom):
               linear_formulas = []
               for syst in systematic_pairs:
                   up_syst = systematic_pairs[syst]["up"]
                   down_syst = systematic_pairs[syst]["down"]

                   files = utils.get_files_given_name(bkg, retriever)
                   has_up = retriever.has_syst(up_syst, files)
                   has_down = retriever.has_syst(down_syst, files)
                   if (has_up and not has_down) or \
                      (has_down and not has_up): raise ValueError("only one of {} exists".format(syst))
                   if not has_up: continue
                   if not has_down: continue
                   params_up = systematically_varied_parameters[up_syst]
                   params_down = systematically_varied_parameters[down_syst]

                   par_up = params_up[par][i]
                   par_down = params_down[par][i]
                   all_parameters+=[par_up, par_down]

                   np = systematics_cache.get_nuisance_parameter(syst, category, channel)

                   linear_formula_name = "piecewise_lin_{}_{}_{}_{}_{}".format(bkg, par_nom.GetName(), syst, channel, category)
                   linear_formula = get_systematic(linear_formula_name, par_up, par_down, par_nom, np) 

                   all_functions.append(linear_formula)
                   linear_formulas.append(linear_formula)

               arglist_formulas = list_to_arglist(linear_formulas + [par_nom])
               par_string_variations = "{}".format(par_nom.GetName())
               for linear_formula in linear_formulas:
                   par_string_variations += " + {}".format(linear_formula.GetName())
               parameter_with_variations_name = "{}_all_variations".format(par_nom.GetName())
               parameter_with_variations = r.RooFormulaVar(parameter_with_variations_name, parameter_with_variations_name, par_string_variations, arglist_formulas)
               all_functions.append(parameter_with_variations)
               pars_varied[par].append(parameter_with_variations)

    poly_bkg_models = {}
    for bkg in bkgs:
        parameters = varied_parameters[bkg]
        nominal_parameters = nominal_parameters_bkgs[bkg]
        for par in parameters:
            if par == "Norm" and bkg == "ZZ" and float_ZZ:
                parameters[par]=nominal_parameters[par]
        for par in nominal_parameters:
            if par == "Norm" and bkg == "ZZ" and float_ZZ:
                all_nuis_parameters+=nominal_parameters[par]
            else: all_parameters += nominal_parameters[par]
        if model_types[bkg] == "Che": name = __bkg_model__.get_full_chebychev_name(mass_observable, channel, category, "_AllVariations", orders[bkg], bkg, True)
        elif model_types[bkg] == "Bern": name = __bkg_model__.get_full_bernstein_name(mass_observable, channel, category, "_AllVariations", orders[bkg], bkg, True)
        if model_types[bkg] == "Che": pdf, pdf_ext = __bkg_model__.get_chebychev_model_from_params(name, parameters, mass_var = mass_observable)
        elif model_types[bkg] == "Bern": pdf, pdf_ext = __bkg_model__.get_bernstein_model_from_params(name, parameters, mass_var = mass_observable)
        poly_bkg_models[bkg] = pdf_ext # the systematically varied models
        all_pdfs.append(pdf)
        all_pdfs.append(pdf_ext)

    all_models = []
    for bkg in bkgs:
       all_models.append(poly_bkg_models[bkg])
    all_models = list_to_arglist(all_models)
    final_name = __bkg_model__.get_full_chebychev_name(mass_observable, channel, category, "_AllVariations", "_".join([str(orders[bkg]) for bkg in bkgs]), "_".join(bkgs) + "_Summed", True)
    pdf = r.RooAddPdf(final_name, final_name, all_models)

    print("Defined env with model {}".format(pdf))
    ws = redefine_env_with_model(pdf)
    #ws.Print()
    poi = ws.var("mass_point")
    all_nuis_parameters = [ws.var(v.GetName()) for v in all_nuis_parameters]
    all_parameters = [ws.var(v.GetName()) for v in all_parameters]
    for p in all_parameters:
        p.setConstant(True)
    all_pdfs = [ws.pdf(p.GetName()) for p in all_pdfs]
    all_functions = [ws.function(f.GetName()) for f in all_functions]
    pdf = ws.pdf(pdf.GetName())
    return poi, pdf, all_pdfs, all_observables, all_parameters, all_nuis_parameters, all_functions

def print_signal_model_params(mass_observable = "m4l_constrained", channel = None, systematic = "Nominal", category = "BDT1", parameter="mean_intercept"):
    tmp_poi, tmp_model,  tmp_pdfs, tmp_observables, tmp_parameters, tmp_nuis_parameters, tmp_functions = get_signal_model(mass_observable = mass_observable, channel=channel, syst=systematic, category=category, extend=False)
    parname = "{}__{}__{}".format(channel, category, parameter)
    return get_variable(parname).getVal()

def get_signal_model_all_systematics(retriever, mass_observable = "m4l_constrained", channel = None, systematics = [], category="Incl", extend=True, do_per=False, pervar="", do_per_scale = False):
    pervar = pervar.format(channel)
    #load everything
    all_pdfs = []
    all_observables = []
    all_parameters = []
    all_nuis_parameters = []
    all_functions = []

    all_observables.append(mass_observable)

    roo_mass_observable = get_variable(mass_observable)
    wcards=["_ggH125_"]
    files = retriever.get_root_files(wcards)
    for syst in systematics + ["Nominal"]:
        if not retriever.has_syst(syst, files): continue
        tmp_poi, tmp_model,  tmp_pdfs, tmp_observables, tmp_parameters, tmp_nuis_parameters, tmp_functions = get_signal_model(mass_observable = mass_observable, channel=channel, syst=syst, category=category, extend=False, do_per=do_per, pervar = pervar)
        #all_pdfs += tmp_pdfs
        all_parameters += tmp_parameters
        all_functions += tmp_functions
        all_nuis_parameters += tmp_nuis_parameters

    nominal_parameters = __signal_model__.get_params(mass_observable, channel, category, "Nominal")
    systematically_varied_parameters = {}
    filtered_systematics = []
    for syst in systematics:
        if not retriever.has_syst(syst, files): continue
        filtered_systematics.append(syst)
        tmp_poi, tmp_model,  tmp_pdfs, tmp_observables, tmp_parameters, tmp_nuis_parameters, tmp_functions = get_signal_model(mass_observable = mass_observable, channel=channel, syst=syst, category=category, extend=False)
        systematically_varied_parameters[syst] = __signal_model__.get_params(mass_observable, channel, category, syst)
    systematic_pairs, _ = systematics_cache.get_all_systematic_pairs(filtered_systematics)

    varied_parameters = {}
    for par in nominal_parameters:
       par_nom = nominal_parameters[par]
       if par == "name": 
          varied_parameters[par] = "{}_all_variations".format(par_nom)
          continue

       if par in get_all_observables():
           varied_parameters[par] = par_nom
           continue
       if type(par) != str and par.GetName() in get_all_observables():
           varied_parameters[par] = par_nom
           continue
       if par == "mass":
           varied_parameters[par] = par_nom
           continue

       linear_formulas = []
       for syst in systematic_pairs:
           up_syst = systematic_pairs[syst]["up"]
           down_syst = systematic_pairs[syst]["down"]
           params_up = systematically_varied_parameters[up_syst]
           params_down = systematically_varied_parameters[down_syst]
           wcard=["_ggH125_"]
           files=retriever.get_root_files(wcard)
           if (retriever.has_syst(up_syst, files) and not retriever.has_syst(down_syst, files)) or \
           (not retriever.has_syst(down_syst, files) and retriever.has_syst(down_syst, files)): raise ValueError("only one of {} exists".format(syst))
           if not retriever.has_syst(up_syst, files): continue
           if not retriever.has_syst(down_syst, files): continue

           par_up = params_up[par]
           par_down = params_down[par]

           np = systematics_cache.get_nuisance_parameter(syst, category, channel)

           linear_formula_name = "piecewise_lin_{}_{}_{}_{}".format(syst, channel, category, par)
           linear_formula = get_systematic(linear_formula_name, par_up, par_down, par_nom, np) 

           linear_formulas.append(linear_formula)
           all_functions.append(linear_formula)

       arglist_formulas = list_to_arglist(linear_formulas + [par_nom])
       par_string_variations = "{}".format(par_nom.GetName())
       for linear_formula in linear_formulas:
           par_string_variations += " + {}".format(linear_formula.GetName())
       parameter_with_variations_name = "{}_all_variations".format(par_nom.GetName())
       parameter_with_variations = r.RooFormulaVar(parameter_with_variations_name, parameter_with_variations_name, par_string_variations, arglist_formulas)
       varied_parameters[par] = parameter_with_variations

    #replace the width with the PER observable
    if do_per:
        if not do_per_scale:
            varied_parameters["sigma"] = get_variable(pervar.format(channel))
            all_observables.append(pervar.format(channel))
        if do_per_scale:
            pervar = get_variable(pervar.format(channel))
            scalename = pervar.GetName() + "_scale"
            if check_for_variable(scalename): scalevar = get_variable(scalename)
            else: scalevar = get_variable(scalename, 1.0, 0.0, 5.0)
            varied_parameters["sigma"] = r.RooFormulaVar(scalename + "_" + pervar.GetName(), scalename + "_" + pervar.GetName(), "{} * {}".format(pervar.GetName(), scalevar.GetName()), r.RooArgList(pervar, scalevar))
            all_functions.append(varied_parameters["sigma"])
            all_nuis_parameters.append(scalevar)
            all_observables.append(pervar.GetName())
    if do_per:
        all_functions_new = []
        for p in all_functions:
            if "sigma" in p.GetName(): continue
            all_functions_new.append(p)
        all_functions = all_functions_new

    #extend the signal model if necessary. This is to let the normalization float
    signal_model = __signal_model__.load_signal_model(**varied_parameters)
    all_pdfs.append(signal_model)

    if extend: 
        extension_name = signal_model.GetName() +  "_Norm_Ext"
        if not check_for_variable(extension_name): extension_var = get_variable(extension_name, 10.0, 0.0, 300.0)
        else: extension_var = get_variable(extension_name)
        all_nuis_parameters.append(extension_var)
        extended_signal_model = r.RooExtendPdf(signal_model.GetName() + "_Ext", signal_model.GetName() + "_Ext", signal_model, extension_var)
        signal_model = extended_signal_model
        all_pdfs.append(signal_model)

    for par in all_parameters: par.setConstant(True) #fix the signal model
    for par in all_nuis_parameters: par.setConstant(False) #float the nuisance parameters
    print("Defined env with model {}".format(signal_model))
    ws = redefine_env_with_model(signal_model)
    #ws.Print()
    poi = ws.var("mass_point")
    all_nuis_parameters = [ws.var(v.GetName()) for v in all_nuis_parameters]
    all_parameters = [ws.var(v.GetName()) for v in all_parameters]
    all_pdfs = [ws.pdf(p.GetName()) for p in all_pdfs]
    all_functions = [ws.function(f.GetName()) for f in all_functions]
    signal_model = ws.pdf(signal_model.GetName())

    return poi, signal_model, all_pdfs, all_observables, all_parameters, all_nuis_parameters, all_functions

def load_one_bkg_model(retriever, mass_observable="m4l_constrained", channel = None, syst = "Nominal", category="Incl", bkg_name="tXX_VVV", extended=True, model_type="Che", model_par=7):
    files = utils.get_files_given_name(bkg_name, retriever)
    if not retriever.has_syst(syst, files): return False
    else:
        if extended: extend_str = "Extended"
        else: extend_str = "Shape"
        ws_name = "bkg_model_{}_{}_{}_{}_{}_{}_{}_{}".format(extend_str, model_type, syst, channel, mass_observable, category, bkg_name,(str(model_par)).replace(".", "_"))
        print(ws_name)
        bkg_model_name = ws_name.replace(".root", "").replace("_" + bkg_name, "") + "_{}".format("_".join([bkg_name]))
        model_file = "{}_{}_{}.root".format(mass_observable, model_type, syst)

        full_path = "/home/ladamek/projects/def-psavard/ladamek/MassModels/BkgModels/{}/{}".format(syst,model_file)
        print(full_path)
        assert os.path.exists(full_path)

        descr = join_cat_varname(channel, category, mass_observable, syst=syst)
        if model_type == "Che":
           if extended: descr += "_Ext"
           else: descr += "_Shape"
           model_name = "_".join(["RooChe", str(model_par), bkg_name,  descr])

        if model_type == "Bern":
           if extended: descr += "_Ext"
           else: descr += "_Shape"
           model_name = "_".join(["RooBern", str(model_par), bkg_name,  descr])

        if model_type == "Keys":
            rho_name = str(model_par).replace(".", "_")
            model_name = "_".join([name, descr, rho_name])

        if extended:
            model_name = "{}_Extended".format(model_name)
        load_model(full_path, ws_name, model_name)
        print("Model {}, {} loaded".format(ws_name, model_name))
        return True

def get_bkg_model(retriever, mass_observable="m4l_constrained", channel = None, syst = "Nominal", category="Incl", bkg_names=["tXX_VVV"], extended=True, model_type="Che", model_par=7, float_ZZ=False):
    to_merge = {}
    bkg_model_name = ""

    for bkg_name in bkg_names:
        if extended: extend_str = "Extended"
        else: extend_str = "Shape"
        ws_name = "bkg_model_{}_{}_{}_{}_{}_{}_{}_{}".format(extend_str, model_type, syst, channel, mass_observable, category, bkg_name,(str(model_par)).replace(".", "_"))
        bkg_model_name = ws_name.replace(".root", "").replace("_" + bkg_name, "") + "_{}".format("_".join(bkg_names))

        model_file = "{}_{}_{}.root".format(mass_observable, model_type, syst)

        full_path = "/home/ladamek/projects/def-psavard/ladamek/MassModels/BkgModels/{}/{}".format(syst,model_file)
        print(full_path)
        assert os.path.exists(full_path)
        #input("opening {}".format(full_path))

        descr = join_cat_varname(channel, category, mass_observable, syst=syst)
        if model_type == "Che":
           if extended: descr += "_Ext"
           else: descr += "_Shape"
           model_name = "_".join(["RooChe", str(model_par), bkg_name,  descr])

        if model_type == "Bern":
           if extended: descr += "_Ext"
           else: descr += "_Shape"
           model_name = "_".join(["RooBern", str(model_par), bkg_name,  descr])

        if model_type == "Keys":
            rho_name = str(model_par).replace(".", "_")
            model_name = "_".join([name, descr, rho_name])

        if extended:
            model_name = "{}_Extended".format(model_name)

        to_merge[bkg_name] = load_model(full_path, ws_name, model_name)

    #merge the background models:
    poi = None
    all_models = []
    all_pdfs = []
    all_observables = []
    all_parameters = []
    all_nuis_parameters = []
    all_functions = []
    for bkg_name in bkg_names:
       this_poi, model, pdfs, observables, parameters, nuis_parameters, functions = to_merge[bkg_name]
       to_pop = -1
       ext_name = "Roo{mt}_{mp}_{bkgn}_{ch}__{cat}__{mo}_Ext_Norm".format(mo=mass_observable, cat=category,ch= channel, bkgn=bkg_name, mt=model_type,mp=str(model_par).replace(".","_"))
       for i, var in enumerate(parameters):
            if float_ZZ and ext_name == var.GetName(): to_pop = i
       if to_pop > -1 and "ZZ" == bkg_name: nuis_parameters.append(parameters.pop(to_pop))

       if poi: assert poi.GetName() == this_poi.GetName()
       poi = this_poi
       all_models.append(model)
       all_pdfs = list(set(all_pdfs + [p.GetName() for p in pdfs] + [model.GetName()]))
       all_observables = list(set(all_observables + observables))
       all_parameters = list(set(all_parameters + [ap.GetName() for ap in parameters]))
       all_nuis_parameters = list(set(all_nuis_parameters + [anp.GetName() for anp in nuis_parameters]))
       all_functions = list(set(all_functions + [af.GetName for af in functions]))

    pdf = r.RooAddPdf(bkg_model_name, bkg_model_name, r.RooArgList(*all_models))
    ws = redefine_env_with_model(pdf)
    poi = ws.var(poi.GetName())
    all_nuis_parameters = list(set([ws.var(v) for v in all_nuis_parameters]))
    all_parameters = list(set([ws.var(v) for v in all_parameters]))
    for par in all_parameters: par.setConstant(True) #fix the background model
    all_pdfs = list(set([ws.pdf(p) for p in all_pdfs]))
    all_functions = list(set([ws.function(f) for f in all_functions]))
    model = ws.pdf(pdf.GetName())

    return poi, model, all_pdfs, all_observables, all_parameters, all_nuis_parameters, all_functions

def merge_bkg_signal_models(retriever, mass_observable = "m4l_constrained", channels = None, systematics = [], categories=["Incl"], do_bkg=False, bkgs=[], do_sig=False, model_types = {"ZZ":"Che", "tXX_VVV":"Bern"}, do_per=False, do_per_scale=False, pervars = {"2e2mu": "sigma_corrected_prediction_{}", "4mu": "sigma_corrected_prediction_{}", "2mu2e": "sigma_corrected_prediction_{}", "4e":"sigma_corrected_prediction_{}"}):
    assert not (do_per_scale and not do_per)

    name = "{}_{}_{}_{}".format("AllVariations","_".join(channels), "_".join(categories), mass_observable)

    all_pdfs = []
    all_observables = []
    all_parameters = []
    all_nuis_parameters = []
    all_functions = []
    all_cats = []

    inclusive = False
    assert len(categories) > 0 and len(channels) > 0
    if len(categories) == 1 and categories[0] == "Incl": inclusive = True
    else: assert "Incl" not in categories

    if inclusive: cat_var = "event_type"
    else: cat_var = "cat_index"
    roo_cat_var = get_variable(cat_var)
    all_cats.append(cat_var)

    if not do_sig and not do_bkg: raise ValueError("OK You have to do either signal, background or both")

    combined_model = r.RooSimultaneous(name, name, roo_cat_var)
    pois = []
    for category in categories:
        for channel in channels:
            if do_sig:
                sig_poi, sig_model, sig_pdfs, sig_observables, sig_parameters, sig_nuis_parameters, sig_functions = get_signal_model_all_systematics(retriever, mass_observable = mass_observable, channel = channel, systematics=systematics, category=category, extend=True, do_per=do_per, do_per_scale=do_per_scale, pervar = pervars[channel])
                sig_model.Print()
                sig_model.getVal()
                all_pdfs = list(set(all_pdfs + [pdf.GetName() for pdf in sig_pdfs] + [sig_model.GetName()]))
                force_roo_variable(sig_model)
                all_observables = list(set(all_observables + sig_observables))
                all_parameters = list(set(all_parameters + [p.GetName() for p in sig_parameters]))
                all_nuis_parameters = list(set(all_nuis_parameters + [a.GetName() for a in  sig_nuis_parameters]))
                all_functions  = list(set(all_functions + [f.GetName() for f in sig_functions]))

            if do_bkg:
                bkg_poi, bkg_model, bkg_pdfs, bkg_observables, bkg_parameters, bkg_nuis_parameters, bkg_functions = get_bkg_model_all_systematics(retriever, mass_observable = mass_observable, channel = channel, systematics=systematics, category=category, bkgs=bkgs, model_types=model_types, orders={"tXX_VVV":4, "ZZ":7},float_ZZ=True)
                bkg_model.Print()
                bkg_model.getVal()
                all_pdfs = list(set(all_pdfs + [pdf.GetName() for pdf in bkg_pdfs] + [bkg_model.GetName()]))
                force_roo_variable(bkg_model)
                all_observables = list(set(all_observables + bkg_observables))
                all_parameters = list(set(all_parameters + [p.GetName() for p in bkg_parameters]))
                all_nuis_parameters = list(set(all_nuis_parameters + [a.GetName() for a in  bkg_nuis_parameters]))
                all_functions  = list(set(all_functions + [f.GetName() for f in bkg_functions]))
            if do_sig and do_bkg:
                model_name = sig_model.GetName() + "_" + bkg_model.GetName()
                model = r.RooAddPdf(model_name, model_name, r.RooArgList(sig_model, bkg_model))
                force_roo_variable(model)
                pois.append(sig_poi)
                pois.append(bkg_poi)
            elif do_sig:
                model = sig_model
                poi = sig_poi
                pois.append(sig_poi)
            else:
                model = bkg_model
                poi = bkg_poi
                pois.append(bkg_poi)

            if inclusive: combined_model.addPdf(model, channel)
            else: combined_model.addPdf(model, "{}_{}".format(category, channel))

    combined_model.getVal()
    #constraints = systematics_cache.get_constraint_list(list(paired_systematics.keys()), categories, channels)
    #new_model_name = "{}_{}".format(combined_model.GetName(), "AllConstraints")
    #model_with_systematics = r.RooProdPdf(new_model_name, new_model_name, list_to_arglist(constraints + [combined_model]))
    #all_pdfs += [c.GetName() for c in constraints]
    paired_systematics, systematics = systematics_cache.get_all_systematic_pairs(systematics, False)
    all_extra_nuis_parameters = systematics_cache.get_np_list(list(paired_systematics.keys()), categories, channels)
    all_nuis_parameters += [np.GetName() for np in all_extra_nuis_parameters]

    ws = redefine_env_with_model(combined_model)
    all_nuis_parameters = [ws.var(v) for v in all_nuis_parameters if ws.var(v)]
    all_parameters = [ws.var(v) for v in all_parameters if ws.var(v)]
    all_pdfs = [ws.pdf(p) for p in all_pdfs if ws.pdf(p)]
    all_functions = [ws.function(f) for f in all_functions if ws.function(f)]
    all_cats = [ws.cat(c) for c in all_cats]
    model = ws.pdf(combined_model.GetName())
    poi = ws.var("mass_point")
    model.getVal()

    return poi, model, all_pdfs, all_observables, all_parameters, all_nuis_parameters, all_functions, all_cats

if __name__ == "__main__":
    pass
    #make plots of all of the models
    #retriever = utils.get_v24_retriever()
    #bkgs = ["tXX_VVV", "ZZ"]
    #for b in bkgs:
        #files = utils.get_files_given_name(b)
        #for cat in ["4mu", "4e", "2e2mu", "2mu2e"]
