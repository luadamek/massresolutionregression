import os
import time
import pickle
import numpy as np
import base64
from array import array
CAMPAIGNS = ["mc16a", "mc16d", "mc16e"]

mass_point_map = {\
"ggH125_":125.0,\
"ggH123_":123.0,\
"ggH124_":124.0,\
"ggH126_":126.0,\
"ggH127_":127.0,\
"ggH123p5_":123.5,\
"ggH124p4_":124.5,\
"ggH125p5_":125.5,\
}

def get_scratch_dir():
    if os.getenv("USER") == "ladamek": return "/project/def-psavard/ladamek/MASSSCRATCHDIR/"
    else: return "/project/def-psavard/hmumu_harish_files/"

from data_retriever import DataRetriever
BINNING = "NOMINAL"
FITTING_RANGE = "NOMINAL"
#BINNING_FLAVOUR = "OPTIMAL"
#[-1.   , -0.304,  0.264,  0.788,  1.   ]

def add_nominal_resolution_models(retriever):
    #models = ["nominal_resolution_models/reco_eta_pt/4mu/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4mu_truth_eta_pt_0_005_MPNormed_truth_to_reco_corrected/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4mu_truth_eta_pt_0_005_MPNormed_truth_to_reco_corrected_popped",\
    #"nominal_resolution_models/reco_eta_pt/2e2mu/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_2e2mu_reco_eta_pt_0_001_MPNormed/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_2e2mu_reco_eta_pt_0_001_MPNormed_popped",\
    #"nominal_resolution_models/reco_eta_pt/4e/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4e_reco_eta_pt_0_001_MPNormed/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4e_reco_eta_pt_0_001_MPNormed_popped",\
    #"nominal_resolution_models/reco_eta_pt/2mu2e/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_2mu2e_truth_eta_pt_0_005_MPNormed/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_2mu2e_truth_eta_pt_0_005_MPNormed_popped_truth_to_reco"]

    models = [\
    "/home/ladamek/MassResolutionRegression/nominal_resolution_models/reco_eta_pt_corrected/2e2mu/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_2e2mu_truth_eta_pt_0_0005_MPNormed_truth_to_reco_corrected/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_2e2mu_truth_eta_pt_0_0005_MPNormed_truth_to_reco_corrected_popped",\
    "/home/ladamek/MassResolutionRegression/nominal_resolution_models/reco_eta_pt_corrected/2mu2e/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_2mu2e_truth_eta_pt_0_005_MPNormed_truth_to_reco_corrected/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_2mu2e_truth_eta_pt_0_005_MPNormed_truth_to_reco_corrected_popped",\
    "/home/ladamek/MassResolutionRegression/nominal_resolution_models/reco_eta_pt_corrected/4e/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4e_truth_eta_pt_0_001_MPNormed_truth_to_reco_corrected/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4e_truth_eta_pt_0_001_MPNormed_truth_to_reco_corrected_popped",\
    "/home/ladamek/MassResolutionRegression/nominal_resolution_models/reco_eta_pt_corrected/4mu/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4mu_truth_eta_pt_0_005_MPNormed_truth_to_reco_corrected/DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4mu_truth_eta_pt_0_005_MPNormed_truth_to_reco_corrected_popped"]

    corrections = [os.path.join(os.path.split(m)[0], "LinearFitResult_ggH, m_{H} = 125 GeV.pickle") for m in models]

    for m, c in zip(models, corrections):
        retriever.register_extra_models([m], [c])

def get_xgboost_model_dir():
    if os.getenv("USER") == "ladamek": to_return = "/home/ladamek/projects/def-psavard/ladamek/MassModels/XGBModels"
    if not os.path.exists(to_return): os.makedirs(to_return)
    return to_return

def get_setup_commands():
    if os.getenv("USER") == "ladamek": return ["cd {}".format(os.getenv("MassRegressionResolutionDir")), "source ./setup.sh"]

def get_scratch_dir():
    if os.getenv("USER") == "ladamek": return "/project/def-psavard/MASSSCRATCHDIR/"
    else: return "/export/data/1/luadamek/MASSSCRATCHDIR/"

def get_signal_model_directory():
    if os.getenv("USER") == "ladamek": to_return= "/home/ladamek/projects/def-psavard/ladamek/MassModels/SignalModels/"
    else: raise ValueError("User Unknown")
    if not os.path.exists(to_return): os.makedirs(to_return)
    return to_return

def get_bkg_model_directory():
    if os.getenv("USER") == "ladamek": to_return= "/home/ladamek/projects/def-psavard/ladamek/MassModels/BkgModels/"
    else: raise ValueError("User Unknown")
    if not os.path.exists(to_return): os.makedirs(to_return)
    return to_return

def get_v24_retriever(models=[]):
    remote_base_directory = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v24/AntiKt4EMPFlow/"
    #signal_remote_base_directory = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v24/massPointSamples/"
    signal_remote_base_directory = ""
    root_remote_directory = 'root://eosatlas.cern.ch'
    directory_structure_type2 = ["__CAMPAIGN__", "Systematics","__SYSTEMATIC__", "__ROOTFILE__"]
    directory_structure_type1 = ["__CAMPAIGN__", "Systematics","NormSystematic", "__ROOTFILE__"]
    directory_structure_nominal = ["__CAMPAIGN__", "Nominal", "__ROOTFILE__"]
    retriever = DataRetriever(remote_base_directory = remote_base_directory, root_remote_directory = root_remote_directory, directory_structure_type1 = directory_structure_type1, directory_structure_type2 = directory_structure_type2, directory_structure_nominal = directory_structure_nominal, model_retrievers = models, extra_vars = [get_inclusive_category_var(), get_global_category_var()], extra_varnames = ["bdt_cat", "cat_index"], signal_remote_base_directory=signal_remote_base_directory, scratch_dir = get_scratch_dir())
    return retriever

def get_mcp_retriever(models=[]):
    #remote_base_directory = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v24/AntiKt4EMPFlow/"
    remote_base_directory = "/eos/atlas/atlascerngroupdisk/perf-muon/MuonMomentumCalibration/Toys/prod_v08/"
    #signal_remote_base_directory = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v24/massPointSamples/"
    signal_remote_base_directory = ""
    root_remote_directory = 'root://eosatlas.cern.ch'
    #directory_structure_type2 = ["__CAMPAIGN__", "Systematics","__SYSTEMATIC__", "__ROOTFILE__"]
    directory_structure_type2 = ["test_set_v2",  "__ROOTFILE__"]
    #directory_structure_type1 = ["__CAMPAIGN__", "Systematics","NormSystematic", "__ROOTFILE__"]
    directory_structure_type1 = ["test_set_v2",  "__ROOTFILE__"]
    #directory_structure_nominal = ["__CAMPAIGN__", "Nominal", "__ROOTFILE__"]
    directory_structure_nominal = ["test_set_v2",  "__ROOTFILE__"]
    retriever = DataRetriever(remote_base_directory = remote_base_directory, root_remote_directory = root_remote_directory, directory_structure_type1 = directory_structure_type1, directory_structure_type2 = directory_structure_type2, directory_structure_nominal = directory_structure_nominal, model_retrievers = models, signal_remote_base_directory=signal_remote_base_directory, tree_name = "MuonMomentumCalibrationTree")
    return retriever


def get_v24_qrrnv2_retriever(models=[]):
    remote_base_directory = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v24/massPointSamples_qrnnRes_v2/AntiKt4EMPFlow/"
    #signal_remote_base_directory = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v24/massPointSamples/"
    signal_remote_base_directory = ""
    root_remote_directory = 'root://eosatlas.cern.ch'
    directory_structure_type2 = ["__CAMPAIGN__", "Systematics","__SYSTEMATIC__", "__ROOTFILE__"]
    directory_structure_type1 = ["__CAMPAIGN__", "Systematics","NormSystematic", "__ROOTFILE__"]
    directory_structure_nominal = ["__CAMPAIGN__", "Nominal", "__ROOTFILE__"]
    retriever = DataRetriever(remote_base_directory = remote_base_directory, root_remote_directory = root_remote_directory, directory_structure_type1 = directory_structure_type1, directory_structure_type2 = directory_structure_type2, directory_structure_nominal = directory_structure_nominal, model_retrievers = models, extra_vars = [get_inclusive_category_var(), get_global_category_var()], extra_varnames = ["bdt_cat", "cat_index"], signal_remote_base_directory=signal_remote_base_directory)
    return retriever

def get_v24_extended_retriever(models=[]):
    remote_base_directory = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v24/massPointSamples/"
    #signal_remote_base_directory = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v24/massPointSamples/"

    signal_remote_base_directory = ""
    root_remote_directory = 'root://eosatlas.cern.ch'
    directory_structure_type2 = ["__CAMPAIGN__", "Systematics","__SYSTEMATIC__", "__ROOTFILE__"]
    directory_structure_type1 = ["__CAMPAIGN__", "Systematics","NormSystematic", "__ROOTFILE__"]
    directory_structure_nominal = ["__CAMPAIGN__", "Nominal", "__ROOTFILE__"]
    retriever = DataRetriever(remote_base_directory = remote_base_directory, root_remote_directory = root_remote_directory, directory_structure_type1 = directory_structure_type1, directory_structure_type2 = directory_structure_type2, directory_structure_nominal = directory_structure_nominal, model_retrievers = models, extra_vars = [get_inclusive_category_var(), get_global_category_var()], extra_varnames = ["bdt_cat", "cat_index"], signal_remote_base_directory=signal_remote_base_directory)
    return retriever

def get_hmumu_retriever(models=[]):
    remote_base_directory = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/common_ntuples/v23/"
    if os.getenv("USER") == "ladamek": local_base_directory = "/project/def-psavard/ladamek/HmumuTuplesTesting/"
    directory_structure_nominal = ["__ROOTFILE__"]
    directory_structure_type2 = ["systematics", "__ROOTFILE__"]
    directory_structure_type1 = ["__ROOTFILE__"]
    signal_remote_base_directory = ""
    root_remote_directory = 'root://eosatlas.cern.ch'
    retriever = DataRetriever(remote_base_directory = remote_base_directory, root_remote_directory = root_remote_directory, directory_structure_type1 = directory_structure_type1, directory_structure_type2 = directory_structure_type2, directory_structure_nominal = directory_structure_nominal, model_retrievers = models, signal_remote_base_directory=signal_remote_base_directory,tree_name = "DiMuonNtuple", weight_var = "GlobalWeight", scratch_dir = get_scratch_dir())
    return retriever

#event_type_string = "ggH125_2e2mu"
#      _4mu,      //  0
#      _4e,       //  1
#      _2mu2e,    //  2
#      _2e2mu,    //  3
#      _4tau,     //  4
#      _2e2tau,   //  5
#      _2tau2e,   //  6
#      _2mu2tau,  //  7
#      _2tau2mu,  //  8
#      _emu2mu,   //  9
#      _2mumu,    // 10
#      _2mue,     // 11
#      _2emu,     // 12
#      _2ee,      // 13

bdt_cat_map = {}
bdt_cat_map["BDT1"] = 0
bdt_cat_map["BDT2"] = 1
bdt_cat_map["BDT3"] = 2
bdt_cat_map["BDT4"] = 3

event_type_map = {}
event_type_map["4mu"] = 0
event_type_map["4e"] = 1
event_type_map["2mu2e"] = 2
event_type_map["2e2mu"] = 3

def get_event_type_map():
    return event_type_map

def get_baseline_selection(mass_observable):
    return "(({mo} > 105.0) and ({mo} < 160.0))".format(mo=mass_observable)

def get_event_type_selection(event_type):
    event_type_string = event_type.replace("_", "")
    selection = "((event_type  < {e} + 0.5) and (event_type > {e} - 0.5))".format(e = get_event_type_map()[event_type_string])
    return selection

def get_event_type_categories():
    selections = {}
    for event_type in event_type_map:
       selections[event_type] = get_event_type_selection(event_type)
    return selections

def get_full_selection(event_type, category, mass_observable):
    event_type_string = event_type.replace("_", "")
    selection =  get_event_type_selection(event_type)
    categories = get_bdt_categories()
    assert category in categories
    cat_selection = categories[category]
    selection = "({}) and ({}) and ({})".format(get_baseline_selection(mass_observable), selection, cat_selection)
    return selection

def get_bdt_categories():
    global BINNING
    if BINNING == "NOMINAL": bins = (-1.0, -0.5, 0.0, 0.5, 1.0)
    if BINNING == "OPTIMAL": bins = [-1.   , -0.304,  0.264,  0.788,  1.   ]
    categories = {}
    categories["Incl"] = "(BDT_Massdiscriminant > {}) and (BDT_Massdiscriminant <= {})".format(-1.0, +1.0)
    for b1, b2, name in zip(bins[:-1],bins[1:],["BDT1","BDT2","BDT3", "BDT4"]):
        categories[name] = "(BDT_Massdiscriminant > ({})) and (BDT_Massdiscriminant <= ({}))".format(b1,b2)
    return categories

def get_global_category_value(bdt_cat, decay_channel):
    return event_type_map[decay_channel] + len(event_type_map.keys()) * bdt_cat_map[bdt_cat]

def get_global_category_var():
    variable_string = []
    bdt_categories = get_bdt_categories()
    event_type_categories = get_event_type_categories()

    for cat in ["BDT1", "BDT2", "BDT3", "BDT4"]:
        for channel in ["4mu", "4e", "2e2mu", "2mu2e"]:
            value = get_global_category_value(cat, channel)
            variable_string.append("( {} * ( ({}) and ({}) ) )".format(value, bdt_categories[cat], event_type_categories[channel]))
    to_return = " + ".join(variable_string)
    return to_return

def get_inclusive_category_value(decay_channel):
    return event_type_map[decay_channel] 

def get_inclusive_category_var():
    variable_string = []
    event_type_categories = get_event_type_categories()

    for channel in ["4mu", "4e", "2e2mu", "2mu2e"]:
        value = get_inclusive_category_value(channel)
        variable_string.append("( {} * ({}) )".format(value, event_type_categories[channel]))
    return " + ".join(variable_string)

def get_model_save_dir():
    if os.getenv("USER") == "ladamek": return "/home/ladamek/projects/def-psavard/ladamek/MassResolutionRegressors/"

def get_binning(minimum, maximum, width, df, variable_name, threshold):
   #adjust the minimum:
   nbins = float(maximum-minimum)/float(width)

   nbins=int(nbins)

   maximum = minimum + nbins * width

   while True:
       lower_bin_cut = minimum
       upper_bin_cut = minimum + width
       bin_selection = "({} < {}) and ({} < {})".format(lower_bin_cut, variable_name, variable_name, upper_bin_cut)
       print("Selecing {}".format(bin_selection))
       tmp_df = df.query(bin_selection)
       total = np.sum(tmp_df.eval("weight").values)
       if total < threshold:
           minimum = minimum + width
           nbins -= 1
       else:
           print("Broken")
           break
   print((maximum-minimum)/width)

   #adjust the maximmum:
   while True:
       lower_bin_cut = maximum - width
       upper_bin_cut = maximum
       bin_selection = "({} < {}) and ({} < {})".format(lower_bin_cut, variable_name, variable_name, upper_bin_cut)
       print("Selecing {}".format(bin_selection))
       tmp_df = df.query(bin_selection)
       total = np.sum(tmp_df.eval("weight").values)
       if total < threshold:
           maximum = maximum - width
           nbins -= 1
       else:
           print("Broken")
           break

   #make sure that the distance between the maximum and the minimum is exactly a multiple of the width
   edges = []
   for i in range(0, nbins + 1):
       edges.append(minimum + i * width)

   return edges

def get_equal_stats_binning(minimum, maximum, df, variable_name, stats_binning):
   #get n events between the minimum and the maximum
   df = df.query("({} < {}) and ({} > {})".format(minimum,variable_name, maximum, variable_name))
   variable = df.eval(variable_name).values
   variable = np.sort(variable)
   nbins = int(float(len(variable))/float(stats_binning))
   split_array = np.split(variable[:nbins * stats_binning], nbins)
   edges = []
   for a in split_array:
       edges.append(a[0])
   edges = sorted(edges)
   maxes = [np.max(a) for a in split_array]
   maximum = max(maxes)
   edges.append(maximum)
   return edges

def get_regressor_tree_dir():
     if os.getenv("USER") == "ladamek": return "/project/def-psavard/ladamek/massresolution_prodv23_trees_with_scores/"

def get_regressor_dir():
    if os.getenv("USER") == "ladamek": return "/home/ladamek/projects/def-psavard/ladamek/MassResolutionRegressors/"

#Functions to retrieve lists of files to run over
def get_local_path(file_type):
    if os.getenv("USER") == "ladamek":
        if file_type == "Prod_v20Fix":
            return '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v20Fix/mass/'
        if file_type == "dummy_trees":
            return None
        if file_type == "bianca_trees":
            return "~/projects/def-psavard/ladamek/bianca_trees/"
        if file_type == "bianca_prodv23_appended":
            return '~/projects/def-psavard/ladamek/bianca_prodv23_appended/AntiKt4EMPFlow/'.replace("~", os.getenv("HOME"))

def get_local_minitree_location(file_type):
    if os.getenv("USER") == "ladamek":
        if file_type == "bianca_prodv23_appended": return os.path.join(get_local_path(file_type).replace("AntiKt4EMPFlow",""), "mini_trees").replace("~", os.getenv("HOME"))

def get_file_list(file_type, sample):
    if os.getenv("USER") == "ladamek":
        path = get_local_path(file_type)
        files = []
        for c in CAMPAIGNS:
            if file_type != "bianca_trees" and file_type != "bianca_prodv23_appended":
                f = "root://eosatlas.cern.ch/" + os.path.join(path, c, "Nominal", sample)
            else:
                f = os.path.join(path, c, "Nominal", sample)
            files.append(f)
        return files

def print_model_params(pickle_file, keys_to_print = None, skip = [], model_file = None):
    with open(pickle_file, 'r') as f:
        stuff = pickle.load(f)

    keys = list(stuff.keys())
    sorted(keys)

    if keys_to_print == None:
        for key in keys:
            if key in skip:
                continue
            print("Param: {} \t value: {}".format(key, stuff[key]))
    else:
        for key in keys_to_print:
            if key in skip:
                continue
            print(stuff[key])

    if model_file != None:
        print("The model parameters were")
        model = keras.models.load_model(model_file, custom_objects={'tf': tf})
        model.summary()

def get_signal_files():
        files = ["mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root",\
        "mc16_13TeV.346228.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4lep_notau.root",\
        "mc16_13TeV.346645.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO_shw.root",\
        "mc16_13TeV.346647.PowhegPythia8EvtGen_NNPDF30_AZNLO_WmH125J_Wincl_MINLO_shw.root",\
        "mc16_13TeV.346646.PowhegPythia8EvtGen_NNPDF30_AZNLO_WpH125J_Wincl_MINLO_shw.root",\
        "mc16_13TeV.346340.PowhegPy8EG_A14NNPDF23_NNPDF30ME_ttH125_ZZ4l_allhad.root",\
        "mc16_13TeV.346341.PowhegPy8EG_A14NNPDF23_NNPDF30ME_ttH125_ZZ4l_semilep.root",\
        "mc16_13TeV.346342.PowhegPy8EG_A14NNPDF23_NNPDF30ME_ttH125_ZZ4l_dilep.root",\
        "mc16_13TeV.346414.aMcAtNloPythia8EvtGen_tHjb125_4fl_ZZ4l.root"
        ]
        return files

def get_all_bkgs():
    ZZ_bkgs = ["364250", "364251", "364252", "345708", "345709", "364364"]
    tXX_bkgs = ["410219", "410218"]
    VVV_bkgs = ["364243", "364245", "364247", "364248"]
    tXX_VVV_bkgs = tXX_bkgs + VVV_bkgs
    bkg_sources = [ZZ_bkgs, tXX_VVV_bkgs]
    bkg_source_names = ["ZZ", "tXX_VVV"]

    return bkg_sources, bkg_source_names

def get_wcards_given_name(in_name):
    bkgs, names = get_all_bkgs()
    for bkg, name in zip(bkgs, names):
         if name == in_name: return bkg
    return []

def get_files_given_name(name, retriever):
    wcards = get_wcards_given_name(name)
    files = retriever.get_root_files(wcards)
    return files

def get_workspace_directory():
    return os.path.join(os.getenv("MassRegressionResolutionDir"), "Workspaces")
