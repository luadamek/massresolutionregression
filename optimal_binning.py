import itertools
import numpy as np
import utils

bdtvar  = "BDT_Massdiscriminant"
selection = utils.get_baseline_selection("m4l_constrained")

def remove_duplicate_rows(data):                 
    sorted_data =  data[np.lexsort(data.T),:]
    row_mask = np.append([True],np.any(np.diff(sorted_data,axis=0),1))
    return sorted_data[row_mask]

def get_selection(boundaries, selvar = "BDT_Massdiscriminant"):
    pairs = zip(boundaries[:-1], boundaries[1:])
    selection_template = "( ({selvar} >= {blank}) and ({selvar} < {blank}) )".format(selvar=selvar, blank="{}")
    selections = []
    for p in pairs:
        selections.append(selection_template.format(p[0], p[1]))
    return " and ".join(selections)

def create_selections(edges, bdtvar):
    selections = {(elow, ehigh): (bdtvar < ehigh) & (bdtvar >= elow) for elow, ehigh in zip(edges[:-1], edges[1:]) }
    return selections

def get_weight_sums(edges, bdtvar, weights):
    selections = create_selections(edges, bdtvar)
    weightsum = {(elow, ehigh): np.sum(weights[selections[(elow, ehigh)]]) for elow, ehigh in zip(edges[:-1], edges[1:]) }
    return weightsum

def getsum(blow, bhigh, signal_sums):
    return sum( [signal_sums[(elow, ehigh)] for (elow, ehigh) in signal_sums if (elow >= blow) and (ehigh <= bhigh) ] )

for ncats in range(2, 5):
    partitions = ncats
    retriever = utils.get_v24_retriever()
    signal = retriever.get_root_files(["H125_"])
    signal_frame = retriever.get_dataframe(signal,variables=[bdtvar],selection=selection)

    edges = np.linspace(-1.0, 1.0, partitions + 1)
    signal_weights = signal_frame.eval("weight").values
    signal_bdt = signal_frame.eval("BDT_Massdiscriminant").values
    signalsums = get_weight_sums(edges, signal_bdt, signal_weights)

    bkg_files = []
    for bkg in ["ZZ", "tXX_VVV"]:
       bkg_files += utils.get_files_given_name(bkg, retriever)

    bkg_frame = retriever.get_dataframe(bkg_files,variables=[bdtvar] ,selection=selection)
    bkg_weights = bkg_frame.eval("weight").values
    bkg_bdt = bkg_frame.eval("BDT_Massdiscriminant").values
    bkgsums = get_weight_sums(edges, bkg_bdt, bkg_weights)
    lowedges = edges[:-1]
    highedges =  edges[1:]
    sigsums = np.array([getsum(blow, bhigh, signalsums) for (blow, bhigh) in zip(edges[:-1], edges[1:])])
    bkgsums = np.array([getsum(blow, bhigh, bkgsums) for (blow, bhigh) in zip(edges[:-1], edges[1:])])

    #alright get the sums in all of the bins
    print("Making Boundaries")
    from sklearn.utils.extmath import cartesian
    bindices = np.arange(0, partitions)
    print("Sorting Boundaries")
    boundaries_tmp_to_split = cartesian([bindices] * (ncats -1))
    boundaries_tmp_to_split = boundaries_tmp_to_split[boundaries_tmp_to_split[:,-1] != partitions]
    boundaries_tmp_to_split = boundaries_tmp_to_split[boundaries_tmp_to_split[:,0] != 0]
    boundaries_tmp_to_split = np.sort(boundaries_tmp_to_split, axis=1)
    print("Boundaries Sorted")
    if ncats > 2:
        boundaries_tmp_to_split = remove_duplicate_rows(boundaries_tmp_to_split)
        duplicates = np.all(np.isclose(boundaries_tmp_to_split[:,0:1], boundaries_tmp_to_split), axis = 1)
        boundaries_tmp_to_split = boundaries_tmp_to_split[np.logical_not(duplicates)]
    proposed_step = int(len(boundaries_tmp_to_split)/2.0)
    if proposed_step != 0: steps = np.arange(0, len(boundaries_tmp_to_split), min(500000, int(len(boundaries_tmp_to_split)/2.0)))
    else: steps = [0, len(boundaries_tmp_to_split)]
    steps[-1] = len(boundaries_tmp_to_split)
    print(steps)
    all_significances = []
    maxsig = -1
    best_binning = None
    for start, stop in zip(steps[:-1], steps[1:]):
        boundaries_tmp = boundaries_tmp_to_split[start:stop]
        boundaries = np.zeros((len(boundaries_tmp), ncats + 1), np.int32)
        boundaries[:,1:-1] = boundaries_tmp
        boundaries[:,0] = np.zeros(len(boundaries_tmp), np.int32)
        boundaries[:,-1] = partitions * np.ones(len(boundaries_tmp), np.int32)
        print("Boundaries Made!")
        possible_cats = len(boundaries)
        #bindices_shaped = np.ones( (partitions, 1) )
        #bindices_shaped[:,0] = bindices
        #bindices_shaped = (np.ones( (partitions, possible_cats) ) * bindices_shaped).swapaxes(0,1)
        bindices_shaped = np.tile(bindices, (possible_cats,1))

        selections_for_cats = []
        for i in range(0, ncats):
            sel_low = boundaries[:, i]
            sel_low_reshaped = np.zeros((possible_cats, 1))
            sel_low_reshaped[:, 0] = sel_low

            sel_hi = boundaries[:, i + 1]
            sel_hi_reshaped = np.zeros((possible_cats, 1))
            sel_hi_reshaped[:, 0] = sel_hi
            
            sel = (bindices_shaped >= sel_low_reshaped) & (bindices_shaped < sel_hi_reshaped)
            selections_for_cats.append(sel)

        print("Computed selections")


        #sigsums_reshaped = np.ones( (partitions, 1) )
        #sigsums_reshaped[:,0] = sigsums
        #sigsums_reshaped = (np.ones( (partitions, possible_cats) ) * sigsums_reshaped).swapaxes(0,1)
        sigsums_reshaped = np.tile(sigsums, (possible_cats,1))

        sig_totals = np.zeros((possible_cats, ncats))
        for i, s in enumerate(selections_for_cats):
            sig_totals[:,i] =  np.sum(sigsums_reshaped * (1.0 * s), axis=1) 
        print("Evaluated signal sums")

        #bkgsums_reshaped = np.ones( (partitions, 1) )
        #bkgsums_reshaped[:,0] = bkgsums
        #bkgsums_reshaped = (np.ones( (partitions, possible_cats) ) * bkgsums_reshaped).swapaxes(0,1)
        bkgsums_reshaped = np.tile(bkgsums, (possible_cats,1))

        bkg_totals = np.zeros((possible_cats, ncats))
        for i, s in enumerate(selections_for_cats):
            bkg_totals[:,i] =  np.sum(bkgsums_reshaped * (1.0 * s), axis=1) 
        print("Evaluated bkg sums")

        safe = ( np.logical_not(np.any(sig_totals <= 2.0, axis=1) ) ) & ( np.logical_not(np.any(bkg_totals <= 0.0,  axis=1) ) )
        print("Checked safety of result")
        if np.sum(1*safe) < 0.5: continue
        
        sig_totals = sig_totals[safe]
        bkg_totals = bkg_totals[safe]
        safe_boundaries = boundaries[safe]

        significances = np.sum(2.0 * (sig_totals + bkg_totals) * np.log(1.0 + sig_totals/bkg_totals) - 2.0 * sig_totals, axis=1) ** 0.5
        print("Evaluated significances")
        best_index = np.argmax(significances)
        all_significances.append(significances)
        this_maxsig = significances[best_index]
        if this_maxsig > maxsig:
            maxsig = this_maxsig
            best_binning = edges[safe_boundaries[best_index]]
            print("Found optimum")

    import pickle as pkl
    with open("Optimal_{}_{}.pkl".format(ncats, partitions), "wb") as f: pkl.dump({"opt":maxsig, "binning":best_binning, "allsigs": all_significances}, f)
