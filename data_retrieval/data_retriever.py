import uproot
import fnmatch
import utils
import numpy as np
import ROOT
import os
import pandas as pd
from datasets import get_x_from_df
from put_scores_in_trees import load_model, load_metadata, get_score_array_from_df, get_prediction_names
import pickle
from models import get_numpy_transform
import shelve
import psutil

def bytes2human(n):
    # http://code.activestate.com/recipes/578019
    # >>> bytes2human(10000)
    # '9.8K'
    # >>> bytes2human(100001221)
    # '95.4M'
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.1f%s' % (value, s)
    return "%sB" % n

def pprint_ntuple(nt):
    for name in nt._fields:
        value = getattr(nt, name)
        if name != 'percent':
            value = bytes2human(value)
        print('%-10s : %7s' % (name.capitalize(), value))


cache_dir = os.path.join(utils.get_scratch_dir(), "cache_dir")
if not os.path.exists(cache_dir): os.makedirs(cache_dir)

class DataRetriever():
    def __init__(self, directory_structure_type1 = [], directory_structure_type2 = [], directory_structure_nominal = [], remote_base_directory = "", root_remote_directory = "", model_retrievers=[], extra_vars = [], extra_varnames=[], signal_remote_base_directory = "", signal_keys = ["_VBFH1", "_ggH1", "_ttH1", "_tH1", "_bbH1", "_ZH1", "_WmH1", "_WpH1", "_tHjb1"], tree_name = "tree_incl_all", weight_var = "weight", scratch_dir = ""):
        self._directory_structure_nominal=directory_structure_nominal
        self._directory_structure_type0=directory_structure_nominal
        self._directory_structure_type1=directory_structure_type1
        self._directory_structure_type2=directory_structure_type2
        self._remote_base_directory = remote_base_directory
        self._root_remote_directory = root_remote_directory
        if signal_remote_base_directory: self._signal_remote_base_directory = signal_remote_base_directory
        else: self._signal_remote_base_directory = remote_base_directory
        self._root_remote_directory
        self._signal_keys = signal_keys
        self._models = model_retrievers
        self._use_cache = True
        self.weight_var = weight_var
        self._scratch_dir = scratch_dir
        self.extra_vars = extra_vars
        self.model_cache = {}
        self.extra_varnames = extra_varnames
        self.safe_cache_list = {}
        self.tree_name = tree_name
        self.residual_correction_cache = {}
        self.remote_cache_check_results = {}
        self.syst_cache_t1 = {}
        self.syst_cache_t2 = None
        self.yield_cache = {}
        self.get_root_files_cache={}
        assert len(extra_vars) == len(extra_varnames)
        assert type(extra_vars) == list
        assert type(extra_varnames) == list 
        if not os.path.exists(self._scratch_dir): os.makedirs(self._scratch_dir)
        self.subsample=False

    def set_subsample(self, sub):
        self.subsample=True
        self.subsamplerate=sub

    def get_list_of_systematics_type2(self):
        if self.syst_cache_t2: 
            return self.syst_cache_t2
        from XRootD import client
        from XRootD.client.flags import DirListFlags
        xrootd_client = client.FileSystem(self._root_remote_directory)
        to_search = os.path.join(self._remote_base_directory, "/".join(self._directory_structure_type2)).replace("__ROOTFILE__", "")
        to_search_mc16a = to_search.replace("__CAMPAIGN__", "mc16a")
        to_search_mc16d = to_search.replace("__CAMPAIGN__", "mc16d")
        to_search_mc16e = to_search.replace("__CAMPAIGN__", "mc16e")
        blacklist = [".",".."]
        blacktokenlist = ["txt"]
        systematics_mc16a = [el.name for el in  xrootd_client.dirlist(to_search_mc16a, DirListFlags.STAT)[1] if el.name not in blacklist and len(el.name.split(".")) > 0 and el.name.split(".")[-1] not in blacktokenlist]
        systematics_mc16d = [el.name for el in  xrootd_client.dirlist(to_search_mc16d, DirListFlags.STAT)[1] if el.name not in blacklist and len(el.name.split(".")) > 0 and el.name.split(".")[-1] not in blacktokenlist]
        systematics_mc16e = [el.name for el in  xrootd_client.dirlist(to_search_mc16e, DirListFlags.STAT)[1] if el.name not in blacklist and len(el.name.split(".")) > 0 and el.name.split(".")[-1] not in blacktokenlist]
        for s in systematics_mc16a:
            if s not in systematics_mc16d: raise ValueError("mc16a systematic {} not in mc16d systematics".format(s))
            if s not in systematics_mc16e: raise ValueError("mc16a systematic {} not in mc16e systematics".format(s))
        for s in systematics_mc16d:
            if s not in systematics_mc16a: raise ValueError("mc16d systematic {} not in mc16a systematics".format(s))
            if s not in systematics_mc16e: raise ValueError("mc16d systematic {} not in mc16e systematics".format(s))
        for s in systematics_mc16e:
            if s not in systematics_mc16a: raise ValueError("mc16e systematic {} not in mc16a systematics".format(s))
            if s not in systematics_mc16d: raise ValueError("mc16e systematic {} not in mc16d systematics".format(s))
        if "NormSystematic" in systematics_mc16a: systematics_mc16a.remove("NormSystematic") #remove the normalization systematics 
        if not self.syst_cache_t2: self.syst_cache_t2 = systematics_mc16a
        return systematics_mc16a


    def check_existence_given_syst_type(self, syst, root_file, syst_type):
        print("Checking that {} exists".format(root_file))
        
        #check that the systematic files exist
        exists = True
        for campaign in ["mc16a", "mc16d", "mc16e"]:
            full_path = self._get_full_path_given_syst_type(root_file, systematic=syst, campaign=campaign, syst_type=syst_type)

            exists = self.check_existence(full_path)
            if not exists:
                print("Previously found to not exist. Continuing")
                break

        return exists

    def has_syst(self, syst, f):
        if type(f) == list:
            files  = f
        else: files = [f]
        files = sorted(files)
        for f in files:
            if syst == "Nominal" and self.check_existence_given_syst_type(syst, f, 0):
                return True

            elif self.check_existence_given_syst_type(syst, f, 2):
                print("{} is type 2".format(syst))
                return True
            elif syst in self.get_list_of_systematics_type1(f):
                print("{} is type 1".format(syst))
                return True
        return False

    def get_safe_branches(self, root_files):
        unsafe_branches = []
        branches_for_file = {}
        for this_file in root_files:
            f = uproot.open(this_file)
            branches_for_file[this_file] = [b.decode("utf-8") for b in f[self.tree_name].allkeys()]
            vector_branches = get_vector_branches(this_file, branches_for_file[this_file], self.tree_name)
            for b in branches_for_file[this_file]:
                if b in unsafe_branches: continue
                if b not in vector_branches: continue
                try: arr = f[self.tree_name].array(b)
                except Exception as e: 
                    unsafe_branches.append(b)
                    continue
                if not hasattr(arr, "_counts"): continue
                unique = np.unique(arr._counts)
                if len(unique) > 1: unsafe_branches.append(b)
                if len(unique) == 1 and unique[0] == 0: unsafe_branches.append(b)
        all_branches = []
        for this_file in root_files: all_branches = list(set(all_branches+branches_for_file[this_file]))
        common_branches = list(set(all_branches).intersection(*[branches_for_file[this_file] for this_file in root_files]))
        for unsafe_branch in unsafe_branches:
            while unsafe_branch in common_branches: common_branches.remove(unsafe_branch)
        return common_branches

    def get_mass_point(self,f):
        if not self.is_signal(f): return -999.0
        else:
            filename = os.path.split(f)[-1]
            if "H122_" in filename: return 122.0
            elif "H123_" in filename: return 123.0
            elif "H123p5_" in filename: return 123.5
            elif "H124_" in filename: return 124.0
            elif "H124p75_" in filename: return 124.75
            elif "H124p5_" in filename: return 124.5
            elif "H124p25_" in filename: return 124.25
            elif "H125_" in filename: return 125.0
            elif "H125J_" in filename: return 125.0
            elif "H125p5J_" in filename: return 125.5
            elif "H124p5J_" in filename: return 124.5
            elif "H126J_" in filename: return 126.0
            elif "H124J_" in filename: return 124.0
            elif "H123J_" in filename: return 123.0
            elif "H127J_" in filename: return 127.0
            elif "tHjb125_" in filename: return 125.0
            elif "H125p5_" in filename: return 125.5
            elif "H126_" in filename: return 126.0
            elif "H126p5_" in filename: return 126.5
            elif "H127_" in filename: return 127.0
            elif "H128_" in filename: return 128.0
            else: raise ValueError("Higgs mass point not found for file {}",format(filename))

    def get_remote_base_directory(self, root_file):
        if self.is_signal(root_file): return self._signal_remote_base_directory
        else: return self._remote_base_directory

    def resolve_full_path(self, root_file, syst_type=0):
        remote_base_directory = self.get_remote_base_directory(root_file)
        full_path = self._root_remote_directory + "/" + remote_base_directory + os.path.join(*getattr(self, "_directory_structure_type{}".format(syst_type)))
        return full_path

    def get_list_of_systematics_type1(self, root_file):
        if root_file in self.syst_cache_t1:
            print("Found syst cache for {}".format(root_file))
            return self.syst_cache_t1[root_file]
        full_path = self.resolve_full_path(root_file, syst_type=1)
        #full_path = self._root_remote_directory + "/" + self._remote_base_directory + os.path.join(*self._directory_structure_type1)
        all_systematics = {}
        for campaign in ["mc16a", "mc16d", "mc16e"]:
            this_file = full_path.replace("__ROOTFILE__", root_file).replace("__CAMPAIGN__", campaign)

            exists = self.check_existence(this_file)
            if not exists:
               print("{} doesnt exist when getting type 1 systematics".format(this_file))
               continue

            if self._use_cache: this_file = self._get_cached_files([this_file])
            assert len(this_file) == 1
            this_file = this_file[0]
            try:
                f = uproot.open(this_file)
                branches = f[self.tree_name].allkeys()
                systematics = []
                for b in branches:
                    b = b.decode("utf-8")
                    if "weight_var" not in b: continue
                    systematics.append(b)
                all_systematics[campaign] = systematics
                del f
            except Exception as e:
                print(e)
                print("WARNING: Couldn't open this file: {}".format(this_file))
        for key in all_systematics:
            for key2 in all_systematics:
                for el in all_systematics[key]:
                    if el not in all_systematics[key2]: raise ValueError("Couldn't find systematic {} from campaign {} in campaign {}".format(el, key, key2))
        if not all_systematics: print("WARNING: Couldn't find systmatics of type 1 for {}".format(root_file))
        if all_systematics: 
            self.syst_cache_t1[root_file] = all_systematics[list(all_systematics.keys())[0]]
            return all_systematics[list(all_systematics.keys())[0]]
        else:
            self.syst_cache_t1[root_file] = []
            return []

    def get_all_systematics(self, root_file):
        if type(root_file) != list: descriptor = [root_file]
        else: descriptor = sorted(root_file)
        descriptor = "_".join([d.replace(".root","") for d in descriptor])
        print(descriptor)
        try:
            with shelve.open(os.path.join(cache_dir, "syst_cache")) as db:
                return db[descriptor]
        except Exception as e:
            to_return = self.get_all_systematics_recursive(root_file)
            try:
                with shelve.open(os.path.join(cache_dir, "syst_cache")) as db:
                    db[descriptor] = to_return
            except Exception as e: pass
            return to_return

    def get_all_systematics_recursive(self, root_file):
        to_return = []
        if type(root_file) == list:
            for el in root_file: to_return = list(set(to_return + self.get_all_systematics(el)))
        else: to_return = list(set((self.get_list_of_systematics_type1(root_file) + self.get_list_of_systematics_type2() + ["Nominal"])))
        return to_return

    def _get_full_path_given_syst_type(self, root_file, systematic="Nominal", campaign = "mc16a", syst_type=2):
        if syst_type == 0:
            full_path = self.resolve_full_path(root_file, syst_type=0)
            full_path = full_path.replace("__ROOTFILE__", root_file)
            full_path = full_path.replace("__CAMPAIGN__", campaign)
            return full_path
        if syst_type == 1:
            full_path = self.resolve_full_path(root_file, syst_type=1)
            full_path = full_path.replace("__ROOTFILE__", root_file)
            full_path = full_path.replace("__CAMPAIGN__", campaign)
            return full_path
        if syst_type == 2:
            full_path = self.resolve_full_path(root_file, syst_type=2)
            full_path = full_path.replace("__ROOTFILE__", root_file)
            full_path = full_path.replace("__SYSTEMATIC__", systematic)
            full_path = full_path.replace("__CAMPAIGN__", campaign)
            return full_path
        else: raise ValueError("{} is not a systematic type. It must be 0, 1 or 2".format(syst_type))

    def _get_full_path(self, root_file, systematic="Nominal", campaign = "mc16a"):
        syst_type = self._get_systematic_type(systematic, root_file)
        return self._get_full_path_given_syst_type(root_file, systematic=systematic, campaign = campaign, syst_type=syst_type)

    def _get_systematic_type(self, systematic, root_file):
        if systematic == "Nominal": return 0
        if self.check_existence_given_syst_type(systematic, root_file, 2):
            print("Systematic {} found for {}".format(systematic, root_file))
            return 2
        elif systematic in self.get_list_of_systematics_type1(root_file):
            print("Systematic {} found for {}".format(systematic, root_file))
            return 1
        else:
            print("WARNING: Systematic {} is not prsent for {}".format(systematic, root_file))
            print("Returning Nominal instead")
            return 0

    def _get_cached_files(self,full_paths):
        import ROOT
        cached_paths = []
        from XRootD import client

        myclient = client.FileSystem(self._root_remote_directory)
        for f in full_paths:
             eos_f = f.replace(self._root_remote_directory, "").replace("//", "/")
             local_f = self._scratch_dir + "/" + eos_f
             extra_folders = os.path.split(local_f)[0]
             if not os.path.exists(extra_folders): os.makedirs(extra_folders)
             local_file = True
             remote_file = f
             if self._root_remote_directory not in remote_file:
                 remote_file = self._root_remote_directory + "//" + remote_file

             if local_f in self.safe_cache_list:
                 print("{} was previously found to be cached and safe".format(local_f))
                 cached_paths.append(local_f)
                 continue

             shelved_path = check_shelved_path(f, local_f)
             if shelved_path:
                 cached_paths.append(local_f)
                 self.safe_cache_list[local_f] = True
                 continue

             try:
                 tf = ROOT.TFile(local_f, "READ")
                 t = tf.Get(self.tree_name)
                 n_entries_local = t.GetEntries()
                 assert t
                 tf.Close()
                 tf_remote = ROOT.TFile.Open(remote_file, "READ")
                 t = tf_remote.Get(self.tree_name)
                 n_entries_remote = t.GetEntries()
                 assert n_entries_local == n_entries_remote
                 tf_remote.Close()
             except Exception as e: 
                 print(e)
                 local_file = False

             if local_file:
                 self.safe_cache_list[local_f] = True
                 add_shelved_path(f, local_f)
                 print("{} is now cached and safe".format(local_f))
                 cached_paths.append(local_f)
                 continue

             print("{} needs to be cached".format(local_f))
             if not local_file:
                  print("Attempting to cache file {}".format(f))
                  for i in range(0, 3):
                      try: 
                          local_f = self._scratch_dir + "/" + eos_f
                          if os.path.exists(local_f): os.system("rm {}".format(local_f))
                          status = myclient.copy(f, local_f, force=False)
                          print("Copying {} to {}".format(f, local_f))
                          tf = ROOT.TFile(local_f, "READ")
                          t = tf.Get(self.tree_name)
                          n_entries_local = t.GetEntries()
                          assert t
                          tf.Close()
                          tf_remote = ROOT.TFile.Open(remote_file, "READ")
                          t = tf_remote.Get(self.tree_name)
                          n_entries_remote = t.GetEntries()
                          assert n_entries_local == n_entries_remote
                          tf_remote.Close()
                          print("Sucessfully made a local copy!")
                          self.safe_cache_list[local_f] = True
                          add_shelved_path(f, local_f)
                          break
                      except Exception as e: 
                          print("Couldn't create a local copy. Using the xrootd instead")
                          print(e)
                          local_f = f
             cached_paths.append(local_f)
        return cached_paths

    def check_existence(self, eos_path):
        if eos_path in self.remote_cache_check_results: return self.remote_cache_check_results[eos_path]
        found = False

        descriptor = eos_path[:]
        descriptor="_".join(descriptor.split("/"))
        try:
            with shelve.open(os.path.join(cache_dir, "file_cache")) as db:
                to_return = db[descriptor]
                self.remote_cache_check_results[eos_path] = to_return
                print("found {} in shelf".format(descriptor))
                return to_return
        except Exception as e:
            print(e)

        for i in range(0, 3):
            try:
                 print("Checking attempt {} for {}".format(i, eos_path))
                 f = ROOT.TFile.Open(eos_path, "READ")
                 print(f)
                 t = f.Get(self.tree_name)
                 print(t)
                 assert f
                 assert t
                 print("{} Found!".format(eos_path))
                 found = True
                 break
            except Exception as e:
                 print(e)
                 print("WARNING With File Retrieval: Couldn't find file {} ".format(eos_path))

        try:
            with shelve.open(os.path.join(cache_dir, "file_cache")) as db:
                print("Updating shelf with {}".format(descriptor))
                db[descriptor] = found
        except Exception as e: pass

        return found

    def get_list_of_fullpaths_files(self, root_files, campaigns, systematic):
         import ROOT
         full_paths = []
         root_files = sorted(root_files)
         for rf in root_files:
            for campaign in campaigns:
                path = self._get_full_path(rf, systematic, campaign)
                exists = self.check_existence(path)
                if not exists:
                    print("WARNING With File Retrieval: Couldn't find file {} for campaign {} and systematic {}".format(rf, campaign, systematic))
                    continue
                else:
                    full_paths.append(path)
         if self._use_cache: full_paths = self._get_cached_files(full_paths)
         return list(set(full_paths))

    def is_signal(self, root_file):
         for key in self._signal_keys:
             if key in os.path.split(root_file)[-1]:
                 return True
         return False

    def get_yield(self, root_files,  systematic="Nominal", campaigns=["mc16a", "mc16d", "mc16e"], selection = ""):
        df = self.get_dataframe(root_files, variables=[self.weight_var], systematic=systematic, campaigns=campaigns, handle_vector_branches=True)
        weights = df[self.weight_var].values
        weights2 = weights ** 2
        return np.sum(weights), np.sum(weights2)**0.5

    def get_dataframe(self, root_files, variables=[], systematic="Nominal", campaigns=["mc16a", "mc16d", "mc16e"], selection = "", handle_vector_branches = True):
        import time
        start_time = time.time()
        reduced_variables = []
        for var in variables: reduced_variables =  merge_variable_lists(reduce_string_to_variables(var),reduced_variables)

        insert_mass_point = False
        all_variables = not bool(reduced_variables)

        full_path_files = self.get_list_of_fullpaths_files(root_files, campaigns, systematic)
        print(full_path_files)

        if all_variables: insert_mass_point = True
        if "mass_point" in reduced_variables: insert_mass_point = True

        selection_variables = reduce_string_to_variables(selection)
        if "mass_point" in selection_variables: insert_mass_point = True

        for var in self.extra_vars: 
            reduced_variables = merge_variable_lists(reduced_variables, reduce_string_to_variables(var))


        df_set = False
        if all_variables: all_safe_branches = self.get_safe_branches(full_path_files)
        dfs = []
        for this_file in full_path_files:
            print("Opening {} for systematic {}".format(this_file, systematic))
            syst_type = self._get_systematic_type(systematic, os.path.split(this_file)[-1])
            print("Syst type is {}".format(syst_type))

            reduced_variables_for_file = merge_variable_lists(reduced_variables, selection_variables)
            reduced_variables_for_file = merge_variable_lists(reduced_variables_for_file, [self.weight_var])
            regressor_reduced_variables = self._get_regressor_variables([this_file])
            if "mass_point" in regressor_reduced_variables:
                regressor_reduced_variables.remove("mass_point")
                insert_mass_point = True
            reduced_variables_for_file = merge_variable_lists(reduced_variables_for_file, regressor_reduced_variables)
            for varname in self.extra_varnames:
                while varname in reduced_variables_for_file: reduced_variables_for_file.remove(varname)
            if reduced_variables_for_file and syst_type == 1: reduced_variables_for_file = merge_variable_lists(reduced_variables_for_file, [systematic])

            if insert_mass_point and "mass_point" in reduced_variables: reduced_variables_for_file.remove("mass_point")

            if all_variables: all_branches = list(set(all_safe_branches + reduced_variables_for_file))
            f = uproot.open(this_file)
            n_entries = f[self.tree_name].numentries
            if n_entries == 0:
                print("Skipping {} because there are no entries".format(this_file))
                continue
            if all_variables: df_tmp = f[self.tree_name].pandas.df(all_branches)
            else: 
                reduced_variables_for_file = self.reduce_to_vectorized_variables(reduced_variables_for_file, [this_file])
                predictions =  self._get_prediction_names()
                print(predictions)
                for p in predictions: 
                    while p in reduced_variables_for_file: 
                        reduced_variables_for_file.remove(p)
                print(reduced_variables_for_file)
                df_tmp, n_entries = get_df_safe(f, self.tree_name, reduced_variables_for_file)

            #print("Finished opening")
            for col in df_tmp.columns:
                if col != "event_type":
                    df_tmp[col] = df_tmp[col].astype(float)

            df_tmp = df_tmp.reset_index()
            vector_branches = get_vector_branches(this_file, reduced_variables_for_file, self.tree_name)
            if vector_branches:
                df_tmp = flatten_vector_branches(df_tmp, vector_branches)
            if insert_mass_point: 
                df_tmp["mass_point"] = self.get_mass_point(this_file) * np.ones(len(df_tmp))
            if syst_type == 1:
                df_tmp[self.weight_var] = df_tmp.eval("{} * {}".format(self.weight_var, systematic)).values
                cols = list(df_tmp.columns)
                cols.remove(systematic)
                df_tmp = df_tmp[cols]

            df_tmp["channel"] = np.ones(len(df_tmp)) * get_channel(this_file)
            df_tmp["event_count"] = np.arange(0, len(df_tmp) ) #add information about what entry in the root file this was. BEFORE selection

            df_tmp = self._add_predictions(df_tmp)

            for var, varname in zip(self.extra_vars, self.extra_varnames):
                 df_tmp[varname] = df_tmp.eval(var).values
                 if "index" in varname:
                     df_tmp[varname] = df_tmp[varname].astype('int32')

            if selection != "": 
                df_tmp = df_tmp.query(selection)

            if self.subsample: #how many events to take for one event in data
               nevents = float(len(df_tmp))
               nweighted = np.sum(df_tmp.eval(self.weight_var).values)
               ratio = nevents/nweighted
               if ratio > self.subsamplerate:
                   df_tmp = df_tmp.reset_index(drop=True)
                   remainder_rate = int(ratio/self.subsamplerate)
                   print("subsampling at a rate of {}".format(remainder_rate))
                   df_tmp = df_tmp[df_tmp.index % remainder_rate == 0]
                   df_tmp[self.weight_var] = df_tmp.eval(self.weight_var).values * (nweighted / np.sum(df_tmp.eval(self.weight_var).values))

            dfs.append(df_tmp)
            #print('MEMORY\n------')
            #pprint_ntuple(psutil.virtual_memory())
            #print('\nSWAP\n----')
            #pprint_ntuple(psutil.swap_memory())

        dfs = pd.concat(dfs,sort=False)
        #print('MEMORY\n------')
        #pprint_ntuple(psutil.virtual_memory())
        #print('\nSWAP\n----')
        #pprint_ntuple(psutil.swap_memory())

        end_time = time.time()
        print("Full frame retrieval took {} seconds".format(end_time-start_time))

        return dfs

    def _get_regressor_variables(self, root_files):
        reduced_variables = []
        for model_retriever in self._models:
            model, metadata = model_retriever()
            if model in self.model_cache:
                model,metadata = self.model_cache[model]
            else:
                model_name  = model
                model = load_model(model_name)
                metadata = load_metadata(metadata)
                self.model_cache[model_name] = model, metadata
            variables = metadata["variables"]
            reduced_variables = self.reduce_to_vectorized_variables(reduced_variables, root_files)
            for v in variables: 
                if type(v) == list:
                    for v2 in v: reduced_variables += reduce_string_to_variables(v2)
                else: reduced_variables += reduce_string_to_variables(v)
        reduced_variables = list(set(reduced_variables)) #remove duplicates in the list
        reduced_variables = self.reduce_to_vectorized_variables(reduced_variables, root_files)
        return reduced_variables

    def reduce_to_vectorized_variables(self, reduced_variables, root_files):
        vector_branches = {}
        for rf in root_files:
            branches = get_all_branches_decoded(rf, self.tree_name)
            vector_branches[rf] = get_vector_branches(rf, branches, self.tree_name)

        for rf1 in vector_branches:
            for rf2 in vector_branches:
                for el in vector_branches[rf1]: 
                    assert el in vector_branches[rf2]

        vector_branches = vector_branches[list(vector_branches.keys())[0]]

        matches = {}
        match_lengths = {}
        for rv in reduced_variables: match_lengths[rv] = 0

        for vb in vector_branches:
            for rv in reduced_variables:
                if vb in rv:
                    match_length = 0
                    while match_length < len(vb) and vb[match_length] == rv[match_length]: match_length += 1
                    if match_length > match_lengths[rv]: match_lengths[rv], matches[rv] = match_length, vb

        vectorized_variables = []
        for var in reduced_variables:
            if var in matches and matches[var] not in vectorized_variables: vectorized_variables.append(matches[var])
            if var not in matches and var not in vectorized_variables: vectorized_variables.append(var)

        return vectorized_variables

    def get_extra_model_functions(self, models):
        extra_models = []
        for m in models:
            metadata, model = m + ".pkl", m + ".h5"
            metadata = load_metadata(metadata)
            if "extra_models" in metadata and  type(metadata["extra_models"]) == list: 
                extra_models = self.get_extra_model_functions(metadata["extra_models"]) + extra_models
            extra_models.append(lambda : (m + ".h5", m + ".pkl"))
        return extra_models

    def register_extra_models(self, models, corrections=[]):
        self._models = self._models + self.get_extra_model_functions(models)
        if corrections:
            for m, c in zip(models, corrections):
                with open(c, "rb") as f: self.residual_correction_cache[m] = pickle.load(f)

    def _get_prediction_names(self):
        all_predictions = []
        for model_retriever in self._models:
            model, metadata = model_retriever()
            if model in self.model_cache:
                model,metadata = self.model_cache[model]
            else:
                model_name  = model
                model = load_model(model_name)
                metadata = load_metadata(metadata)
                self.model_cache[model_name] = model, metadata
            predictions = get_prediction_names(metadata, model, get_extra_description = True)
            all_predictions += predictions
        return all_predictions

    def _add_predictions(self, df):
        for model_retriever in self._models:
            model, metadata = model_retriever()
            if model in self.model_cache:
                model_name = model
                model,metadata = self.model_cache[model]
            else:
                model_name  = model
                model = load_model(model_name)
                metadata = load_metadata(metadata)
                self.model_cache[model_name] = model, metadata
            scores, predictions = get_score_array_from_df(metadata, model, df, get_extra_description=True)
            for i, prediction in enumerate(predictions):
                transformed_score = get_numpy_transform(scores[:,i], varname = prediction)
                if model_name.replace(".h5", "") in self.residual_correction_cache:
                    p1 = self.residual_correction_cache[model_name.replace(".h5", "")]["p1"][0]
                    p0 = self.residual_correction_cache[model_name.replace(".h5", "")]["p0"][0]
                    print("Applying correction with p1 = {} and p0 = {}".format(p1, p0))
                    transformed_score = p1 * transformed_score + p0
                df[prediction] = transformed_score
                print("Prediction for {} is {}".format(prediction, transformed_score))
        return df

    def get_root_files(self, wildcards, skip = []):
        #if type(wildcards) == list: descr = "_".join(wildcards) + "__SKIP__" + "_".join(skip)
        #else: descr = "_".join([wildcards]) + "__SKIP__" + "_".join(skip)
        #if descr in self.get_root_files_cache: return self.get_root_files_cache[descr]
        from XRootD import client
        from XRootD.client.flags import DirListFlags
        xrootd_client = client.FileSystem(self._root_remote_directory)
        to_search = os.path.join(self._remote_base_directory, "/".join(self._directory_structure_nominal))
        to_search_mc16a = to_search.replace("__ROOTFILE__", "")
        to_search_mc16d = to_search.replace("__ROOTFILE__", "")
        to_search_mc16e = to_search.replace("__ROOTFILE__", "")
        to_search_mc16a = to_search_mc16a.replace("__CAMPAIGN__", "mc16a")
        to_search_mc16d = to_search_mc16d.replace("__CAMPAIGN__", "mc16d")
        to_search_mc16e = to_search_mc16e.replace("__CAMPAIGN__", "mc16e")
        files_mc16a = [el.name for el in  xrootd_client.dirlist(to_search_mc16a, DirListFlags.STAT)[1]]
        files_mc16d = [el.name for el in  xrootd_client.dirlist(to_search_mc16d, DirListFlags.STAT)[1]]
        files_mc16e = [el.name for el in  xrootd_client.dirlist(to_search_mc16e, DirListFlags.STAT)[1]]
        matched_files_mc16a = []
        matched_files_mc16d = []
        matched_files_mc16e = []
        if type(wildcards) == list:
            for w in wildcards:
                matched_files_mc16a += fnmatch.filter(files_mc16a, "*" + w + "*")
                matched_files_mc16d += fnmatch.filter(files_mc16d, "*" + w + "*")
                matched_files_mc16e += fnmatch.filter(files_mc16e, "*" + w + "*")
        elif type(wildcards) == str:
                matched_files_mc16a = fnmatch.filter(files_mc16a, "*" + wildcards + "*")
                matched_files_mc16d = fnmatch.filter(files_mc16d, "*" + wildcards + "*")
                matched_files_mc16e = fnmatch.filter(files_mc16e, "*" + wildcards + "*")
        else: raise TypeError("Wildcard {} is not type list or string".format(wildcards))
        matched_files_mc16a = list(set(matched_files_mc16a))
        matched_files_mc16d = list(set(matched_files_mc16d))
        matched_files_mc16e = list(set(matched_files_mc16e))

        #find the common elements among all three lists
        matched_files = list( set(matched_files_mc16a).intersection(*[matched_files_mc16a, matched_files_mc16d, matched_files_mc16e]) )

        skipped_files = []
        for f in matched_files:
            to_skip = False
            for s in skip:
                if s in f:
                    to_skip = True
            if not to_skip: skipped_files.append(f)

        #self.get_root_files_cache[descr] = skipped_files
        return skipped_files

def get_df_safe(f, tree_name, reduced_variables):
    #arrays = f[tree_name].arrays(reduced_variables)
    n_entries = f[tree_name].numentries
    unsafe = np.zeros(0, dtype = np.int)
    import time
    start_time = time.time()
    for rv in reduced_variables:
        if not hasattr(f[tree_name][rv.encode("utf-8")], "_fClassName"): continue
        if "vector" not in f[tree_name][rv.encode("utf-8")]._fClassName.decode("utf-8"): continue
        arr = f[tree_name].array(rv)
        if not hasattr(arr, "_counts"): continue
        uniqlos = np.unique(arr._counts)
        if len(uniqlos) == 1: continue
        else:
            counts = []
            for el in uniqlos:
                counts.append(np.sum(1* (arr._counts == el) ) )
            counts = np.array(counts)
            max = np.argmax(counts)
            these_unsafes = np.where(arr._counts != uniqlos[max])[0]
            unsafe = np.concatenate([unsafe, these_unsafes], axis=0)
    unsafe = np.sort(np.unique(unsafe))
    if len(unsafe) > 0:
        print("WARNING: Skipping {} events because they are unsafe for reading. The vector branches vary in length".format(len(unsafe)))
        unsafe_count = len(unsafe)
        unsafe = [0] + list(unsafe) + [n_entries]
        dfs = []
        i = 0
        for start, stop in zip(unsafe[:-1], unsafe[1:]):
            if i != 0: start += 1
            if start == stop: continue
            df = f[tree_name].pandas.df(reduced_variables,entrystart=start,entrystop=stop)
            dfs.append(df)
            i += 1
        dfs = pd.concat(dfs)
        assert len(dfs)/4 == n_entries - unsafe_count
        end_time = time.time()
        print("Frame retrieval took {} seconds".format(end_time-start_time))
        return dfs, n_entries - unsafe_count
    #there is no problem with variable length arrays
    df = f[tree_name].pandas.df(reduced_variables), n_entries
    end_time = time.time()
    print("Frame retrieval took {} seconds".format(end_time-start_time))
    return df 

def merge_variable_lists(a, b):
    return list(set(a+b))

def reduce_string_to_variables(selection):
    keys_to_replace = ["(", ")", " and ", " or ", "+", "-", "/", "*","==", "<", ">", "<=", ">="," ",".","="]
    for key in keys_to_replace:
        selection = selection.replace(key, ",")
    variables = selection.split(",")
    while "" in variables: variables.remove("")

    #remove those keys composed only of numbers
    numbers = [str(i) for i in range(0, 10)] + ["."]
    to_remove = []
    for var in variables:
        all_numbers = True
        for char in var:
            if char not in numbers: all_numbers = False
        if all_numbers: to_remove.append(var)
    for var in to_remove:
        while var in variables: variables.remove(var)
    return list(set(variables))

def flatten_vector_branches(df, vec_branches, length=4):
    branches = df.columns
    non_vector_branches = [b for b in branches if b not in vec_branches]
    df_flat = df[non_vector_branches].query("subentry == 0")
    df_flat = df_flat[ [b for b in df_flat.columns if "subentry" != b] ]
    for vec_branch in vec_branches:
        for i in range(0, 4):
             df_flat[vec_branch + "_{}".format(i + 1)] = df.query("subentry == {}".format(i))[vec_branch].values
    df_flat = df_flat.set_index("entry")
    return df_flat

def check_shelved_path(f, local_f):
   descriptor = f
   try:
       with shelve.open(os.path.join(cache_dir, "safe_file_cache")) as db:
           if f in db and local_f == db[f]: return True
           if f in db and local_f != db[f]:
               raise ValueError("{} is alredy in the safe file cache, but it's local filename {} did not match that one in the cache {}".format(f, local_f, df[f]))
   except Exception as e:
       print(e)
   return False

def add_shelved_path(f, local_f):
    descriptor = f
    try:
    	with shelve.open(os.path.join(cache_dir, "safe_file_cache")) as db:
           db[f] = local_f
    except Exception as e:
        print(e)

def get_all_branches(full_path):
    for t in uproot.tree.iterate(full_path, self.tree_name,entrysteps=1):
        return [b for b in list(t.keys())]

def get_all_branches_decoded(full_path, tree_name):
    t = uproot.open(full_path)[tree_name]
    return [b.decode("utf-8") for b in list(t.keys())]

def get_vector_branches(full_path, branches, tree_name):
    to_return = []
    t = uproot.open(full_path)[tree_name]
    
    for b in list(t.keys()):
        if b.decode("utf-8") not in branches: continue
        if not hasattr(t[b], "_fClassName"): continue
        if "vector<" in t[b]._fClassName.decode("utf-8"):
            to_return.append(b.decode("utf-8"))
    return to_return

def is_data(f):
    #TODO implement this...
    return False

def get_channel(f):
    if is_data(f): return None
    filename = os.path.split(f)[-1]
    return int(filename.split(".")[1])

def main():
    retriever = utils.get_v24_retriever()

if __name__ == "__main__":
    main()
