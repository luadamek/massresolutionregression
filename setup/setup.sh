export MassRegressionResolutionDir=$PWD
source venv_MassRegressionResolution/bin/activate
export X509_USER_PROXY=${MassRegressionResolutionDir}/grid_proxy
source ${MassRegressionResolutionDir}/setup/setup_env.sh
export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2016/bin/x86_64-linux:$PATH
source RooFitExtensions/setup.sh
chmod 777 ${X509_USER_PROXY}
#python tests/test_DCB_layer.py
