import argparse
import os
import glob
from multiprocessing import Pool
import multiprocessing

def get_jobs(wcard, sd):
    wildcards = ["*" + wcard + "*", wcard + "*", "*" + wcard]

    directories = []
    for wildcard in wildcards:
        directories_found = glob.glob(os.path.join(sd, wildcard))
        for directory in directories_found:
            if directory not in directories: directories.append(directory)

    tmp_directories = []
    for d in directories:
        if sd not in d: tmp_directories.append(os.path.join(sd, d))
        else: tmp_directories.append(d)

    directories = tmp_directories
    del tmp_directories
    return directories

def check_if_finished(d):
    print("Checking {}".format(d))

    filename = os.path.join(d, "{}_slurm.err".format(os.path.split(d)[-1]))
    if os.path.exists(filename):
        with open(filename, "r") as f: lines = f.readlines()
    else:
        return False
            
    for l in lines:
        print(l)
        if "Disk quota exceeded" in l:
            print("{} didn't finish. Disk Quota exceeded".format(d))
            return False
        if "Error in <TFile::ReadKeys>" in l:
            print("{} didn't finish. There was an error writing to the file".format(d))
            return False

    filename = os.path.join(d, "{}_slurm.out".format(os.path.split(d)[-1]))
    if os.path.exists(filename):
        with open(filename, "r") as f: lines = f.readlines()
    else:
        return False
            
    for l in lines:
        if "Disk quota exceeded" in l:
            print("{} didn't finish. Disk Quota exceeded".format(d))
            return False
        if "FINISHED" in l:
            print("{} finished".format(d))
            return True

    print("{} didn't finish".format(d))
    return False

def check_completion(wcard, search_directory, skipcard=""):

    directories = get_jobs(wcard, search_directory)
    if skipcard: skip = get_jobs(skipcard, search_directory)
    else: skip = []
    for d in skip:
        while d in directories: directories.remove(d)

    p = Pool(len(os.sched_getaffinity(0)))
    finished = p.map(check_if_finished, directories)
    
    print("The following jobs did not complete")
    unfinished = []
    for d, finished in zip(directories, finished):
        if not finished:
            print(d)
            unfinished.append(d)

    print("{} of {} finished".format(len(directories) - len(unfinished), len(directories)))

    input("Now printing the finished jobs:")
    for d in sorted(directories):
        if d in unfinished: continue
        print(d)

    return unfinished

def submit_job(job):
    batch_script = glob.glob(os.path.join(job,"*_slurm.sh"))
    assert len(batch_script) == 1
    batch_script = os.path.abspath(batch_script[0])
    if job not in batch_script: batch_script = os.path.abspath(os.path.join(job, batch_script))
    outfile_err = os.path.abspath(os.path.join(job, "{}_slurm.err".format(os.path.split(job)[1])))
    outfile_out = os.path.abspath(os.path.join(job, "{}_slurm.out".format(os.path.split(job)[1])))
    #os.system("rm {}".format(outfile_err))
    #os.system("rm {}".format(outfile_out))
    slurm_command = "sbatch --mem=30000M --time=12:00:00 --error={} --output={} {}".format( outfile_err , outfile_out, batch_script )
    pwd = os.getenv("PWD")
    home = os.getenv("HOME")
    commands = ["cd {}".format(home), slurm_command, "cd {}".format(pwd)]
    for command in commands:
        print(command)
        os.system(command)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-wc", "--wildcard", help="the group of classifiers to search for", type=str, dest="wcard", default="")
    parser.add_argument("-sc", "--skipcards",required=False, help="the group of classifiers to search for", type=str, dest="skipcard", default="")
    parser.add_argument("-sd", "--search_directory", help="the directory in which to search", type=str, dest="sd", default="/scratch/ladamek/ModelFitting/")
    parser.add_argument("-resub", "--resub", help="whether or not to resubmit the jobs", default = False, action="store_true", dest="resub")
    args = parser.parse_args()

    assert args.wcard != ""
    unfinished_jobs = check_completion(args.wcard, args.sd, args.skipcard)

    print("Do resubmission: {}".format(args.resub))

    if args.resub:
        for d in unfinished_jobs: submit_job(d)

