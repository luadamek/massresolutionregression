import argparse
from create_and_submit_training_jobs import job_dir
import os
import glob
from check_for_completion import check_completion, get_jobs
import utils
from pop_model import pop_model
import pickle
from datetime import datetime
from train import train
from post_processing import get_model_metadata, get_complete_list_of_classifiers

parser = argparse.ArgumentParser(description="Train Residual Correction Network")
parser.add_argument('--wildcard', '-wc', type=str, required=True, help="The wildcards to search for when deriving the residual correction networks")
args = parser.parse_args()

model_flavour = "DCB_residual_correction_network"
model_directory = utils.get_model_save_dir()
flavours = ["4mu", "2e2mu", "2mu2e", "4e"]
models_to_correct, metadatas_to_correct = get_complete_list_of_classifiers(args.wildcard)
models = []
for m in models_to_correct:
     if "corrected" not in m: models.append(m)
models_to_correct=models
names = [os.path.split(m)[-1].replace(".h5","") for m in models_to_correct]

for model, metadata, name in zip(models_to_correct, metadatas_to_correct, names):
    stamp = name.replace("_popped", "") + "_corrected" #create a new name for the model
    print("Training {}".format(stamp))
    with open(metadata, "rb") as md: 
        import pickle
        md = pickle.load(md)
        print(list(md.keys()))
        files = md["files"]
    file_tokens = []
    for f in files:
        file_tokens.append(os.path.split(f)[-1])
    file_tokens = ",".join(file_tokens)

    file_directory = name
    for f in flavours:
        file_directory = file_directory.replace("_{}".format(f), "")

    for flavour in flavours:
        if "_{}".format(flavour) not in name: continue
        commands = ["#!/bin/sh"] 
        commands += utils.get_setup_commands()
        train_command = "python {wd}/train_nn/train.py --flavour={fl} --stamp={st} --model_flavour={mf} --file_tokens={ft} --var_selection=truth_eta_pt --extra_models  {em} --lr {lr}".format(wd=os.getenv("MassRegressionResolutionDir"), fl=flavour, st=stamp, ft=file_tokens, mf = model_flavour, em=metadata.replace(".pkl",""), lr=md["lr"][0])
        commands.append(train_command)

        if not os.path.exists(job_dir):
            os.makedirs(job_dir)

        sub_dir = os.path.join(job_dir, stamp)
        if not os.path.exists(sub_dir):
            os.makedirs(sub_dir)

        output_file = os.path.join(sub_dir, "output.out")
        error_file = os.path.join(sub_dir, "error.out")
        script = os.path.join(sub_dir, "submit.sh")
        batch_script = os.path.join(sub_dir, "submit_slurm.sh")
        with open(script, "w") as f:
             for c in commands:
                 f.write(c + "\n")
                 print(c)
             f.close()

        #os.system("batchScript \"source {}\" -O {}".format(script, batch_script))
        os.system("sbatch --mem=10000M --time=2:00:00 --gres=gpu:1 --output={} --error={} {}".format(output_file, error_file, script))
