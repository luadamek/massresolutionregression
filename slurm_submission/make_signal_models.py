import signal_model
import utils
import os

retriever = utils.get_v24_retriever()

files = retriever.get_root_files(["_ggH125_"])
assert len(files) == 1
systematics = []
systematics = retriever.get_all_systematics(files)
if "Nominal" in systematics:
    while "Nominal" in systematics: systematics.remove("Nominal")
systematics = ["Nominal"] + systematics


mass_vars = ["m4l_constrained", "m4l_unconstrained"]
#mass_vars = ["m4l_unconstrained"]
channels = ["4mu,4e,2e2mu,2mu2e"]
selections = ["Incl,BDT1,BDT2,BDT3,BDT4"]

fitting_directory = os.path.join("/scratch/ladamek/", "ModelFitting")
if not os.path.exists(fitting_directory): os.makedirs(fitting_directory)

for syst in systematics:
    for mass_var in mass_vars:
        for channel in channels:
             for selection in selections:
                 commands = []
                 commands.append("cd {}".format(os.getenv("MassRegressionResolutionDir")))
                 commands.append("source ./setup.sh")
                 commands.append("export USER={}".format(os.getenv("USER")))
                 commands.append("python WorkspaceManager/signal_model.py --syst {syst} --channel {channel} --mass_var {mass_var} --category {category}".format(syst=syst, mass_var=mass_var, channel=channel, selection=selection,category=selection))
                 f = "Signal_Model_{}_{}_{}_{}.sh".format(syst, channel.replace(",","_"), mass_var, selection.replace(",","_"))
                 folder = os.path.join(fitting_directory, f.split(".")[0])
                 if not os.path.exists(folder): os.makedirs(folder)
                 f = os.path.join(folder, f)
                 with open(f, "w") as script_f:
                     for c in commands:
                         script_f.write(c + "\n")
                         print(c)
                 batch_script = f.replace(".sh", "_slurm.sh")
                 error = batch_script.replace(".sh",".err")
                 output = batch_script.replace(".sh",".out")
                 os.system("rm {}".format(error))
                 os.system("rm {}".format(output))
                 os.system("batchScript \"source {}\" -O {}".format(f, batch_script))
                 os.system("sbatch --mem=15000M --time=07:00:00 --error={} --output={} {}".format(error, output, batch_script))
