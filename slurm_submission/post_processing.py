import argparse
from nll_layer import NllLayer
import os
import glob
from check_for_completion import check_completion, get_jobs
from pop_model import pop_model
import pickle
import utils
from check_for_completion import check_completion, get_jobs

def get_model_metadata(base_dir, skip = [], contains = []):
    model_file = None
    metadata = None
    print("Searching {}".format(base_dir))
    print("Skipping {}".format(skip))
    print("Must contain {}".format(contains))
    regressor_files = glob.glob(os.path.join(base_dir, "*.pkl")) + glob.glob(os.path.join(base_dir, "*.h5"))
    cleaned_regressor_files = []
    for regressor_file in regressor_files:
        to_continue = False
        for el in skip:
            if el in regressor_file.split("/")[-1]:  to_continue = True
        for el in contains:
            if el not in regressor_file.split("/")[-1]: to_continue = True
        if to_continue: continue
        cleaned_regressor_files.append(regressor_file)
    regressor_files = cleaned_regressor_files
    print("Found {}".format(regressor_files))
    print("\n")
    success = len(regressor_files) == 2
    for el in regressor_files:
        if ".h5" in el: model_file = el
        elif ".pkl" or ".pickle" in el: metadata = el
    if success:
        if not base_dir in model_file: model_file = os.path.join(base_dir, model_file)
        if not base_dir in metadata: metadata = os.path.join(base_dir, metadata)
    if not success: print("Failed to find!")
    print("\n" * 5)
    return model_file, metadata, success

def get_score_retriever(model, metadata):
    return lambda : model, metadata

def create_popped_classifiers(wcard, skip = []):
    models = glob.glob(os.path.join(utils.get_regressor_dir(), wcard))
    unfinished = check_completion(wcard, "/scratch/ladamek/NetworkTraining/")
    popped_models, popped_metadatas = [], []
    for m in models:
       if m in unfinished: 
           print("Skipping {} because the job didn't complete".format(m))
           continue
       model, metadata, success = get_model_metadata(m, skip = ["_popped"] + skip, contains = [os.path.split(m)[-1]])
       if success:
           popped_model, popped_metadata = pop_model(metadata, model, rm_truth = False)
           popped_models.append(popped_model)
           popped_metadatas.append(popped_metadata)
           rm_truth = "_truth" in os.path.split(m)[-1] and "_corrected" not in os.path.split(m)[-1]
           if rm_truth: 
               popped_notruth_model, popped_notruth_metadata = pop_model(metadata, model, rm_truth = True)
               popped_models.append(popped_notruth_model)
               popped_metadatas.append(popped_notruth_metadata)
       else: print("Failed to retrieve {}\n\n\n".format(m))
    return popped_models, popped_metadata

def prepare_list_of_classifiers(wcard, skip=[], contains = [],bd="/scratch/ladamek/NetworkTraining/"):
    models = glob.glob(os.path.join(utils.get_regressor_dir(), wcard + "*"))
    unfinished = check_completion(wcard, bd)
    to_return_models, to_return_metadatas = [], []
    for m in models:
       if m in unfinished: 
           print("Skipping {} because the job didn't complete".format(m))
           continue
       model, metadata, success = get_model_metadata(m, skip = skip, contains = [os.path.split(m)[-1]] + contains)
       if not success: print("failed to retrieve {}".format(m))
       if success:
           to_return_models.append(model)
           to_return_metadatas.append(metadata)
    return to_return_models, to_return_metadatas

def get_complete_list_of_classifiers(wcard, bd="/scrach/ladamek/NetworkTraining/"):
    print("Getting list of classifiers for {}".format(wcard))
    models_d, metadatas_d = prepare_list_of_classifiers(wcard, contains=["_popped", "_corrected","_truth_to_reco"], skip=[],bd=bd)
    models_a, metadatas_a = prepare_list_of_classifiers(wcard, contains=["_popped", "_truth_to_reco"], skip=["_corrected"],bd=bd)
    models_b, metadatas_b = prepare_list_of_classifiers(wcard, contains=["_popped"], skip=["_corrected", "_truth_to_reco"],bd=bd)
    models_c, metadatas_c = prepare_list_of_classifiers(wcard, contains=["_popped", "_corrected"], skip=["_truth_to_reco"],bd=bd)
    return models_a + models_b + models_c + models_d, metadatas_a + metadatas_b + metadatas_c + metadatas_d

if __name__ == "__main__":
    #models, metadatas = create_popped_classifiers("DCB_Models_CompareLRandNorm_NewLoss_2020_3_19_16_43_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_Deep_Pyramid_4mu_truth_eta_pt_5e-05_*", skip = [])
    #models, metadatas = create_popped_classifiers("DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29*", skip = [])
    #models, metadatas = create_popped_classifiers("DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4mu_truth_eta_pt_0_005_MPNormed*", skip=[])
    #models, metadatas = create_popped_classifiers("DCB_Models_*Norm*", skip=[])
    #models, metadats = create_popped_classifiers("DCB_Models_CompareLRandNorm_NewLosst4_2020*corrected*")
    #models, metadatas = create_popped_classifiers("DCB_Models_CompareLRandNorm_NewLosst4_2020_4_23_16_31*")
    models, metadats = create_popped_classifiers("*_bare_*")
