import load_workspace
import os
import glob

wsname = "AllSystematics"

ws_names = glob.glob("/home/ladamek/MassResolutionRegression/Workspaces/Sig*")
ws_names = list(set([os.path.split(ws)[-1].replace(".pkl", "").replace(".root", "") for ws in ws_names]))
time = "04:00:00"
local = False
bootstrap = True
if bootstrap: time="08:00:00"
do_prefit = True
do_postfit = False
check_finished = False

def check_if_finished(jobdir):
   print("Checking {}".format(jobdir))
   output = os.path.join(jobdir, "output.out")
   error = os.path.join(jobdir, "error.err")
   result = os.path.join(jobdir, "result.pkl")

   with open(output, "r") as f: lines = f.readlines()
   finished = False
   for l in lines[::-1]:
      if "FINSIHED THE JOB! :)" in l:
          print("found finished line")
          finished = True
      if finished: break
   if not finished: print("failed to find the finished line in the jobfile")

   if not os.path.exists(os.path.join(jobdir, "result.pkl")):
       print("failed to find output file")
       return False

   import pickle
   try:
      with open(os.path.join(jobdir, "result.pkl"), "rb") as f:
          stuff = pickle.load(f)
   except Exception as e:
       print(e)
       print("failed to load output file")
       return False

   return finished
   

for wsname in ws_names:
    if "Syst" in wsname: continue
    #if "Incl" in wsname: continue
    pws = load_workspace.pyworkspace(wsname)
    print(pws.nuis)

    parameters = [el.GetName() for el in pws.nuis] + ["mass_point"]
    save_directory = "/project/def-psavard/ladamek/MassFitResult/{}".format(wsname)

    if not os.path.exists(save_directory): os.makedirs(save_directory)

    if do_prefit:
        for p in parameters:
            if not bootstrap: jobdir = os.path.join(save_directory, p)
            else: jobdir = os.path.join(save_directory, "bootstrap", p)
            if check_finished:
                finished = check_if_finished(jobdir)
                if not finished: print("{} didn't finished".format(jobdir))
            if not check_finished or (not finished and check_finished):
                if not os.path.exists(jobdir): os.makedirs(jobdir)
                output = os.path.join(jobdir, "output.out")
                if os.path.exists(output): os.system("rm {}".format(output))
                error = os.path.join(jobdir, "error.err")
                if os.path.exists(error): os.system("rm {}".format(error))
                script = os.path.join(jobdir, "run.sh")
                slurm_script = os.path.join(jobdir, "run_slurm.sh")
                lines = ["#!/bin/sh", "cd /home/ladamek/MassResolutionRegression", "source ./setup.sh"]
                if not bootstrap: lines.append("python WorkspaceScanning/load_workspace.py --slurmjob --wsname {wsname} --mps {p} --directory {d}".format(p=p, d=jobdir, wsname=wsname))
                else: lines.append("python WorkspaceScanning/load_workspace.py --slurmjob --wsname {wsname} --mps {p} --directory {d} --bootstrap".format(p=p, d=jobdir, wsname=wsname))
                with open(script, "w") as f:
                    for l in lines: f.write(l + "\n")
                if local: os.system("source {}".format(script))
                else:
                    os.system("batchScript \"source {}\" -O {}".format(script, slurm_script))
                    print("batchScript \"source {}\" -O {}".format(script, slurm_script))
                    os.system("sbatch --mem=30000M --time={time} --error={err} --output={out} {script}".format(err=error, out=output, script=slurm_script, time=time))

        parameters = [el.GetName() for el in pws.nuis if "_Norm" not in el.GetName() and not ("_scale" in el.GetName() and "prediction" in el.GetName() ) ] 
        fixtype = "PreFit"
        for fixname, val in zip(["1up", "1down"], [1.0, -1.0]):
            for p in parameters:
                if not bootstrap: jobdir = os.path.join(save_directory, p)
                else: jobdir = os.path.join(save_directory, "bootstrap", p)
                jobdir = os.path.join(save_directory, "{}_{}_{}".format(p, fixtype, fixname))
                if not os.path.exists(jobdir): os.makedirs(jobdir)
                output = os.path.join(jobdir, "output.out")
                error = os.path.join(jobdir, "error.err")
                script = os.path.join(jobdir, "run.sh")
                slurm_script = os.path.join(jobdir, "run_slurm.sh")
                lines = ["#!/bin/sh", "cd /home/ladamek/MassResolutionRegression", "source ./setup.sh"]
                if not bootstrap: lines.append("python WorkspaceScanning/load_workspace.py --slurmjob --wsname {wsname} --mps mass_point --directory {d} --parname_forfix {p} --parfix {pf}".format(p=p, d=jobdir, pf=val, wsname=wsname))
                else: lines.append("python WorkspaceScanning/load_workspace.py --slurmjob --wsname {wsname} --mps mass_point --directory {d} --parname_forfix {p} --parfix {pf} --bootstrap".format(p=p, d=jobdir, pf=val, wsname=wsname))
                with open(script, "w") as f:
                    for l in lines: f.write(l + "\n")
                if local: os.system("source {}".format(script))
                else:
                    os.system("batchScript \"source {}\" -O {}".format(script, slurm_script))
                    print("batchScript \"source {}\" -O {}".format(script, slurm_script))
                    os.system("sbatch --mem=80000M --time={time} --error={err} --output={out} {script}".format(err=error, out=output, script=slurm_script, time=time))

    if do_postfit:
        pass
