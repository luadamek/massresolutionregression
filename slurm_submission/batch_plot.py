import os
import utils
import glob
from check_for_completion import check_completion
from post_processing import get_complete_list_of_classifiers

#wcard = "DCB_Models_*Norm*"
#wcard = "DCB_Models_CompareLRandNorm_NewLoss_2020_3_19_16_43_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_Deep_Pyramid_4mu_truth_eta_pt_5e-05_MPNormed"
#wcard = "DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_Deep_Pyramid_2e2mu_truth_eta_pt_0_001_MPNormed"
#wcard = "DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29*"
#wcard = "DCB_Models_CompareLRandNorm_NewLosst4*correct*"
#wcard = "DCB_Models_CompareLRandNorm_NewLosst4_2020_3_19_21_29_ggH123__ggH124__ggH125__ggH126__ggH127_DCB_Model_4mu_truth_eta_pt_0_005_MPNormed*"
wcard = "*_bare_*"
bd = "/scratch/ladamek/NetworkTraining/"
if "correct" in wcard: bd = os.path.join(os.getenv("MassRegressionResolutionDir"), "ResidualCorrectionNetworkTraining")
plotting_script = "plotting_tools/plots_in_bins_of_sigma_cb.py"

commands = ["#!/bin/sh"] + utils.get_setup_commands() + ["cd {}".format(os.getenv("MassRegressionResolutionDir"))]
all_models, all_metas = get_complete_list_of_classifiers(wcard)
flavours = ["4mu", "2e2mu", "2mu2e", "4e"]

all_models_for_jobs = []
for am in all_models:
    all_models_for_jobs.append(am.replace(".pkl", "").replace(".h5", ""))

for f in all_models_for_jobs:
    print("Processing {}".format(f))
    for flavour in flavours:
        if "_{}_".format(flavour) not in os.path.split(f)[-1]: continue #make sure that this model was for this flavour
        #make a folder for this job:
        job_dir = os.path.join("/scratch/ladamek/", "MassResolutionPlottingJobs")
        if not os.path.exists(job_dir):
            os.makedirs(job_dir)
        job_dir = os.path.join(job_dir, f.split("/")[-1])
        if not os.path.exists(job_dir):
            os.makedirs(job_dir)
        job_dir = os.path.join(job_dir, flavour)
        if not os.path.exists(job_dir):
            os.makedirs(job_dir)
        plots_dir = os.path.join(job_dir, "plots")
        if not os.path.exists(plots_dir):
            os.makedirs(plots_dir)
        job_dir = os.path.join(job_dir, "job_submission")
        if not os.path.exists(job_dir):
            os.makedirs(job_dir)

        if "_corrected" in os.path.split(f)[-1]: score_base = "sigma_corrected_prediction_{}"
        else: score_base = "sigma_prediction_{}"
#        with open(f + ".pkl", "rb") as pickle_file: metadata = pickle.load(pickle_file)
#        lr = metadata["lr"]

        plot_command = "python plotting_tools/plots_in_bins_of_sigma_cb.py --flavour {flav} --extra_models {em} --out_folder {pd} --score_base {sb}".format(flav = flavour, bd = f, pd = plots_dir, sb = score_base, em=f)
        submission_file = os.path.join(job_dir, "submit.sh")
        print("Running the following commands")
        with open(submission_file, "w") as open_file:
            for c in commands + [plot_command]:
                 open_file.write(c + "\n")
                 print(c)
        open_file.close()

        output_file = os.path.join(job_dir, "output.out")
        error_file = os.path.join(job_dir, "error.out")

        submission_file_slurm = os.path.join(job_dir, "submit_slurm.sh")
        if os.path.exists(submission_file_slurm): os.system("rm {}".format(submission_file_slurm))
        #os.system("source {}".format(submission_file))
        os.system("batchScript \"source {}\" -O {}".format(submission_file, submission_file_slurm))
        os.system("sbatch --mem=8000M --time=01:40:00 --output={} --error={} {}".format(output_file,error_file, submission_file))
        print("\n"*5)
