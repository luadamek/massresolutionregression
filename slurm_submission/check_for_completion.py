import argparse
import os
import glob

def get_jobs(wcard, sd):
    wildcards = ["*" + wcard + "*", wcard + "*", "*" + wcard]

    directories = []
    for wildcard in wildcards:
        directories_found = glob.glob(os.path.join(sd, wildcard))
        for directory in directories_found:
            if directory not in directories: directories.append(directory)

    tmp_directories = []
    for d in directories:
        if sd not in d: tmp_directories.append(os.path.join(sd, d))
        else: tmp_directories.append(d)

    directories = tmp_directories
    del tmp_directories
    print("Found the following directories filled with regressors {}".format(directories))
    return directories

def check_completion(wcard, search_directory):

    directories = get_jobs(wcard, search_directory)
    unfinished_jobs = []
    for d in directories:
        filename = os.path.join(d, "output.out")
        if os.path.exists(filename):
            with open(filename, "r") as f: lines = f.readlines()
        else:
            unfinished_jobs.append(d)
            continue
            
        finished = False
        for l in lines:
            if "FINISHED!" in l:
                finished = True
                print("{} finished".format(d))
        if not finished: unfinished_jobs.append(d)


    print("The following jobs did not complete")
    for d in unfinished_jobs:
        print(d)

    print("This many finished {}".format(len(directories) - len(unfinished_jobs)))
    print("This many did not finish {}".format(len(unfinished_jobs)))

    return unfinished_jobs

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-wc", "--wildcard", help="the group of classifiers to search for", type=str, dest="wcard", default="")
    parser.add_argument("-sd", "--search_directory", help="the directory in which to search", type=str, dest="sd", default="/scratch/ladamek/NetworkTraining/")
    parser.add_argument("-resub", "--resub", help="whether or not to resubmit the jobs", default = False, action="store_true", dest="resub")
    args = parser.parse_args()

    assert args.wcard != ""
    unfinished_jobs = check_completion(args.wcard, args.sd)
    print(unfinished_jobs)
    print("This many didn't finish {}".format(len(unfinished_jobs)))
    input()

    print("Do resubmission: {}".format(args.resub))

    if args.resub:
        for job in unfinished_jobs:
            batch_script = glob.glob(os.path.join(job,"*.sh"))
            print(job)
            assert len(batch_script) == 1
            batch_script = batch_script[0]
            if job not in batch_script: batch_script = os.path.join(job, batch_script)
            outfile_err = os.path.join(job, "output.err")
            outfile_out = os.path.join(job, "output.out")
            os.system("rm {}".format(outfile_err))
            os.system("rm {}".format(outfile_out))
            slurm_command = "sbatch --mem=30000M --time=10:00:00 --gres=gpu:1 --error={} --output={} {}".format( outfile_err , outfile_out, batch_script )
            pwd = os.getenv("PWD")
            home = os.getenv("HOME")
            commands = ["cd {}".format(home), slurm_command, "cd {}".format(pwd)]
            for command in commands:
                print(command)
                os.system(command)

