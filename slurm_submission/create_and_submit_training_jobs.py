import os
import utils
from datetime import datetime
from job_submission import Job, JobSet
import pickle as pkl

jobsetname = "test_various_fits_1"
jobset = JobSet(jobsetname)

test_one = True
submit = False
create_jobset = True

job_dir = "/scratch/ladamek/NetworkTraining"

if create_jobset:
    #variables = ["reco_eta", "m4lerr", "reco_eta_pt", "truth_eta_pt"]
    #variables = ["truth_eta_pt_phi_m4lerr"]
    variables = ["truth_bare_eta_pt", "truth_bare_eta_pt_phi_m4lerr"]
    networks = ["DCB_Model","DCB_Model_Deep_Pyramid", "DCB_Model_Deepest_Pyramid"]
    flavours = ["4mu", "2e2mu", "2mu2e", "4e"]
    mass_variable = "m4l_unconstrained"
    file_tokens = ["_ggH123_,_ggH124_,_ggH125_,_ggH126_,_ggH127_", "_ggH125_"]
    normalize=[True]#,False]
    #normalize = [True]

    time = datetime.now()
    test  = False
    time_stamp = "DCB_Models_CompareLRandNorm_NewLosst4_{}_{}_{}_{}_{}".format(time.year, time.month, time.day ,time.hour, time.minute)
    if test: time_stamp = "TEST"

    #lrs = [0.005, 0.001,0.005]
    lrs = [0.005, 0.001,0.005]
    #lrs = [0.05, 0.01]
    #lrs = [0.00005, 0.00001, 0.000005,  0.000001]

    if __name__ == "__main__":
       workdir = os.getenv("MassRegressionResolutionDir")
       for variable in variables:
          for network in networks:
            for flavour in flavours:
               for file_token in file_tokens:
                 for norm in normalize:
                   for lr in lrs:
                    if norm and len(file_token.split(",")) == 1: continue #it doesn't matter if there is only one file
                    this_descriptor = time_stamp + "_" + file_token.replace(",","_") + "_" + network + "_" + flavour + "_" + variable + "_" + str(lr).replace(".", "_")
                    if norm: this_descriptor += "_MPNormed"
                    this_descriptor = this_descriptor.replace("__","_")
                    job_commands = ["#!/bin/sh"]
                    job_commands += utils.get_setup_commands()
                    job_commands.append("cd {}".format(workdir))
                    job_commands.append("export USER={}".format(os.getenv("USER")))
                    job_commands.append("python train_nn/train.py --flavour {flav}  --model_flavour {model_flav} --var_selection {var_sel} --file_tokens {file_token} --stamp {stamp} --lr {lr} --normalize_mass_points={norm}".format(flav = flavour, var_sel = variable, model_flav = network, file_token = file_token, stamp = this_descriptor, lr=lr, norm=norm))
                    outputs_dir = os.path.join(job_dir, this_descriptor)

                    job = Job(this_descriptor, outputs_dir, job_commands, time="4:00:00", memory="30000M", gres="gpu:1") 
                    jobset.add_job(job)
       jobset.save()

if test_one:
    with open(jobsetname + "_JOBSET.pkl", "rb") as f:
        jobset = pkl.load(f)
    jobset.jobs[0].run_local()
    jobset.save()

if submit_jobs:
    with open(jobsetname + "_JOBSET.pkl", "rb") as f:
        jobset = pkl.load(f)
    jobset.submit()
    jobset.save()


