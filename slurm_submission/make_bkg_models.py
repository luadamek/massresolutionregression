import bkg_model
import utils
import os

model_type = "Che"
jobsetname = "BackgroundFittingJobs_{}_1".format(model_type)

make_jobset = False
test_one = False
submit_jobs = False

if make_jobset:
    retriever = utils.get_v24_retriever()

    binning = "NOMINAL"
    mass_vars = ["m4l_constrained"]
    channels = ["4mu,4e,2e2mu,2mu2e"]
    selections = ["Incl,BDT1,BDT2,BDT3,BDT4"]
    fitting_directory = os.path.join("/scratch/ladamek/", "ModelFitting")
    if not os.path.exists(fitting_directory): os.makedirs(fitting_directory)

    test = False

    bkg_sources, bkg_source_names = utils.get_all_bkgs() 
    bkg_list = []
    for source in bkg_sources:
        bkg_list += retriever.get_root_files(wildcards = source)

    reduced = list(set(bkg_list))
    bkg_list_clone = [b for b in bkg_list]
    for el in reduced:
       bkg_list_clone.remove(el)
    print(bkg_list_clone)

    len(bkg_list) == list(set(bkg_list)) #check for duplicates in the backgrounds
    systematics = retriever.get_all_systematics(bkg_list)
    if "Nominal" in systematics:
        while "Nominal" in systematics: systematics.remove("Nominal")
    systematics = ["Nominal"] + systematics #make sure that nominal is processed first

    from job_submission import JobSet, Job
    jobset = JobSet(jobsetname)

    for syst in systematics:
        for mass_var in mass_vars:
            for channel in channels:
                 for selection in selections:
                     commands = []
                     commands.append("cd {}".format(os.getenv("MassRegressionResolutionDir")))
                     commands.append("source ./setup.sh")
                     commands.append("export USER={}".format(os.getenv("USER")))
                     commands.append("python WorkspaceManager/bkg_model.py --syst {syst} --channel {channel} --mass_var {mass_var} --category {category} --model_type {model_type} --categorization {binning}".format(syst=syst, mass_var=mass_var, channel=channel, selection=selection,category=selection, model_type=model_type, binning=binning))
                     f = "bkg_model_{}_{}_{}_{}_{}_{}.sh".format(model_type, syst, channel.replace(",","_"), mass_var, selection.replace(",","_"), binning)
                     folder = os.path.join(fitting_directory, f.split(".")[0])

                     job = Job(f, folder, commands, time = "10:00:00", memory = "15000M")
                     jobset.add_job(job)

    jobset.save()

if test_one:
    with open(jobsetname + ".pkl", "rb") as f:
        jobset = pkl.load(f)
    jobset.jobs[0].run_local()

if submit_jobs:
    with open(jobsetname + ".pkl", "rb") as f:
        jobset = pkl.load(f)
    jobset.submit()

                 #if not os.path.exists(folder): os.makedirs(folder)
                 #f = os.path.join(folder, f)
                 #with open(f, "w") as script_f:
                 #    for c in commands:
                 #        script_f.write(c + "\n")
                 #batch_script = f.replace(".sh", "_slurm.sh")
                 #error = batch_script.replace(".sh",".err")
                 #output = batch_script.replace(".sh",".out")
                 #if not test and os.path.exists(error): os.system("rm {}".format(error))
                 #if not test and os.path.exists(output): os.system("rm {}".format(output))
                 #os.system("batchScript \"source {}\" -O {}".format(f, batch_script))
                 #if test: print(f)
                 #else:
                 #    os.system("rm {}".format(error)) #make sure that the error and output files are cleared, so that job completion can checked later
                 #    os.system("rm {}".format(output))
                 #    os.system("sbatch --mem=15000M --time=10:00:00 --error={} --output={} {}".format(error, output, batch_script))
