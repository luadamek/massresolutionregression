import argparse
import os
import glob
from check_for_completion import check_completion, get_jobs
import utils
from pop_model import pop_model
from put_scores_in_trees import append_score_to_file
import make_local_files
import pickle
from  post_processing import get_groups, get_model_metadata

if __name__ == "__main__":
     
    parser = argparse.ArgumentParser(description="Create the minitrees after a NN has been trained")
    parser.add_argument('--file', '-f', dest="file", type=str, required=True, help='The file to open and run the tree processing for')
    parser.add_argument('--job', '-job', dest="job", type=str, required=True, help="The name of the job to process")
    parser.add_argument('--minitree_location', '-mt', dest='minitree_location', type=str, required = False, help="The minitree location in whcih to search for files", default = utils.get_local_minitree_location("bianca_prodv23_appended"))
    args = parser.parse_args()

    #now go an run the jobs!
    with open(args.file, "rb") as f:
        job_groups = pickle.load(f)
    job = args.job

    minitree_location = args.minitree_location
    datasets = make_local_files.datasets
    base_directory = os.path.join("/scratch/ladamek/TreesWithRegressors/",job)
    if not os.path.exists(base_directory): os.makedirs(base_directory)
    files_to_score = []
    for d in datasets:
        original_f = os.path.join(minitree_location, d)
        print(original_f)
        new_f = os.path.join(base_directory, d)
        print(new_f)
        assert new_f != original_f
        print("Callig cp {} {}".format(original_f,new_f))
        os.system("cp {} {}".format(original_f,new_f))
        files_to_score.append(new_f)
    print(list(job_groups.keys()))
    channels = list(job_groups[job].keys())

    for channel in channels:
        regressor_dir = job_groups[job][channel]
        contains = []
        skip = []
        if "_corrected" in job: 
            if not "_truth_to_reco" in job: contains.append("_popped_corrected_popped")
            else: contains.append("_popped_truth_to_reco_corrected_popped")
        else: skip.append("_corrected")
        if "_truth_to_reco" in job: contains.append("_truth_to_reco")
        else: skip.append("_truth_to_reco")
        contains.append("_popped")
        model_file, metadata = get_model_metadata(regressor_dir, skip=skip, contains=contains)
        for new_f in files_to_score:
            append_score_to_file(new_f, model_file, metadata)
        print("FINISHED!")

