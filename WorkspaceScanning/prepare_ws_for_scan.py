import utils
import ROOT as r
import workspace_manager
from plotting_utils import prepare_for_fit
import os
import systematics_cache
from datetime import datetime
prepare_for_fit()

#r.RooMsgService.instance().setGlobalKillBelow(r.RooFit.ERROR)
r.RooAbsReal.defaultIntegratorConfig().method1D().setLabel("RooAdaptiveGaussKronrodIntegrator1D")  ## Better numerical integrator

xlabel_map = {"m4l_constrained": "m_{4l,\ constrained}[GeV]"}

#from atlasplots import atlas_style as astyle
#astyle.SetAtlasStyle()

def beautiful_plots():
    r.gStyle.SetLabelFont(43,"xyz")
    r.gStyle.SetLabelSize(30,"xyz")
    r.gStyle.SetTitleFont(43,"xyz")
    r.gStyle.SetTitleSize(40,"xyz")
    r.gStyle.SetTitleOffset(2.6, "x")
    r.gStyle.SetTitleOffset(1.3, "y")
beautiful_plots()

def plot_model_on_data(data, model, mass_observable, all_observables, name):
    if type(mass_observable) == str: mass_observable = workspace_manager.get_variable(mass_observable)
    #get all of the category observables
    cat_observables = []
    for obs in all_observables:
        if workspace_manager.get_variable_type(obs) == "category":
            cat_observables.append(obs)
    cat_observables = [workspace_manager.get_variable(obs) for obs in cat_observables]
    assert len(cat_observables) == 1
    cat_observable = cat_observables[0]
    categories = list(workspace_manager.category_bank(cat_observable.GetName()).keys())
    for cat in categories[::-1]:
        print("Plotting cat {}".format(cat))
        descr = cat.split("_")
        has_both = True
        for d in descr:
            if d not in model.GetName(): has_both = False
        if not has_both: continue

        c = r.TCanvas(cat, cat, 1000, 1000)
        cat_data = getattr(data,"reduce")(r.RooFit.Cut("{}=={}::{}".format(cat_observable.GetName(), cat_observable.GetName(), cat)))
       
        cat_model = model.getPdf(cat)
        locals = plot_model(cat_data, cat_model, c)
        c.Print("{}_{}.png".format(cat, name))
        c.Close()
    return locals + locals()

def get_cat_from_model(model, cat):
    constraints = []
    pdf = None
    if hasattr(model, "pdfList"):
        pdf_list = model.pdfList()
        for i in range(0, pdf_list.getSize()):
            el = pdf_list.at(i)
            if "_constraint_" in el.GetName(): constraints.append(pdf_list.at(i))
            else:
                assert not pdf
                pdf = el
        pdf_in_cat = pdf.getPdf(cat)
        for_prod_pdf = list_to_arglist([pdf_in_cat] + constraints)
        new_pdf_name = "{}_constrained".format(pdf_in_cat.GetName())
        constrained_pdf = r.RooProdPdf(new_pdf_name, new_pdf_name, for_prod_pdf)
        return constrained_pdf
    else: return model.getPdf(cat) 
        

def plot_model(data, model, c, mass_observable, descriptions=[]):
    c.Draw()
    c.cd()

    top = r.TPad("top", "top", 0.0, 0.3, 1.0, 1.0)
    top.SetBottomMargin(0.0)
    top.SetLeftMargin(0.2)
    top.Draw()
    top.cd()

    frame = mass_observable.frame()
    data.plotOn(frame)
    model.plotOn(frame, r.RooFit.ProjWData(r.RooArgSet(mass_observable),data),  r.RooFit.Normalization(1.0, r.RooAbsReal.RelativeExpected))
    frame.SetTitle("")
    frame.Draw()
    top.Modified()
    top.Update()

    from plotting_utils import DrawText
    size = 0.06
    y = 0.9
    x=0.65
    for d in descriptions:
        y-=(size+0.02)
        DrawText(x, y, d, color=1, size=size)

    c.cd()
    bottom = r.TPad("botom", "bottom", 0.0, 0.0, 1.0, 0.3)
    bottom.SetTopMargin(0.0)
    bottom.SetBottomMargin(0.3)
    bottom.SetLeftMargin(0.2)
    bottom.Draw()
    bottom.cd()
    hResidual = frame.pullHist()
    frame2 = mass_observable.frame()
    frame2.SetTitle("")
    frame2.addPlotable(hResidual,"P")
    frame2.GetYaxis().SetTitle("Pulls")
    frame2.GetXaxis().SetTitle(xlabel_map[mass_observable.GetName()])
    frame2.Draw()
    bottom.Update()
    bottom.Modified()
    c.cd()
    c.Update()
    c.Modified()

    return locals()

def flatten_dict(d):
    output = []
    assert len(d.items()) == 1
    for key, vals in d.items():
        for val in vals:
            output.append({key:val})
    return output

def merge_dicts(d1_list, d2_list):
    output = []
    for d1 in d1_list:
        for d2 in d2_list:
            output.append({**d1, **d2})
    return output

def generate_combinations(args):
    dicts = []
    for key in args:
        to_flatten = {key:args[key]}
        flat = flatten_dict(to_flatten)
        dicts.append(flat)

    output = dicts[0]
    for set in dicts[1:]:
        output = merge_dicts(output, set)

    return output

def plot_all_functions():
    mass_observable = ["m4l_constrained"]
    channels = ["4mu", "4e", "2e2mu", "2mu2e"]
    categories = ["BDT1", "BDT2", "BDT3", "BDT4", "Incl"]
    #model_pars = list(range(1, 10))
    model_pars = list(range(1, 8))
    systs = ["Nominal"]

    args = {}
    args["mass_observable"] = mass_observable
    args["channel"] = channels
    args["categories"] = categories
    args["model_par"] = model_pars
    bkgs, bkg_names = utils.get_all_bkgs()
    args["backgrounds"] = [(bkg, bkg_name) for bkg, bkg_name in zip(bkgs, bkg_names) if bkg_name == "ZZ" or bkg_name == "tXX_VVV"]
    retriever = utils.get_v24_retriever()

    combinations = generate_combinations(args)
    canvases = []
    for syst in systs:
        for config in combinations:
            print("Processing {} {}".format(syst, config))
            workspace_manager.clear_cache()
            mass_observable = config["mass_observable"]
            channel = config["channel"]
            category = config["categories"]
            model_par = config["model_par"]
            bkg, bkg_name = config["backgrounds"]
            if bkg_name == "ZZ": descriptor = "ZZ*"
            if bkg_name == "tXX_VVV": descriptor = "tXX, VVV"

            channel_description = channel.replace("mu", "#mu")
            category_description = category
            
            data = workspace_manager.get_dataset(retriever, retriever.get_root_files(bkg), [mass_observable], selection=utils.get_full_selection(channel, category, mass_observable), syst = syst, campaigns = ["mc16a", "mc16d", "mc16e"], normalize_mass_points=False)
            poi, model, _, __, ___, ____, _____ = workspace_manager.get_bkg_model(retriever, mass_observable=mass_observable, channel = channel, syst = syst, category=category, bkg_names=[bkg_name], extended=True, model_type="Che", model_par=model_par)
            c = r.TCanvas(model.GetName(), model.GetName(), 1000, 1000)
            #if bkg_name == "tXX_VVV": plot_options = r.RooFit.Binning()
            locals = plot_model(data, model, c, workspace_manager.get_variable(mass_observable), [descriptor, channel_description, category_description])
            plots_directory = os.path.join(os.getenv("MassRegressionResolutionDir"), "model_plots")
            if not os.path.exists(plots_directory): os.makedirs(plots_directory)
            c.Print(os.path.join(plots_directory, model.GetName() + ".eps"))
            canvases.append(c)
          
    print("Done!")

def __main__():
    retriever = utils.get_v24_retriever()
    mass_observable = "m4l_constrained"
    channels = ["4mu", "4e", "2e2mu", "2mu2e"]
    systs = ["Nominal"]
    categories = ["BDT1", "BDT2", "BDT3", "BDT4"]
    selection = utils.get_baseline_selection(mass_observable)

    #make some bkg only plots:
    #bkgs, names = utils.get_all_bkgs()
    bkgs=[]
    names=[]
    for bkg, name in zip(bkgs, names):
        poi, model, pdfs, observables, parameters, nuis_parameters, functions = workspace_manager.merge_bkg_signal_models(mass_observable = mass_observable, channels = channels, syst = syst, categories=categories, do_bkg=True, do_sig=False, model_type="Che", bkgs=[name])
        isMC = True
        if isMC:
            if "weight" not in observables: observables.append("weight")
        files = retriever.get_root_files(bkg) 
        data = workspace_manager.get_dataset(retriever, files, observables, selection=selection, syst = syst, campaigns = ["mc16a", "mc16d", "mc16e"], normalize_mass_points=False)
        plot_model_on_data(data, model, mass_observable, observables, name)

    bkgs = []
    files = retriever.get_root_files("_ggH125_")
    systematics = retriever.get_all_systematics(files)
    poi, model, pdfs, observables, parameters, nuis_parameters, functions = workspace_manager.merge_bkg_signal_models(retriever, mass_observable = mass_observable, channels = channels, systematics = systematics, categories=categories, do_bkg=bool(bkgs), do_sig =True, model_types={"ZZ":"Che", "tXX_VVV":"Bern"}, bkgs=bkgs)

    print("POI : {}".format(poi))
    print("Model : {}".format(model))
    print("Dependent pdfs : {}".format(pdfs))
    print("Observables: {}".format(observables))
    print("Paramters: {}".format(parameters))
    print("Nuis Paramters: {}".format(nuis_parameters))
    print("All Functions: {}".format(functions))
    print("Selection : {}".format(selection))
    assert workspace_manager.check_for_variable("mass_point")

    isMC = True
    if isMC:
        if "weight" not in observables: observables.append("weight")

    wcards = [ ["_ggH127_"],["_ggH126_"],["_ggH125_"],["_ggH124_"],["_ggH123_"]]
    signal_names = ["ggH127", "ggH126", "ggH125", "ggH124", "ggH123"]
    mps = [127.0, 126.0, 125.0, 124.0, 123.0]

    bkg_wcards = []
    dsids, names = utils.get_all_bkgs()
    for dsid, name in zip(dsids, names):
        if name in bkgs:
            for d in dsid: bkg_wcards.append(d)

    for wcard,mp in zip(wcards, mps):
        poi.setVal(mp)
        poi.setConstant(True)
        files = retriever.get_root_files(wcard + bkg_wcards) 
        data = workspace_manager.get_dataset(retriever, files, observables, selection=selection, syst = syst, campaigns = ["mc16a", "mc16d", "mc16e"], normalize_mass_points=False)
        plot_model_on_data(data, model, mass_observable, observables, "_".join(wcard + bkgs))
        measured_mass, lower_lim, upper_lim = scan_likelihood(data, model, poi, nuis_parameters, str(mp).replace(".", "p"), isMC=True)
        print(measured_mass, lower_lim, upper_lim)

def test_systematics():
    import argparse
    parser = argparse.ArgumentParser("Parsethemargs")
    parser.add_argument("--stamp", required=False, default="", type=str, dest="stamp")
    parser.add_argument("--do_per", dest="do_per", action="store_true")
    parser.add_argument("--decay", dest="decay", type=str, default = "All", required=False)
    parser.add_argument("--do_per_scale", dest="do_per_scale", action="store_true")
    parser.add_argument("--do_syst", dest="do_syst", action="store_true")
    parser.add_argument("--skip_bkg", dest="skip_bkg", action="store_true")
    parser.add_argument("--skip_cats", dest="skip_cats", action="store_true")
    args = parser.parse_args()
    stamp = args.stamp

    do_per = args.do_per
    do_per_scale = args.do_per_scale
    do_bkgs = not args.skip_bkg
    do_cats = not args.skip_cats
    do_syst = args.do_syst
    do_per_scale = args.do_per_scale
    decay = args.decay

    signal_wcard = ["_ggH125_"]
    bkgs = []
    if do_bkgs: bkgs = ["ZZ", "tXX_VVV"]
    wcard = signal_wcard
    for b in bkgs: wcard += utils.get_wcards_given_name(b)
    retriever = utils.get_v24_retriever()
    files = retriever.get_root_files(wcard)
    if do_syst: systematics = retriever.get_all_systematics(files)
    else: systematics = []

    #now define all nuisance parameters for theses systematics
    pairs, systematics = systematics_cache.get_all_systematic_pairs(systematics)

    #clean up the systematics
    to_del = []
    for ps in pairs:
        if "_AF2_" in ps:             to_del.append(ps)#skip AF2 systematics
        if "weight_var_EFF_FT" in ps: to_del.append(ps) #skip btagging systematics
        if "weight_var_FT_EFF" in ps: to_del.append(ps)
        if "JET" in ps: to_del.append(ps)
    for el in list(set(to_del)): del pairs[el]
    paired_systematics = list(pairs.keys())
    systematics = [s for s in systematics if "_AF2_" not in s and "weight_var_EFF_FT" not in s and "weight_var_FT_EFF" not in s and "JET" not in s]

    systematics_cache.define_all_nps(systematics, paired_systematics) #treat them all as correlated
    mass_observable = "m4l_constrained"
    if decay == "All": channels = ["4mu", "4e", "2e2mu", "2mu2e"]
    else: channels = [decay]
    if do_cats: categories = ["BDT1", "BDT2", "BDT3", "BDT4"]
    else: categories = ["Incl"]
    selection = utils.get_baseline_selection(mass_observable)
    poi, model, pdfs, observables, parameters, nuis_parameters, functions, categories = workspace_manager.merge_bkg_signal_models(retriever, mass_observable = mass_observable, channels = channels, systematics = systematics, categories=categories, do_bkg=bool(bkgs), do_sig =True, model_types={"ZZ":"Che", "tXX_VVV":"Bern"}, bkgs=bkgs, do_per=do_per, do_per_scale = do_per_scale)
    #print("POI : {}".format(poi))
    #print("Model : {}".format(model))
    #print("Dependent pdfs : {}".format(pdfs))
    #print("Observables: {}".format(observables))
    #print("Paramters: {}".format(parameters))
    #print("Nuis Paramters: {}".format(nuis_parameters))
    #print("All Functions: {}".format(functions))
    #print("Selection : {}".format(selection))
    #model.getVal()
    assert workspace_manager.check_for_variable("mass_point")

    #alright lets dump everything
    model_directory = utils.get_workspace_directory()
    if not os.path.exists(model_directory): os.makedirs(model_directory)
    to_dump = {}
    to_dump["poi"] = poi.GetName()
    to_dump["params"] = [p.GetName() for p in parameters]
    to_dump["model"] = model.GetName()
    to_dump["pdfs"] = [p.GetName() for p in pdfs]
    to_dump["obs"] = observables
    to_dump["nuis"] = [np.GetName() for np in nuis_parameters]
    to_dump["func"] = [f.GetName() for f in functions]
    to_dump["cats"] = [c.GetName() for c in categories]
    to_dump["seleciton"] = selection
    to_dump["systematics"] = list(paired_systematics)
    to_dump["files"] = files
    to_dump["bkgs"] = bkgs

    print("Dumping model into workspace")
    time = datetime.now()
    stamps = []
    if not stamp: stamps.append("{}_{}_{}_{}_{}".format(time.year, time.month, time.day ,time.hour, time.minute))
    if not do_bkgs: stamps.append("SignalOnly")
    else: stamps.append("SigBkg")
    if do_per: stamps.append("PER")
    if do_per_scale: stamps.append("Scaled")
    if not do_cats: stamps.append("Incl")
    if decay != "All": stamps.append(decay)
    if do_syst: stamps.append("Systematics")
    stamp = "_".join(stamps)
    filename = "{}.root".format(stamp)
    outfilename = os.path.join(model_directory, filename)
    picklefilename = os.path.join(model_directory, "{}.pkl".format(stamp))
    rf_out = r.TFile(outfilename, "RECREATE")
    rf_out.cd()
    ws_name = "ws_{}".format(stamp)
    ws = r.RooWorkspace(ws_name, ws_name)
    getattr(ws, "import")(model)
    ws.Write()
    rf_out.Close()
    import pickle
    with open(picklefilename, "wb") as f: pickle.dump(to_dump, f)
    print("File dumped with stamp {}".format(stamp))

def test_systematics_background():
    pass

if __name__ == "__main__":
    #__main__()
    #plot_all_functions()
    test_systematics()
