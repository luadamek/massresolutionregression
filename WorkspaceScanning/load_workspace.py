import utils
import ROOT as r
import workspace_manager
from workspace_manager import h as h
import os
import numpy as np

CB_color_cycle = ['#ffffcc',\
'#ffeda0',\
'#fed976',\
'#feb24c',\
'#fd8d3c',\
'#fc4e2a',\
'#e31a1c',\
'#bd0026',\
'#800026']
pdf_cache = {}

r.gROOT.SetBatch()

class BootStrapper():
    def __init__(self, dataframe, observables):
        self.dataframe = dataframe.query("weight > 0.0")
        self.observables = observables
        self.dataframe = self.dataframe.reset_index(drop=True)
        self.nevents = np.sum(dataframe.eval("weight").values)
        self.weights = self.dataframe.eval("weight").values
        self.weights/=np.sum(self.weights)
        self.indices = np.array(list(range(0, len(self.dataframe.eval("weight").values) ) ) )
        self.dataframe["weight"] = np.ones(len(self.indices))

    def bootstrap(self):
        nsample = np.random.poisson(self.nevents)
        indices = np.random.choice(self.indices, nsample, p = self.weights) # do a weighted sampling
        selframe = self.dataframe.iloc[indices]
        data = workspace_manager.get_dataset_from_df(selframe, self.observables)
        return data


if __name__ == "__main__":
    pass
    #r.RooMsgService.instance().setGlobalKillBelow(r.RooFit.ERROR)
    r.RooAbsReal.defaultIntegratorConfig().method1D().setLabel("RooAdaptiveGaussKronrodIntegrator1D")  ## Better numerical integrator

def get_multicore(multicore):
    if multicore: return r.RooFit.NumCPU(len(os.sched_getaffinity(0)))
    if not multicore: return r.RooFit.NumCPU(1)

all_commands=[]

def get_minimizer(nll):
    minimizer = r.RooMinimizer(nll)
    minimizer.setPrintEvalErrors(2)
    minimizer.setOffsetting(True)
    #minimizer.setEps(1e-4)
    minimizer.setStrategy(2)
    #minimizer.setVerbose(False)
    return minimizer

def run_minos(arguments):
    jobfile, ws_name, item_name, nll_name, base_ws, base_ws_file = arguments
    tf_ws = r.TFile(base_ws_file, "READ")
    base_ws=tf_ws.Get(base_ws)
    tf = r.TFile(jobfile, "READ")
    ws = tf.Get(ws_name)
    param = ws.var(item_name)
    parameters=r.RooArgSet(param)
    print(nll_name)
    nll = ws.function(nll_name)
    print(nll.getVal())
    parameters.Print()
    minimizer = get_minimizer(nll)
    minimizer.minos(parameters)
    errs = {}
    iter = parameters.createIterator()
    item = iter.Next()
    while item:
        errs[item.GetName()] = (item.getAsymErrorLo() , item.getAsymErrorHi())
        print(item.GetName())
        item = iter.Next()
    print(errs)
    return errs

from multiprocessing import Pool
def get_pool():
    return Pool(len(os.sched_getaffinity(0)))

def register_command(linked_list, command):
    global all_commands
    all_commands.append(command)
    linked_list.Add(command)

def migrad_minos(data, model, poi, observables=[], multicore=True, constraints=[], nuisance_parameters = [], specific_errs = "", globs = [], condobs = []):
    r.RooMsgService.instance().setGlobalKillBelow(r.RooFit.ERROR)

    process_errs = bool(specific_errs)
    errs_to_process = specific_errs.split(",")

    parameters = model.getParameters(data)
    print("These are all of the floating parameters")
    params = {}
    results = {}
    errs = {}
    iter = parameters.createIterator()
    item = iter.Next()
    while item :
        if item.GetName() in observables or item.isConstant(): 
            item =iter.Next()
            continue
        params[item.GetName()] = item
        results[item.GetName()] = None
        errs[item.GetName()] = None
        print(item.GetName(), item.getVal())
        item = iter.Next()

    #Run a pre-fit To get an idead of the interval
    print("Preparing to scan likelihood")
    poi.setConstant(False)
    print("Fitting Model")
    constraint_pdf_set = workspace_manager.list_to_argset(constraints)
    globs_set = workspace_manager.list_to_argset(globs)
    condobs_set = workspace_manager.list_to_argset(condobs)
    constraint_pdf_set.Print()
    globs_set.Print()
    nll =  model.createNLL(data, get_multicore(multicore), r.RooFit.CloneData(False), r.RooFit.ExternalConstraints(constraint_pdf_set), r.RooFit.GlobalObservables(globs_set), r.RooFit.ConditionalObservables(condobs_set))
    minimizer = get_minimizer(nll)
    #minimizer.optimizeConst(True)
    print("Running migrad")
    minimizer.migrad()
    print("Finished") #don't run hesse, just use minos
    print("Model has been fit.")

    params_for_errs = []
    iter = parameters.createIterator()
    item = iter.Next()
    while item :
        if item.GetName() in observables or item.isConstant():
            item =iter.Next()
            continue
        results[item.GetName()] = item.getVal()
        if process_errs and item.GetName() in errs_to_process: params_for_errs.append(item)
        print(item.GetName())
        item = iter.Next()

    if process_errs: stat = minimizer.minos(r.RooArgSet(*params_for_errs))
    else: stat = minimizer.minos()
    err_dict = {}
    iter = parameters.createIterator()
    item = iter.Next()
    while item :
        if item.GetName() in observables or item.isConstant():
            item =iter.Next()
            continue
        print(item.GetName())
        if not process_errs or item.GetName() in errs_to_process: err_dict[item.GetName()] = (item.getAsymErrorLo() , item.getAsymErrorHi(), stat)
        item = iter.Next()

    print(results, err_dict)
    return results, err_dict

class pyworkspace():
     def __init__(self, stamp):
         print("\n"*10)
         print("Opening {}".format(stamp))
         print("\n"*10)
         self.pickle_file = os.path.join(utils.get_workspace_directory(), stamp + ".pkl")
         self.root_file = os.path.join(utils.get_workspace_directory(), stamp + ".root")
         self.ws_name = "ws_{}".format(stamp)
         rf = r.TFile.Open(self.root_file)
         self.ws = rf.Get(self.ws_name)
         import pickle
         with open(self.pickle_file, "rb") as f: self.params = pickle.load(f)
         for p in self.parameters:
             p.setConstant(True)
         for f in self.functions:
             f.getVal()
         for f in self.functions: workspace_manager.force_roo_variable(f) 
         for f in self.params["obs"]: workspace_manager.force_roo_variable(self.ws.var(f))
         for f in self.params["cats"]: workspace_manager.force_roo_variable(self.ws.cat(f))
         self.fixes={}
         self.retriever = utils.get_v24_retriever()
         if "_PER" in self.ws_name: utils.add_nominal_resolution_models(self.retriever)

     @property
     def poi(self):
         return self.ws.var(self.params["poi"])

     @property
     def constraints(self):
        constraints = []
        for n in self.nuis:
           name = "{}_constraint_".format(n.GetName())
           if "_Norm" in n.GetName():
               print("Skipping constraint for {}".format(n.GetName()))
               continue
           else: constraints.append(r.RooGaussian(name, name, n, r.RooFit.RooConst(0.0), r.RooFit.RooConst(1.0)))
        return constraints

     @property
     def observables(self):
         return self.params["obs"] + [self.catvar.GetName()]

     @property
     def constrained_nuis_parameters(self):
         return [el for el in self.nuis if "_Norm" not in el.GetName()] 

     @property
     def model(self):
         return self.ws.pdf(self.params["model"])

     @property
     def functions(self):
         return [self.ws.function(el) for el in self.params["func"]]

     @property
     def nuis(self):
         return [self.ws.var(el) for el in self.params["nuis"]]

     def remove_limits(self):
         for el in self.params["nuis"]: self.ws.var(el).removeRange()

     @property
     def catvar(self):
         return [self.ws.cat(el) for el in self.params["cats"]][0]

     @property
     def systematics(self):
         return self.params["systematics"]

     @property
     def parameters(self):
         for p in self.params["params"]:
             self.ws.var(p).GetName()
         to_return = [self.ws.var(p) for p in self.params["params"]]
         return to_return

     def set_norm(self, selection="(105.0 < m4l_constrained) and (160.0 > m4l_constrained)", syst="Nominal"):
         new_retriever = utils.get_v24_retriever()
         signal_files = new_retriever.get_root_files(args.signal_wcard)
         signal_data = workspace_manager.get_dataset(new_retriever, signal_files, [el for el in self.observables if "_prediction_" not in el], selection=selection, syst=syst)
         signal_data.Print()
         yield_dict = {}
         for cat in self.categories:
             if "BDT" not in cat: catname = "Incl_{}".format(cat)
             else: catname = cat
             yield_dict[catname] = getattr(signal_data,"reduce")(r.RooFit.Cut("{}==({}::{})".format(self.catvar.GetName(),self.catvar.GetName(),cat))).sumEntries()
         for np in self.nuis:
             if "_Norm" not in np.GetName(): continue
             if "ZZ" in np.GetName(): continue
             cat = "{}_{}".format(np.GetName().split("__")[1], np.GetName().split("__")[0])
             np.setVal(yield_dict[cat])


     @property
     def categories(self):
         cats = []
         #if "Incl" in self.model.GetName(): cats += ["Incl"]
         bdt_categories = ["BDT1", "BDT2", "BDT3", "BDT4"]
         if "Incl" in self.model.GetName():
            if "_4e_" in self.model.GetName(): cats += ["4e"]
            if "_4mu_" in self.model.GetName(): cats += ["4mu"]
            if "_2e2mu_" in self.model.GetName(): cats += ["2e2mu"]
            if "_2mu2e_" in self.model.GetName(): cats += ["2mu2e"]
         else:
            for b in bdt_categories:
                for d in ["4e", "4mu", "2e2mu", "2mu2e"]:
                    if "_{}_".format(b) and "_{}_".format(d) in self.model.GetName(): cats += ["{}_{}".format(b,d)]
         return cats

     def freeze_systematics(self, freeze):
         for np in self.nuis:
             if "_Norm" not in np.GetName() and not ("prediction" in np.GetName() and "_scale" in np.GetName()):
                 np.setConstant(freeze)
             else: np.setConstant(False)

     def globs(self):
         return [el for el in self.nuis if ("_Norm" in el.GetName() )] + [self.ws.function(el) for el in self.params["func"] if ("_Norm" in el and "all_variations" in el )]

     @property
     def functions(self):
         return [self.ws.function(f) for f in self.params["func"]]

     @property
     def model_and_constraints(self):
         print("Getting model and constraints")
         pdf_list = self.model.pdfList()
         pdf = None
         constraints = [pdf_list.at(i) for i in range(0, pdf_list.getSize()) if "_constraint_" in pdf_list.at(i).GetName()]
         pdf = [pdf_list.at(i) for i in range(0, pdf_list.getSize()) if "_constraint_" not in pdf_list.at(i).GetName()]
         print(pdf)
         assert len(pdf) == 1
         pdf = pdf[0]
         print("Retrieved model and constraints")
         return pdf, constraints

     @property
     def get_constraint_free_model(self):
         pdf, constraints = self.model_and_constraints
         return pdf

     def get_pdf_in_cat_unconstrained(self, cat = ""):
         pdf, constraints = self.model_and_constraints
         if cat: pdf_in_cat = pdf.getPdf(cat)
         else: pdf_in_cat = pdf
         return pdf_in_cat

     def get_pdf_in_cat(self, cat):
         global pdf_cache
         if cat in pdf_cache: return pdf_cache[cat]
         pdf_in_cat = self.model.getPdf(cat)
         pdf_cache[cat] = pdf_in_cat
         return pdf_in_cat

     def float_all_np(self, to_float=True):
         for np in self.nuis: np.setConstant(to_float)

     def get_wcards(self, signal_wcard):
         bkgs = self.params["bkgs"]
         wcards = []
         for b in bkgs:
             wcards += utils.get_wcards_given_name(b)
         if type(signal_wcard) == list: wcards += signal_wcard
         else: wcards += [signal_wcard]
         return wcards

     @property
     def systematics(self):
         return [s for s in self.params["systematics"] if "_AF2" not in s]

     def register_parfix(self, name, value):
         self.fixes[name] = value

     def set_fixed_syst_vals(self):
         for np in self.nuis:
             if np.GetName() in self.fixes:
                 np.setConstant(True)
                 np.setVal(self.fixes[np.GetName()])

     def condobs(self):
         return [self.ws.var(el) for el in self.params["obs"] if "_prediction" in el]
         

     @property
     def nuis_lookup(self):
         to_return = {}
         for np in self.nuis:
             to_return[np.GetName().replace("Nuis_","")] = np
         return to_return

     def get_np(self, syst):
         if syst in self.nuis_lookup: return self.nuis_lookup[syst]
         return None

     def perform_scan(self, signal_wcard, syst="Nominal", category="", poi=poi, constraint_free=False, constraints=[], nuisance_parameters=[], do_syst=False, specific_errs = "", save_file = "", subsample=-1, bootstrap=False, nboots=50000):
         pws.remove_limits()

         if do_syst and len(constraints) == 0: do_syst = False
         poi.removeRange()
         model = self.model
         if subsample > -0.1 and not bootstrap: self.retriever.set_subsample(subsample)
         wcards = self.get_wcards(signal_wcard)
         files = self.retriever.get_root_files(wcards)
         selection = self.params["seleciton"]
         data = workspace_manager.get_dataset(self.retriever, files, self.observables, selection=selection, syst=syst)
         if category:
             cat_observable = self.catvar.GetName()
             data = getattr(data,"reduce")(r.RooFit.Cut("{}=={}::{}".format(cat_observable, cat_observable, category)))
             model = self.get_pdf_in_cat(category)

         self.freeze_systematics(True)
         self.set_fixed_syst_vals()
         if not bootstrap: no_syst = migrad_minos(data, model, poi, observables=self.observables, constraints=[], nuisance_parameters=[], multicore=True, specific_errs = specific_errs, globs=self.globs(), condobs=self.condobs())
         else:
             
             dataframe = self.retriever.get_dataframe(files, self.observables, selection=selection, systematic=syst)
             bs = BootStrapper(dataframe, self.observables)
             no_syst = []
             for n in self.nuis:
                 if "_Norm" in n.GetName():
                      n.setRange(0.0, 1000000.0)
             for i in range(0, nboots):
                 print("Boostrap {}".format(i))
                 bs_dataset = bs.bootstrap()
                 no_syst.append(migrad_minos(bs_dataset, model, poi, observables=self.observables, constraints=[], nuisance_parameters=[], multicore=True, specific_errs = specific_errs, globs=self.globs(), condobs=self.condobs()))
                 if save_file and (((i % 200) == 0) and not (i == (nboots -1))):
                    import pickle
                    with open(save_file, "wb") as f: pickle.dump({"no_syst": no_syst, "syst": None}, f)

         if save_file:
             import pickle
             with open(save_file, "wb") as f: pickle.dump({"no_syst": no_syst, "syst": None}, f)
         if not do_syst: return {"no_syst": no_syst, "syst":None}

         self.freeze_systematics(False)
         self.set_fixed_syst_vals()
         if not bootstrap: syst = migrad_minos(data, model, poi, observables=self.observables, constraints=constraints, nuisance_parameters=nuisance_parameters, multicore=True, specific_errs = specific_errs, globs=self.globs(), condobs=self.condobs())
         else:
             dataframe = self.retriever.get_dataframe(files, self.observables, selection=selection, systematic=syst)
             bs = BootStrapper(dataframe, self.observables)
             syst = []
             for n in self.nuis:
                 if "_Norm" in n.GetName():
                      n.setRange(0.0, 1000000.0)
             for i in range(0, nboots):
                 print("Boostrap {}".format(i))
                 bs_dataset = bs.bootstrap()
                 syst.append(migrad_minos(data, model, poi, observables=self.observables, constraints=constraints, nuisance_parameters=nuisance_parameters, multicore=True, specific_errs = specific_errs, globs=self.globs(), condobs=self.condobs()))
                 if save_file and (((i % 200) == 0) and not (i == (nboots -1))):
                    import pickle
                    with open(save_file, "wb") as f: pickle.dump({"no_syst": no_syst, "syst": syst}, f)

         if save_file:
             import pickle
             with open(save_file, "wb") as f: pickle.dump({"no_syst": no_syst, "syst": syst}, f)
         return {"no_syst": no_syst, "syst": syst}

     def bootstrap(self, signal_wcard, nboots, syst="Nominal", poi=poi):
         wcards = self.get_wcards(signal_wcard)
         files = self.retriever.get_root_files(wcards)
         selection = self.params["seleciton"]
         dataframe = self.retriever.get_dataframe(files, self.observables, selection=selection, systematic=syst)
         bs = BootStrapper(dataframe, self.observables)
         results = []
         for i in range(0, nboots):
             print("Boostrap {}".format(i))
             bs_dataset = bs.bootstrap()
             results.append(migrad_minos(bs_dataset, self.model, self.poi, multicore=False, nuisance_parameters=self.nuis, constraints=self.constraints, specific_errs = ",".join([el.GetName() for el in self.nuis] + ["mass_point"]), globs = self.globs(), condobs=self.condobs()))
         return results

     def plot_model_variation(self, syst=""):
        if not syst or syst not in self.systematics:
            print("systematic {} is not a syst, returning")

        variations = [-3, -1, -0.5, 0, 0.5, +1, +3]
        np = self.get_np(syst)
        for cat in self.categories:
            plotting_dir = os.path.join(os.getenv("MassRegressionResolutionDir"), "SystematicVariationPlots", syst)
            if not os.path.exists(plotting_dir): os.makedirs(plotting_dir)
            plot_name = "{}_{}.eps".format(cat, syst)
            plot_full_name = os.path.join(plotting_dir, plot_name)

            if "m4l_constrained" in self.model.GetName(): frame = workspace_manager.get_variable("m4l_constrained").frame()
            else: frame = workspace_manager.get_variable("m4l_unconstrained").frame()

            c = r.TCanvas("c", "c", 1000, 1000)
            c.cd()
            leg = r.TLegend(0.6, 0.7, 0.88, 0.89)
            leg.SetBorderSize(0)
            colors = [r.kRed, r.kGreen, r.kBlue, r.kBlack, r.kMagenta, r.kYellow, r.kCyan]
            pdfs = []
            for i, variation in enumerate(variations):
                np.setVal(variation)
                pdf = self.get_pdf_in_cat(cat)
                pdfs.append(pdf)
                color = colors[i]
                print("Plotting pdf {} on frame".format(pdf.GetName()))
                pdf.plotOn(frame, r.RooFit.LineColor(color), r.RooFit.LineWidth(2), r.RooFit.Name("{}".format(i)), r.RooFit.Normalization(1.0, r.RooAbsReal.RelativeExpected))
                print("Done plotting")
            c.Draw()
            c.cd()
            frame.Draw()
            for i, (p, variation) in enumerate(zip(pdfs, variations)):
                leg.AddEntry(frame.findObject("{}".format(i)), "{} #sigma variation".format(variation), "l")
            leg.Draw()
            c.Print(plot_full_name)
        np.setVal(0.0) #reset the NP after plotting

     def make_model_plots(self):
        for s in self.systematics:
            self.plot_model_variation(syst=s)

def check_systematic_variations(syst):
        if "Norm_" in syst: return
        nuis_poi = pws.get_np(syst)
        pws.poi.setConstant(True)
        pws.poi.setVal(125.0)
        pws.get_np(syst).setVal(0.0)
        pws.get_np(syst).setConstant(False)
        for other_syst in pws.systematics:
            if other_syst == syst or "Norm_" in other_syst: continue
            pws.get_np(other_syst).setVal(0.0)
            pws.get_np(other_syst).setConstant(True)

        import time
        print("Testing an eval")
        start = time.time()
        print(pws.model.getVal())
        end = time.time()
        print("Done. {}".format(end - start))
        print("Testing an second eval")
        start = time.time()
        pws.poi.setVal(124.5)
        print(pws.model.getVal())
        end = time.time()
        print("Done. {}".format(end - start))
        pws.poi.setVal(125.0)
        pws.poi.setConstant(True)
        print("performing the scan")
        for variation in ["__1down", "__1up"]:
            result = pws.perform_scan(["_ggH125_"], poi=nuis_poi, syst="{}{}".format(syst, variation), constraint_free=True, constraints=self.constraints, nuisance_parameters=self.nuis)
            print("Result for syst {}".format(syst))

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser("parser")
    parser.add_argument("--slurmjob", dest="slurmjob", help="is this running in a slurm job?", action="store_true")
    parser.add_argument("--bootstrap", dest="bootstrap", help="are we bootstrapping?", action="store_true")
    parser.add_argument("--wsname", dest="wsname", help="is this running in a slurm job?", default="AllSystematics")
    parser.add_argument("--directory", dest="directory", help="the directory in which to save the results of the job", required=False)
    parser.add_argument("--mps", dest="mps", help="list of systematics to run minos for -- separated by commas", required=False)
    parser.add_argument("--do_parfix", dest="do_parfix", help="Should we do a parameter value fix?", required = False, action="store_true")
    parser.add_argument("--parfix", dest="parfix", help="The parameter value to fix to", required = False, type=float)
    parser.add_argument("--parname_forfix", dest="parname_forfix", help="The paraemter to fix", required = False)
    parser.add_argument("--plot_systematics", dest="plot_syst", help="Should we plot the systematic variations?",action="store_true")
    parser.add_argument("--signal_wcard", dest="signal_wcard", help="The wildcard to be used for signal MC", type=str, required=False, default="H125_")

    args = parser.parse_args()

    if args.plot_syst:
        pws = pyworkspace(args.wsname)
        pws.set_norm()
        for syst in pws.systematics: pws.plot_model_variation(syst)
        
    if not args.slurmjob:
        pws = pyworkspace(args.wsname)
        pws.condobs()
        print("DONE!")
        import time
        pws.set_norm()
        start = time.time()
        results_125 = pws.perform_scan([args.signal_wcard], syst="Nominal", category="", poi=pws.poi, constraint_free=False, constraints=pws.constraints, nuisance_parameters=pws.nuis, do_syst=True, specific_errs="mass_point", bootstrap=False)
        print(results_125)
        end = time.time()
        print(end - start)

    if args.slurmjob:
        pws = pyworkspace(args.wsname)
        pws.set_norm()
        pws.model.getVal()
        if args.do_parfix: pws.register_parfix(args.parname_forfix, args.parfix)
        save_file=args.directory
        save_file = os.path.join(args.directory, "result.pkl")
        pws.perform_scan([args.signal_wcard], syst="Nominal", category="", poi=pws.poi, constraint_free=False, constraints=pws.constraints, nuisance_parameters=pws.nuis, do_syst=True, specific_errs=args.mps, save_file=save_file, subsample=30000, bootstrap=args.bootstrap)
        print("FINSIHED THE JOB! :)")
