import uproot as ur
from root_numpy import fill_hist
import ROOT
import numpy as np
import imp
from array import array
from utils import get_binning

def ATLASLabel(x, y, text=None, color=1, size = None):
    """Draw the ATLAS Label.
    Parameters
    ----------
    x : float
        x position in NDC coordinates
    y : float
        y position in NDC coordinates
    text : string, optional
        Text displayed next to label (the default is None)
    color : TColor, optional
        Text colour (the default is 1, i.e. black).
        See https://ROOT.cern.ch/doc/master/classTColor.html
    """
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextColor(color)
    if size != None:
        l.SetTextSize(size)
    l.DrawLatex(x, y, r"#bf{#it{ATLAS}} " + text)

def DrawText(x, y, text, color=1, size=0.05):
    '''Draw text.
    Parameters
    ----------
    x : float
        x position in NDC coordinates
    y : float
        y position in NDC coordinates
    text : string, optional
        The text
    color : int, optional
        Text colour (the default is 1, i.e. black).
        See https://ROOT.cern.ch/doc/master/classTColor.html.
        If you know the hex code, rgb values, etc., use ``ROOT.TColor.GetColor()``
    size : float, optional
        Text size
        See https://ROOT.cern.ch/doc/master/classTLatex.html
    '''
    l = ROOT.TLatex()
    l.SetTextSize(size)
    l.SetNDC()
    l.SetTextColor(color)
    l.DrawLatex(x, y, text)

try:
    imp.find_module('atlasplots')
    foundAtlasPlots = True
    print("Found atlas plots module. Setting atlas style")
except ImportError:
    foundAtlasPlots = False
    print("Didn't find atlas plots module. Did NOT set atlas style. Continuing")

if foundAtlasPlots:
    from atlasplots import atlas_style as astyle
    astyle.SetAtlasStyle()

files = [\
"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root"\
]
files = [\
"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root",\
"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.345576.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH123_ZZ4l.root",\
"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.345577.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH124_ZZ4l.root",\
"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.345578.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH126_ZZ4l.root",\
"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.345579.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH127_ZZ4l.root",\
"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.364251.Sherpa_222_NNPDF30NNLO_llll_m4l100_300_filt100_150.root",\
#"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.345581.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH123p5_ZZ4l.root",\
#"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.345582.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH124p5_ZZ4l.root",\
#"truthsub_2019_11_14_21_5_bianca_trees/mc16_13TeV.345583.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125p5_ZZ4l.root",\
]

stamp = "_".join(files[0].split("_")[:6])

selection = "((m4l_constrained > 100.0) and (m4l_constrained < 150.0)) and ((m4l_truth_matched_bare != -999)) and (event_type  < 0.5 and event_type > -0.5)"

def cleanUpHistograms(h):
    '''Create a histogram with it's characteristics set to standard values.'''
    h.SetLineStyle(1)
    h.SetLineWidth(2)
    h.SetFillStyle(0)
    h.SetFillColor(0)
    h.SetMarkerSize(3)
    return h

first_pass_types = []
first_pass_type_names = []

if __name__ == "__main__":
    for flavour in ["event_class", "pt_class"]:
       descriptors = []
       histograms = []
       sigma_histograms = []
       sigma_histogram_identifiers = []
       for file_counter,f in enumerate(files):
          print("Opening {}".format(f))
          df = ur.open(f)["tree_incl_all"].pandas.df()
          for col in df.columns:
              df[col] = df[col].astype(float)
          df = df.query(selection)

          if file_counter == 0:
             minimum = np.min(df.eval("sigma_prediction_4mu").values)
             maximum = np.max(df.eval("sigma_prediction_4mu").values)
             bin_width = 0.03
             bin_array = get_binning(minimum, maximum, bin_width, df, "sigma_prediction_4mu", 0.03)
             bin_array=array("d", bin_array)

          low_bins =  [b for b in bin_array[:-1]]
          high_bins =  [b for b in bin_array[1:]]

          #get all of the possible configurations for the leptons:
          types = np.unique(df.eval(flavour).values)

          assert len(types)< 100 #make sure there aren't too many types for the next for-loop

          types = np.sort(types) #types sorted in descending order

          type_names = []
          for t in types:
              print(t)
              n_cent = int(t)/100
              n_gap = int(t - 100 * n_cent)/10
              n_forward = int(t - (100 * n_cent) - (10 * n_gap))
              if flavour == "event_class":
                  assert n_cent + n_gap + n_forward == 4
                  string = n_forward * "E" + n_gap * "B" + n_cent * "C"
              else:
                  assert n_cent + n_gap + n_forward == 2
                  string = n_forward * "L" + n_gap * "M" + n_cent * "H"
              print("Found selection type {}".format(string))
              type_names.append(string)

          background = False
          if "ggH" in f:
              identifier = f.split("ggH")[-1].rstrip(".root")
          elif f and "llll" in f:
              identifier = "ZZ* #rightarrow 4#mu"
              background = True
          else:
              print("Couldn't find identifier")
              assert False
          hist = ROOT.TH1D()
          hist.SetName(flavour)
          hist.SetCanExtend(ROOT.TH1.kAllAxes)
          hist.SetName(identifier)

          max_value = -1.0
          min_value = 100000000.0

          bin_counter = 0
          for bindex, t, t_name in zip(list(range(1, len(types) + 1)), types, type_names):
              this_selection = "{} == {}".format(t, flavour)
              tmp_df = df.query(this_selection)
              sigmas = tmp_df.eval("sigma_prediction_4mu").values
              weights = tmp_df.eval("weight").values
              mean_sigma = np.sum(weights * sigmas)/np.sum(weights)

              if mean_sigma > max_value:
                  max_value = mean_sigma
              if mean_sigma < min_value:
                  min_value = mean_sigma

              std_def_sigma = np.sqrt(np.sum(weights * sigmas**2)/np.sum(weights) - mean_sigma **2)
              print(len(sigmas))
              if (file_counter == 0) and (len(sigmas) > 10000):
                  first_pass_types.append(t)
                  first_pass_type_names.append(t_name)
              if t_name in first_pass_type_names:
                  bin_counter += 1
                  hist.GetXaxis().FindBin(t_name)
                  hist.SetBinContent(bin_counter, mean_sigma)
                  hist.SetBinError(bin_counter, std_def_sigma)

          c1 = ROOT.TCanvas("Resolution Canvas", "Title", 3200, 1800)
          c1.Draw()
          c1.SetRightMargin(0.1)

          hist.SetMaximum(max_value * 1.3)
          hist.SetMinimum(min_value * 0.85)
          hist=cleanUpHistograms(hist)

          hist.Draw("P")
          hist.SetMarkerStyle(ROOT.kBlack)
          hist.GetYaxis().SetTitle("#sigma_{NN}")
          #hist.GetYaxis().SetTitleSize(0.17)
          hist.SetMarkerStyle(8)
          hist.SetMarkerSize(1.0)

          c1.Update()
          c1.Modified()

          ATLASLabel(0.2, 0.87, "Internal")
          mass = (identifier.replace("_ZZ4l", "")).replace("p5",".5")

          if "ggH" in f:
              DrawText(0.2, 0.8, "ggH, M_{H}" +  " = {} GeV, ZZ#rightarrow 4 #mu".format(identifier.split("_")[0]))
          elif f and "llll" in f:
              DrawText(0.2, 0.8, "ZZ#rightarrow 4 #mu".format(identifier.split("_")[0]))
          DrawText(0.2, 0.73, stamp)
          c1.Print("ResolutionPlot_{}_{}_{}.png".format(identifier,flavour, stamp))

          if not background:
              descriptors.append("m_{H} = " + identifier.replace("_ZZ4l","") + " GeV")
          else:
              descriptors.append(identifier)

          histograms.append(hist)

          #make a histogram of the inclusive sigma distribution
          sigmas = df.eval("sigma_prediction_4mu").values
          weights = df.eval("weight").values
          c1 = ROOT.TCanvas("Resolution Predictions", "Predictions", 3200, 1800)
          c1.Draw()
          sigma_hist = ROOT.TH1D("sigma_hist_{}".format(identifier), "sigma_hist_{}".format(identifier),len(bin_array) - 1, bin_array)
          sigma_hist.Sumw2()
          fill_hist(sigma_hist, sigmas, weights)
          sigma_hist.GetXaxis().SetTitle("#sigma_{NN}")
          sigma_hist.GetYaxis().SetTitle("Number of Events")
          sigma_hist.Draw()
          ATLASLabel(0.2, 0.87, "Internal")
          if not background:
              DrawText(0.2, 0.8, "ggH, M_{H}" + " = {} GeV, ZZ#rightarrow 4 #mu".format(identifier.split("_")[0]))
          else:
              DrawText(0.2, 0.8, "ZZ*#rightarrow 4#mu")
          DrawText(0.2, 0.73, stamp)
          c1.Update()
          c1.Modified()
          c1.Print("SigmaPredicted{}_{}.png".format(identifier, stamp))

          sigma_hist_normed = ROOT.TH1D("sigma_hist_normed_{}".format(identifier), "sigma_hist_normed_{}".format(identifier), len(bin_array) - 1, bin_array)
          sigma_hist.Sumw2()
          fill_hist(sigma_hist_normed, sigmas, weights/np.sum(weights))
          sigma_hist_normed.GetXaxis().SetTitle("#sigma_{NN}")
          sigma_hist_normed.GetYaxis().SetTitle("Normalized.")
          sigma_histograms.append(sigma_hist_normed)
          if not background: sigma_histogram_identifiers.append("M_{H}" + " = {} GeV, ZZ#rightarrow 4 #mu".format(identifier.split("_")[0]))
          else: sigma_histogram_identifiers.append(identifier)

          print(np.sum(weights))
          print(np.sum(weights[sigmas>2.5]))
          raw_input()

       CB_color_cycle = [\
       ROOT.kBlack,\
       r'#377eb8', r'#ff7f00',r'#4daf4a',\
       r'#f781bf', r'#a65628', r'#984ea3',\
       r'#999999', r'#dede00']
       #CB_color_cycle = [CB_color_cycle[0]] + CB_color_cycle[::-1]

       for hist,c in zip(histograms, CB_color_cycle):
           hist.SetLineColor( ROOT.TColor.GetColor(c))
           hist.SetMarkerColor( ROOT.TColor.GetColor(c))

       ###########################################################
       for hist,c in zip(sigma_histograms, CB_color_cycle):
           hist.SetLineColor( ROOT.TColor.GetColor(c))
           hist.SetMarkerColor( ROOT.TColor.GetColor(c))

       all_ratio_canvas = ROOT.TCanvas("AllRatioCanvas_{}".format(flavour), "Title", 3200, 1800)
       nominal_hist = sigma_histograms[0]
       all_ratio_canvas.Draw()
       all_ratio_canvas.cd()

       top = ROOT.TPad("top", "top", 0.0, 0.3, 1.0, 1.0)
       top.SetLeftMargin(0.15)
       top.SetBottomMargin(0.0)
       top.Draw()
       top.cd()
       nominal_hist.Draw("P")
       nominal_hist.SetMinimum(0.00001)
       nominal_hist.SetMaximum(nominal_hist.GetMaximum() * 1.2)
       #nominal_hist.SetMinimum(max([hist.GetMaximum() for hist in sigma_histograms]))

       nominal_hist.GetXaxis().SetTitleFont(43)
       nominal_hist.GetXaxis().SetTitleSize(25)
       nominal_hist.GetXaxis().SetTitleOffset(4.0)
       nominal_hist.GetXaxis().SetLabelSize(35)
       nominal_hist.GetXaxis().SetLabelFont(43)
       nominal_hist.GetYaxis().SetLabelSize(35)
       nominal_hist.GetYaxis().SetLabelFont(43)
       nominal_hist.GetYaxis().SetTitleSize(35)
       nominal_hist.GetYaxis().SetTitleFont(43)

       leg = ROOT.TLegend(0.6, 0.5, 0.85, 0.8)
       leg.AddEntry(nominal_hist, sigma_histogram_identifiers[0])
       for other_hist, identifier in zip(sigma_histograms[1:], sigma_histogram_identifiers[1:]):
           other_hist.Draw("P SAME")
           leg.AddEntry(other_hist, identifier)
       leg.Draw("SAME")
       ATLASLabel(0.2, 0.87, "Internal")
       DrawText(0.2, 0.8, stamp)
       top.Update()
       top.Modified()
       all_ratio_canvas.cd()

       bottom=ROOT.TPad("bottom", "bottom", 0.0, 0.0, 1.0, 0.3)
       bottom.SetBottomMargin(0.4)
       bottom.SetLeftMargin(0.15)
       bottom.SetTopMargin(0.0)
       bottom.Draw()
       bottom.cd()
       sigma_hist_counter = -1
       ratios = []
       for other_hist, identifier in zip(sigma_histograms[1:], sigma_histogram_identifiers[1:]):
           sigma_hist_counter += 1
           to_divide=other_hist.Clone(other_hist.GetName() + "Divided")
           to_divide.SetMarkerColor(other_hist.GetMarkerColor())
           to_divide.SetLineColor(other_hist.GetLineColor())
           to_divide.Divide(nominal_hist)
           if sigma_hist_counter == 0:
               to_divide.SetMaximum(1.199)
               to_divide.SetMinimum(0.801)
               to_divide.Draw("P")
               to_divide.GetYaxis().SetTitle("#frac{X}{M_{H} = 125 GeV}")

               to_divide.GetXaxis().SetTitleFont(43)
               to_divide.GetXaxis().SetTitleSize(45)
               to_divide.GetXaxis().SetTitleOffset(4.5)
               to_divide.GetXaxis().SetLabelSize(35)
               to_divide.GetXaxis().SetLabelFont(43)
               to_divide.GetYaxis().SetLabelSize(35)
               to_divide.GetYaxis().SetLabelFont(43)
               to_divide.GetYaxis().SetTitleSize(35)
               to_divide.GetYaxis().SetTitleFont(43)

           else:
               to_divide.Draw("P SAME")
           ratios.append(to_divide)

       ##Draw a set of solid and dotted lines on the ratio plot to guide the reader's eyes
       ratio_max = 1.2
       ratio_min = 0.8
       straight_line = ROOT.TF1("line1", str(1.0) , -10e6, + 10e6)
       straight_line.SetLineWidth(2)
       straight_line.Draw("Same")

       straight_line_up = ROOT.TF1("line2",  str(1.0 + (2.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
       straight_line_up.SetLineWidth(1)
       straight_line_up.SetLineStyle(1)
       straight_line_up.Draw("Same")

       straight_line_up2 = ROOT.TF1("line3",  str(1.0 + (1.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
       straight_line_up2.SetLineWidth(1)
       straight_line_up2.SetLineStyle(3)
       straight_line_up2.Draw("Same")

       straight_line_up3 = ROOT.TF1("line4",  str(1.0 + (3.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
       straight_line_up3.SetLineWidth(1)
       straight_line_up3.SetLineStyle(3)
       straight_line_up3.Draw("Same")

       straight_line_down3 = ROOT.TF1("line5",  str(1.0 - (3.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
       straight_line_down3.SetLineWidth(1)
       straight_line_down3.SetLineStyle(3)
       straight_line_down3.Draw("Same")

       straight_line_down = ROOT.TF1("line6",  str(1.0 - (2.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
       straight_line_down.SetLineWidth(1)
       straight_line_down.SetLineStyle(1)
       straight_line_down.Draw("Same")

       straight_line_down2 = ROOT.TF1("line7",  str(1.0 - (1.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
       straight_line_down2.SetLineWidth(1)
       straight_line_down2.SetLineStyle(3)
       straight_line_down2.Draw("Same")
       bottom.Update()
       bottom.Modified()
       all_ratio_canvas.Update()
       all_ratio_canvas.Modified()
       all_ratio_canvas.Print(sigma_hist.GetName() + "_Ratios_{}".format(stamp) + ".png")
       ###########################################################

       all_canvas = ROOT.TCanvas("AllCanvas_{}".format(flavour), "Title", 3200, 1800)
       all_canvas.SetRightMargin(0.1)
       all_canvas.Draw()
       bin_width = 0.5*histograms[0].GetBinWidth(1)
       n_histograms = float(len(histograms))
       shift = -1.0 * bin_width
       step = bin_width/(n_histograms/2.0)
       shift += (step/2.0)
       legend = ROOT.TLegend(0.53, 0.62, 0.85, 0.89)
       legend.SetBorderSize(0)
       for i, hist, descriptor in zip(list(range(1, len(histograms) + 1)),histograms, descriptors):
           legend.AddEntry(hist, descriptor)
           hist.GetXaxis().SetLimits(hist.GetXaxis().GetXmin()+shift,hist.GetXaxis().GetXmax()+shift)
           if i == 1:
              hist.Draw("P")
           else:
              hist.Draw("P SAME")
           shift += step

       if flavour == "event_class":
          ATLASLabel(0.2, 0.87, "Internal")
          DrawText(0.2, 0.8, "ggH #rightarrow ZZ #rightarrow 4 #mu")
          DrawText(0.2, 0.73, stamp)
          DrawText(0.2, 0.35, "E: |#eta|>1.47",size=0.05)
          DrawText(0.2, 0.27, "B: 0.6<|#eta|<1.47",size=0.05)
          DrawText(0.2, 0.19, "C: |#eta|<0.6",size=0.05)
       else:
          ATLASLabel(0.2, 0.87, "Internal")
          DrawText(0.2, 0.8, "ggH #rightarrow ZZ #rightarrow 4 #mu")
          DrawText(0.2, 0.73, stamp)
          DrawText(0.7, 0.35, "L: P_{T} < 40.0 GeV",size=0.05)
          DrawText(0.7, 0.27, "M: 40.0 GeV < P_{T} < 60.0 GeV",size=0.05)
          DrawText(0.7, 0.19, "H: 60.0 GeV <P_{T}",size=0.05)
       legend.Draw()
       all_canvas.Modified()
       all_canvas.Update()
       all_canvas.Print("AllResolutionComparison{}_{}.png".format(flavour, stamp))
       all_canvas.Close()

