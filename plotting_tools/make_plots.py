import ROOT
import os
import root_numpy
from plotting_utils import plot_roofit_on_canvas, prepare_for_fit
os.system("rm -rf /home/ladamek/.cache/matplotlib/tex.cache/.matplotlib_lock-*")
CB_color_cycle = [r'#377eb8', r'#ff7f00',r'#dede00', r'#4daf4a',\
                          r'#f781bf', r'#a65628', r'#984ea3',\
                                            r'#999999'] 
##'#dede00'
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.tight_layout()
matplotlib.rcParams['axes.prop_cycle'] = matplotlib.cycler(color=CB_color_cycle) 
matplotlib.rcParams['font.size'] = 13
matplotlib.rcParams['figure.autolayout'] = True
matplotlib.rcParams['mathtext.fontset'] = 'custom'
matplotlib.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
matplotlib.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
matplotlib.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
import numpy as np
import pandas as pd
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
plt.tight_layout()

#import atlas_mpl_style as ampl
#ampl.use_atlas_style()

def make_error_boxes(xdata, ydata, xerror, yerror, facecolor='r',
                     edgecolor='None', alpha=0.5):
    ax = plt.gca()

    # Create list for all the error patches
    errorboxes = []
    # Loop over data points; create box from errors at each point
    for x, y, xe, ye in zip(xdata, ydata, xerror, yerror):
        rect = Rectangle((x - xe, y - ye), xe * 2, ye * 2)
        errorboxes.append(rect)

    # Create patch collection with specified colour/alpha
    pc = PatchCollection(errorboxes, facecolor=facecolor, alpha=alpha,
                         edgecolor=edgecolor, lw=0, zorder = 100)
    pc.set_hatch("/////")
    pc.set_facecolor("none")
    pc.set_edgecolor("k")

    # Add collection to axes
    ax.add_collection(pc)

def check_model(dfs, weight="weight", mass_var = "m4l_constrained", descriptor = "", out_dir = None, mass_points = [125.0, 126.0, 127.0, 123.0, 124.0], retrieval_function = None, event_type_string=""):
    fit_results = {}
    signal_parameterization = {}
    print(dfs.eval("mass_point").values)
    for mp in mass_points:
        this_df = dfs.query("(mass_point == {})".format(mp))
        print(len(np.unique(this_df.eval("mass_point").values))) 
        assert(len(np.unique(this_df.eval("mass_point").values)) == 1)

        masses = this_df.eval(mass_var).values
        to_weight = this_df.eval(weight).values
        #to_weight *= np.average(1.0 * (to_weight!=0.0)) * (float(len(to_weight))/np.sum(to_weight))

        m4l_mass = ROOT.RooRealVar("m4l_constrained","m4l_constrained", 100.0,150.0)
        m4l_mass.setRange("fit_range",100.0, 150.0)
        mean = ROOT.RooRealVar("mean", "mean",retrieval_function(event_type_string,"mean", mp), 110.0, 130.0)
        sigma = ROOT.RooRealVar("sigma","sigma", retrieval_function(event_type_string,"sigma", mp), 0.0, 10.0)
        alpha_hi = ROOT.RooRealVar("alpha_hi", "alpha_hi",retrieval_function(event_type_string,"alpha_hi", mp), 0.0, 5.0)
        alpha_low = ROOT.RooRealVar("alpha_low", "alpha_low",retrieval_function(event_type_string,"alpha_low", mp), 0.0, 5.0)
        n_low  =ROOT.RooRealVar("n_low","n_low",retrieval_function(event_type_string,"n_low", mp), 1.0, 100.0)
        n_hi = ROOT.RooRealVar("n_hi", "n_hi", retrieval_function(event_type_string,"n_hi", mp), 1.0, 100.0)
        roodcb = ROOT.RooTwoSidedCBShape("roodcb", "roodcb",m4l_mass, mean, sigma, alpha_low, n_low, alpha_hi, n_hi)
        roo_weight = ROOT.RooRealVar("weight", "weight", -10000.0, +10000.0)

        signal_parameterization[mp] ={}
        signal_parameterization[mp]["mean"] = mean.getVal()
        signal_parameterization[mp]["sigma"] = sigma.getVal()
        signal_parameterization[mp]["alpha_low"] = alpha_low.getVal()
        signal_parameterization[mp]["alpha_hi"] = alpha_hi.getVal()
        signal_parameterization[mp]["n_low"] = n_low.getVal()
        signal_parameterization[mp]["n_hi"] = n_hi.getVal()

        tmp_filename = "tmp.root"
        masses =  np.core.records.fromarrays( [masses], names=m4l_mass.GetName())
        root_numpy.array2root(masses, tmp_filename, treename="Tree", mode="recreate")
        to_write = np.core.records.fromarrays( [to_weight], names=roo_weight.GetName())
        root_numpy.array2root(to_write, tmp_filename, treename="Tree", mode="update")

        prepare_for_fit()

        #now get the tree and make the data
        f = ROOT.TFile(tmp_filename, "READ")
        t = f.Get("Tree")
        #data = ROOT.RooDataSet("Data", "Data", t, ROOT.RooArgSet(m4l_mass, roo_weight), "", "weight")
        data = ROOT.RooDataSet("Data", "Data", ROOT.RooArgSet(m4l_mass, roo_weight), ROOT.RooFit.WeightVar(roo_weight), ROOT.RooFit.Import(t))
        f.Close()
        os.system("rm {}".format(tmp_filename))

        #let everything vary
        sigma_const = False
        mean_const = True
        alpha_hi_const = True
        alpha_low_const = True
        n_low_const = True
        n_hi_const = True

        fit_dof = ( 1 * (not sigma_const) + 1 * (not mean_const) + 1 * (not alpha_hi_const) + 1 * (not alpha_low_const) + 1 * (not n_low_const) + 1 * (not n_hi_const) )
        fine_binning = ROOT.RooBinning(100, 100.0, 150.0)
        #draw everything
        if True:
            c, top, bottom, f, frame2, pull, lines, chi2 = plot_roofit_on_canvas(m4l_mass, data,roodcb, "m_{4l\ constrained}\ \ [GeV]", "fit_range", fine_binning, draw_count="", fit_dof=fit_dof, plot_commands=[])
            c.Print(os.path.join(out_dir, "ModelFitFitOn_MassPoint_{}.png".format(mp)))
            c.Close()

        mean.setConstant(mean_const)
        sigma.setConstant(sigma_const)
        alpha_hi.setConstant(alpha_hi_const)
        alpha_low.setConstant(alpha_low_const)
        n_low.setConstant(n_low_const)
        n_hi.setConstant(n_hi_const)

        roodcb.fitTo(data, ROOT.RooFit.SumW2Error(True), ROOT.RooFit.Range("fit_range"), ROOT.RooFit.Save(True))
        fit_results[mp] ={}
        fit_results[mp]["mean"] = mean.getVal()
        fit_results[mp]["sigma"] = sigma.getVal()
        fit_results[mp]["alpha_low"] = alpha_low.getVal()
        fit_results[mp]["alpha_hi"] = alpha_hi.getVal()
        fit_results[mp]["n_low"] = n_low.getVal()
        fit_results[mp]["n_hi"] = n_hi.getVal()

        fit_results[mp]["mean_err"] = mean.getError()
        fit_results[mp]["sigma_err"] = sigma.getError()
        fit_results[mp]["alpha_low_err"] = alpha_low.getError()
        fit_results[mp]["alpha_hi_err"] = alpha_hi.getError()
        fit_results[mp]["n_low_err"] = n_low.getError()
        fit_results[mp]["n_hi_err"] = n_hi.getError()

        fit_dof = 1 * (not sigma_const) + 1 * (not mean_const) + 1 * (not alpha_hi_const) + 1 * (not alpha_low_const) + 1 * (not n_hi_const) + 1 * (not n_low_const)

        #draw everything
        if True:
            c, top, bottom, f, frame2, pull, lines, chi2 = plot_roofit_on_canvas(m4l_mass, data, roodcb, "m_{4l\ constrained}\ \ [GeV]", "fit_range", fine_binning, draw_count="", fit_dof=fit_dof, plot_commands=[])
            c.Print(os.path.join(out_dir, "ModelRefitFitOn_MassPoint_{}.png".format(mp)))
            c.Close()

    for mp in signal_parameterization:
        print("Checking MP {} GeV".format(mp))
        for var in signal_parameterization[mp]:
            print("var {}, Tom's parameterization {}, This parameterization {}, diff {}, error {}".format(var, signal_parameterization[mp][var], fit_results[mp][var], signal_parameterization[mp][var] - fit_results[mp][var], fit_results[mp][var + "_err"]))

def make_plots(df, weight="weight", variables=[], descriptor="", base_directory=""):
    to_weight = df.eval("weight").values
    for vars in variables:
        for var in vars:
            print("Plotting {}".format(var))
            to_plot = df.eval(var).values
            print(to_plot)
            exp_sq = (1.0/np.sum(to_weight)) * np.sum(to_weight * (to_plot ** 2))
            exp = (1.0/np.sum(to_weight)) * np.sum(to_weight * (to_plot))
            if exp_sq - (exp**2) < 0.0001: std_dev = 1.0
            else: std_dev = (exp_sq - (exp**2)) ** 0.5
            width = 3.5 * std_dev / (float(len(to_plot)) ** 0.33)
            print("Histogram width {}".format(width))
            nbins = int( (max(to_plot) - min(to_plot))/width)
            print("Bins {}".format(nbins))
            bins = np.linspace(min(to_plot), max(to_plot), nbins+1)
            cont, edges, stuff = plt.hist(to_plot, weights = to_weight, bins=bins)
            cont_sq, edges, stuff = plt.hist(to_plot, weights = to_weight**2, bins=bins)
            err=np.sqrt(cont_sq)
            plt.close()
            bin_centres = [(edges[i] + edges[i+1])/2.0 for i in range(0, len(edges) - 1)]
            #create hatch error boxes for the uncertainties
            cont, edges, stuff = plt.hist(to_plot, weights = to_weight, bins=bins)
            make_error_boxes(bin_centres, cont, [(edges[i] - edges[i+1])/2.0 for i in range(0, len(edges) - 1)], err)
            plt.xlabel(var)
            if "m4l_constrained - " in var and "mass_point == " in var: var = "m4l_constrained_shifted"
            if "blank + " in var and "mass_point == " in var: var = "blank"
            if "blank - " in var and "mass_point == " in var: var = "blank"
            plt.xlabel(var)
            fig = os.path.join(base_directory,"{}_{}.png".format(var, descriptor))
            print("saving this figure {}".format(fig))
            plt.savefig(fig)
            plt.close()
