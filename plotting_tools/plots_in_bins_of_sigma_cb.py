import uproot as ur
import utils
from root_numpy import fill_hist
import ROOT
import numpy as np
import imp
from plots import ATLASLabel, DrawText
from parameters import  get_parameters, get_new_parameters
import root_numpy
import os
from array import array
from utils import get_binning, get_equal_stats_binning
from plotting_utils import plot_roofit_on_canvas, DrawText, ATLASLabel, prepare_for_fit, return_bindex
import workspace_manager

import matplotlib
matplotlib.use("Agg")
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['font.size'] = 13
matplotlib.rcParams['figure.autolayout'] = True
matplotlib.rcParams['mathtext.fontset'] = 'custom'
matplotlib.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
matplotlib.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
matplotlib.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
import matplotlib.pyplot as plt

import argparse

plot_low = 105.0
plot_high = 160.0

parser = argparse.ArgumentParser(description="Plotting Jobs")
parser.add_argument('--flavour', '-f', dest="flavour", type=str, required=True, help='the type of variable to make plots for')
parser.add_argument('--score_base', '-sb', dest="score_base", type=str, required=False, default="sigma_prediction_{}", help='the name of the variable to be studied')
parser.add_argument('--out_folder', '-of', dest="out_folder", type=str, required=True, help="The directoty in which to save the plots")
parser.add_argument('--extra_models', '-em', dest="extra_models", type=str, required=True, help="The models required for plotting")
parser.add_argument("--add_nominal", "-an", dest="add_nominal", action="store_true")
args = parser.parse_args()


try:
    imp.find_module('atlasplots')
    foundAtlasPlots = True
    print("Found atlas plots module. Setting atlas style")
except ImportError:
    foundAtlasPlots = False
    print("Didn't find atlas plots module. Did NOT set atlas style. Continuing")

if foundAtlasPlots:
    from atlasplots import atlas_style as astyle
    astyle.SetAtlasStyle()

files = [\
"mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root",\
"mc16_13TeV.345576.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH123_ZZ4l.root",\
"mc16_13TeV.345577.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH124_ZZ4l.root",\
"mc16_13TeV.345578.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH126_ZZ4l.root",\
"mc16_13TeV.345579.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH127_ZZ4l.root",\
]

score_base = args.score_base
flavours = args.flavour.split(",")
output_folder = args.out_folder

import os
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

#this is the offset from 125 GeV
offsets = [\
0.0,\
-2.0,\
-1.0,\
+1.0,\
+2.0,\
]

descriptions = [\
"ggH, m_{H} = 125 GeV",\
"ggH, m_{H} = 123 GeV",\
"ggH, m_{H} = 124 GeV",\
"ggH, m_{H} = 126 GeV",\
"ggH, m_{H} = 127 GeV",\
]

event_type_map = {}
event_type_map["4mu"] = 0
event_type_map["4e"] = 1
event_type_map["2mu2e"] = 2
event_type_map["2e2mu"] = 3

params_lookup = {}
retriever = utils.get_v24_retriever()
if not args.add_nominal: retriever.register_extra_models(args.extra_models.split(","))
else: utils.add_nominal_resolution_models(retriever)

for flavour  in flavours:
    selection = utils.get_full_selection(flavour, "Incl", "m4l_constrained")
    fit_histograms = []
    deeper_plots_folder = output_folder

    for i_f, (f, offset, description) in enumerate(zip(files,offsets,descriptions)):
        df = retriever.get_dataframe([f], variables = ["m4l_constrained", "weight", "m4l_truth_matched_bare","event_type"], selection=selection)

        if i_f == 0:
           minimum = np.min(df.eval(score_base.format(flavour)).values)
           maximum = np.max(df.eval(score_base.format(flavour)).values)
           bin_width = 0.05
           bin_array = get_binning(minimum, maximum, bin_width, df, score_base.format(flavour), 0.1)

        low_bins =  [b for b in bin_array[:-1]]
        high_bins =  [b for b in bin_array[1:]]
        values_from_regression = []
        error_on_values_from_regression = []
        sigma_values_from_roofit = []
        error_on_sigma_values_from_roofit = []
        mean_values_from_roofit = []
        error_on_mean_values_from_roofit = []
        alpha_low_values_from_roofit = []
        error_on_alpha_low_values_from_roofit = []
        alpha_hi_values_from_roofit = []
        error_on_alpha_hi_values_from_roofit = []
        n_hi_values_from_roofit = []
        error_on_n_hi_values_from_roofit = []
        n_low_values_from_roofit = []
        error_on_n_low_values_from_roofit = []
        bin_width = low_bins[1] - low_bins[0]
        bin_centers = []

        TotalCanvas = ROOT.TCanvas("AllOfThePlots", "AllOfThePlots", 2000, 3000)
        nx = 3
        ny = 4
        TotalCanvas.Divide(nx,ny)
        # do a MLE fit in each bin
        draw_count = 0
        drawn = 0

        print("Getting arrays")
        nbins_fine = 100
        fine_binning = ROOT.RooBinning(100, 105.0, 160.0)
        fine_bin_edges = np.linspace(105.0, 160.0, nbins_fine + 1)
        nbins_coarse = 50
        coarse_binning = ROOT.RooBinning(50, 105.0, 160.0)
        coarse_bin_edges = np.linspace(105.0, 160.0, nbins_coarse + 1)
        nbins_coarsest = 25
        coarsest_binning = ROOT.RooBinning(25, 105.0, 160.0)
        coarsest_bin_edges = np.linspace(105.0, 160.0, nbins_coarsest + 1)
        print("Got arrays")
        poi, model, pdfs, observables, parameters, nuis_parameters, functions, categories = workspace_manager.merge_bkg_signal_models(retriever, mass_observable = "m4l_constrained", channels = [flavour], systematics = [], categories=["Incl"], do_bkg=False, do_sig =True, model_types="Che", bkgs=[])
        poi.setVal(125.0 + offset)

        keep_pads_alive = []
        for low_bin, high_bin in zip(low_bins, high_bins):
            draw_count += 1
            #create strings to select the events from the dataframe
            low_selection = "{} < {}".format(low_bin, score_base.format(flavour))
            high_selection = "{} > {}".format(high_bin, score_base.format(flavour))

            #create strings to put on plots
            formatted_low_selection = "{0:.2f} GeV < {1}".format(low_bin, score_base.format(flavour))
            formatted_high_selection = "{0:.2f} GeV > {1}".format(high_bin, score_base.format(flavour))

            #create the formatted and non-formatted selections strings
            sigma_bin_selection = "({}) and ({})".format(low_selection, high_selection) #for selecting
            formatted_sigma_bin_selection = "({}) and ({})".format(formatted_low_selection, formatted_high_selection) #for putting on the plot
            df_in_bin = df.query(sigma_bin_selection) #select the events in this bin

            #load the parameters
            import signal_model as __signal_model__
            params = __signal_model__.get_params("m4l_constrained", flavour, "Incl", "Nominal")

            m4l_mass = workspace_manager.get_variable("m4l_constrained")
            mean = ROOT.RooRealVar("mean", "mean", 125.0, 0.0, 300.0)
            sigma = ROOT.RooRealVar("sigma", "sigma", 1.0, 0.01, 10.0)
            alpha_hi = ROOT.RooRealVar("alpha_hi", "alpha_hi", 1.0, 0.1, 5.0)
            alpha_low = ROOT.RooRealVar("alpha_low", "alpha_low", 1.0, 0.1, 5.0)
            n_hi = ROOT.RooRealVar("n_hi", "n_hi", 2.0, 1.0, 12.0)
            n_low = ROOT.RooRealVar("n_low", "n_low", 2.0, 1.0, 12.0)

            mean.setVal(params["mean"].getVal())
            sigma.setVal(params["sigma"].getVal())
            alpha_hi.setVal(params["alpha_hi"].getVal())
            alpha_low.setVal(params["alpha_low"].getVal())
            n_hi.setVal(params["n_hi"].getVal())
            n_low.setVal(params["n_low"].getVal())

            #let everything vary
            sigma_const = False
            mean_const = True
            alpha_hi_const = True
            alpha_low_const = True
            n_low_const = True
            n_hi_const = True

            fit_dof = 1 * (not sigma_const) + 1 * (not mean_const) + 1 * (not alpha_hi_const) + 1 * (not alpha_low_const) + 1 * (not n_hi_const) + 1 * (not n_low_const)

            sigma.setConstant(sigma_const)
            mean.setConstant(mean_const)
            alpha_hi.setConstant(alpha_hi_const)
            alpha_low.setConstant(alpha_low_const)
            n_low.setConstant(n_low_const)
            n_hi.setConstant(n_hi_const)

            #create a temporary local ttree with unbinned information
            masses_in_bin = df_in_bin.eval("m4l_constrained").values

            #also get the information about an optimal binning
            bindices = return_bindex(masses_in_bin, fine_bin_edges)

            lowest_plotting_bin = return_bindex(np.array([plot_low + offset]), fine_bin_edges)[0]
            highest_plotting_bin = return_bindex(np.array([plot_high + offset]), fine_bin_edges)[0]

            binning = fine_binning
            binning_edges = fine_bin_edges
            for i in range(lowest_plotting_bin, highest_plotting_bin + 1):
                print(len(bindices[ (bindices > i-0.5) & (bindices < i+0.5)]))
                if len(bindices[ (bindices > i-0.5) & (bindices < i+0.5)]) < 10:
                    binning = coarse_binning
                    bin_edges = coarse_bin_edges
                    #raw_input("Broken loop")
                    break

            from workspace_manager import get_dataset_from_df
            fit_to_this_data = get_dataset_from_df(df_in_bin, ["m4l_constrained"], selection, False)

            #define a ROOT.DoubleSidedCB
            roodcb = ROOT.RooTwoSidedCBShape("roodcb{}".format(draw_count), "roodcb",m4l_mass, mean, sigma, alpha_low, n_low, alpha_hi, n_hi)
            prepare_for_fit()
            m4l_mass.setRange("FitRange", 105.0, 160.0)
            result = roodcb.fitTo(fit_to_this_data, ROOT.RooFit.SumW2Error(True),ROOT.RooFit.Range("FitRange"), ROOT.RooFit.Save(True))

            #draw_roofit_on_canvas(xvar, data, function, xlabel, plot_range, binning)
            c, top, bottom, frame1, frame2, pull, lines, chi2 = plot_roofit_on_canvas(m4l_mass, fit_to_this_data, roodcb, "m_{4l\ constrained} [GeV]", "FitRange", binning, draw_count = draw_count,  fit_dof = fit_dof)
            top.cd()

            top.cd()
            DrawText(0.2, 0.75, "Selection: " + formatted_sigma_bin_selection.replace("_prediction","_{NN}").replace("_corrected","").replace("sigma","#sigma"), size = 0.06)
            DrawText(0.2, 0.68, "#sigma_{CB}" + " = {0:.2f} #pm {1:.2f} GeV".format(sigma.getVal(), sigma.getError()), size = 0.06)
            #DrawText(0.2, 0.58, "#frac{#Chi^{2}}{nDOF}" + " = {0:.2f}".format(chi2), size = 0.06)
            ATLASLabel(0.2, 0.87, "Internal")

            if flavour == "4mu":
                DrawText(0.2, 0.82, "ggH #rightarrow ZZ #rightarrow 4 #mu")
            if flavour == "4e":
                DrawText(0.2, 0.82, "ggH #rightarrow ZZ #rightarrow 4 e")
            if flavour == "2mu2e":
                DrawText(0.2, 0.82, "ggH #rightarrow ZZ #rightarrow 2#mu 2e")
            if flavour == "2e2mu":
                DrawText(0.2, 0.82, "ggH #rightarrow ZZ #rightarrow 2e 2#mu")

            #DrawText(0.7, 0.2, "Raw Event Yield {}".format(len(df_in_bin.eval("weight"))))
            #DrawText(0.7, 0.12, "Weighted Event Yield {:.1f}".format(np.sum(df_in_bin.eval("weight"))))

            #if not mean_const: DrawText(0.7, 0.68, "#mu" + " = {:.2f} #pm {:.2f} GeV".format(mean.getVal(), mean.getError()))
            #else: DrawText(0.7, 0.68, "#mu" + " = {:.2f} GeV".format(mean.getVal()))
            #if not sigma_const: DrawText(0.7, 0.6, "#sigma_{CB}" + " = {:.2f} #pm {:.2f} GeV".format(sigma.getVal(), sigma.getError()))
            #else: DrawText(0.7, 0.6, "#sigma_{CB}" + " = {:.2f} GeV".format(sigma.getVal()))
            #if not alpha_hi_const: DrawText(0.7, 0.52, "#alpha_{hi}" + " = {:.2f} #pm {:.2f}".format(alpha_hi.getVal(), alpha_hi.getError()))
            #else: DrawText(0.7, 0.52, "#alpha_{hi}" + " = {:.2f}".format(alpha_hi.getVal()))
            #if not alpha_low_const: DrawText(0.7, 0.44, "#alpha_{low}"+ " = {:.2f} #pm {:.2f}".format(alpha_low.getVal(), alpha_low.getError()))
            #else: DrawText(0.7, 0.44, "#alpha_{low}"+ " = {:.2f}".format(alpha_low.getVal()))
            #if not n_hi_const: DrawText(0.7, 0.36, "n_{hi}" + " = {:.2f} #pm {:.2f}".format(n_hi.getVal(), n_hi.getError()))
            #else: DrawText(0.7, 0.36, "n_{hi}"+ " = {:.2f}".format(n_hi.getVal()))
            #if not n_low_const: DrawText(0.7, 0.28, "n_{low}" + " = {:.2f} #pm {:.2f}".format(n_low.getVal(), n_low.getError()))
            #else: DrawText(0.7, 0.28, "n_{low}" +" = {:.2f}".format(n_low.getVal()))

            c.cd()
            c.Update()
            c.Modified()
            c.Print(output_folder + "/FitIn{}_{}.png".format(sigma_bin_selection, description))

            if "364250" not in f:
               sigma_values_from_roofit.append(sigma.getVal())
               error_on_sigma_values_from_roofit.append(sigma.getError())

               mean_values_from_roofit.append(mean.getVal())
               error_on_mean_values_from_roofit.append(mean.getError())

               alpha_low_values_from_roofit.append(alpha_low.getVal())
               error_on_alpha_low_values_from_roofit.append(alpha_low.getError())

               alpha_hi_values_from_roofit.append(alpha_hi.getVal())
               error_on_alpha_hi_values_from_roofit.append(alpha_hi.getError())

               n_low_values_from_roofit.append(n_low.getVal())
               error_on_n_low_values_from_roofit.append(n_low.getError())

               n_hi_values_from_roofit.append(n_hi.getVal())
               error_on_n_hi_values_from_roofit.append(n_hi.getError())

               values_from_regression.append(low_bin + bin_width/2.0)
               #error_on_values_from_regression.append(bin_width/2.0)

               bin_centers.append((low_bin + high_bin)/2.0)

            TotalCanvas.cd(draw_count)

            frame2.GetXaxis().SetTitleOffset(frame2.GetXaxis().GetTitleOffset() * 4)
            frame2.GetYaxis().SetTitleOffset(frame2.GetYaxis().GetTitleOffset() * 2.5)
            frame2.GetYaxis().SetNdivisions(505)
            frame2.GetXaxis().SetTitleSize(int(float(frame2.GetXaxis().GetTitleSize())/1.5))
            pull.SetMarkerSize(pull.GetMarkerSize()/1.5)
           #bin_array = array('d', get_equal_stats_binning(minimum, maximum, df, score_base.format(flavour), stats))
            frame1.GetYaxis().SetTitleOffset(frame1.GetYaxis().GetTitleOffset() * 1.5)
            bottom.SetBottomMargin(0.4)
            c.DrawClonePad()
            keep_pads_alive.append(c)
            keep_pads_alive.append(frame2)
            keep_pads_alive.append(f)
            keep_pads_alive.append(top)
            keep_pads_alive.append(bottom)
            keep_pads_alive.append(roodcb)
            keep_pads_alive.append(mean)
            keep_pads_alive.append(sigma)
            keep_pads_alive.append(alpha_hi)
            keep_pads_alive.append(n_low)
            keep_pads_alive.append(n_low)
            keep_pads_alive.append(n_hi)

            if draw_count == 12:
                drawn += 1
                TotalCanvas.Draw()
                TotalCanvas.Print(output_folder + "/TotalCanvas_{}_{}_{}.png".format(selection, description, drawn))
                TotalCanvas.Close()
                TotalCanvas = ROOT.TCanvas("AllOfThePlots{}".format(drawn), "AllOfThePlots{}".format(drawn), 2000, 3000)
                TotalCanvas.Divide(nx,ny)
                draw_count = 0

        drawn += 1
        TotalCanvas.Draw()
        TotalCanvas.Print(output_folder + "/TotalCanvas_{}_{}.png".format(selection,  description))
        TotalCanvas.Close()


        #create a ROOT plot to fit to
        f = ROOT.TF1("f","[0] * x + [1]",0.0,10.0)
        f.SetParameters(1.0,0.05);
        c = ROOT.TCanvas("PlotCanvas", "PlotCanvas", 2000, 2000)
        plot = ROOT.TGraphErrors(len(sigma_values_from_roofit), array("d", bin_centers), array("d", sigma_values_from_roofit), array("d", np.zeros(len(sigma_values_from_roofit))), array("d", error_on_sigma_values_from_roofit))
        plot.Fit(f)
        plot.Draw("AP")
        DrawText(0.2, 0.8, "p1 = {:.2f} #pm {:.2f} [GeV]".format(f.GetParameter(0), f.GetParError(0)))
        DrawText(0.2, 0.72, "p0 = {:.2f} #pm {:.2f} [GeV]".format(f.GetParameter(1), f.GetParError(1)))
        DrawText(0.2, 0.64, "#frac{#Chi^{2}}{nDOF} " + "= {:.2f}".format(float(f.GetChisquare())/float(f.GetNDF())), size = 0.06)
        DrawText(0.2, 0.56, "{}".format(description))
        leg = ROOT.TLegend(0.6, 0.2, 0.85, 0.4)
        leg.AddEntry(f,"p1 * #sigma_{NN} + p0", "L")
        leg.AddEntry(plot,"#sigma_{CB}")
        leg.Draw("SAME")
        plot.GetYaxis().SetTitle("#sigma_{CB} [GeV]")
        plot.GetXaxis().SetTitle("#sigma_{NN}")
        c.Print(deeper_plots_folder + "/LinearFit_{}.png".format(description))
        c.Draw()

        import pickle
        with open(os.path.join(deeper_plots_folder, "CBvsNN_{}.pickle".format(description)), "wb") as f1:
            pickle.dump( (array("d", bin_centers), array("d", sigma_values_from_roofit), array("d", np.zeros(len(sigma_values_from_roofit))), array("d", error_on_sigma_values_from_roofit) ), f1 )
        with open(os.path.join(deeper_plots_folder, "LinearFitResult_{}.pickle".format(description)), "wb") as f1: 
            pickle.dump( {"p1": (f.GetParameter(0), f.GetParError(0) ),  "p0" : (f.GetParameter(1), f.GetParError(1) ) }, f1 )

        tmp_edges = array('d', low_bins + [high_bins[-1]])
        fit_histogram = ROOT.TH1D("fit_histogram_" + description, "fit_histogram_" + description, len(tmp_edges)-1, tmp_edges)
        for bin in range(1, fit_histogram.GetNbinsX() + 1):
            fit_histogram.SetBinContent(bin, sigma_values_from_roofit[bin-1])
            fit_histogram.SetBinError(bin, error_on_sigma_values_from_roofit[bin-1])
        fit_histograms.append(fit_histogram)

    CB_color_cycle = [\
    ROOT.kBlack,\
    r'#377eb8', r'#ff7f00',r'#4daf4a',\
    r'#f781bf', r'#a65628', r'#984ea3',\
    r'#999999', r'#dede00',\
    ]

    fit_histograms[0].SetLineColor( ROOT.TColor.GetColor(CB_color_cycle[0]))
    fit_histograms[0].SetMarkerColor( ROOT.TColor.GetColor(CB_color_cycle[0]))
    fit_histograms[1].SetLineColor(  ROOT.TColor.GetColor(CB_color_cycle[1]))
    fit_histograms[1].SetMarkerColor(  ROOT.TColor.GetColor(CB_color_cycle[1]))

    canvas = ROOT.TCanvas("total_canv", "total_canv", 1000, 1000)
    canvas.SetBottomMargin(0.25)
    canvas.SetLeftMargin(0.2)

    top = ROOT.TPad("top", "top", 0.0, 0.3, 1.0, 1.0)
    top.SetLeftMargin(0.2)
    top.SetBottomMargin(0.0)
    top.Draw()
    top.cd()

    #leg = ROOT.TLegend(0.6, 0.2, 0.85, 0.38)
    #leg = ROOT.TLegend(0.2, 0.2, 0.88, 0.45)
    leg = ROOT.TLegend(0.29, 0.55, 0.52, 0.85)

    these_ratios = []
    for i, (h, description) in enumerate(zip(fit_histograms, descriptions)):
        h.SetLineColor(ROOT.TColor.GetColor(CB_color_cycle[i]))
        h.SetMarkerColor(ROOT.TColor.GetColor(CB_color_cycle[i]))
        leg.AddEntry(fit_histograms[i], descriptions[i])
        if i > 0: h.Draw("P SAME")
        else: h.Draw("P")
        h.GetXaxis().SetTitleFont(43)
        h.GetXaxis().SetTitleSize(25)
        h.GetYaxis().SetTitleFont(43)
        h.GetYaxis().SetTitleSize(25)
        h.GetXaxis().SetTitleOffset(4.0)
        h.GetYaxis().SetTitleOffset(2.0)
        h.GetYaxis().SetTitleSize(35)
        h.GetXaxis().SetTitleSize(35)
        h.GetXaxis().SetLabelSize(35)
        h.GetXaxis().SetLabelFont(43)
        h.GetYaxis().SetLabelSize(35)
        h.GetYaxis().SetLabelFont(43)
        h.GetXaxis().SetTitle("#sigma_{NN}")
        h.GetYaxis().SetTitle("#sigma_{CB} [GeV]")
        if i > 0:
           h2 = h.Clone(h.GetName() + "ToDivide")
           h2.Divide(fit_histograms[0])
           h2.SetMarkerColor(ROOT.TColor.GetColor(CB_color_cycle[i]))
           h2.SetLineColor(ROOT.TColor.GetColor(CB_color_cycle[i]))
           these_ratios.append(h2)
    leg.Draw("SAME")

    canvas.cd()
    bottom=ROOT.TPad("bottom", "bottom", 0.0, 0.0, 1.0, 0.3)
    bottom.SetBottomMargin(0.4)
    bottom.SetLeftMargin(0.2)
    bottom.SetTopMargin(0.0)
    bottom.Draw()
    bottom.cd()

    ##Draw a set of solid and dotted lines on the ratio plot to guide the reader's eyes
    ratio_max = 1.1
    ratio_min = 0.9

    for j, r in enumerate(these_ratios):
        r.GetYaxis().SetTitle("#frac{M_{H} = X}{M_{H} = 125 GeV}")
        r.SetMinimum(ratio_min)
        r.SetMaximum(ratio_max)
        r.GetXaxis().SetTitleFont(43)
        r.GetXaxis().SetTitleSize(25)
        r.GetYaxis().SetTitleFont(43)
        r.GetYaxis().SetTitleSize(25)
        r.GetXaxis().SetTitleOffset(4.0)
        r.GetYaxis().SetTitleOffset(2.0)
        r.GetYaxis().SetTitleSize(35)
        r.GetXaxis().SetTitleSize(35)
        r.GetXaxis().SetLabelSize(35)
        r.GetXaxis().SetLabelFont(43)
        r.GetYaxis().SetLabelSize(35)
        r.GetYaxis().SetLabelFont(43)
        r.GetYaxis().SetNdivisions(505)
        if j == 0 : r.Draw("P")
        else: r.Draw("P SAME")

    straight_line = ROOT.TF1("line1", str(1.0) , -10e6, + 10e6)
    straight_line.SetLineWidth(2)
    straight_line.Draw("Same")

    straight_line_up = ROOT.TF1("line2",  str(1.0 + (2.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
    straight_line_up.SetLineWidth(1)
    straight_line_up.SetLineStyle(1)
    straight_line_up.Draw("Same")

    straight_line_up2 = ROOT.TF1("line3",  str(1.0 + (1.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
    straight_line_up2.SetLineWidth(1)
    straight_line_up2.SetLineStyle(3)
    straight_line_up2.Draw("Same")

    straight_line_up3 = ROOT.TF1("line4",  str(1.0 + (3.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
    straight_line_up3.SetLineWidth(1)
    straight_line_up3.SetLineStyle(3)
    straight_line_up3.Draw("Same")

    straight_line_down3 = ROOT.TF1("line5",  str(1.0 - (3.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
    straight_line_down3.SetLineWidth(1)
    straight_line_down3.SetLineStyle(3)
    straight_line_down3.Draw("Same")

    straight_line_down = ROOT.TF1("line6",  str(1.0 - (2.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
    straight_line_down.SetLineWidth(1)
    straight_line_down.SetLineStyle(1)
    straight_line_down.Draw("Same")

    straight_line_down2 = ROOT.TF1("line7",  str(1.0 - (1.0 * (ratio_max - 1.0)/4)) , -10e6, + 10e6)
    straight_line_down2.SetLineWidth(1)
    straight_line_down2.SetLineStyle(3)
    straight_line_down2.Draw("Same")

    canvas.Update()
    canvas.Modified()
    canvas.Print(output_folder + "/CompareAllRatios.png")
