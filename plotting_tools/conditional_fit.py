import ROOT
import uproot as ur
from root_numpy import fill_hist
import ROOT
import numpy as np
import imp
from plots import ATLASLabel, DrawText
from parameters import  get_parameters, get_new_parameters
import root_numpy
import os
from array import array
from utils import get_binning
from plotting_utils import plot_roofit_on_canvas, DrawText, ATLASLabel, prepare_for_fit
files = [\
"truthsub_2019_11_19_0_49_prodv23_trees/mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l.root",\
"truthsub_2019_11_19_0_49_prodv23_trees/mc16_13TeV.345582.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH124p5_ZZ4l.root",\
"truthsub_2019_11_19_0_49_prodv23_trees/mc16_13TeV.345583.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125p5_ZZ4l.root",\
"truthsub_2019_11_19_0_49_prodv23_trees/mc16_13TeV.345576.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH123_ZZ4l.root",\
"truthsub_2019_11_19_0_49_prodv23_trees/mc16_13TeV.345577.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH124_ZZ4l.root",\
"truthsub_2019_11_19_0_49_prodv23_trees/mc16_13TeV.345578.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH126_ZZ4l.root",\
"truthsub_2019_11_19_0_49_prodv23_trees/mc16_13TeV.345579.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH127_ZZ4l.root",\
#"truthsub_2019_11_19_0_49_prodv23_trees/mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.root",\
#"truthsub_2019_11_19_0_49_prodv23_trees/mc16_13TeV.364251.Sherpa_222_NNPDF30NNLO_llll_m4l100_300_filt100_150.root",\
]
stamp = files[0].split("/")[0]
#stamp = "Dummy"

#this is the offset from 125 GeV
offsets = [\
0.0,\
-0.5,\
0.5,\
-2.0,\
-1.0,\
+1.0,\
+2.0]

descriptions = [\
"ggH125",\
"ggH124p5",\
"ggH125p5",\
"ggH123",\
"ggH124",\
"ggH126",\
"ggH127"]


event_type_map = {}
event_type_map["4mu"] = 0
event_type_map["4e"] = 1
event_type_map["2mu2e"] = 2
event_type_map["2e2mu"] = 3

for et in ["4mu", "4e", "2e2mu", "2mu2e"]:
    #start writing everything to the file
    ws = ROOT.RooWorkspace("FittingSpacePerEventErrors")
    roo_m4l_mass = ROOT.RooRealVar("m4l_constrained", "m4l_constrained", 100.0,150.0)
    getattr(ws, "import")(roo_m4l_mass)
    roo_weight = ROOT.RooRealVar("weight", "weight", -10000.0, +10000.0)
    getattr(ws, "import")(roo_weight)
    roo_sigmas = ROOT.RooRealVar("sigmas", "sigmas", 0.0, 10.0)
    getattr(ws, "import")(roo_sigmas)
    selection = "((m4l_constrained > 100.0) and (m4l_constrained < 150.0)) and ((event_type  < {e} + 0.5) and (event_type > {e} - 0.5))".format(e = event_type_map[et])
    to_import = []
    for i_f, (f, offset, description) in enumerate(zip(files,offsets,descriptions)):
        f = os.path.join(os.getenv("MassRegressionResolutionDir"), f)
        print("Opening ".format(f))
        df = ur.open(f)["tree_incl_all"].pandas.df(["m4l_constrained", "m4l_truth_matched_bare", "event_type", "weight", "blank"] + ["sigma_prediction_{}".format(m) for m in ["4mu", "2e2mu", "4e", "2mu2e"]])
        for col in df.columns:
            df[col] = df[col].astype(float)
        df = df.query(selection)

        #alright now we need to make a 2d dataset
        to_weight = df.eval("weight").values
        sigmas = df.eval("sigma_prediction_{}".format(et)).values
        #sigmas = df.eval("blank + {}".format(get_parameters(et, "sigma"))).values
        masses = df.eval("m4l_constrained").values

        tmp_filename = "tmp.root"
        to_write = to_write_scores = np.core.records.fromarrays( [masses], names=roo_m4l_mass.GetName())
        root_numpy.array2root(to_write, tmp_filename, treename="Tree", mode="recreate")
        to_write = to_write_scores = np.core.records.fromarrays( [to_weight], names=roo_weight.GetName())
        root_numpy.array2root(to_write, tmp_filename, treename="Tree", mode="update")
        to_write = to_write_scores = np.core.records.fromarrays( [sigmas], names=roo_sigmas.GetName())
        root_numpy.array2root(to_write, tmp_filename, treename="Tree", mode="update")

        #load the information as a dataset
        #now get the tree and make the data
        f = ROOT.TFile(tmp_filename, "READ")
        t = f.Get("Tree")
        fit_to_this_data = ROOT.RooDataSet("Data_{}".format(description), "Data", t, ROOT.RooArgSet(roo_m4l_mass, roo_sigmas, roo_weight), "", roo_weight.GetName())
        f.Close()
        os.system("rm {}".format(tmp_filename))
        to_import.append(fit_to_this_data)

        to_weight = df.eval("weight").values
        masses = df.eval("m4l_constrained").values

        #we have got nothing else
        tmp_filename = "tmp.root"
        to_write = to_write_scores = np.core.records.fromarrays( [masses], names=roo_m4l_mass.GetName())
        root_numpy.array2root(to_write, tmp_filename, treename="Tree", mode="recreate")
        to_write = to_write_scores = np.core.records.fromarrays( [to_weight], names=roo_weight.GetName())
        root_numpy.array2root(to_write, tmp_filename, treename="Tree", mode="update")

        #load the information as a dataset
        f = ROOT.TFile(tmp_filename, "READ")
        t = f.Get("Tree")
        fit_to_this_data = ROOT.RooDataSet("Data_nosigma_{}".format(description), "Data_nosigma_{}".format(description), t, ROOT.RooArgSet(roo_m4l_mass, roo_weight), "", roo_weight.GetName())
        f.Close()
        os.system("rm {}".format(tmp_filename))
        to_import.append(fit_to_this_data)

    roo_mean = ROOT.RooRealVar("mean_{}".format("model"), "mean_{}".format("model"),get_new_parameters(et, "mean", 125.0), 110.0, 130.0)
    roo_sigma = ROOT.RooRealVar("sigma_{}".format("model"), "sigma_{}".format("model"),get_new_parameters(et, "sigma", 125.0), 0.0, 5.0)
    roo_sigma_slope = ROOT.RooRealVar("sigma_slope_{}".format("model"), "sigma_slope_{}".format("model"), 0.8, -10.0, +10.0)
    roo_sigma_interectp = ROOT.RooRealVar("sigma_intercept_{}".format("model"), "sigma_intercept_{}".format("model"), 0.8, -10.0, +10.0)
    roo_alpha_hi = ROOT.RooRealVar("alpha_hi_{}".format("model"), "alpha_hi_{}".format("model"),get_new_parameters(et, "alpha_hi", 125.0), 0.0, 5.0)
    roo_alpha_low = ROOT.RooRealVar("alpha_low_{}".format("model"), "alpha_low_{}".format("model"), get_new_parameters(et, "alpha_low", 125.0), 0.0, 5.0)
    roo_n_low  =ROOT.RooRealVar("n_low_{}".format("model"), "n_low_{}".format("model"), get_new_parameters(et, "n_low", 125.0), 1.0, 10.0)
    roo_n_hi = ROOT.RooRealVar("n_hi_{}".format("model"), "n_hi_{}".format("model"), get_new_parameters(et, "n_hi", 125.0), 1.0, 10.0)

    mass_hypothesis = ROOT.RooRealVar("MassHypo_{}".format("model"), "MassHypo_{}".format("model"), 125.0, 115.0, 135.0)
    mass_slope = ROOT.RooRealVar("MassSlope_{}".format("model"), "MassSlope_{}".format("model"), 0.0, -2.0, 2.0)
    mass_slope.setConstant(True)
    mass_slope.setVal(get_new_parameters(et, "slope", 125.0))
    mass_int = ROOT.RooRealVar("MassInt_{}".format("model"), "MassInt_{}".format("model"), 0.0, -2.0, 2.0)
    mass_int.setConstant(True)
    mass_int.setVal(get_new_parameters(et, "intercept", 125.0))
    masshypo_mx_plus_b =  ROOT.RooFormulaVar("mass_linear_{}".format("model"), "{}*({} - 125) + 125 + {}".format(mass_slope.GetName(), mass_hypothesis.GetName(), mass_int.GetName()), ROOT.RooArgList(mass_hypothesis,mass_slope,mass_int) )

    #let everything vary
    mean_const = True
    alpha_hi_const = True
    alpha_low_const = True
    n_low_const = True
    n_hi_const = True

    roo_mean.setConstant(mean_const)
    roo_sigma.setConstant(True)
    roo_alpha_hi.setConstant(alpha_hi_const)
    roo_alpha_low.setConstant(alpha_low_const)
    roo_n_hi.setConstant(n_hi_const)
    roo_n_low.setConstant(n_low_const)

    roo_m4l_mass.setRange("FitRange", 100.0, 150.0)
    to_import.append(roo_sigma)

    #create a model for the width of the signal model:
    signal_model = ROOT.RooTwoSidedCBShape("signal_model", "signal_model", roo_m4l_mass, masshypo_mx_plus_b, roo_sigmas, roo_alpha_low, roo_n_low, roo_alpha_hi, roo_n_hi)
    #signal_model_no_per = ROOT.RooTwoSidedCBShape("signal_model_no_per", "signal_model_no_per", roo_m4l_mass, masshypo_mx_plus_b, roo_sigma, roo_alpha_low, roo_n_low, roo_alpha_hi, roo_n_hi)

    #only write the weight, m4l_constrained and sigmas variable once
    ws.Print()
    for el in to_import:
        getattr(ws, "import")(el)
    getattr(ws, "import")(signal_model)
    ws.factory("RooTwoSidedCBShape::signal_model_no_per({},{},{},{},{},{},{})".format(roo_m4l_mass.GetName(), masshypo_mx_plus_b.GetName(), roo_sigma.GetName(), roo_alpha_low.GetName(), roo_n_low.GetName(), roo_alpha_hi.GetName(), roo_n_hi.GetName()))
    outfile = ROOT.TFile("workspaces/Workspace_for_model_{}_{}.root".format(stamp, et), "Recreate")
    outfile.cd()
    ws.Write()
    ws.Print()
    outfile.Close()



