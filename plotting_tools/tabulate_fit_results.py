import pandas as pd
import numpy as np
import ROOT as r
import glob
import os

import argparse
parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--sig_only', dest='sig_only', action="store_true")
parser.add_argument('--sigbkg_only', dest='sigbkg_only', action="store_true")
parser.add_argument('--skip_per', dest='skip_per', action="store_true")
parser.add_argument('--channel', dest='channel', type=str, default="")
parser.add_argument('--skip_bdt', dest='skip_bdt', action="store_true")
parser.add_argument('--bdt_only', dest='bdt_only', action="store_true")
parser.add_argument('--skip_scaled', dest='skip_scaled', action="store_true")
args = parser.parse_args()

base_wcard = "/project/def-psavard/ladamek/MassFitResult/Sig*/bootstrap/*"
directories = glob.glob(base_wcard)
directories = [d for d in directories if "Syst" not in d]

bootstrapped = "bootstrap" in base_wcard
input(bootstrapped)

to_remove = []
for d in directories:
    if "Syst" in os.path.split(d)[-1]: to_remove.append(d)

for el in to_remove:
    if el in directories: directories.remove(el)

include_bootstrap = False
categories = ["2e2mu", "2mu2e", "4e", "4mu", "Incl"]
BDT = ["Y", "N"]
scaled = ["Y", "N"]
per = ["Y", "N"]
incl_bkg = ["Y", "N"]

def resolve_files(directories, scaled, per, cat, bdt, incl_bkg):
     must_contain = []
     must_not_contain = []

     if scaled == "Y": must_contain.append("_Scaled")
     else: must_not_contain.append("_Scaled")
     if per == "Y": must_contain.append("_PER")
     else: must_not_contain.append("_PER")
     if bdt == "N": must_contain.append("_Incl")
     else: must_not_contain.append("_Incl")
     if cat == "Incl":
         must_not_contain.append("4mu")
         must_not_contain.append("4e")
         must_not_contain.append("2e2mu")
         must_not_contain.append("2mu2e")
     else: must_contain.append(cat)
     if incl_bkg == "Y": must_contain.append("Bkg")
     else: must_not_contain.append("Bkg")
     print(must_contain)
     print(must_not_contain)

     selected_directories = []
     for d in directories:
         d_name = d
         keep = True
         for el in must_contain:
             if el not in d_name: keep = False
         for el in must_not_contain:
             if el in d_name: keep = False
         if keep: selected_directories.append(d)
     print(selected_directories)
     if len(selected_directories) == 1: return selected_directories[0]
     else: return None

for_frame = {}
for_frame["decay"] = []
for_frame["scaled"] = []
for_frame["per"] = []
for_frame["BDT"] = []
for_frame["bkgs"] = []
for_frame["mH [GeV]"] = []
#for_frame["mH [GeV] from bootstrap"] = []
for_frame["mH unc. up [GeV]"] = []
for_frame["mH unc. down [GeV]"] = [] 
for_frame["mH unc. int. [GeV]"] = []
#for_frame["mH median bootstrapped unc. up [GeV]"] = []
#for_frame["mH median bootstrapped unc. down [GeV]"] = [] 
#for_frame["mH mean bootstrapped unc. up [GeV]"] = []
#for_frame["mH mean bootstrapped unc. down [GeV]"] = [] 
import pickle

for c in categories:
  for in_bkg in incl_bkg:
    for s in scaled:
        for p in per:
          if s == "Y" and p == "N": continue
          for b in ["N"]:
            directory = resolve_files(directories, s, p, c, b, in_bkg)
            if not directory: continue
            if not bootstrapped:
                mp_file = os.path.join(directory, "mass_point", "result.pkl")
                if os.path.exists(mp_file):
                    try:
                        print(mp_file)
                        with open(mp_file, "rb") as f: 
                            stuff = pickle.load(f)
                            meas, errs = stuff["no_syst"]
                            mh = meas["mass_point"]
                            err_low, err_hi = errs["mass_point"][0], errs["mass_point"][1]
                    except Exception as e:
                        print(e)
                        continue
                else:
                    print("The file {} doesn't exist. skipping".format(mp_file))
                    continue
            if bootstrapped:
                mp_file = os.path.join(directory, "mass_point", "result.pkl")
                if os.path.exists(mp_file):
                    try:
                        print(mp_file)
                        with open(mp_file, "rb") as f: 
                            stuff = pickle.load(f)
                            input(stuff)
                    except Exception as e:
                        print(e)
                        continue
                continue
                
            for_frame["decay"].append(c)
            for_frame["scaled"].append(s)
            for_frame["per"].append(p)
            for_frame["BDT"].append(b)
            for_frame["bkgs"].append(in_bkg)
            for_frame["mH unc. up [GeV]"].append(err_hi)
            for_frame["mH unc. down [GeV]"].append(err_low)
            for_frame["mH unc. int. [GeV]"].append(err_hi - err_low)
            for_frame["mH [GeV]"].append(mh)

results = pd.DataFrame.from_dict(for_frame)
results_incl = results.query("per == \"N\"")
print(results_incl)

for_frame = {}
for_frame["decay"] = []
for_frame["scaled"] = []
for_frame["per"] = []
for_frame["BDT"] = []
for_frame["bkgs"] = []
for_frame["mH [GeV]"] = []
for_frame["Improvement %"] = []
#for_frame["mH [GeV] from bootstrap"] = []
for_frame["mH unc. up [GeV]"] = []
for_frame["mH unc. down [GeV]"] = [] 
for_frame["mH unc. int. [GeV]"] = []
for_frame["(Bias/Unc)%"] = []

for c in categories:
  for in_bkg in incl_bkg:
    for s in scaled:
        for p in per:
          if s == "Y" and p == "N": continue
          for b in BDT:
            directory = resolve_files(directories, s, p, c, b, in_bkg)
            mp_file = os.path.join(directory, "mass_point", "result.pkl")
            if os.path.exists(mp_file):
                try:
                    with open(mp_file, "rb") as f: 
                        stuff = pickle.load(f)
                        meas, errs = stuff["no_syst"]
                        mh = meas["mass_point"]
                        err_low, err_hi = errs["mass_point"][0], errs["mass_point"][1]
                except Exception as e:
                    print(e)
                    continue
            else:
                print("The file {} doesn't exist. skipping".format(mp_file))
                continue

            for_frame["decay"].append(c)
            for_frame["scaled"].append(s)
            for_frame["per"].append(p)
            for_frame["BDT"].append(b)
            for_frame["bkgs"].append(in_bkg)
            for_frame["mH unc. up [GeV]"].append(err_hi)
            for_frame["mH unc. down [GeV]"].append(err_low)
            for_frame["mH unc. int. [GeV]"].append(err_hi - err_low)
            for_frame["(Bias/Unc)%"].append(100.0 * abs(125.0 - mh)/(err_hi - err_low))
            for_frame["mH [GeV]"].append(mh)
            mh_incl = results_incl.query("(decay == \"{d}\") and (BDT == \"N\") and (bkgs == \"{in_bkg}\")".format(d = c, s=s, bdt = b, in_bkg = in_bkg))["mH unc. int. [GeV]"].values
            assert len(mh_incl) == 1
            mh_incl = mh_incl[0]
            for_frame["Improvement %"].append( (( mh_incl - ( err_hi - err_low)) / mh_incl) * 100.0)

three_places = ["mH unc. up [GeV]", "mH unc. down [GeV]", "mH unc. int. [GeV]", "mH [GeV]"]
one_places = ["Improvement %", "(Bias/Unc)%"]
formatters = {}
for t in three_places:
    formatters[t] = "{:0.3f}".format
for t in one_places:
    formatters[t] = "{:0.1f}".format

results = pd.DataFrame.from_dict(for_frame)

keys = list(for_frame.keys())
if args.sig_only:
    if "bkgs" in keys:
        keys.remove("bkgs")
        #keys.remove("Improvement %")
    results = results.query("bkgs == \"N\"")

if args.sigbkg_only:
    if "bkgs" in keys: keys.remove("bkgs")
    results = results.query("bkgs == \"Y\"")

if args.skip_per:
    if "per" in keys:
        keys.remove("per")
    if "scaled" in keys:
        keys.remove("scaled")
    results = results.query("per == \"N\"")

if args.channel:
    if "decay" in keys: keys.remove("decay")
    results = results.query("decay == \"{}\"".format(args.channel))

if args.skip_scaled:
    if "scaled" in keys: keys.remove("scaled")
    results = results.query("scaled == \"{}\"".format("N"))

if args.skip_bdt:
    if "BDT" in keys: keys.remove("BDT")
    results = results.query("BDT == \"N\"")

if args.bdt_only:
    results = results.query("BDT == \"Y\"")

results = results[keys]
results = results.reset_index(drop=True)

print(results.to_latex( formatters=formatters))
print(results)
