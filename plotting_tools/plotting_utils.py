import ROOT
import numpy as np

def return_bindex(arr, bins):
    return np.searchsorted(bins, arr, side='left') - 1

def prepare_for_fit():
    #minimize the output from roofit
    #ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.NumIntegration)
    #ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Fitting)
    #ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Minimization)
    #ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.InputArguments)
    #ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Eval)
    #ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.ERROR)
    ROOT.RooAbsReal.defaultIntegratorConfig().method1D().setLabel("RooAdaptiveGaussKronrodIntegrator1D")  ## Better numerical integrator
    pass

def ATLASLabel(x, y, text=None, color=1, size = None):
    """Draw the ATLAS Label.
    Parameters
    ----------
    x : float
        x position in NDC coordinates
    y : float
        y position in NDC coordinates
    text : string, optional
        Text displayed next to label (the default is None)
    color : TColor, optional
        Text colour (the default is 1, i.e. black).
        See https://ROOT.cern.ch/doc/master/classTColor.html
    """
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextColor(color)
    if size != None:
        l.SetTextSize(size)
    l.DrawLatex(x, y, r"#bf{#it{ATLAS}} " + text)

def DrawText(x, y, text, color=1, size=0.05):
    '''Draw text.
    Parameters
    ----------
    x : float
        x position in NDC coordinates
    y : float
        y position in NDC coordinates
    text : string, optional
        The text
    color : int, optional
        Text colour (the default is 1, i.e. black).
        See https://ROOT.cern.ch/doc/master/classTColor.html.
        If you know the hex code, rgb values, etc., use ``ROOT.TColor.GetColor()``
    size : float, optional
        Text size
        See https://ROOT.cern.ch/doc/master/classTLatex.html
    '''
    l = ROOT.TLatex()
    l.SetTextSize(size)
    l.SetNDC()
    l.SetTextColor(color)
    l.DrawLatex(x, y, text)

#def make_a_nice_histogram(histogram):
#    


def plot_roofit_on_canvas(xvar, data, function, xlabel, plot_range, binning, draw_count="", fit_dof=0, plot_commands=[]):
        lines = []

        #draw the fit result
        c = ROOT.TCanvas("c{}".format(draw_count), "c{}".format(draw_count))
        c.Draw()
        top = ROOT.TPad("top{}".format(draw_count), "top", 0.0, 0.3, 1.0, 1.0)
        top.SetLeftMargin(0.15)
        top.SetBottomMargin(0.00)
        top.Draw()
        top.cd()

        f = xvar.frame()
        f.SetTitle("")
        print("Plotting")
        data.plotOn(f, ROOT.RooFit.Range(plot_range), ROOT.RooFit.Binning(binning))
        f.GetXaxis().SetLabelSize(0)
        print("Done Plotting")
        function.plotOn(f, ROOT.RooFit.Range(plot_range), *plot_commands)
        chi2 = f.chiSquare(fit_dof)
        f.Draw()
        pull = f.residHist()
        f.GetXaxis().SetTitle()
        f.GetXaxis().SetTitleSize(0.2)
        f.GetXaxis().SetTitleFont(43)
        f.GetXaxis().SetTitleSize(25)
        f.GetXaxis().SetTitleOffset(1.0)
        f.GetXaxis().SetLabelSize(20)
        f.GetXaxis().SetLabelFont(43)
        f.GetYaxis().SetLabelSize(13)
        f.GetYaxis().SetLabelFont(43)
        f.GetYaxis().SetTitleSize(20)
        f.GetYaxis().SetTitleFont(43)

        f.SetMaximum(f.GetMaximum() * 1.5)
        f.SetMinimum(0.0)

        c.cd()
        bottom=ROOT.TPad("bottom{}".format(draw_count), "bottom", 0.0, 0.0, 1.0, 0.3)
        bottom.SetBottomMargin(0.4)
        bottom.SetLeftMargin(0.15)
        bottom.SetTopMargin(0.00)
        bottom.Draw()
        bottom.cd()

        #get the histograms
        #h_fromRooData = ROOT.TH1D("h_fromRooData", "histograms", 
        #data.createHistogram("h_fromRooData",xvar,ROOT.RooFit.Binning(binning))
        #pdfDataHis = generateBinned(xvar,0,true)
        #h_pdfDataHis = function.createHistogram("h_pdfDataHis", xvar, ROOT.RooFit.Binning(binning))

        #create the ratio plot
        #ratio_hist = pdfDataHis.Divide(h_fromRooData)
        #roofit_ratio_hist = ROOT.RooDataHist("RatioHist{}".format(draw_count), "RatioHist{}".format(draw_count), ROOT.RooArgList(xvar), ratio_hist)
        #h_pdfDatHis.Sumw2()
        #for bin in range(1, h_pdfDataHis.GetNbinsX()):
        #    h_pdfDataHis.SetBinError(bin, 0.0)
        #frame2.addPlotable(roofit_ratio_hist, "P")

        frame2 = xvar.frame()
        frame2.GetYaxis().SetTitle("MC - Fit")
        frame2.addPlotable(pull, "P")
        frame2.SetTitle("")
        frame2.Draw()
        frame2.GetXaxis().SetTitle(xlabel)
        frame2.GetXaxis().SetTitleFont(43)
        frame2.GetXaxis().SetTitleSize(25)
        frame2.GetXaxis().SetTitleOffset(2.7)
        frame2.GetXaxis().SetLabelSize(20)
        frame2.GetXaxis().SetLabelFont(43)
        frame2.GetYaxis().SetLabelSize(13)
        frame2.GetYaxis().SetLabelFont(43)
        frame2.GetYaxis().SetTitleSize(20)
        frame2.GetYaxis().SetTitleFont(43)
        frame2.GetYaxis().SetTitleOffset(1.0)
        f1 = ROOT.TF1("line", "0.0", 0.0, 100000.0)
        f1.SetLineColor(ROOT.kBlack)
        f1.Draw("l SAME")
        lines.append(f1)
        bottom.Update()
        bottom.Modified()

        return c, top, bottom, f, frame2, pull, lines, chi2
