Setup

## Create the virtual environment with and lcg view.
```
git clone ssh://git@gitlab.cern.ch:7999/luadamek/lcg_virtualenv.git
git clone ssh://git@gitlab.cern.ch:7999/luadamek/MassRegressionResolution.git
setupATLAS
CMTCONFIG=x86_64-centos7-gcc8-opt ./../lcg_virtualenv/create_lcg_virtualenv venv_MassRegressionResolution LCG_94python3
source venv_MassRegressionResolution/bin/activate
```

## Install the required python sofware using pip. This package uses uproot, pyroot fit, atlas-mpl-style and atlas-plots. 
```
pip install --upgrade pip
pip install atlas-mpl-style --no-cache-dir
git clone https://github.com/joeycarter/atlas-plots.git
cd atlas-plots
pip install -e .
cd ..
git clone https://github.com/xrootd/xrootd.git
cd xrootd/bindings/python/
python setup_standalone.py install
cd ../../../
pip install uproot
pip install guppy3
pip install pdfkit
```

## Make sure that RooFitExtensions are installed to support the double sided crystal ball functional form
```
git clone https://gitlab.cern.ch/atlas_higgs_combination/software/RooFitExtensions.git
cd RooFitExtensions
mkdir build
cd build
cmake .. && make
cd ../../
```

## Get boost python to install:
```
git clone https://github.com/PySlurm/pyslurm.git
cd pyslurm/
python setup.py build --slurm=/opt/software/slurm/current/lib/
python setup.py install
```

#generate a grid proxy that is valid for as long as possible. This is so that slurm jobs can access the files on eos
```
voms-proxy-init --voms atlas --hours 10000
```

## Finally, run the setup script
```
source ./setup/setup.sh
```

# Login
```
setupATLAS
source ./setup/setup.sh
```

# Make mini trees
```
python macros/make_local_files.py
```
