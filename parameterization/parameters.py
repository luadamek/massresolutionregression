#alpha 1.26 0.02
#alpha2 1.50 0.02
#mean 124.79 0.01
#n 2.77 0.10
#n2 10.77 0.18
#sigma 1.53 0.01
import ROOT
ROOT.gSystem.Load("~/RooFitExtensions/build/libRooFitExtensions.dylib")

#RooWorkspace(ws) ws contents
#
#variables
#---------
#(alpha,alpha2,event_type,m4l_constrained,mu,n,n2,norm,w_MCw,weight_corr,width)
#
#p.d.f.s
#-------
#RooAddPdf::DCB_pdf[ norm * dcb ] = 0.24792
#RooTwoSidedCBShape::dcb[ m=m4l_constrained m0=mu sigma=width alphaLo=alpha nLo=n alphaHi=alpha2 nHi=n2 ] = 0.24792

#datasets
#--------
#RooDataSet::datasettraining(m4l_constrained,event_type,weight_corr,w_MCw)
#RooDataSet::datasetvalidation(m4l_constrained,event_type,weight_corr,w_MCw)
#RooFormulaVar::alpha124.54mu_m1_1[ actualVars=(alpha4mu_m1_1) formula="alpha4mu_m1_1" ] = 1.26951
#RooFormulaVar::alpha1244mu_m1_1[ actualVars=(alpha4mu_m1_1) formula="alpha4mu_m1_1" ] = 1.26951
#RooFormulaVar::alpha125.54mu_m1_1[ actualVars=(alpha4mu_m1_1) formula="alpha4mu_m1_1" ] = 1.26951
#RooFormulaVar::alpha1264mu_m1_1[ actualVars=(alpha4mu_m1_1) formula="alpha4mu_m1_1" ] = 1.26951
#RooFormulaVar::alpha2124.54mu_m1_1[ actualVars=(alpha24mu_m1_1) formula="alpha24mu_m1_1" ] = 1.52654
#RooFormulaVar::alpha21244mu_m1_1[ actualVars=(alpha24mu_m1_1) formula="alpha24mu_m1_1" ] = 1.52654
#RooFormulaVar::alpha2125.54mu_m1_1[ actualVars=(alpha24mu_m1_1) formula="alpha24mu_m1_1" ] = 1.52654
#RooFormulaVar::alpha21264mu_m1_1[ actualVars=(alpha24mu_m1_1) formula="alpha24mu_m1_1" ] = 1.52654

#RooFormulaVar::alpha124.54mu_m1_1[ actualVars=(alpha4mu_m1_1) formula="alpha4mu_m1_1" ] = 1.26951
#RooFormulaVar::alpha1244mu_m1_1[ actualVars=(alpha4mu_m1_1) formula="alpha4mu_m1_1" ] = 1.26951
#RooFormulaVar::alpha125.54mu_m1_1[ actualVars=(alpha4mu_m1_1) formula="alpha4mu_m1_1" ] = 1.26951
#RooFormulaVar::alpha1264mu_m1_1[ actualVars=(alpha4mu_m1_1) formula="alpha4mu_m1_1" ] = 1.26951
#RooFormulaVar::alpha2124.54mu_m1_1[ actualVars=(alpha24mu_m1_1) formula="alpha24mu_m1_1" ] = 1.52654
#RooFormulaVar::alpha21244mu_m1_1[ actualVars=(alpha24mu_m1_1) formula="alpha24mu_m1_1" ] = 1.52654
#RooFormulaVar::alpha2125.54mu_m1_1[ actualVars=(alpha24mu_m1_1) formula="alpha24mu_m1_1" ] = 1.52654
#RooFormulaVar::alpha21264mu_m1_1[ actualVars=(alpha24mu_m1_1) formula="alpha24mu_m1_1" ] = 1.52654
#RooFormulaVar::alpha2_validation4mu_m1_1[ actualVars=(alpha24mu_m1_1) formula="alpha24mu_m1_1" ] = 1.52654
#RooFormulaVar::alpha_validation4mu_m1_1[ actualVars=(alpha4mu_m1_1) formula="alpha4mu_m1_1" ] = 1.26951
#RooFormulaVar::mu124.54mu_m1_1[ actualVars=(mugrad4mu_m1_1,muint4mu_m1_1) formula="mugrad4mu_m1_1*( 124.5 - 125) + 125 + muint4mu_m1_1" ] = 124.294
#RooFormulaVar::mu1244mu_m1_1[ actualVars=(mugrad4mu_m1_1,muint4mu_m1_1) formula="mugrad4mu_m1_1*( 124 - 125) + 125 + muint4mu_m1_1" ] = 123.799
#RooFormulaVar::mu125.54mu_m1_1[ actualVars=(mugrad4mu_m1_1,muint4mu_m1_1) formula="mugrad4mu_m1_1*( 125.5 - 125) + 125 + muint4mu_m1_1" ] = 125.283
#RooFormulaVar::mu1264mu_m1_1[ actualVars=(mugrad4mu_m1_1,muint4mu_m1_1) formula="mugrad4mu_m1_1*( 126 - 125) + 125 + muint4mu_m1_1" ] = 125.778
#RooFormulaVar::mu_validation4mu_m1_1[ actualVars=(mugrad4mu_m1_1,muint4mu_m1_1,mH) formula="mugrad4mu_m1_1*( mH - 125) + 125 + muint4mu_m1_1" ] = 124.789
#RooFormulaVar::n124.54mu_m1_1[ actualVars=(n4mu_m1_1) formula="n4mu_m1_1" ] = 2.62016
#RooFormulaVar::n1244mu_m1_1[ actualVars=(n4mu_m1_1) formula="n4mu_m1_1" ] = 2.62016
#RooFormulaVar::n125.54mu_m1_1[ actualVars=(n4mu_m1_1) formula="n4mu_m1_1" ] = 2.62016
#RooFormulaVar::n1264mu_m1_1[ actualVars=(n4mu_m1_1) formula="n4mu_m1_1" ] = 2.62016
#RooFormulaVar::n2124.54mu_m1_1[ actualVars=(n24mu_m1_1) formula="n24mu_m1_1" ] = 8.24165
#RooFormulaVar::n21244mu_m1_1[ actualVars=(n24mu_m1_1) formula="n24mu_m1_1" ] = 8.24165
#RooFormulaVar::n2125.54mu_m1_1[ actualVars=(n24mu_m1_1) formula="n24mu_m1_1" ] = 8.24165
#RooFormulaVar::n21264mu_m1_1[ actualVars=(n24mu_m1_1) formula="n24mu_m1_1" ] = 8.24165
#RooFormulaVar::n2_validation4mu_m1_1[ actualVars=(n24mu_m1_1) formula="n24mu_m1_1" ] = 8.24165
#RooFormulaVar::n_validation4mu_m1_1[ actualVars=(n4mu_m1_1) formula="n4mu_m1_1" ] = 2.62016
#RooFormulaVar::sigma124.54mu_m1_1[ actualVars=(sigma4mu_m1_1) formula="sigma4mu_m1_1" ] = 1.56401
#RooFormulaVar::sigma1244mu_m1_1[ actualVars=(sigma4mu_m1_1) formula="sigma4mu_m1_1" ] = 1.56401
#RooFormulaVar::sigma125.54mu_m1_1[ actualVars=(sigma4mu_m1_1) formula="sigma4mu_m1_1" ] = 1.56401
#RooFormulaVar::sigma1264mu_m1_1[ actualVars=(sigma4mu_m1_1) formula="sigma4mu_m1_1" ] = 1.56401
#RooFormulaVar::sigma_validation4mu_m1_1[ actualVars=(sigma4mu_m1_1) formula="sigma4mu_m1_1" ] = 1.56401

#named sets
#----------
#observables:(m4l_constrained)

import os
ws_dict = {}
ws_dict["ggH125_4mu"] = os.path.join(os.getenv("MassRegressionResolutionDir"), "parameterization/DCB_fullMC_oldVals/ws125_4mu_BDTBin-1_1.root")
ws_dict["ggH125_4e"] = os.path.join(os.getenv("MassRegressionResolutionDir"), "parameterization/DCB_fullMC_oldVals/ws125_4e_BDTBin-1_1.root")
ws_dict["ggH125_2e2mu"] = os.path.join(os.getenv("MassRegressionResolutionDir"), "parameterization/DCB_fullMC_oldVals/ws125_2e2mu_BDTBin-1_1.root")
ws_dict["ggH125_2mu2e"] = os.path.join(os.getenv("MassRegressionResolutionDir"), "parameterization/DCB_fullMC_oldVals/ws125_2mu2e_BDTBin-1_1.root")

map_names_to_key = {}
map_names_to_key["mean"] = "mu"
map_names_to_key["sigma"] = "width"
map_names_to_key["alpha_low"] = "alpha"
map_names_to_key["alpha_hi"] = "alpha2"
map_names_to_key["n_hi"] = "n2"
map_names_to_key["n_low"] = "n"

def get_parameters(name, variable):
     assert name in ws_dict
     f = ROOT.TFile(ws_dict[name], "READ")
     ws = f.Get("ws")
     print(ws)
     val = ws.var(map_names_to_key[variable]).getVal()
     f.Close()
     return val

ws2_dict = {}
ws2_dict["4mu"] = os.path.join(os.getenv("MassRegressionResolutionDir"), "parameterization/2019_10_11/ws4mu_m1_1_window_100_150.root")
ws2_dict["4e"] = os.path.join(os.getenv("MassRegressionResolutionDir"), "parameterization/2019_10_11/ws4e_m1_1_window_100_150.root")
ws2_dict["2e2mu"] = os.path.join(os.getenv("MassRegressionResolutionDir"), "parameterization/2019_10_11/ws2e2mu_m1_1_window_100_150.root")
ws2_dict["2mu2e"] = os.path.join(os.getenv("MassRegressionResolutionDir"), "parameterization/2019_10_11/ws2mu2e_m1_1_window_100_150.root")

map_names_to_key = {}
map_names_to_key["mean"] = "mu"
map_names_to_key["intercept"] = "int"
map_names_to_key["slope"] = "slope"
map_names_to_key["sigma"] = "sigma"
map_names_to_key["alpha_low"] = "alpha"
map_names_to_key["alpha_hi"] = "alpha2"
map_names_to_key["n_hi"] = "n2"
map_names_to_key["n_low"] = "n"

def get_new_parameters(name, variable, mp):
     print("getting {}".format(name))
     print("getting variable {}".format(variable))
     print("for mass point {}".format(mp))
     assert name in ws2_dict
     f = ROOT.TFile(ws2_dict[name], "READ")
     decriptor = None
     if "4e" in name:
         descriptor = "4e"
     if "4mu" in name:
         descriptor = "4mu"
     if "2e2mu" in name:
         descriptor = "2e2mu"
     if "2mu2e" in name:
         descriptor = "2mu2e"
     ws = f.Get("ws")
     ws.var("mH").setVal(mp)
     if variable == "mean":
         to_return = ws.function("mu_validation{}_m1_1".format(descriptor)).getVal()
         f.Close()
         return to_return
     if variable == "sigma":
         to_return =  ws.function("sigma_validation{}_m1_1".format(descriptor)).getVal()
         f.Close()
         print("sigma was {}".format(to_return))
         return to_return
     if variable == "intercept":
         to_return = ws.var("muint{}_m1_1".format(descriptor)).getVal()
         f.Close()
         return to_return
     if variable == "slope":
         to_return = ws.var("mugrad{}_m1_1".format(descriptor)).getVal()
         f.Close()
         return to_return
     if variable == "alpha_hi":
         to_return =  ws.function("alpha2_validation{}_m1_1".format(descriptor)).getVal()
         f.Close()
         return to_return
     if variable == "alpha_low":
         to_return = ws.function("alpha_validation{}_m1_1".format(descriptor)).getVal()
         f.Close()
         return to_return
     if variable == "n_hi":
         to_return = ws.function("n2_validation{}_m1_1".format(descriptor)).getVal()
         f.Close()
         return to_return
     if variable == "n_low":
         to_return = ws.function("n_validation{}_m1_1".format(descriptor)).getVal()
         f.Close()
         return to_return
